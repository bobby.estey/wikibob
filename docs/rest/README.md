# REpresentational State Transfer (REST) Services Using Spring REST Controllers

- REST is a software architectural style that defines how systems communicate
- Stateless 
     - No data is stored on the client or server
     - non persistent, REST communications don't retain state information
     - More secure than stateful, creates a new transaction, execute the transaction and done.  A new transaction, process, every time
- Utilizes Hypertext Transfer Protocol (HTTP) or HTTPS (Secure) to transfer data
- REST provides a consistent way for clients to interact with resources through a Standard / Uniform Interface
- REST allows servers to send executable code to clients, e.g. Code on demand
- Uses URLs to find resources
- Uses standard HTTP methods (GET, POST, PUT, and DELETE)
- REST uses formats like JavaScript Object Notation (JSON) or Extensible Markup Language (XML), e.g. Self-describing data format

|JSON|XML|
|--|--|
|![JSON](json.png)|![XML](xml.png)|

## Terminology

- [Application Programming Interface (API)](https://www.youtube.com/watch?v=bxuYDT-BWaI)
     - Application - any software program with a specific purpose, functionality or service
     - Interface - contract or protocol (rules) that dictates how applications communicate to each other, using requests or responses

- [HTTP Status Codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)

- REST API Request Example
     - HTTP Method:  GET
     - Endpoint:  http://localhost:6464/service/getxml
     - Headers:  authentication data, miscellaneous data / information
     - Body:  parameters, objects

- REST API Response Example
     - HTTP Response Status, e.g. 200 OK, content type, e.g. text/html
     - Response Body
     
![REST API](rest.drawio.png)

## Questions and Solutions

- What happens when an API call returns a large response?  Exception message is posted, e.g. timeout, server error, etc.
- [Delivering Large API Responses As Efficiently As Possible](https://apievangelist.com/2018/04/20/delivering-large-api-responses-as-efficiently-as-possible/)
- [Asynchronous REST API](https://www.youtube.com/watch?v=J4J-eHq_Oms)
- [Search Engine Example](https://www.whitepages.com/name/Estey/Yorktown-VA?fs=1&searchedName=estey)
- [API Filters & Pagination](https://www.youtube.com/watch?v=MbslvX0AMVE)
     - Filter:  http://localhost:6464/service/getxml?firstName=bobby&lastName=estey
     - Pagination:  http://localhost:6464/service/getxml?limit=10&offset=20 - return records 20-29 (server has a maxLimit)
     - Filter and Pagination:  http://localhost:6464/service/getxml?lastName=estey&limit=10&offset=20
