# Progressive Web Applications (PWAs)

## Links

- [PWAs in 100 Seconds](https://www.youtube.com/watch?v=sFsRylCQblw)
- [7 PWA Features](https://www.youtube.com/watch?v=ppwagkhrZJs)
- [Web Dev](https://web.dev)

## Resources

- Trusted Web Activities (TWAs)
