# Time Zones and Time

### My biggest pet peeve:  When you are in Daylight Time, the time is called Daylight Time, NOT STANDARD TIME.  For example:

- Nevada - Fall, the time is Pacific Standard Time (PST), Spring, the time is Pacific Daylight Time (PDT)
- Arizona - Always Mountain Standard Time (MST) because most of the State, doesn't change to Daylight Time
     - Arizona Time = Mountain Standard Time
     - The Navajo Reservation in the northeast of the state of Arizona, does go to Daylight Time
     
### Stop using AM / PM - I have been in 23 time zones in my life and the majority of the planet uses a 24 hour clock, get rid of AM / PM.  It's not that hard to figure out.

# ISO 8601 Date Format - YYYY-MM-DDThh:mm:ss[.mmm]TZD -> Time Zone Designator, milliseconds optional

|Field|Format|Example|
|--|--|--|
|4-digits Year|YYYY|2013|
|2-digits Month, 10 = October|MM|10|
|2-digits Day of the month|DD|13|
|The date and time delimiter, commonly "T" indicating the start of the time element, space, or nothing|T|T|
|2-digit Hour, 24 hour, 13 = 1PM|hh|13|
|2-digits Minute|mm|13|
|2-digits Second|ss|13|
|3-digits Milliseconds|mmm (optional)|013|
|Time Zone Designator (TZD)|- [A through Z](./timeLatitude.md)<br>- or +-HH:mm|[M<br>- or +12:00](./timeLatitude.md)|

|Example TZD Character|Example TZD + -|Description|
|--|--|--|
|2013-10-13T13:13:13.013T|2013-10-13T13:13:13.013-06:00|October 13 2013, 10:05:45 Mountain Time (T) is [Mountain Time](./timeLatitude.md)|
|2012-03-29T10:05:45U|2012-03-29T10:05:45-07:00|March 29 2012, 10:05:45 Pacific Time (U) is [Pacific Time](./timeLatitude.md)|
|2012-03-29T10:05:45M|2012-03-29T10:05:45+12:00|March 29 2012, 10:05:45 New Zealand Time (M) is [New Zealand Time](./timeLatitude.md)|

Why use the ISO 8601 Date Format?

Write two programs in any Language with the instructions in [Programming Assignment](#Programming Assignment)

# <a name="Programming Assignment"></a>Programming Assignment

### Write two programs that read a dataset containing date, time and description
     - The first dataset will contain only Non-ISO format date / time
     - The second dataset will contain only ISO format date / time

|Format|Date|Time|Description|
|--|--|--|--|
|Non-ISO|December 06 2013|6:59 PM MST|December 6 2013 @ 6 PM Arizona Time Zone (Always Standard Time, not Daylight Time)|
|Non-ISO|January 26 2023|6:59 AM MDT Time|January 26 2023 @ 6:59 AM Mountain Daylight Time Zone|
|Non-ISO|January 26 2013|6:59 AM CST Time|January 26 2023 @ 6:59 AM Central Standard Time Zone|
|Non-ISO|January 26 2023|6:59 AM NZT Time|January 26 2023 @ 6:59 AM New Zealand Time Zone|
|ISO|2013-12-07T00:59:00T|not required|December 6 2013 @ 6 PM Arizona Time Zone (Always Standard Time, not Daylight Time)|
|ISO|2023-01-26T00:59:00-06:00|not required|January 26 2023 @ 6:59 AM Mountain Daylight Time Zone|
|ISO|2023-01-26T00:59:00S|not required|January 26 2023 @ 6:59 AM Central Standard Time Zone|
|ISO|2023-01-26T06:00:00+12:00|not required|January 26 2023 @ 6:59 AM New Zealand Time Zone|

# Here's the Point, why ISO 8601 Date Format?

- Less Code, Variables and Logic
     - Strip the Date field anything with characters, e.g. 
          - 2023-01-26 -> 20230126
     - Put into a single Integer or Long field and a simple sort can be executed
     - Obsolete approach required three fields (year, month, day), additional code and sorting is complicated, e.g.
          - Which date comes before the other, from our dataset:  12062013 or 01262023
          - Do you see the problem?  Here's the fix use ISO Format, e.g. YYYYMMDD format:  20131206 and 20230126

- Maintainable and Easy to Follow
     