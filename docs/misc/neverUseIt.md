# Never Use "it"

I had a Professor a long time ago who informed the Class to never use the word "it".  The Professor warned the Class, if he saw the word "it" anywhere in any assignment, the assignment was given an "F".  The entire Class thought, this is terrible.  Before the Professor implemented the policy he explained why:

- "it" slows down the reader, meaning when you put the word "it", the reader has to stop reading, go back and find out what "it" is referring to and then have to reread the sentence
- "it" is confusing, sometimes "it" is a reference to one or multiple subjects in a sentence
- "it" is unprofessional, not just scholars but all
- "it" grammatically reads terrible on a resume

## There are many more reasons, why you should never use "it"

I am glad my Professor provided this advice and habit as he was correct.  I hope others can start learning to get along without the word "it" and see how you never needed the word.

Thank You 🙂
