# ripgrep

## ripgrep linux installation

- curl -Lo ripgrep.deb "https://github.com/BurntSushi/ripgrep/releases/latest" | grep -Po '"tag_name": "\K[0-9.]+')
- sudo apt install -y ./ripgrep.deb
- rg --version

## ripgrep windoze installation

- [Windows Download](https://github.com/BurntSushi/ripgrep)
- extract ZIP file
- copy folder into c:\opt\ripgrep
- execute rg.exe

## examples

- rg <search term> case sensitive
- rg -i <search term> example of ignore case
