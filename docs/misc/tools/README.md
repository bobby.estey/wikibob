[TOC]

# Miscellaneous Freeware Tools

- Stop using Proprietary Limited Licensed Software - that simple.  I personally only use FREEWARE, ROBUST, INNOVATIVE technology

- There are many freeware products available and cost nothing. These products are not SHAREWARE, they are FREEWARE
- If you are interested, contact me and I can set you up with these superior products

#### Operating System

- Linux Operating System – [Ubuntu](https://ubuntu.com) or [Mint](https://linuxmint.com/)
- My preferred Operating System is Linux with the distribution base of either Ubuntu or Mint
- The Linux Operating System has so many advantages over both Mac OS and Windows
- All my courses are facilitated utilizing the Linux Operating System. If you are interested, contact me and I can set you up with the superior OS
- My view and comments about Linux are not biased but factual and I will be glad to share these facts with anyone who has an open mind on technology :-)

#### [Git](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/git/README.md)

#### Office Software 

- I would highly suggest utilizing [MARKDOWN](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/markdown/README.md) instead of Office
- [LibreOffice](https://www.libreoffice.org/) / OpenOffice / Google Docs
- [Office History](officeHistory.md)

###### NOTE:  That I never call technologies word, excel or powerpoint
- These are not technologies but product offerings
- The correct terminology is word processor, spreadsheet or presentation software

#### [Draw IO](drawio.md) - Documents Generator - powerful design diagram tool

#### [Aspose](https://blog.aspose.com/diagram/visio-viewer-online/) - Free Visio Reader

#### [Google Meet](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/tools/googleMeet/googleMeet.md) - Collaboration Software

#### [OBS Studio](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/tools/obsStudio.mp4) - Recording Software

#### [VLC Media Player](https://www.videolan.org/) - Best media player on the planet

- Most of my videos have audio
- If your sound fails this is due the inferior m$ media player
