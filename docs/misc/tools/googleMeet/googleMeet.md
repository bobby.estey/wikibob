# Google Meet

#### Start Up

- Use the Chrome Browser only
- Go to the Link that was sent to you
- Enable Audio and Video, you do not have to turn on your Camera

#### Setup

|Artifact|Link|
|--|--|
|Share Screen Recording|[Share Screen Recording](./googleMeetShareScreen.mp4)|
|Audio Settings|[Audio Settings](./googleMeetAudioSettings.png)|
