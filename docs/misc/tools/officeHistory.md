# Office History

This document was created to provide the TRUTH about Office History.  The founders, pioneers and inventors of the technology.

While the exact dates / origins of first written language, ledgers and presentation documents are unknown this documentation is about the automation of the following:

- Word Processor - electronic documentation
     - [Reference 1](https://www.templafy.com/blog/the-history-of-word-processing/)
     - [Reference 2](https://bytecellar.com/2016/06/05/a-look-back-at-three-decades-of-word-processors/)
- Spreadsheet - electronic programs utilizing ledgers of rows and columns
     - [Spreadsheet](https://blog.sheetgo.com/spreadsheets-tips/history-of-spreadsheets/) 
- Presentation - electronic programs to display information in the form of a slide show
     - [Presentation]()

## Word Processor History

|Year|Description|
|--|--|
|1867|Typewriter Invented|
|1874|Typewriter available to the public|
|1878|Shift Key<br>- http://www.explainthatstuff.com/typewriter.html<br>- http://cdn4.explainthatstuff.com/typewriter-type-hammers.jpg|
|1897|Tab Key|
|1920|Electric Typewriter|
|1961|Ball Headed Typewriter|
|1964|Magnetic Recorder|
|1972|Video Display – Stephen Dorsey, AES|
|1976|Electric Pencil - First Word Processor Program|
|1978|WordStar - First Commercially Word Processor Program|

## Spreadsheet History

|Year|Description|
|--|--|
|1969|LANPAR - first electronic spreadsheet|
|1979|VisiCalc - first electronic spreadsheet on personal computers|

## Presentation Software History

|Year|Description|
|--|--|

## Popular Office Suites

|Categories / Products|Google Docs|LibreOffice.org / OpenOffice.org|m$ Office|
|--|--|--|--|
|Licensing|<span style="background-color: rgb(0, 255, 0);">Freeware</span>|<span style="background-color: rgb(255, 255, 0);">Freeware</span>|<span style="background-color: rgb(255, 0, 0);">Licensed / $$$</span>|
|OS Support|<span style="background-color: rgb(0, 255, 0);">All Oses</span>|<span style="background-color: rgb(255, 255, 0);">Most Oses</span>|<span style="background-color: rgb(255, 0, 0);">Windows / Mac Only</span>|
|Word Processor|Docs|Writer|Word|
|Spreadsheet|Sheets|Calc|Excel|
|Presentation|Slides|Impress|PowerPoint|
