# You Learn better by Teaching

- [DeVry University](https://gitlab.com/bobby.estey/devrystudent)
- [Grand Canyon University](https://gitlab.com/bobby.estey/gcuStudent)

## A picture is worth a thousand words.  A video is worth a million.

## "If you want to understand a subject, then teach" (Estey, 2000)

I have mentioned my experiences with facilitating and teaching.  I have a statement, **“If you want to understand a subject, then teach”.**  I have personally created technical videos of my experiences and I will continue to perform this duty as this clarifies (ideas, processes, examples, concepts) to others who are interested in computing technology.

2000, my personal website http://cv64.us was stellar and awesome, I had several real world examples of everything I knew, both personal and technical.  The site had several web pages, with content that covered almost every subject.

2021, my subscription with a web service provider was consistently increasing every year, the inflated prices continued to increase, so I decided to cancel after over 2 decades.  My new service provider is now providing the same support from $160 per year to $36.  I could simply move my site from previous to new, however, utilizing newer technologies, I decided, let’s start fresh with everything.

## So let’s start over again, with new technology :-)

20190817, I created my own git repository.  I have moved all my technical work (still a work in progress) to that site which also provides Continuous Integration / Continuous Delivery (CI/CD) capabilities.  Currently my Classes are sent this site so that they can learn faster with an organized structured approach.

## BOTTOM LINE:  

#### I continuously share my knowledge and experiences for the world to learn.

#### You Learn better by Teaching
