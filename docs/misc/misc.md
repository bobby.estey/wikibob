# Miscellaneous

## Interesting
|Program|Description|
|--|--|
|[aircraftPlotter.c](aircraft_plotter.c)|Didn't want to lose the equation|
|[aircraftPlotter.java](AircraftPlotter.java)|Didn't want to lose the equation|
|[Crash Me](crashme.c)|This was an old program that would crash Unix systems back in the VMS / Unix war days, early 90's.  VMS still rules and still exists.  Increment each letter of the VMS acronym and you get WNT.  Dave Cutler the author of VMS is also one of the authors of WNT.  I wonder what Dave is doing now?|
|[Cheap Password Stealer](grabber.com)|Cheap password stealer on VMS system.|
|[undelete-vax.mar](undelete-vax.mar)|If you ever deleted a file on a VAX / VMS system just use this macro and then execute analyze/disk/repair.  This program is also known as the BUTT SAVER.|
|[undelete-axp.mar](undelete-axp.mar)|If you ever deleted a file on a AXP / VMS system just use this macro and then execute analyze/disk/repair.  This program is also known as the BUTT SAVER.

## [Chromebook](chromebook.md)

## Tools
|Tool|Description|
|--|--|
|[Time](https://www.worldtimezone.com)|Time Zones and Time around the World|
|[Google Meet](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/tools/googleMeet/googleMeet.md)|Instructions on how to share screen on Google Meet|
|[Libreoffice](libreoffice.md)|LibreOffice disable default slide presentation|
|[OBS Studio Installation](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/tools/obsStudio.mp4)|[Recording Software Download](https://obsproject.com/)

## Other Links

- [Time and Latitude](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/time/timeLatitude.md)
- [ISO Date Format](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/time/isoDateFormat.md)
- [Never Use It](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/misc/neverUseIt.md)
