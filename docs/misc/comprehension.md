# Comprehension

## Dr Gary A Berg

- Professor Dr Gary A Berg, Western International University (WIU), Phoenix, AZ
- I only had one course Information Resources Management (IS620) with this incredibly Intelligent, Energetic and Organized man

- First Day of Class

     - Introduction to the Course (2 Months, 16 Classes - two Classes a week)
          - [Comprehension Presentation](#comprehension-presentation) was an excellent start
          
     - Requirements the beginning of each week
          - Find two new / recent technologies, write a professional report, that was succinct and technical
          - Present to the class with a Presentation of one the articles
          - Reports and Presentations that contained the word "it" was an automatic F
               - Think this is harsh, there was a very good reason why to never use "it"
               - "it" is inefficient, requires sentences to be reread and other bad things
               - There were other words, which I have forgot that would automatically receive an F
          - Reports and Presentations were never allowed, resubmissions and if posted late was an automatic F
               - You are already digging a hole not keeping up, procrastinating continues to dig a deeper hole
          - Students also had homework requirements from the University
          - There were other artifacts required for the course, there was a lot of work, a lot

     - Fair Warning
          - Students are all looking at each other thinking, "THIS GUY IS CRAZY" and there is no way we can keep up with all the work required
          - Dr Berg was clear, he will not alter the requirements
          - FINALLY, every week Dr Berg would always say, "I hope this is enough, if not let me know and I will find more".  He had a big smile on his face, almost laughing.
          
- Third Week of Class

     - Dr Berg taught the Class, once you are organized, have a sound process, performance with increase.  This was so true
     - The first two weeks were very rough, third week, the student could perform the same requirements in shorter time and quality increased
     
- Forth Week of Class

     - Dr Berg challenged two other parallel classes to a Debate
     - Our class destroyed the other classes, with content, facts, technologies, demonstrations, etc
     
- Last Day of Class

     - The last day of Class, Dr Berg did a recap, reminding the Class, the Introduction to the Course and how each of us have changed
     - All Students agreed, they were better trained, proficient, leaders and learned good habits and dropped the bad habits
     - The remaining courses were a lot simpler, because we all changed

## Comprehension Presentation

#### Title

- Information Overload, Underload, NoLoad

#### People Remember / Learn ... after 24 hours or sooner!

- 16% of what they "READ"
- 20% of what they "SEE"
- 30% of what they are "TOLD"
- 50% of what they "SEE and are TOLD"
- 70% of what they "SEE, are TOLD and RESPOND TOO"
- 90% of what they "SEE, are TOLD and RESPOND TOO and do DIRECTED FUNCTIONS"

#### Bottom Line (Bobby Estey point of View)

- Dr Berg was making a point.  When learning, this is a TWO-WAY Street.  Accept Input (Student) while at the same time Provide Output (Teacher)
- When you learn something, teach others
- The people you teach, have them reply to you with their comprehension and collaborate with you
- [You Learn better by Teaching](teaching.md)
