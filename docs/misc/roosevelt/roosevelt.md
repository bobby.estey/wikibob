# Theodore Roosevelt - The Man

![theMan](theMan.jpg)  ![lawsNeedToBeEnforced](lawsNeedToBeEnforced.jpg)

Theodore Roosevelt was born on October 27, 1858 in New York City.  As a child, he struggled against frailty, nearsightedness and asthma.  His love for reading helped foster a love for nature and the outdoors. He also exercised vigorously and developed a lifelong interest in what he called "the strenuous life".

He entered Harvard at 18 intent of becoming a naturalist.  As a senior he began work on a book, "The Naval War of 1812."  Roosevelt graduated 21st in a class of 177 in 1880 and married Alice Hathaway Lee.

After graduation, at the age of 22, Roosevelt joined New York City's 21 District Republican Club and was elected to the New York Assembly.

Roosevelt 's mother died of typhoid in February 1884, and his wife died two days later giving birth to their daughter, Alice.  Roosevelt left New York to regain his strength and confidence at the Elkhorn Ranch in the North Dakota Badlands.

Returning to New York City in 1886, Roosevelt ran unsuccessfully for Mayor.  That year, he married Edith Kermit Carow, who would bear him five children.  Political service to Benjamin Harrison won Roosevelt a seat on the Civil Service Commission in 1889.

He gained national attention by staging a fight against favoritism.  Roosevelt 's position: Jobs should go to the most qualified applicants.

In 1895, Roosevelt took the post of New York City Police Commissioner and fought Democrats and Republicans to establish a merit system for appointments and promotions.  Roosevelt was appointed Assistant Secretary of the Navy in 1897.  He immediately began building the strength of the Navy.

Concerning an experimental steam powered naval aircraft, Roosevelt wrote, "It seems to me worthwhile for this government to try whether it will work on a large enough scale to be of use in event of war." The war he was referring to was brewing with Spain over control of Cuba.

During the 1898 Spanish American War, Roosevelt resigned to go to battle. He organized the First U.S. Cavalry Regiment "The Rough Riders" and saw action at San Juan Hill.  Returning from Cuba a hero, Roosevelt was elected Governor of New York in 1899 and resumed his work for reform.  He tightened control of sweatshops and pushed for government supervision of utilities and insurance companies.

Roosevelt angered the Republican bosses who were now torn between a desire to get him out of their hair and a wish to exploit his vote getting vigor. Their solution:  Bury him in the Vice Presidency.  Roosevelt became the running mate of President McKinley in the 1900 election.  His popularity increased McKinley's margin of victory.

President McKinley was mortally wounded by an assassin on Sept. 6, 1901.  A week later, Roosevelt was sworn in as the 26th President at the age of 42 years and the youngest president of the United States, Kennedy was 43.
 
|President|Birth Date|Presidential Date|Age of Presidency|
|--|--|--|--|
|Roosevelt|10/27/1858|09/14/1901|42.88|
|Kennedy|05/29/1917|01/20/1961|43.68|

President Roosevelt took action on his calls for reform by suing the Northern Securities Company, then trusts in the beef, coal and sugar industries.  Roosevelt was also active in conservation.  He set aside 150 million acres for national use, doubled the number of national parks and created 16 national monuments.

In 1902, Roosevelt moved to create the Panama Canal.  He mediated a peace, which brought an end to the Russo Japanese War in 1905, and won the Nobel Peace Prize.  In 1907, Roosevelt sent 16 American battleships around the world. The Great White Fleet was, as Roosevelt remarked, "the most important service I ever rendered to peace."

In 1909, Roosevelt left the White House but continued to live the "strenuous life."  He began a Smithsonian sponsored African safari, bagging more than 500 animals and birds.

He was back in politics for the 1912 election though Roosevelt 's "Bull Moose" party never gained the support needed to bring him to the Presidency again.

Roosevelt stumped hard for the Liberty Bond drive after the outbreak of war in Europe.  However, with the death of his son, Quentin, in 1918, Roosevelt 's spirit began to wane.  In the early morning of January 6, 1919, Roosevelt died.  "Death had to take him sleeping," said Vice President Thomas R. Marshall. "For if Roosevelt had been awake, there would have been a fight."

Mr. Roosevelt was the first president to fly and the first to submerge in a submarine.  As Assistant Secretary of the Navy, he supported research and development in carrier aviation.

In naming CVN71, former Secretary of the Navy John F. Lehman said, “Theodore Roosevelt was one of the architects of our modern Navy.  His complete faith in the necessity for a strong Navy has been fully justified by most recent history."

## Theodore Roosevelt Quotes

"It is not the critic who counts, nor the man who points out how the strong man stumbled or where the doer of deeds could have done better.  The credit belongs to the man who is actually in the arena.  Who's face is marred by dust and sweat and blood and who at the worst if he fails at least fails while doing greatly, so that his place shall never be with those cold and timid souls who know neither victory nor defeat."

"If I had to pick between righteous and peace, I would pick righteous."

"In the first place, we should insist that if the immigrant who comes here in good faith becomes an American and assimilates himself to us, he shall be treated on an exact equality with everyone else, for it is an outrage to discriminate against any such man because of creed, or birthplace, or origin. But this is predicated upon the person's becoming in every facet an American, and nothing but an American...There can be no divided allegiance here. Any man who says he is an American, but something else also, isn't an American at all. We have room for but one flag, the American flag... We have room for but one language here, and that is the English language... and we have room for but one sole loyalty and that is a loyalty to the American people."  (Theodore Roosevelt, 1907) 

 