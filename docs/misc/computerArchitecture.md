# Computer Architecture

#Computer Science 001, not 101, Really

- I wrote this thread to give everyone who is interested in Computer Architecture basics.  The job is to dig a hole.  What architecture can dig faster and more efficient?  A Power Shovel or a Human Being?

|Technology|Processor Speed|Bus Speed|Total Computation|
|--|--|--|--|
|Power Shovel|2 Scoops / Minute|1 Ton|2 Scoops * 2000 Pounds = 4000 Pounds / Minute|
|Human Being|12 Scoops / Minute|20 Pounds|12 Scoops * 20 Pounds = 240 Pounds / Minute|

- So which technology won?  The Power Shovel of course.  Let's put this into Computer Architecture analogy.  Even though the Power Shovel was 6 times slower (Processor Speed), the Shovel (bus), memory, etc. is a considerably larger.  The Power Shovel is doing a lot more work.

- The point is, you have to design your computer architecture for the job that is to be accomplished.  Is this a great analogy?  :-)
