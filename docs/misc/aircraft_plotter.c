#include <stdio.h>
#include <math.h>
main()
/*                				*/
/* This procedure emulates a pilots computer 	*/
/*                				*/
{	float 	radians	=	0.0174533;
	float 	degrees	=	57.29578;
	int	circle	= 	360;
	int	variance=	-14;
	int	zero	=	0;

	float	gl,wradians,wf,ratiosin,
		headingsin,headingcos,
		headingtan, headingrad;
	int	gs,tc,wv,wd,tas,rw,azimuth,wca,mc,mh;
/*                				*/
/* Recieve inputs from user.			*/
/*                				*/
	printf("True Course: ");
	scanf("%i", &tc);
	printf("Wind Velocity: ");
	scanf("%i", &wv);
	printf("Wind Direction: ");
	scanf("%i", &wd);
	printf("True Airspeed: ");
	scanf("%i", &tas);
	printf("Variance: ");
	scanf("%i", &variance);
/*                				*/
/* Calculation of angles & velocity.		*/
/* Law of sines:  sin(a)/A = sin(b)/B = sin(c)/C*/
/* Law of circle: sin^2(a) - cos^2(a) = 1	*/
/*                				*/
	if(tc > zero) 
		azimuth = circle - tc;

	rw = wd + azimuth;
	if(rw > circle)
		rw = rw - circle;

	wradians = rw * radians;
	ratiosin = sin(wradians) / tas;
	headingsin = ratiosin * wv;
	headingcos = sqrt(1 - (headingsin * headingsin));
	headingtan = headingsin / headingcos;
	headingrad = atan(headingtan);
/*						*/
	wca = (int)(headingrad * degrees);
	gl = tas * headingcos;
	wf = wv * cos(wradians);
	gs = (int)(gl - wf);
	mc = tc + variance;
	mh = mc + wca;
	if (mh < zero)
		mh = mh + circle;
/*						*/
/*	printout.				*/
/*						*/
	printf("mh %d\n",mh);
}
