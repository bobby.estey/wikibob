# Windoze - the worst Operating System

|Command|Description|Example|
|--|--|--|
|doskey|convoluted solution to aliases|doskey rg=c:\opt\ripgrep\rg.exe $1|
|dir /s /b *wordSearch*|search for a word in files||
|tail|use powershell|cat .\<filename> -tail 10 -wait|
