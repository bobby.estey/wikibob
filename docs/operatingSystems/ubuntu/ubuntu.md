# Ubuntu

## Software

#### SNAP

- sudo apt update
- sudo apt install snapd

#### Software Installation

|Software|Installation|
|--|--|
|filezilla|snap install --beta filezilla|
|gedit|sudo snap install gedit|
|pinta|sudo snap install pinta|

## Shortcuts

|Shortcut||
|--|--|
|Alt + Esc|Move between Applications|
|Alt + F2|Command Line|
|Ctrl + Alt + Delete|Like Windoze|
|Ctrl + Alt + T|Open Terminal|
|Ctrl + Alt + Tab||
|Super + A|Shows all Applications|
|Super + L|Lock Screen|
|Super + V|Show Notifications|
|Super + Page Up or Super + Page Down||
|Super + Tab|Opens Up Applications|

## Environment Settings

|Environment Settings|Description|
|--|--|
|Sound|[Sound](sound.png)|
|Settings -> Keyboard-> Keyboard Shortcuts -> View and Customize Shortcuts|Adjust Shortcuts|
|Settings -> Accessibility -> Locate Pointer|Press Ctrl to locate Pointer|
|Resize Icons|gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 24|

## Commands

|Command|Description|
|--|--|
|Ubuntu Install|sudo apt install ./<file>.deb|
|gsettings set org.gnome.desktop.interface clock-show-seconds true|set seconds|
|sudo apt --fix-broken install|fixes broken dependencies|

## Fix sudo apt upgrade

|Command|Description|
|--|--|
|- echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf > /dev/null|temporary fix|
|- sudo nano /etc/resolve.conf<br>- add or modify namespace 8.8.8.8|permanent fix|
|- sudo nano /etc/apt/sources.list||

## Troubleshooting

|Issue|Solution|
|--|--|
|Disk Drive not loading|- sudo fdisk -l<br>- sudo apt install nfs-common<br>- sudo apt install cifs-utils<br>- sudo ntfsfix -d /dev/sda1<br>- sudo apt-get install ntfs-3g<br>- lsblk<br>- sudo ntfsfix -b -d /dev/sda1|

- Start Software Updater
- Select Other Software
- Select Software to Remove
- Press Remove Button

#### PPA Error

![Software Updater](otherSoftware.png)

- |PPA Error](https://askubuntu.com/questions/65911/how-can-i-fix-a-404-error-when-using-a-ppa-or-updating-my-package-lists)
