# Unix

![unix History 1](unixHistory1.jpg)
![unix History 2](unixHistory2.jpg)

#### Unix - Commands

|Command|Description|
|--|--|
|find . -exec grep "searchString" {} /dev/null \;|recursive grep|
|find . -exec grep -n "searchString" {} \; -print|recursive grep|

## Solaris 

#### Solaris - Commands

|Command|Description|
|--|--|
|ggrep -HIR "mysearchstring" *|recursive grep|
|grep "searchString" *|search files in pwd|
|printenv|list environment variables|

#### Solaris - vi editor

|Command|Description|
|--|--|
|g/searchstring/s//newstring/g|substitute globally|
