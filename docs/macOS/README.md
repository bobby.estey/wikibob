# Mac OS

# Links

- [How To Install Homebrew on Mac](https://www.howtogeek.com/211541/install-homebrew-on-mac/)

# Commands

- Open up Terminal (finder search for Terminal)

## Notes (SOME STEPS TAKE A VERY LONG TIME, SO WAIT):  

- The Mac will ask for sudo privileges, enter password when prompted
- Some steps will take a VERY LONG TIME - SUPER LONG LONG TIME - Get a Linux Machine

# Install Brew

- Copy the following 3 separate commands EXACTLY (including double quotes, etc.) in the Terminal

```
Command 1:  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
Command 2:  echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> /Users/$USER/.zprofile
Command 3:  eval $(/opt/homebrew/bin/brew shellenv)
```

# Install Git

- Copy the following command EXACTLY in the Terminal

```
brew install git
```

# Images

![mac 01](mac01.png)
![mac 02](mac02.png)
![mac 03](mac03.png)
![mac 04](mac04.png)
![mac 05](mac05.png)
