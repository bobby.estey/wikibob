# Spring Model View Controller (MVC)

## Spring MVC (Model 2 Architecture) uses a Front Controller (Dispatcher Servlet)
     - All requests flow into a central controller called as Front Controller
     - Front Controller controls flow to Controllers and Views
     - Common features can be implemented in the Front Controller
     - Dispatcher Servlet receives all requests and redirects them to the right controller and view
     
## Spring Method Calls
     - GET - Retrieve details of a resource 
     - POST - Create a new resource 
     - PUT - Update an existing resource 
     - PATCH - Update part of a resource 
     - DELETE - Delete a resource
     
## Annotations

- @Controller
- @RestController
- @RequestMapping("/path")
