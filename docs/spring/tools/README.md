# Spring Tools

## Links

- [Spring Tools](https://spring.io/tools)
- [Spring Boot Reference Manual](https://docs.spring.io/spring-boot/)

## Installation

#### Linux

- Extract to /opt
- mkdir spring
- chown owner:group spring
     - e.g. chown cv64:cv64 spring
     
![sts0](sts0.png)

![sts1](sts1.png)

![sts2](sts2.png)

![sts3](sts3.png)
     
#### Eclipse

![Eclipse Installation 1](./springTools1.png)

![Eclipse Installation 2](./springTools2.png)

![Eclipse Installation 3](./springTools3.png)

###### Check Trust Authorities

![Trust Authorities 1](trustAuthorities1.png)

![Trust Authorities 2](trustAuthorities2.png)

## Create Spring Starter Project

![Spring Starter Project](springStarterProject.png)

## Execution

![execution](execution.png)
