# Spring Framework

## Architecture

- Client - Usually a Browser or a Server calling another Server
- HTTP Methods - (GET, POST, PUT, DELETE)
- Controller - Interface between Client and Server, Management of the Software, Routes requests to Services
- Services - Business Logic, Software that does work
- Data Access Object (DAO) - Persistence Logic
     - DAO Interface: Defines the contract for data access operations, such as create, read, update, and delete (CRUD)
     - Concrete DAO Class: Implements the DAO interface, providing the actual logic for interacting with the data source
- Data Transfer Object (DTO): Represents the data being transferred between the DAO and the application
- Data Source: A database or file system

![Architecture](architecture.drawio.png)

## File Layout

```
-- src
   -- main
      -- java - Files:  java
         -- package - e.g. us.cv64
            -- business or services (Business Logic)
            -- controller (API Layer)
            -- data
               -- entity
               -- mapper
               -- respository
            -- model
            -- SpringBootApplicationMain.java
            -- SpringConfig.java
      -- resources
         -- static (static content) - Files:  css, images, JavaScript, index.html
         -- templates - Files:  html
         -- application.properties (yaml) - Files:  properties files, e.g. messages.properties
   -- webapp
      -- WEB-INF
         -- jsp - Files:  jsp
-- pom.xml          
```

```java
@SpringBootApplication
SpringApplication.run(SpringBootApplicationMain.class, args);

```

- Spring Framework provides all the core features to build:
     - Web Applications - software executing within a Browser
     - Representational State Transfer (REST) Application Programming Interface (API)s
          - Contract between a Consumer (request) and Producer (response)
     - Microservices - software architecture breaks down large applications into smaller independent services that communicate through APIs
          - Independent - each service is built independently
          - Loosely Coupled - a failure in one component doesn't break the entire application
          - Communicative - services communicate with each other using well defined APIs
     - Full Stack Applications - software application the encompasses both frontend and backend in a single code base

# Terminology

## Spring Container (Spring Context) AKA Inversion of Control (IOC) Container is created at runtime using the definitions in the file

- Manages Spring Beans and their Lifecycle, including creation, configuration and destruction.  AKA Inversion of Control (IOC) Container is created at runtime using the definitions in the file
- Two types of Spring Containers
     - Bean Factory - Basic Spring Container
     - Application Context - Advanced Spring Container with Enterprise Specific Features (Web Apps, Web Services, REST API, Microservices, I18N, Spring (Aspect-Oriented Programming (AOP)


# What is Spring

- Type Inference - Java var - allows the Compiler to infer the type of the variable based on the value assigned
- Coupling - how much work is involved in changing something, software changes due to business requirements, frameworks, code, etc.
- Tightly Coupling (Concrete Classes) - simple example:  Hardcoded, hard and time consuming to change, not Flexible
     - Tight coupling reduces flexibility, making the code harder to maintain and evolve because changes propagate across the system
     - Tight coupling occurs when classes or components are highly dependent on each other
     - Requires changes throughout the application
- Loose Coupling (Interfaces) - simple example:  Dynamic Code, easier and faster to change
     - Loose coupling is preferred because the system is more maintainable and adaptable to changes, as components can be modified independently
     - Spring manages creation and writing of beans and dependencies (building applications and unit tests)
     - Reduced Boilerplate Code - focuses on business logic and checked exceptions are converted to Runtime or Unchecked exceptions
     - Architecture Flexibility - use only the modules and projects required, smaller footprint
     - Continuously evolving
- Wiring of Dependencies
     - The process of providing an object with the dependencies to another Class / Interface

- Spring Framework uses Inversion of Control (IoC), Spring Container (Context) to manage the objects
     
- Plumbing of Enterprise Applications, in other words, provided a framework to common tasks, so that developers can concentrate on their code and not installations, configurations, etc.
- Make testable applications
- Reduce the Plumbing of Code
- Simple Architecture, templates replaced code
- Integrates with other Frameworks, e.g. Hibernate, Struts
- Moving from Monolithic Applications to Microservices
- Reduced Configuration, Framework Setup, Deployment, Logging, Transaction Management, Monitoring, Web Service Configuration

## Acronyms and Terminology

|Term|Description|Reference|
|--|--|--|
|@AnnotationConfigApplicationContext|Creates and manages the Spring application context, initializing with the Beans defined in the configuration class||
|Application Context|Advanced Spring Container - see Configuration File - Inversion of Control (IoC) Container|Enterprise Features, Web applications, REST API and Microservices|
|Aspect Oriented Programming|- Layered Approach (Web - view logic, Business - business logic, Data - persistence logic)<br>- different layers have common aspects (Security, Performance, Logging) this is called Cross Cutting Concerns<br>- AOP - implement cross cutting concerns|- Spring AOP - not a complete AOP, only works with Spring Beans, Intercept method calls to Spring Beans<br>- Aspect J, complete AOP solution, intercept any method call on any Java class, intercept change of values in a field|
|@Autowired|Process of wiring in dependencies for a SpringBean|SpringBean preferred to use, in other words, provide the preferred Candidate (SpringBean) amoung the multiple Candidates to use|
|Bean Factory|Basic Spring Container|- Developer has to do everything|
|@Bean|The context.getBean("name") method retrieves the Bean that is managed by the Spring container and identified by the name "name"|Used on methods in Spring Configuration Classes|
|@Component|Instance of Class is managed by Spring Framework|- Spring creates SpringBeans using Components<br>-Used on any Java Class|
|@ComponentScan|Spring Framework performs a component scan to find component classes|- without path, searches the current package<br>- otherwise search specific  @ComponentScan("path")|
|@Configuration|The class contains one or more bean definitions and is used by Spring to manage these Beans||
|Dependency|Interface is depending on the Implementation||
|Dependency Injection|Identifies Beans, Dependencies and wire together, e.g. IoC||
|Inversion of Control (IoC)|Spring Framework, does all the work, setting up everything, only have to make calls to Spring Boot.  Object that knows how to instantiate and configure other objects, including doing the dependency injection|makes code loosely coupled, object creation and wiring the dependency|
|@PostConstruct|Method to be executed after dependency injection is complete and perform any initialization||
|@PreDestroy|Method processed prior to the SpringBean being removed from the Container||
|IOC Container|Manages the lifecycle of SpringBeans and Dependencies||
|@Lazy|Do not initialize a SpringBean (@Configuration / @Component / @Bean) until called|- Eager is recommended if errors are discovered after application startup|
|@Lazy(value=false)|There is no @Eager annotation||
|@PostConstruct||Method executed after dependency injection|
|@PreDestroy|Method to be executed when instance is in the process to be removed by the container||
|@Primary|SpringBean given preference when multiple Candidates (SpringBeans) are Qualified||
|@Qualifier|Specific SpringBean to be auto wired|Higher Priority over @Primary|
|@Scope|Singleton - default (one instance per IOC Container), Prototype|@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE) - creates a new instance is created|
|Spring / Spring Boot|Framework to write Web Applications, REST API, Full Stack Applications||
|Spring Bean|any Java object managed by Spring Container|- Default Initialization - Eager (initialize before called) - Lazy (do not initialize until called)|
|Spring Container (Context)|Manages Spring Beans and Lifecycle - Combines POJOs and Container into a Context and then is moved into a Ready System|[IoC Container](https://www.udemy.com/course/spring-boot-and-spring-framework-tutorial-for-beginners/learn/lecture/35020096#overview)|

## Dependency Injection Types
   
- Constructor Based
     - Spring recommends using Constructor Injection over Field and Setter Based (Method) Injection
     - Constructor creates the Bean, dependencies are set by calling the Constructor
     - @Autowired is not required
- Setter Based (Method)
     - Method creates the Bean
     - Dependencies are set by calling setter methods
- Field 
     - Created by Injection by Reflection
     - Dependency injected using reflection, e.g. @Autowired on a field

## Dependency Example

```
var myObject = new MyClass(); // object creation
var myDriver = new Driver(myObject); // wiring the dependency, e.g. myObject is a dependency of Driver
myDriver.run();
```
## Jakarta Contexts & Dependency Injection (CDI) - Rarely Used

- a specification (interface)
- Spring Framework implements CDI

|CDI|Spring|
|--|--|
|@Inject|@Autowired|
|@Named|@Component|
|@Qualifier|@Qualifier|
|@Scope|@Scope|
|@Singleton|@Singleton|

## @Component

- Generic annotation applicable for any class
- Base for all Spring Stereotype Annotations
- Specializations of @Component
     - @Service - class has business logic
     - @Controller - class is a controller, e.g. Web Applications and REST API
     - @Repository - retrieve or manipulate data in a database
     
## Spring Framework and Modules 

- Spring Core (IOC Container, Dependency, Injection, Autowiring)
- Building Web Applications (MVC)
- Creating REST APIs (MVC)
- Authentication and Authorization
- Database Operations (JDBC, JPA, etc)
- Integration with other Systems (JMS)
- Unit Testing (MVC, Test)

## Spring Projects

- Spring Framework - 1st Project (Core, Test, MVC, JDBC)
- Spring Security - securing Web Application, REST API or Microservices
- Spring Data - NoSQL and SQL
- Spring Integration - integration with other applications
- Spring Boot - microservices
- Spring Cloud - cloud native applications
