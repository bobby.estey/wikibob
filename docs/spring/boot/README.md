# Spring Boot (Spring Project)

- Framework to build Web Applications, REST API, Full Stack Applications quickly

## Overview

- [Spring Boot Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#application-properties.server)
- [Spring Tools](./springTools.png)
- [Spring Initializer - Start Spring IO](https://start.spring.io/)
- Spring is a Dependency Management Framework
- Create standalone Spring Application execution
- Tries to Auto Configure products and projects
- Provides several features automatically, e.g.  embedded servers, security, metrics, health checks
- Spring Boot is not a Application or Web Server
- Spring Boot does not implement any specific framework but a wrapper, e.g.  JPA or JMS
- Spring Boot does not generate code
- Spring Boot Actuator - provides Monitoring
- Spring Boot Auto Configuration - provide configurations based on the Dependencies provided
- Spring Boot DevTools - allow making changes without manually restarting the server, increase developer productivity
- Spring Boot Embedded Server - no need to separate application servers
- Spring Boot provides logging and error handling
- Spring Boot has Profiles and @ConfigurationProperties
- Spring Boot Starter Projects - help define dependencies for projects
- Spring Boot provides non functional requirements (NFRs) - Actuator and Embedded Server, Profiles and ConfigurationProperties
- Any object managed by Spring is called a Spring Bean
- Spring Bean Scopes
     - Singleton (default) - one object instance per Spring IoC container, used with stateless (do not save state) beans
     - Prototype (rarely used) - one to many object instances per Spring IoC container, used with stateful (save state) beans
     - Web Applications Scope
          - Request - one object instance per single HTTP request
          - Session - one object instance per user HTTP session
          - Application - one object instance per web application runtime
          - Websocket - one object instance per WebSocket instance

- Execution:  http://localhost:6464/basicService/<<message>>
     - For Example:  http://localhost:6464/basicService/USS%20Constellation%20CV-64
![SpringBoot eclipse IDE](./springBoot0.png)
          
- Java Singleton Gangs of Four (GOF)
     - one object instance per Java Virtual Machine (JVM)
- Spring Singleton
     - one object instance per Spring IoC container

## Spring Boot Starter Projects - help find dependencies for projects

- starter parent|contains several items for a SpringBoot project, e.g. default plugins, **dependency descriptors**, versioning, java version, etc.  Versions are inherited from Starter Parent
- spring-boot-starter-data-jpa - provides Interfaces to access data and expose services with JPA and Hibernate
- spring-boot-starter-data-rest - provides Interfaces to access data and expose services with REST
- spring-boot-starter-jdbc - JDBC
- spring-boot-starter-security - Security
- spring-boot-starter-test - JUnit Jupiter, Hamcrest and Mockito
- spring-boot-starter-web - REST API and Web Applications

## Spring Boot Auto Configuration

- Automated Configuration for the application
- Decided on frameworks in Class path
- What is the existing Configuration (Annotations, etc)
- Configuration Files are located in Maven Dependencies:  org.springframework.boot.autoconfigure
- Logs - CONDITIONS EVALUATION REPORT
     - Positive Matches - display configurations that were auto configured
     - Negative Matches - display configurations that were not auto configured

![SpringBoot Tab](./springBoot1.png)
![SpringBoot Arguments Tab](./springBoot2.png)
![SpringBoot Classpath Tab](./springBoot3.png)
![SpringBoot Common Tab](./springBoot4.png)

## Spring Boot DevTools

- allow making changes without manually restarting the server, increase developer productivity
- POM file updates required a restart of the server

## Profiles - Environment Specific Configurations

- Environments:  Development, QA, Stage, Production
- Different Databases
- Different Web Services

|Heading|Lazy Initialization|Eager Initializaton|
|--|--|--|
|Initialization Time|Bean initialized when application is first used|Bean initialized at application start up|
|Default|Not Default|Default|
|Errors|Errors only during runtime|Errors prevent application startup|
|Usage|Rarely|Frequenty|
|Memory Consumption|Less (until Bean is initialize)|All Beans are initialized|
|Recommended Scenario|Use on Beans rarely used and lazy resolution proxy will be injected instead of actual dependency|Most Beans|
|Performance|Faster on Application Startup|Slower on Application Startup|

|Heading|@Component|@Bean|
|--|--|--|
|Implementation|Any Java Class|methods in Configuration classes|
|Code|Add Annotation|Write all the Code|
|Autowiring|Use any of the Dependency Injection Types|method call or parameters|
|Bean Creation|Spring Framework|Developer|
|Which to use|Used most of the time for general computing|- Custom Business Logic<br>- 3rd party libraries|

## pom.xml

```
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

## application.properties

```
# set the logging level
logging.level.org.springframework=debug
# server port setting
server.port=6464
# active profile dev, e.g. application-dev.properties
spring.profiles.active=dev
# actuator endpoints
management.endpoints.web.exposure.include=*
```

# Logging Levels and Order

- Levels:  trace, debug, info, warning, error, off
- trace - log everything - trace, debug, info, warning, error
- debug - debug, info, warning, error
- info - info, warning, error
- warning - warning, error
- error - only errors
- off - log nothing

# Maven build

- mvn clean install
- located the Building Jar file in the logs
- cd to the directory, e.g. /home/cv64/git/cv64/spring-boot/target
- java -jar <jarfile>, e.g. spring-boot-0.0.1-SNAPSHOT.jar
- /opt/java/jdk-20.0.1/bin/java -jar spring-boot-0.0.1-SNAPSHOT.jar

# Spring Boot Execution

- Execute either of the following
     - right click BasicApplication.java and run as Java Application
     - right click BasicApplication.java and run as Spring Boot App
        
 - Navigate in a Browser to http://localhost:**<port number>**/basicService/**<enter a String>**
     - for instance:  http://localhost:**6464**/basicService/**USS Constellation CV-64**
     
# Annotation Example

```
@PutMapping(value = "/{contactId}", consumes = "application/json", produces = "application/json")
public ContactDto updateContact(@RequestBody ContactDto toUpdate, @PathVariable int contactId) {
    //Set the id since in the spec that wouldn't be sent to me
    toUpdate.setId(contactId);
    return contactService.updateContact(toUpdate);
} 
```

## Examples

@Autowired - Primary
@Autowired & @Qualifier - Qualifier (highest priority)

## Glossary

|items|description|
|--|--|
|application launcher|starts the springboot application, like a main method for the Java Virtual Machine (JVM)|
|autoconfiguration|/src/main/resources/application.properties - logging.level.org.springframework: DEBUG|
|@Autowired|- injects objects with Spring Dependency injection into other objects (the application)<br>- injects the Environment object into the REST Controller or Service, e.g. @Autowired<br>private Environment environment;<br>Resolve and Inject Collaborating Beans into our Bean|Wiring dependencies for a Spring Bean|
|@Bean|Method produces a bean to be managed by Spring Container|
|@Component|register Java Beans as Spring components|
|@ComponentScan|the root package to scan for Springboot Classes:  @Component, @Controller, @Repository, @Service|
|@Configuration|Class declares @Bean methods to be processed by the Spring Container|
|@ConfigurationProperties|gets all the properties and reads into a Bean, e.g. @ConfigurationProperties("spring.datasource", returns all the properties with the prefix "spring.datasource" in application.properties|
|@DeleteMapping|Deletes the entity|
|@Entity|Plain Old Java Object (POJO)|
|EXCEPTIONS|@ResponseStatus @ExceptionHandler @ControllerAdvice|
|Filter and Interceptor|Both are the same, except Interceptor (Spring only) is more powerful because of the Handler object|
|@GetMapping|uses the path and gets the entity, e.g. @GetMapping("/path")|
|@Id|a primary key is required|
|[Interceptor](https://www.linkedin.com/pulse/introduction-interceptor-spring-mvc-aneshka-goyal/)||
|@Lazy|Initialization is not called|
|MessageConverter|look at the request and converts to the appropriate type of message, e.g. JSON|
|@PathVariable|{pathVariable}, the variable being passed in to the method @GetMapping("/path/{pathVariable}|
|@PersistenceContext|Starting tracking transactions in the persistence context|
|@PostMapping|Adds the entity @PostMapping(value = "/content", produces = MediaType.APPLICATION_JSON_VALUE)|
|@Primary|Bean is priority over other candidates|
|@PutMapping|Update the entity|
|@Qualifier|Bean is AUTOWIRED - Meaning the name is passed using the QUALIFIER keyword, e.g. Explicit|
|@Query|Query statement, e.g. @Query("select ...")|
|@RequestBody|HttpRequest body transferred to the Server, e.g. @RequestBody SomeDto someObject)|
|@RequestMapping|all incoming HTTP request URLs, e.g. @RequestMapping("path")|
|@RestController|the controller for a Controller or Service|
|@Repository|the class provides storage, retrieval, search, update and delete operations|
|@Service|the service itself|
|@SpringBootApplication|does a ComponentScan of the package this annotation is defined, e.g. us.cv64.springboot, a combination of 3 annotations: @SpringBootConfiguration + @EnableAutoConfiguration + @ComponentScan|
|@Transactional|Open / Close Transactions for each method|
|@Value|read the property, e.g. @Value("${server.port}")|

## Troubleshooting

|Issue|Solution|
|--|--|
|Multiple Matching Beans (Multiple Candidates)|- @Primary or @Qualifier annotation added to the Spring Bean resolves multiple candidates<br>- [Which to use](https://www.udemy.com/course/spring-boot-and-spring-framework-tutorial-for-beginners/learn/lecture/35020034#overview)|
|NoSuchBeanDefinition|@ComponentScan annotation to show Spring where to look for the Bean definition|
|Print out all the Bean Definitions managed by Spring Framework|Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);|
|UnsatisfiedDependencyException|add @Component to Class|

