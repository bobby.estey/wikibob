# Spring Boot Validation

- Spring Boot Starter Validation - pom.xml
- Command Bean (Form Backing Object) - 2 way binding (todo.jsp & TodoController.java)
- Add Validations to Bean - Todo.java
- Display Validation Errors in the View - todo.jsp
