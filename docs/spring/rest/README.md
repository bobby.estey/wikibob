# Spring Rest Services

- REpresentational State Transfer (REST) - a set of Architectural Constraints.  A Service provides these constraints.
  - Client (Service Consumer) / Server (Service Producer)
  - Uniform Resource Locator (URL) - to expose resources
  - Service is Stateless - no persistence
  - Results should be Cacheable.
  - Service should assume a Layered architecture
  
- REST API (also known as RESTful API) is an Application Programming Interface that conforms to the constraints of REST architectural style and allows for interaction with RESTful web services
- REST is an architectural style that allows computing systems to communicate over the Internet.  A set of APIs that follow the REST specification.  Resources are available through resource mapping and commands, e.g. get, put, push, delete
- The types of test for REST are Unit Tests (core testing at the unit level / low level), integration / contract, stress testing and then acceptance test

## Demo Program

- http://localhost:6464/students/Student1/courses
    - user:  user1 or admin1
    - password:  secret1
    
## Postman

- http://localhost:6464/students/Student1/courses/Course1

![Basic](postmanBasicAuthorization.png)

```
curl --location --request GET 'http://localhost:6464/students/Student1/courses/Course3' \
--header 'Authorization: Basic dXNlcjE6c2VjcmV0MQ==' \
--header 'Cookie: JSESSIONID=83C84E3621AC1B8E3EA27D47DB6D68E4' \
--data-raw ''
```

- http://localhost:6464/students/Student1/courses

```
curl --location --request POST 'http://localhost:6464/students/Student1/courses' \
--header 'Authorization: Basic dXNlcjE6c2VjcmV0MQ==' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=83C84E3621AC1B8E3EA27D47DB6D68E4' \
--data-raw '{
  "name": "Microservices",
  "description": "10 Steps",
  "steps": [
    "Learn How to Break Things Up",
    "Automate the hell out of everything",
    "Have fun"
  ]
}'
```

## Tutorials
 * [Excellent Tutorial - Spring REST Web Services](https://www.youtube.com/watch?v=YEEUn5JZ9t0&list=PLBBog2r6uMCRzaJqr-uUC8gakwSxkPSBh&index=4)
 
## [Spring Initializer](https://start.spring.io/)

[Application](startSpringInitializer.png)
    
## Notes

- @PathVariable - variable
- @RequestBody - body of the request, JSON
