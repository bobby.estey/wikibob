# Spring Actuator

- Monitor and Manage Application

# pom.xml

```
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

# resources/application.properties

- Show all ActuatorEndpoints
- management.endpoints.web.exposure.include=* - return all Actuator Endpoints
- management.endpoints.web.exposure.include=health,metrics - only return these Actuator Endpoints

# Actuator Endpoints - http://localhost:8080/actuator/

- self - actuator itself
     - http://localhost:6464/actuator
- beans - list of spring beans in the application, e.g. @Autowired
     - http://localhost:6464/actuator/beans
- configprops - configuration properties
     - http://localhost:6464/actuator/configprops
- env - environment
     - http://localhost:6464/actuator/env
- health - application health information
     - http://localhost:6464/actuator/health
- metrics - application metrics, e.g. metrics/<metric name> metrics/http.server.requests
     - http://localhost:6464/actuator/metrics/jvm.compilation.time
- mappings - details on request mappings
     - http://localhost:6464/actuator/mappings
