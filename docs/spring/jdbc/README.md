# Spring JDBC

- Less code needs to be written

## JDBC

```
public void deleteRecord(int id) {
    PreparedStatement preparedStatement = null;
    
    try {
        preparedStatement = database.connection.preparedStatement("delete from myTable where id=?");
        
        preparedStatement.setInt(1, id);
        preparedStatement.execute();
    } catch (SQLException e) {
        System.err.log("Delete Record, id: " + id);
        System.err.log(e.getMessage);
    } finally {
        if (preparedStatement != null) {
            try {preparedStatement.close();}
            catch (SQLException e) {}
        }
    }
}
```

# SpringJDBC

```
public void deleteRecord(int id) {
     jdbcTemplate.update("delete from myTable where id=?", id);
}
```
