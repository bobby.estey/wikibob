# GPS

## Links

- [Lat / Long Calculator](https://www.fcc.gov/media/radio/dms-decimal)
- [Installing and Using ExifTool on Linux](https://www.geeksforgeeks.org/installing-and-using-exiftool-on-linux/)
- [Google Maps](https://support.google.com/maps/answer/18539?hl=en&co=GENIE.Platform%3DDesktop)

## ExifTool - GPS Utility

#### Installation

- Clone the git repository
- Test with the test file, ExifTool.jpg

```
git clone https://github.com/exiftool/exiftool.git
cd exiftool
./exiftool t/images/ExifTool.jpg
```

#### Execution

- Extract entire list of metadata - **exiftool &lt;filename&gt;**

## Google Maps

- Decimal degrees (DD): 41.40338, 2.17403
- Degrees, minutes, and seconds (DMS): 41°24'12.2"N 2°10'26.5"E
- Degrees and decimal minutes (DMM): 41 24.2028, 2 10.4418
