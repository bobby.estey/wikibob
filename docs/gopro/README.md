# Go Pro

## Reset

- Press Settings Button PORT side
- Screen Swipe Down
- Top of Screen with 8 Icons, Swipe Right
- Select Connections
- Scroll to Bottom, Select Reset Connections
- Press Reset

## [Video](goProReset.mp4)

## SD Card and Battery

- [Remove / Replace](https://www.youtube.com/watch?v=Nvw4Y9SBq4w)
