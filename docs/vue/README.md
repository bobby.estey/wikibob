# Vue JS

This page is about the Vue JavaScript (JS) architecture

## Installation

- [Install NodeJS and Node Package Manager (NPM)](https://nodejs.org/en)
- Validate NodeJS and NPM are installed
     - node --version
     - npm --version
- [Vue CLI Website](https://cli.vuejs.org/)
- Install Vue Command Line Interface (CLI)
     - npm install -g @vue/cli
- Validate Vue Installation
     - vue --version
     
## Create Vue Project
- vue create vue-first-project
     - Preset:  Default ([Vue 3] babel, eslint)
     - Package Manager:  NPM
- cd vue-first-project
- npm run server
- Open Browser at IP address:  http://localhost:8081/

![vueLocalhost8081.png](vueLocalhost8081.png)

## References

- [Installation Video](https://www.youtube.com/watch?v=7HPM9E1PJnw)
