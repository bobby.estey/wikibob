# JavaScript

# References

- [The Greatest Ever - PUT YOUR SEATBELTS ON BECAUSE HE IS FAST -> GO HERE](https://www.youtube.com/watch?v=fju9ii8YsGs&list=PLBA965A22D89CF13B&index=10)

# Definitions

|Definition|Description|
|--|--|
|EcmaScript|A Standard / Interface|
|JavaScript|An Implementation|
|use strict|strict coding standards, e.g. all attributes must be initialized|
|typeof|typeof operator<br>- returns a string indicating the type of the operand<br>- console.log(typeof 64); // prints "number"<br>- console.log(typeof "Hello world");  // prints "string"|
     
## Template Literals (this character `)

[Template Literal Example](javascript.html)

#
<p id="demo">someTitle</p>
var x = document.getElementById("demo");
x.style.color = "red"

## Prototype

- JavaScript objects inherit properties and methods from a prototype

```
<script>
  // function constructor
  function Person(name, job, yearOfBirth){  
    this.name = name;
    this.job = job;
    this.yearOfBirth = yearOfBirth;
  }

  // adding calculateAge() method to the Prototype property
  Person.prototype.calculateAge= function(){ 
    console.log('The current age is: '+(2019- this.yearOfBirth));
  }

  console.log(Person.prototype);
  
  // creating Object Person1
  let Person1= new Person('Jenni', 'clerk', 1986); 
  console.log(Person1)
  let Person2= new Person('Madhu', 'Developer', 1997);
  console.log(Person2)
  
  Person1.calculateAge();
  Person2.calculateAge();
</script>
```

## What are the differences between var, let, const?

- const, constant
- var, global scope, doesn't need to be initialized AKA Hoisting = var = undefined, redeclaration legal
- let, local scope, cannot be a global variable, required to be initialized = let = ReferenceError, redeclaration illegal

## What is difference between == and ===?

|compares value|compares value and type|
|--|--|
|if('1' == 1) // true|if('1' === 1) // false|

## What is the difference null and undefined (both are empty value)?

|null|undefined|
|--|--|
|you are setting null|when define a variable, automatically puts a placeholder called undefined|
|typeof(null) => object|js is setting undefined|
||typeof(undefined) => undefined|

## What is the use of arrow function?

```
Const profile = {
	firstname: '',
	lastname: '',
	
	setName: function(name) {
		let splitName = function(name) {
			let splitName = (n) => {
				let nameArray = n.split(' ');
				this.firstName = nameArray[0];
				this.lastname = nameArray[1];
			}
		splitName(name);
	}
}

profile.setName('john doe');

console.log(window.firstname);
console.log(profile.firstname);
```

# JavaScript History
 
|Year|Specification|Details|
|--|--|--|
|1995|Creation of JavaScript|Created by Brendan Eich at Netscape<br>- Originally named "Mocha" and later "LiveScript," before settling on JavaScript<br>- Designed for adding interactivity to web pages<br>- First implemented in Netscape Navigator 2.0.|
|1997|ECMAScript 1|Formalized as ECMAScript 1 (ES1) by Ecma International<br>- First standardized version.|
|1999|ECMAScript 3|Significant improvements over ES1<br>- Widely adopted and implemented across browsers<br>- Introduced features like try/catch for error handling|
|2005|AJAX and Web 2.0|JavaScript's role in AJAX (Asynchronous JavaScript and XML) becomes prominent. <br>- Enabled asynchronous data retrieval without reloading the entire page<br>- Contributed to the rise of dynamic and interactive web applications|
|2009|ECMAScript 5|Introduced strict mode for better error handling<br>- Added new methods for arrays<br>- Improved JSON support|
|2015|ECMAScript 6 (ES6)|Major update with significant new features: let and const for variable declarations, arrow functions, classes, modules, and more<br>- Transpilers like Babel allowed developers to use ES6 features in older environments|
|2017|ECMAScript 8 	Introduced async/await for asynchronous programming<br>- Added Object.values/Object.entries|
|2018|ECMAScript 9 (ES2018)|New features include asynchronous iteration, rest/spread properties for objects, and Promise.prototype.finally()|
|2019|ECMAScript 10 (ES2019)|Introduced features like Array.prototype.flat() and Array.prototype.flatMap()<br>- Enhancements to JSON.stringify()|
|2020|ECMAScript 11 (ES2020)|Added nullish coalescing operator (??) and optional chaining (?.)<br>- BigInt for working with arbitrary precision integers|
|2021|ECMAScript 12 (ES2021|- Introduces features like String.prototype.replaceAll() and Promise.any()<br>- Enhanced support for asynchronous programming|
|Present|- Continued Evolution and Widespread Use|JavaScript remains a core technology for web development<br>- Continued updates to the ECMAScript standard<br>- Popular frameworks and libraries (e.g., React, Angular, Vue) contribute to JavaScript's versatility|
 