# Async / Await

# Definitions

- Asynchronous - make a request and not wait for the response, continue processing not waiting for the response
- Synchronous - make a request and wait for response, do not go forward until the response has been returned

# References

- [Brad Traversy - Callbacks, Promises, Async, Await](https://youtu.be/PoRJizFvM7s?t=0)
- [Initial JavaScript](https://youtu.be/PoRJizFvM7s?t=408)
# 