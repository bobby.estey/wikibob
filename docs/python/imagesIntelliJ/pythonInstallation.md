# Python Installation

##### [Download Python](https://www.python.org/downloads/)

# Install Python - Check the checkboxes and Press Customize Install (NEVER SELECT DEFAULT INSTALLATION)
![01](python01.png)

##### Optional Features - Check all checkboxes and Press Next
![02](python02.png)

##### Advanced Options - Check the following Boxes and SELECT the directory, either git directory or Program Files (NEVER SELECT AUTOMATED DIRECTORY) ...
![03](python03.png)

##### Setup Progress Screen
![04](python04.png)

##### Setup Successful
![05](python05.png)
