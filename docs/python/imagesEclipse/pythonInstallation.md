# Python Installation

##### [Download Python](https://www.python.org/downloads/)

##### Create opt (optional directory)
![01](python01.png)

##### Press Manual config
![02](python02.png)

##### Press New ...
![03a](python03.png)

##### Press Browse for python/pypy exe
![03b](python03.png)

##### Open c:\opt\python
![04](python04.png)

##### Update Interpreter Name to python3
![05](python05.png)

##### Select All
![06](python06.png)

##### Configuration appears, press Apply All
![07](python07.png)

##### Progress Meter Appears
![08](python08.png)

##### Configure Parameters, press Finish
![09](python09.png)

##### Check Remember my decision
![10](python10.png)

##### Press Open Perspective
![11](python11.png)

##### Python appears in eclipse IDE
![12](python12.png)

##### Right click code -> Run As -> Python Run
![13](python13.png)
