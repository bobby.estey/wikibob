# Wildfly - the new JBoss

- Wildfly Path:  **/opt/wildfly**
- Configuration Directory:  **standalone/configuration**

[Install on Ubuntu](https://computingforgeeks.com/install-wildfly-application-server-on-ubuntu-debian/)

# Installation Steps

```
sudo apt install curl wget
WILDFLY_RELEASE=$(curl -s https://api.github.com/repos/wildfly/wildfly/releases/latest|grep tag_name|cut -d '"' -f 4)
echo $WILDFLY_RELEASE
wget https://github.com/wildfly/wildfly/releases/download/${WILDFLY_RELEASE}/wildfly-${WILDFLY_RELEASE}.tar.gz
mv wildfly-27.0.1.Final.tar.gz ~/Downloads/
cd ~/Downloads/
tar xvf wildfly-27.0.1.Final.tar.gz 
sudo mv wildfly-27.0.1.Final /opt/wildfly
cd /opt
sudo groupadd --system wildfly
sudo useradd -s /sbin/nologin --system -d /opt/wildfly  -g wildfly wildfly
sudo mkdir /etc/wildfly
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/
sudo chmod +x /opt/wildfly/bin/launch.sh
sudo chown -R wildfly:wildfly /opt/wildfly
gedit ~/.bashrc &
sudo systemctl daemon-reload
sudo systemctl start wildfly
sudo systemctl enable wildfly
sudo systemctl status wildfly
ss -tunelp | grep 8080
sudo gedit /opt/wildfly/domain/configuration/mgmt-users.properties &
sudo gedit /opt/wildfly/domain/configuration/mgmt-groups.properties &
sudo gedit /opt/wildfly/domain/configuration/host.xml &
```

## ~/.bashrc

```
export WILDFLY_PATH=/opt/wildfly/bin
export PATH=${PATH}:${WILDFLY_PATH}
```

## mgmt-users.properties / mgmt-groups.properties
 
 - added the following comments at the end of the files:
 
     - # To represent the user add the following to the server-identities definition ``<secret value="cmViYnlHNjReJA=="`` />
 
## host.xml

- added the following inside the **security-realms** element

```
<security-realms>
  <identity-realm name="local" identity="$local"/>
  <properties-realm name="ManagementRealm">
    <users-properties path="mgmt-users.properties" relative-to="jboss.domain.config.dir" digest-realm-name="ManagementRealm"/>
    <groups-properties path="mgmt-groups.properties" relative-to="jboss.domain.config.dir"/>
  </properties-realm>
                
  <security-realm name="ManagementRealm">
    <authentication>
      <local default-user="$local" />
        <properties path="mgmt-users.properties" relative-to="jboss.domain.config.dir"/>
    </authentication>
    <server-identities>
      <secret value="<secret value generated>" />
    </server-identities>
  </security-realm> 
</security-realms>
```

# Browser Console

- http://localhost:9990/console/index.html