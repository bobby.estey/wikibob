# [arlo software](https://www.arlo.com)

## Product Name:  Arlo Essential 2 Outdoor 2K (VMC3050A)

- I wrote this page, because this company while having a good idea, both hardware and software engineers are Morons
     - [Moron](https://www.google.com/search?channel=fs&client=ubuntu-sn&q=moron)

## Procedures

|Issue|Solution|
|--|--|
|[Arlo Account](https://my.arlo.com)|Launch the Arlo Secure App or log in to Arlo Account|
|Add Device to Dashboard|- Dashboard --> Press "+ upper right corner" --> Add a Widget --> Select Camera "check" --> Next --> Small Icon|
|[Smart Detection Settings](smartDetection.png)|- Settings --> Home --> Smart Detection --> Devices --> Device|
|[Erase All Content](eraseAllContent.png)|- Navigate to Settings --> Privacy Center --> Content<br>- Select Erase Arlo Cloud<br>- Press Erase All Content<br>- Erase All Content|
|[Camera Battery](battery.png)|- Settings --> Home --> Devices --> Select Device --> Battery|

## Equipment

- Hardware
     - the mounting brackets are low quality, flawed and crap
     - someone got a patent on this crap
- Software - let me be clear
     - I am a software developer and know exactly the type of people who write this code and instructions
     - People automatically think these engineers are smart, they are not even close, IQ Levels are (70 to 84: Borderline mental disability)
          - Here's an example of what I am talking about:  Send them a text message, what is 2 + 2, reply a thumbs up


## Procedures

#### Pairing

- Press button on top of Camera for 3 seconds
- Blue light flashes
- Blue light stops flashing when paired

#### Record manually

- Launch the Arlo Secure App
- Tap Devices
- Tap the Play icon
- Tap the Record icon
- Tap the screen and tap the Stop icon to end the recording
- Tap Feed

#### Remove the Camera

- Launch the Arlo Secure App or log in to your Arlo account at my.arlo.com
- Tap or click Devices
- Tap or click on the camera you want to reset
- Tap or click Gear
- Tap or click the Device Name
- Tap or click Remove Device
