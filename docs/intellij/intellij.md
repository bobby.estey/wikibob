# IntelliJ

- [IntelliJ Shortcuts Website 1](https://www.shortcutfoo.com/app/dojos/intellij-idea-win/cheatsheet)
- [IntelliJ Shortcuts Website 2](https://www.jetbrains.com/help/idea/mastering-keyboard-shortcuts.html#advanced-features)
- [IntelliJ Wildfly Installation](https://www.youtube.com/watch?v=RScxirs2zCc)

|Command|Description|
|--|--|
|ctrl alt L|reformat|
|ctrl alt O|fix imports|
|ctrl click| |
|ctrl h|show hidden files|
|ctrl shift f|find|
|target|locate code in Project explorer|

|database commands|Description|
|--|--|
|Database -> QL button|opens up the console|


|git commands|Description|
|--|--|
|Git -> Show Git Log| Repository Tree|
|Right Git -> Show History| |

# Testing

## IntelliJ Project Setup

**NOTE:  DO NOT Configure GIT or add files to GIT until later**

- File -> New -> Project...
- Select Spring Initializr
- Fill out the Fields
    - Name:  Name of the Project
    - Location:  Where you want the project rooted, e.g. Linux:  ~/git/<yourProjectFolder> Windows:  C:/git/<yourProjectFolder>
    - Language:  Java
    - Type:  Maven
    - Group:  Package Name Root
    - Artifact:  Match Name of the Project
    - JDK:  The JDK you are using
    - Java:  Select the Version of Java
    - Packaging:  Jar
    
![Spring Project](springInitializr.png)

- Spring Boot Dropdown:  Select the latest version, not M or SNAPSHOT versions
- Developer Tools -> Spring Boot DevTools
- Developer Tools -> Lombok
- Web -> Spring Web
- Press Create

![Dependencies](dependencies.png)
    

## Automated Testing

- Edit Test Run/Debug Configurations
- Press "+" in upper left corner -> Select Jest
- Jest Options add:  --watch

## Wildfly

- Settings
- Build, Execution, Deployment -> Application Servers
- Press "+"
- Select JBoss / Wildfly Server

![JBoss / Wildfly Server](jbossWildflyServer.png)

- Fill out Form and press OK

![JBoss / Wildfly Server Configuration](jbossWildflyServerConfig.png)

- Fill out Form and press OK
- Open Java Enterprise Edition (JEE) Project
    - Select Project Root Folder and press OK
- Run / Debug Configurations
    - Press "+"
    - Select JBoss / Wildfly Server -> Local
    - Select Deployment Tab and press "+"
    - Press Artifact -> artifact.war exploded and press OK
    - Select Server Tab and URL should be populated with http://localhost:8080/<artifact> and press OK
    - Press Run "Green Arrow"
- Application Executing
    - Open up Browser and go to localhost:8080/<artifact>
