# Swagger / OpenAPI

- 2011 - Swagger Specification and Tools (Swagger UI
     - Swagger UI - Visualize and Interact with your REST API
- 2016 - OpenAPI Specification

# Spring Doc OpenAPI

[WebSite](https://springdoc.org/)

- Java library automating generation of API documentation of Spring boot projects.

```
<dependency>
  <groupId>org.springdoc</groupId>
  <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
  <version>2.1.0</version>
</dependency>
```

# Swagger UI

[OpenAPI definition](http://localhost:6464/swagger-ui/index.html)

- Try It Out - Press Execute
