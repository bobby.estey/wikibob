# Simple Screen Recorder

- [Website](https://www.maartenbaert.be/simplescreenrecorder/)

## Installation

- apt - file located in /usr/bin/simplescreenrecorder
- snap - ~/snap/simplescreenrecorder

```
sudo apt-get update
sudo apt-get install simplescreenrecorder

sudo snap install simplescreenrecorder 
```

## Removal

```
sudo apt autoremove simplescreenrecorder
sudo apt purge simplescreenrecorder

sudo snap remove simplescreenrecorder
```

## Configuration

- [Simple Screen Recorder 1](ssr1.png)
- [Simple Screen Recorder 2](ssr2.png)

## Execution

```
simplescreenrecorder
```

## Troubleshooting

|Issue|Solution|
|--|--|
|[x11/xorg warning](x11xorg.png)|Change Login for Ubuntu to:  Ubuntu on Xorg|
