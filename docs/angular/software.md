# Software

* bootswatch.com - http://bootswatch.com/simplex/bootstrap.min.css - put in index.html
* bootstrap.com - http://getbootstrap.com/components/
* angular-cli.json - put the following in this file for bootstrap:
    * "styles": [ "../node_modules/bootstrap/dist/css/bootstrap.min.css",  "styles.css" ],
* maps
* https://angular-maps.com/guides/getting-started/
* npm install @agm/core --save-dev
* npm install @types/googlemaps --save-dev