# Artificial Intelligence (AI)

## Links

- [Spring AI](https://spring.io/projects/spring-ai)

## Videos

- [packtpub.com](https://www.youtube.com/watch?v=mbNcq5q8jQ4&list=PLTgRMOcmRb3Mwbve2rtK4Bsq0enJ5wMyV)
- [Packt - 1 Introduction](https://www.youtube.com/watch?v=mbNcq5q8jQ4)
- [Packt - 2 State Space](https://www.youtube.com/watch?v=hhrzWYdc6JI)
- [Packt - 3 Min/Max Nodes](https://www.youtube.com/watch?v=1F5QhtkM3bI)
- [Packt - 4 Weka](https://www.youtube.com/watch?v=lyW8m699ln4)
- [Packt - 5 Attributes](https://www.youtube.com/watch?v=fwaE3IvYAc4)
     - [Code](https://gitlab.com/bobby.estey/ai-chatgpt/-/blob/main/src/main/java/us/cv64/ai_chatgpt/weka/FilterAttribute.java?ref_type=heads)
- [Packt - 6 Classifier](https://www.youtube.com/watch?v=I4hC1t_rEbE)
     - [Code](https://gitlab.com/bobby.estey/ai-chatgpt/-/blob/main/src/main/java/us/cv64/ai_chatgpt/weka/DevelopClassifier.java?ref_type=heads)
- [Packt - 7 K-means Clustering](https://www.youtube.com/watch?v=4h39sm1vW60)
     - [Code](https://gitlab.com/bobby.estey/ai-chatgpt/-/blob/main/src/main/java/us/cv64/ai_chatgpt/weka/Clustering.java?ref_type=heads)

## Terminology

|Term|Description|
|--|--|
|Game Playing|Classified as Game Trees|
|Min nodes|Minimum Cost Successor|
|Max nodes|Maximum Cost Successor|
|Tree|- Root Node and has Children<br>- Terminal Nodes are Trees|
|Waikato Environment for Knowledge Analysis [(Weka)](https://ml.cms.waikato.ac.nz/weka)|Machine Learning Software written in Java|
|Starting Point|S - where the starting point Starts|
|s|State Transitional Operators from node to node|
|O|Exhaustive List - Transitions keep track of the Exhaustive List|
|G|Goal State|
|State Space|- Given [S, s, O, G]<br>- A minimum cost transition to a goal state<br>- A sequence of transitions to a minimum cost goal<br>- A minimum cost transition to a minimum cost goal|

# ChatGPT (Generative Pre-trained Transformer)

- The GPT architecture was first introduced by OpenAI in 2018

- A type of neural network architecture that is used for natural language processing tasks such as:
    - language translation
    - text completion
    - language generation and translating
    - answering questions
    - generating summaries
    