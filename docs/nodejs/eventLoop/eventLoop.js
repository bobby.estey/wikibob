// https://www.youtube.com/watch?v=EQL9r8UlrPA
console.log('The first statement');

setTimeout(function() {
  console.log('The second statement');
}, 2000);

console.log('The third statement');