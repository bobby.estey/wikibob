# NodeJS Express

# Installation

- Install NodeJS
- npm install express –save

# NodeJS Express Creation

![NodeJS Express Creation](express.png)
- mkdir <application> e.g. mkdir expressApplication
- cd expressApplication
- npm init
- enter defaults, builds package.json
- npm install express --save, downloads node_modules

# Terminology

- [Routing](https://expressjs.com/en/guide/routing.html) is an approach of navigating to web pages.  The document mentions endpoints, these are Uniform Resource Identifier (URI) addresses that a client makes a request to a service.  The syntax for a route is **app.httpMethod(PATH, HANDLER)**.  The most popular HTTP Methods are the following and I will post Structured Query Language (SQL) examples that should help:
     - get – select
     - post – insert into
     - put – update
     - delete – delete

- **Route Paths** are a URI which includes strings, regular expressions, AKA (REGEX).  If you have seen URIs with #, ? and % these are utilized to pass key values and much more.  For example, the following is informing the browser to go to the address and the # is identifying an anchor on the page to start:
     - https://gitlab.com/bobby.estey/wikibob#wikibob-bobbys-wiki
 
- **Route Parameters** are key values that are passed to the server.  For example, req.params: {“from”: “PHX”, “to”: “OMA”} the keys are from and to and values are PHX and OMA in that order.

- **Route Handlers**  a process of how to handle a request.

- **Response Methods** are listed on the page, the most popular would be res.end(), res.json(), res.send()
