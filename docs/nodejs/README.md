# [NodeJS](https://nodejs.org)

- [NodeJS Installation](https://nodejs.org/en/download)
     - Linux - follow the instructions in the Bash Panel
- NodeJS (Node JavaScript) 
     - Asynchronous event driven JavaScript runtime engine that builds network applications    
     - Application that executes JavaScript code outside the Web Browser
- Node Package Manager (NPM)
     - Install, Update, Delete JavaScript packages
- Node Package Executor (NPX)
     - Execute JavaScript packages directly, without installing

# Commands

|command|description|
|--|--|
|npm init|Create Project|
|npm run build|Build production deployable unit<br>- minified<br>- optimized for performance|
|npm run test -- --coverage . --watchAll=false|Execute Unit Tests|
|npm start|Execute application in development mode
|npm test --watch|unit testing|
|npm install --save react-router-dom|add a dependency to the project|
|remove .npmrc, package_lock.json and node_modules|reset npm project|
|nvm install v<version>|upgrade node, e.g. nvm install v18.15.0|
|node --version|get node version|
|npm --version|get Node Package Management version|
|npm troubleshooting|Provides documentation on issues with Node / NPM|

# Extensions
- npm install --save-dev --save-exact prettier
- [Configure Prettier](configurePrettier.png)

# Helloworld Application

```
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

# Starting NodeJS

- Navigate to the JavaScript file you wish to execute and type node filename, e.g. node helloworld.js
     - https://nodejs.org/en/docs/guides/getting-started-guide/
     
# Terminology

- Blocking (single threaded) – an operation waiting on another operation and cannot continue
- Non-Blocking (multi-threaded) – many operations can be executing in parallel

# Event Loop

- JavaScript is single threaded, meaning there are no callbacks, parallel processing, etc. which can hamper performance and blocking operations.  NodeJS utilizes the single threaded Event Loop which bypasses the JavaScript architecture and utilizes the capabilities of the Kernel.

     - [Event Loop Documentation](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/)

- Looking at the diagram on the link above are the various phases with definitions.  The website explains in CONFUSING DETAILS how the Event Loop works.  Anytime I get confused, I ALWAYS search for a Video.  Please look at this video, an AHA MOMENT :-)  This video is SUCCINCT and clear exactly how Event Loop executes.

      - [Event Loop Video](https://www.youtube.com/watch?v=EQL9r8UlrPA)

- Starting at 1:50 we can see requests being queued up as events.  If the event is non-blocking, then the event continues through the queue and completes, in other words, stays on the left blue side of the diagram that is being rendered.  If the event is blocking the the Event Loop takes over and places the blocking event on the right green thread pool and leaves that event on the pool until the event is no longer blocked.  When no longer blocked, the event is complete and sent back to the event queue.

- Starting at 3:00 an actual example utilizing NodeJS and a simple JavaScript file.  You do not need SUBLIME, this is simply an editor.  Create a file, e.g. eventLoop.js and execute the file with NodeJS, e.g. node [eventLoop.js](eventLoop/eventLoop.js)
