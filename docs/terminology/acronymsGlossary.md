# Acronyms / Glossary

## Quick Terms

|Term|Description|Reference|
|--|--|--|
|Abstract Class|objects can't be instantiated (only subclasses can be instantiated), abstract methods (methods without implementation), concrete methods;|always a no arg constructor and can have constructors with arguments|
|Abstraction|working with the interface not the implementation, reducing the object with only the necessary characteristics exposed to the user|complex details hidden from the user, reduces complexity, avoid code duplication, easier to maintain, increase security|
|Application Context|||
|Application Server vs Web Server|||
|[Asynchronous](https://martech.zone/wp-content/uploads/2012/09/asynchronous.png)|non-blocking, code continues without waiting for completion or code freezing, callsbacks and promises|parallel, not scheduled, offload to other processes.  Callback - threading, doing things in the background|
|Cascading Style Sheets (CSS)|element: h1 {...} class .classname {...} id #id {...} ||
|Component Scan|||
|Content Delivery Network (CDN)|Maven, Git, etc.|Network of Servers that distribute content from an origin|
|Coupling|How much work is involved in changing something|- Tightly - engine to a car<br>- Loosely - wheel to a car|
|CI/CD pipelines|Jenkins||
|Continuous Delivery (CD)|e.g. 2 week sprints|continuous testing, release, deploy, operate, monitor|
|Continuous Integration (CI)|- updating<br>version control - git|plan, code, build, continuous testing|
|Cross-Site Scripting (XSS)|Script gets placed on suspected site and clients accidentally execute the script and information is stolen||
|CRUD|Create Read Update Delete||
|[Dependency Injection](https://belief-driven-design.com/decouple-your-code-with-dependency-injection-77b8d39cc93/)|- giving an object its instance variables<br>- decouples the classes construction from the construction of the dependency<br>- Class is dependent on another Class<br>- e.g. Model depends on a Database and the Database is Injected to the Model through a Constructor or setter<br>- DI decouples the Class construction from the construction of the dependencies|[video example](https://www.youtube.com/watch?v=IKD2-MAkXyQ)|
|Dependency Injection Container (DIC)|- Map of Dependencies that the programs needs including the logic to build the dependency||
|Dependency Inversion Principal|- Code depends on abstractions, decoupling the implementations from each other|- Depend on Interfaces|
|DevOps (Development / Operations)|plan, code, build, test, release, deploy, operate, monitor||
|[Document Object Model](https://en.wikipedia.org/wiki/Document_Object_Model#/media/File:DOM-model.svg)|- web page loaded, browser creates DOM which consists of nodes of a document, e.g. <html>, <head>, etc.||
|Domain Driven Design|Software should match the Business Domain, e.g. Classes, objects, methods, attributes should match domain characteristics.  Aircraft Manufactures would have Fuselage, Engine, Wing Classes||
|[draw.io](https://www.youtube.com/watch?v=WlCKv49Pkvg)|- tutorial on how to use a fantastic drawing tool||
|Data Access Object|provides an abstract interface to a database||
|Data Transfer Object|an object of attributes only and only behavior is storage, retrieval, serialization, deserialization||
|Dynamically Typed|Type associated with a runtime value and can change during runtime||
|Eager|observer events to subscriptions|wait to start|
|Encoding|converts characters into a format that can be transmitted, e.g. Bobby Estey = Bobby%20%Estey||
|Extreme Programming|- increase updates many times||
|Framework|foundation of code to build applications, controls the flow and calls your code, e.g. Spring, NodeJS, Angular, Vue.  NOT REACT||
|[Functional Interface](https://gitlab.com/bobby.estey/wikibob/-/tree/master/src/main/java/lambda/functional/interfaces)|@FunctionalInterface (receives 0 to 1 values and returns a single value)|Interface with implementation|
|Functional Programming|decompose a program into small functions, that can be combined.  programming paradigm applying and composing functions, result = 300|const result = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].filter(n => n % 2 === 0).map(a => a * 10).reduce((a, b) => a + b);|
|[Idempotency](https://nordicapis.com/understanding-idempotency-and-safety-in-api-design/)|Produces the same result when called over and over||
|Imperative Programming|programming paradigm using statements changing a programs state, result = 300|const numList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];let result = 0;for (let i = 0; i <numList.length; i++) {  if (numList[i] % 2 === 0) {result += numList[i] * 10;}}|
|Interface|Abstract Type, describes behavior that Classes must implement.  Contract, Protocol|Common actions that can be performed on different set of Classes|
|[Inversion of Control (IoC) Container](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/spring/spring.md)|- Spring Framework, does all the work, setting up everything, only have to make calls to spring boot through annotations<br>- Object that knows how to instantiate and configure other objects, including performing dependency injection|makes your code loosely coupled|
|[Lambda](https://gitlab.com/bobby.estey/wikibob/-/blob/master/src/main/java/lambda/README.md)|anonymous function (without name, return type and access modifier).  Can pass / return data from a method|block of code which takes in parameters and returns a value|
|Lazy|non blocking - load only when needed|promise||
|Library|subset of framework, predefined functions, Classes.  your code controls the flow and calls, e.g. JQuery, React||
|Loosely Coupling|- Flexible / Dynamic code because technology is always changing<br>- Interfaces|Laptop can be taken anywhere, Business Requirements, Frameworks, Code all change.  Functional changes with less code changes|
|[markdown syntax](https://www.markdownguide.org/basic-syntax/)|markdown cheatsheet||
|Pipeline|Jenkins and Azure Builds|Git Checkout->Build Custom Environment->Test(client and server)->build image(docker)->misc ops, e.g. database,patches->deploy|
|Polymorphism|Class providing different implementations|Method Overloading|
|Reactive Form|Angular ReactiveFormsModule|synchronous|
|Representational State Transfer (REST)|- Architectural protocol and standards between computer systems on the web<br>- RESTful, REST compliant systems are stateless and separate the implementation of client and server||
|Stateful|persistent|less secure than stateless, used with binding operations|
|Stateless|non persistent|more secure than stateful, execute and done.  A new transaction, process, etc. every time|
|Synchronous|serialized, one task at a time, scheduled, realtime, standing in line||
|Test-Driven Development (TDD)|Software Requirements converted to Test Cases before software is fully developed.  Software development being repeatedly tested against test cases|Write Test -> Check Test -> Write Code -> Check with All Tests -> Refactoring (update tests)|
|Tightly Coupling|Static code, change the code manually|Desktop Computer is tightly coupled to the work environment|
|Transpile|Translates a language into another language and compiles|TypeScript compiled into JavaScript|
|Type Erasure|[Type Erasure](https://www.youtube.com/watch?v=lxcywrPzCGI)<br>- used at compile time to check the types, not during runtime||
|Type Inference|The type is known, so doesn't have to be declard|- Type lambda = (String s) -> s.length();<br>- Instead Type lambda = (s) -> s.length();|
|Typed vs Untyped (loose) Language|JavaScript allows variables to be of any type, e.g. var, where Typed is specifying the type||
|Typed Form (Template Driven)|Angular FormsModule|asynchronous|
|URI - URL - URN|Uniform Resource Identifier - Locator - Name|http://cv64.us/bobby.html#posts - http://cv64.us/bobby.html - bobby.html#posts|
|URI - URL - URN|![uniform resource image](uniformResource.drawio.png)||
|Varargs / Variadic Function|void printNumbers(int... numbers) { for (int num : numbers) { System.out.println(num); }}|printNumbers(1,2,3,4,5);|

## Product Terms

- [JPA / Hibernate](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/jpaHibernate/jpaHibernate.md) 
- [Patterns](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/terminology/patterns/patterns.md)

## [Storage](https://i.stack.imgur.com/sMNoo.png)

|Term|Description|Reference|
|--|--|--|
|Local Storage|Not Session Based, data is persists even when browser is closed and reopened, Client Side Reading Only|localStorage.setItem('key', 'value');<br>localStorage.getItem('key');<br>localStorage.removeItem(name);<br>localStorage.clear();//Delete all
|Session Storage|Session Based available only during the session, works per window or tab, Client Side Reading Only|sessionStorage.setItem('key', 'value');<br>var data = sessionStorage.getItem('key');|
|Cookie|Expiry depends on the setting and working per window or tab, Server and Client Side Reading||
