# Kafka

## What is Kafka

- Kafka manages and processes events - Event Driven Architecture - Realtime Event Streaming Platform - Distributive Messaging System
     - stream - unbounded, continuose real-time flow of records
- Producer - application written to get data into the Kafka Cluster (KC) and an acknowledgement is sent back from the KC to the Producer
     - writes to a Topic, which is written to a Partition that is stored on separate Brokers
- Consumer - application written, polls the KC, receives data and then provides results, e.g. Reports, Dashboards, Processes, etc.
- Broker 
     - each KC contains Brokers, Brokers have their own local storage, are networked to each other
     - provide services / data to (Consumers) / from (Producers)
     - contain replication of data and the one of the Replicas is called the Leader, the remaining Replicas are called Follower
- Topic - collection of related messages or events
- Partition - topics broken up and allocated to different Brokers, contain 1 to many log files
- Segments -individual files on local storage on a Broker
- Log - immutable entry written at the end
- Stateless Processing - filters, maps, etc.
- Stateful Processing - joins, aggregations, etc.

## Decoupling Producers and Consumers

- Producers and Consumers are not aware of each other

## Data Elements / Data Records

- headers - meta data
- key - can be any type
- value
- timestamp - creation or time of event

# References

- [Apache Kafka Fundamentals](https://www.youtube.com/watch?v=-DyWhcX3Dpc&list=PLa7VYi0yPIH2PelhRHoFR5iQgflg-y6JA)
- [Apache Kafka Fundamentals](https://www.youtube.com/watch?v=Z3JKCLG3VP4)
