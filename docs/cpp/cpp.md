# ![](../../docs/icons/cv64us50x50.png) c++ / cpp - C Plus Plus

## Francis Sandy Hill, University Massachusetts, Amherst
- I was fortunate to have two Graphics courses with Dr. Hill who was not only fascinating and knowledgeable, he was also an excellent Professor.  He was one of my favorite teachers and I learned a lot from him.  Dr. Hill was a specialist in Graphics.  We learned a lot from him, anything from basics to Ray Tracers.

Below is a ZIPped up collection of some of my submissions to Dr. Hill
- [ECE 660 / 661](https://gitlab.com/bobby.estey/cpp/-/blob/master/cpp-ece661.zip)

## pointers - [hobgoblin of C/C++](https://www.oreilly.com/library/view/object-oriented-programming-in/0672323087/ch10.html)

## installation

### cygwin installation (gcc toolchain and make)
 - navigate to [https://www.cygwin.com/](https://www.cygwin.com/)
 - locate setup-x86_64.exe, press the Link.  This will start up the cygwin installation UI
 - the UI is rendered, open up the All widget
 - select Devel -> gcc-g++ -> change Skip to the latest version
 - select Devel -> make -> change Skip to the latest version
 - windows type env - brings up the environments UI
 - edit System variables -> Path
 - press New and enter C:\cygwin64\bin
 - press OK, Press OK, Press OK

### This video illustrates how to setup up C or C++ in the eclipse IDE:
 - [eclipse installation](https://www.youtube.com/watch?v=O0oExvj5CE4)
 - install eclipse
 - start up eclipse
 - select Help -> Select Install New Software
 - select --All Available Sites--
 - select Programming Languages
 - select C/C++ Autotools support
 - select C/C++ Development Tools
 - select C/C++ Development Tools SDK
 - press Next >
 - a list of what was selected will display, press Next >
 - press Finish and software will be installed into eclipse
 - restart eclipse dialog appears, press Restart Now, eclipse restarts
 - go to the Workbench
 
### C Installation
  - select File -> New -> Other -> C/C++ -> C Project
  - press Next >
  - select Toolchains:  Linux GCC
  - select Hello World ANSI C Project
  - C Project:  Enter Project name:  c  Press Next >
  - Basic Settings:  Press Next >
  - Select Configurations:  Select Debug and not Release.  Press Finish
  - Open Associated Perspective?  Select Open Perspective
  
### C++ Installation
 - select File -> New -> Other -> C/C++ -> C++ Project -> C++ Managed Build
 - fill out Project fields - project name, suggest:  cpp and Hello World
 - make sure Toolchains is Linux GCC or Cygwin GCC
 - press Next > and Finish
 - select Open Perspective for C/C++
 - C/C++ Perspective appears
 - select the project -> New -> Class and press Finish
 
### Program Execution
 - select the project, e.g. cpp -> Project -> Build All (Ctrl-b) or Build Project
 - right click the project, e.g. cpp -> Run As -> Local C/C++ Application
 - the Console displays the program execution

### These images show the files configured for this installation
 - [installation 1 image](./eclipseCPPInstall1.png)
 - [installation 2 image](./eclipseCPPInstall2.png)
 
### C / Client for Uniform Resource Locator (CURL) / REST Example

- [C CURL Program making a REST call](https://gitlab.com/bobby.estey/cpp/-/blob/master/src/c/cRESTful.c)
- Execution Process
     - Start Web Service, e.g. SprintBootRestfulWebServicesApplication
     - Start Postman, test end point, e.g. GET http://localhost:6464/users
     - Execute cRESTful program
- [C REST Example Recording](cCurlRestExample.mp4)
- [C REST Example Log](cCurlRestExample.log.txt)
 
### Commands

```
#define DEBUG

#ifdef DEBUG
...
#endif
```

### Resources

- [C debugger and visualizer](https://pythontutor.com/c.html#mode=edit)

### Errors

|Error|Solution|
|--|--|
|Fatal Error:  Unexpected end of line (makefile)|Remove spaces and replace with tabs|

 