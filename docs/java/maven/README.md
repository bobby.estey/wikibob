# Maven - Java Dependency Management Tool

- The Group ID and Artifact ID are used to uniquely identify a project in Maven, similar to how a package name and class name are used in Java
- Snapshot versions are under active development, and their features and stability are not guaranteed, making them unsuitable for learning
