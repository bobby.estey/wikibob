# Jakarta Enterprise Edition (JEE)

# History

- JDK - (1.0 - 1.1) - Enterprise capabilities were included in the standard JDK
- J2EE - (1.2 - 1.4) - Enterprise capabilities were separated from the JDK into a separate API, e.g. Servlets, Java Server Pages (JSP), etc.
- JEE - (5 - 8) - only a rebranding, just dropped the "2" since we are now working with Java versions 5 through 8
- Jakarta EE (9+) - Oracle gave Java EE rights to the Eclipse Foundation
     - Specifications
          - Jakarta Server Pages (JSP)
          - Jakarta Standard Tag Library (JSTL)
          - Jakarta Enterprise Beans (EJB)
          - Jakarta RESTful Web Services (JAX-RS)
          - Jakarta Bean Validation
          - Jakarta Contexts and Dependency Injection (CDI)
               - Spring5 / JavaEE
               - Spring6 / SpringBoot3+ / JakartaEE
          - Jakarta Persistence (JPA)
     - Spring Support
          - jakarta instead of javax packages

# Why JEE

- JEE Provides
     - Distributed Computing
     - Web Services

- Why JEE
     - Powerful API Support
     - Reduce Development Time
     - Reduce Application Complexity
     - Improve Application Performance
     
 - Java EE Specification
      - Enterprise Specification 
           - Context and Dependency Injection - software container developed to enable the injection of dependencies in Swing
           - Java Enterprise Java Bean - APIs that an object container executes to provide transactions, remote procedure calls and currency control
           - Java Persistence API - specifications of object-relational mapping between relational database tables and Java classes
           - Java Transaction API - interfaces and annotations to establish interaction between transaction support offered by Java EE
      - Web Specification
           - Java Servlet - manages HTTP requests either synchronous or asynchronous
           - Web Socket - communication protocol providing a set of APIs to facilitate WebSocket connections
           - Java Server Faces - services that build GUIs from server components
           - Unified Expression Language - facilitates web application development
      - Web Service Specification
           - Representational State Transfer (RESTful) Web Service - provides services having RESTful schema
           - JavaScript Object Notation (JSON) Processing - manages the information provided in JSON format
           - JSON Binding - binding or parsing a JSON file into Java Classes
           - eXtensible Markup Language (XML) Binding - binding of XML into Java objects
      - Other Specification
           - Validation - interfaces and annotations for declarative validation support offered by Bean Validation API
           - Batch Applications - execute background tasks, e.g. large volume of data executing periodically
           - Java EE Connectors - technological solution connecting Java servers to Enterprise Information System

# Java SE vs Java EE

|Java SE|Java EE|
|--|--|
|- basic functionality, e.g. defining Classes and objects|- focus on high end corporate applications|
|- Java SE Standard Specification|- Advanced Specification to support Web Applications and Servlets|
|- Class libraries and deployment environments|- Structured application with clients, business and enterprise layers|
|- Desktop and Mobile Application Development|- Web Development|
|- No Authentication|- Authentication|   
           
