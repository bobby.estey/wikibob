# Object Oriented Overview - Robert Estey - 07 April 1999

# References

- [Code](https://gitlab.com/bobby.estey/wikibob/-/tree/master/src/main/java/fundamentals/ooo)
- [Original Slides](./images/original)
- [Homework](homework.md)

#### Introduction

- This is a presentation I gave at the Phoenix Java Users Group in 1999, originally phxorg.org.  This presentation is very old, however, the concepts are still the same today

- A personal note:
     - If I want to learn something, I document and write up what I have learned and then share what I have learned
     - I learn better by teaching
     - I have a saying, if you think you understand something, then teach that something and you will quickly realize, there are a lot of questions you didn't think about

- My previous development experience is from the Structured Programming, for example:  Fortran and C.  I wrote a little C++, however, everyone said my C++ really looked like C.  I did not understand Object Oriented Programming, the advantages, the techniques or concepts

- I had an instructor named Dr. Gary Berg who wrote up the secrets on Comprehension, here are a few, "People remember..."
     - 16% of what they read
     - 20% of what they see
     - Eventually 90% of what they read, see, told, respond to and do directed functions
     - I will come back to comprehension in a minute

#### "Anyone can Program, can they Program Object Oriented" (Eric Richardson, 1999)

- Eric Richardson was my Mentor for my Master Thesis.  Eric truly understood Object Oriented Programming (OOP) and his code was always a lot smaller than everyone else's because Eric took advantage of all the Object Oriented Principals that were available.  If I could write a program in 100 lines, Eric could do the same functionality in 10 lines.  That much better.  Eric worked smarter and not harder.  The code was also more maintainable and comprehensible

#### ![Object Oriented Technology](https://img.thriftbooks.com/api/images/l/8e785f9145f76547b135a8c23f93819ce9071248.jpg)

- Eric gave me a 1st edition of [David Taylor's Book, Object Oriented Technology, A Manager's Guide, 1st edition](https://www.thriftbooks.com/w/object-oriented-technology-a-managers-guide_david-a-taylor/1260047/item/46101508/?utm_source=google&utm_medium=cpc&utm_campaign=pmax_high_vol_frontlist_under_%2410&utm_adgroup=&utm_term=&utm_content=&gclid=EAIaIQobChMIr_fv69j1_wIVDwetBh1ykQDOEAQYASABEgJ2DPD_BwE#idiq=46101508&edition=2351103) was less than 100 pages and best part, mostly PICTURES.  Going back to Dr Berg, people respond well to reading and seeing.  The Internet is full of excellent videos that explain technologies today

#### Background

- There is the software crisis, corporations are drowning in data, meaning, they have all the data, however, corporations didn't need data, they needed information.  Many people mix these two terms up as they are the same, they are not.  Data is raw, where as information is processed data with an answer to the question

- We have the same issues today with software being delivered late or over budget.  This will continue until companies continue to invest in the newer technologies with well architected requirements

- Structured Programming was simply functional decomposition.  Then CASE tools made their entrance to help speed up development and later the 4th Generation Languages.  Simple programs to well understood problems, but what about the difficult problems?

#### Data Within Programs

![Slide 6](./images/new/06new.png)

- First issue - Data within Programs.  Sharing data is a violation of modular programming, which requires that modules be as independent as possible.  This never happened.  Data was open to all.  The diagram below shows that Module 1 can go directly to Module 2 or Module 3's data and modify their data.

#### Information Hiding

![Slide 7](./images/new/07new.png)

- Information Hiding - one Object Oriented idea, information hiding.  Modules are restricted access and have to access the data through the owners data, for example.  If Module 1 wants Module 2's data, Module 2 becomes the proxy and authority to that data.

#### Models

- [Hierarchical Model](https://en.wikipedia.org/wiki/Hierarchical_database_model)
- [Network Model](https://en.wikipedia.org/wiki/Network_model)
- [Relational Model](https://en.wikipedia.org/wiki/Relational_model)
- [Object Model](https://en.wikipedia.org/wiki/Object_model)
     - There are the various data modules, we are going to concentrate on the Object Module

#### Object Model

- What is the Object Model?  Surprisingly, the Object Model is very old
- [Simula](https://en.wikipedia.org/wiki/Simula) - 1960's - First Object Oriented Language
- Focuses on data in applications instead of methods

![Slide 10](./images/new/10new.png)

- Definitions - there are several parts to the Object Model and 4 definitions that must be understood before continuing with an Object Oriented Programming Language (OOPL).  This presentation will cover all 4 definitions
     - Class is a collection / encapsulation of attributes and methods.  Think of a Class as a TYPE of thing.  A Lamp, Vehicle
     - Objects are an instance of a Class, a kitchen lamp, bedroom lamp.  Lamp is the type (a Class) and an object is an instance of a Lamp       - Another example, there are Vehicles, which are types or classes and objects of Vehicles, esters vehicle, bobbys vehicle
     - Methods are functions that operate on the attributes and variables
     - Attributes are state of something, a red light, a white light, a light that is on or off

#### Coding Conventions

![Slide 11](./images/new/11new.png)

- Software (computing, databases, etc.) have coding conventions.  Why are coding conventions so important, here's the bottom line.  Everyone is on the same page, THAT'S ALL.  If everyone has the same rules and practices, then code is maintainable and this is why previous languages, the rule was everyone for themselves.
- [Java Coding conventions](https://www.oracle.com/java/technologies/javase/codeconventions-contents.html)

#### Class Anatomy

![Slide 12](./images/new/12new.png)


#### Encapsulation

![Slide 13](./images/new/13new.png)

#### Super / Sub Classes

![Slide 14](./images/new/14new.png)

#### Objects

![Slide 15](./images/new/15new.png)

#### Constructors

![Slide 16](./images/new/16new.png)

#### Initializing Objects

![Slide 17](./images/new/17new.png)

#### Initializing Objects

![Slide 18](./images/new/18new.png)

#### Truck Class - Coding Example

![Slide 19](./images/new/19new.png)

#### Example Class - Coding Example

![Slide 20](./images/new/20new.png)

#### Example Class - Coding Example

![Slide 21](./images/new/21new.png)

#### Abstract Types

![Slide 22](./images/new/22new.png)

#### Abstract Classes

![Slide 23](./images/new/23new.png)

#### Overriding / Overloading

![Slide 24](./images/new/24new.png)

#### Interface

![Slide 25](./images/new/25new.png)

#### Object Oriented Overview Exam - Test

![Slide 26](./images/new/26new.png)

#### Object Oriented Overview Exam - Results

![Slide 27](./images/new/27new.png)
