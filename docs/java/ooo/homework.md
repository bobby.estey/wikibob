# Program Assignment

#### Basics

- What Year was the Java Programming Language available to the public?
      
- Define the following:
     - Information Hiding
     - Class
     - object
     - method
     - attribute
     - Constructor()
     - SuperClass
     - SubClass
     - Hierarchy
     - private, public
     - this
          
#### Coding Conventions

- Why are Coding Conventions so important?
- What are the Coding Conventions for the following:
     - Class, Interfaces, Constructors(), Adapters
     - objects, methods(), attributes

#### Assignment

- Create Class Ship with the following:
     - attributes:
          - String country
          - String classification
          - int hullNumber
          - int displacement
          - double length
          - double width
          
     - methods
          - create setters and getters for each of the attributes
          - create a toString() method that prints out each of the attributes returning nothing
            
     - Constructors
          - create a no argument Constructor that sets the values as follows:
               - all Strings empty
               - all int to 0
               - all double to 0
           - create a 6 argument Constructor, taking in all 6 attributes and setting the object with the 6 attributes
           - EXTRA CREDIT:  Have the no argument Constructor pass values to the 6 argument Constructor

- Create Class Driver with the following:
          - create a main method
          - The Driver Class is to create at least 3 instances of the Ship Class
               - first instance named noName, calling the no argument Constructor()
               - second instance named cv64, calling the 6 argument Constructor, pass in the arguments (“US”, “cv”, 64, 88000, 1072, 252)
               - third instance named yourShip, calling the 6 argument Constructor, pass in the arguments you wish for your own ship.
               - Have each of the instances print out their attributes
