# ![](../../docs/icons/cv64us50x50.png) Java 

- [Java General Examples](https://gitlab.com/bobby.estey/wikibob/-/tree/master/src)
- [Maven](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/java/maven/README.md)
- [Java8](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/java/java8/README.md)
- [JavaFX](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/java/javaFx/README.md)
- [Javalib](javalib.md)

# Some History

- [Java Song](https://www.youtube.com/watch?v=fqwIpH6phJs)
- [Java Instrumental](https://en.wikipedia.org/wiki/Java_(instrumental))
- [Java Indonesian Island](https://en.wikipedia.org/wiki/Java)
- [Java Programming Language](https://en.wikipedia.org/wiki/Java_(programming_language))

# Acronyms / Glossary

|Term|Description|Reference|
|--|--|--|
|::|Method Reference|see Java8|
|[Anonymous Class](https://docs.oracle.com/javase/tutorial/java/javaOO/anonymousclasses.html)|Declare and instantiate a class, simultaneously.  Local class without a name||
|Boxed|primitive to Class|int -> Integer|
|Constructor Chaining|subclass calling this() itself or super() calling a superclass||
|Copy Constructor|clone() method||
|[equals hashcode](https://www.geeksforgeeks.org/equals-hashcode-methods-java/)|||
|[@FunctionalInterface](https://www.geeksforgeeks.org/functional-interfaces-java/)|one abstract method||
|[Inner Nested Static Class](https://www.geeksforgeeks.org/static-class-in-java/)|- Nested - class within class<br>- Inner (static) - Static Class<br>- 
Inner (non-static) - Inner Class||
|[Lambda](https://www.javatpoint.com/java-lambda-expressions)|One method interface using expressions (implementation of a functional interface)||
|[Polymorphism](https://www.w3schools.com/java/java_polymorphism.asp)|Classes that are related to each other by inheritance|Dog and Cat are both Animals|
|[Private Constructors](https://www.geeksforgeeks.org/private-constructors-and-singleton-classes-in-java/?ref=lbp)|Internal Constructor Chaining and Singleton Pattern||
|Serialization|Translating a Data Structure into a format that can be transmitted, e.g. bits over the network||
|[Static Block](https://www.geeksforgeeks.org/static-blocks-in-java/?ref=lbp)|Code executed once when the code is loaded into memory, e.g. static { code }||
|Unboxed|Class to primitive|Integer -> int|
|var|- Type Inference - Java var - allows the Compiler to infer the type of the variable based on the value assigned|- var string = "cv64"; // compiler declares as string<br>- var number = 64; // compiler declares as int|
|Variable Scope|Access / Data Modifier|private, protected, public, package|
|Wrapper Class|Use primitive types as Objects, e.g. instead of int -> Integer, double -> Double||

# Java Examples

- record GreetResponse(String greet) {} // record returns JSON

```
@GetMapping("/greet")
public GreetResponse greet() {
  return new GreetResponse(
    greet: "Hello";
    List.of("Java", "GoLang", "JavaScript");
    new Person("Bobby");
  );
  
  return response;
}

record Person(String name) {}

record GreetResponse(String greet, List<String> favoriteLanguage, Person person) {}
```
