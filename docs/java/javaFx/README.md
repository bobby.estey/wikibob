# [Java FX](https://openjfx.io/index.html)

## Terminology

- launch - calls the JavaFX Application Class's start method - initializing JavaFX
```
  public static void main(String[] args) {
    launch(args);
  }
  
  @Override
  public void start(Stage stage) throws Exception {
    ...
  }
```
- Stage -> Frame
- Scene -> Panel

## Installation
[JavaFX SDK & jmods](https://gluonhq.com/products/javafx/)

- Download the zip files (SDK & jmods) place into the appropriate directory, see examples below
- https://openjfx.io/index.html

#### vm arguments to be place in eclipse Arguments

Linux Example:

- --module-path <path to zip files> --add-modules javafx.controls,javafx.fxml
- e.g.  --module-path /opt/javafx/javafx-sdk-18.0.1/lib --add-modules javafx.controls,javafx.fxml
- e.g.  --module-path /git/javafx/javafx-sdk-18.0.1/lib --add-modules javafx.controls,javafx.fxml

Macintosh Example:

- --module-path <path to zip files> --add-modules javafx.controls,javafx.fxml
- e.g.  --module-path "/git/javafx/javafx-sdk-18.0.1/lib" --add-modules javafx.controls,javafx.fxml

Windoze Example:

- --module-path <path to zip files> --add-modules javafx.controls,javafx.fxml
- e.g.  --module-path "C:\git\javafx\javafx-sdk-18.0.1\lib" --add-modules javafx.controls,javafx.fxml
 
![JavaFX 1](javafx1.png)
![JavaFX 2](javafx2.png)
![JavaFX 3](javafx3.png)

#### eclipse Run Configurations

![eclipse Run Configurations](eclipseRunConfigurations.png)

#### eclipse VM Arguments

![eclipse VM Arguments](eclipseVMarguments.png)

#### recordings

- [JavaFX Setup](javafxSetup.mp4)
- [JavaFX Demonstration](javafxConstellationDemo.mp4)
