# Function Interface Consumer<T> { void accept(T t); }

- predefined Functional Interface having one abstract method
- Only consume never supply or return anything
- For example, consumes object and saves in database

```
import java.util.function.Consumer;
public class ConsumerExample {
  public static void main(String[] args) {

    ConsumerExample consumerExample = new ConsumerExample();
    consumerExample.squareInt(5);
    
    Consumer<Integer> squareMe = i -> System.out.println("Input and Returning nothing: " + i * i);
    squareMe.accept(5);
    
    Consumer<Integer> doubleMe = i -> System.out.println("Input and Returning nothing: " + i * 2);
    squareMe.accept(5);
    
    squareMe.andThen(doubleMe).accept(5);
    
    public void squareInt(int i) {
      int squared = i * i;
      System.out.println("Squared: " + squared);
    }
  }  
}
```

# Consumer Chaining

consumer1.andThen(consumer2).accept(input);


