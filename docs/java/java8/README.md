# Java 8

[Part 1](https://www.youtube.com/watch?v=oUdENE7ljjw)

- Java 8 - Concisness (Shortness / Succinct)
     - Compact Code (Class boiler plate code)
     - Readable / Reusable Code
     - Testable Code
     - Parallel Operations
     
- Functional Programming (FP) - Lambda Expressions
- Object Oriented Programming (OOP) + FP - consise code base

# Lambda Expression - anonymous functions
[lambdaExpression](lambdaExpression.md)

# Functional Interfaces
[functionalInterface](functionalInterface.md)

# Method References
[methodReferences](methodReferences.md)

[Part 2](https://www.youtube.com/watch?v=bxtNFqldAuk)

# Default Methods in the Interface
[defaultMethods](defaultMethods.md)

# Static Methods
[staticMethods](staticMethods.md)

[Part 3](https://www.youtube.com/watch?v=3b8I4rzqF0c)

# Predicates
[Predicate<T> { boolean test(T t); }](predicates.md)

# Functions
[Function<T, R> { R apply(T t); }](functions.md)

[Part 4](https://www.youtube.com/watch?v=7-ALyd6OAaQ)

# Consumer Functional Interface
[Consumer<T> { void accept(T t); }](consumer.md)

# Supplier Functional Interface
[Supplier<R> { R get(); }](supplier.md)

# BiConsumer, BiFunction, BiPredicate, no BiSupplier
- Allows two input elements
- Supplier has no input so there is no BiSupplier
[Bi Examples](biExamples.md)

[Part 5](https://www.youtube.com/watch?v=J-RTlluJv7E)
[Part 6](https://youtu.be/cqQN8g4oR98?t=368)

# Stream API
[Streams](streams.md)

[Part 7](https://www.youtube.com/watch?v=1Cs7X-JYkTY)

# Stream Operations / Operators
[Streams2](streams2.md)

[Part 8](https://www.youtube.com/watch?v=TM6TZvVoAko)
[Map vs Flat Map](mapFlatMap.md)

# Optional Class
[Optional](optional.md)

# Date API

# Nashorn, JavaScript Engine

