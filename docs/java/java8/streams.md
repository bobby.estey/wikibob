# stream 

- create, configure, process a collection
- Not java.io.Stream (related to files) stream (related to character / binary data)
- Collection is a single entity to a group of data
- Process bulk objects of collections
- Approach to operate on collections
- Iterator Class allowing processing collections of objects in a functional manner
- Fetches all objects from a collection

## filter - removes elements from a stream collection

## map - create new object based on a function, maintaining the same number of elements

## processing

- count - returns the size of the stream or filter / map
- sort - sorts the stream or- filter / map
     - stream.sorted();  // natural order
     - stream.sorted((i1, i2) -> i2.compareTo(i1));  // using comparator - descending order
- min / max - return the min or max value in stream
     - stream.min((i1, i2) -> i1.compareTo(i2)).get();
     - stream.max((i1, i2) -> i1.compareTo(i2)).get();
- forEach - returns nothing, takes the lambda expression as an argument and adds to each value
- toArray - copy the stream elements to an array
     - ```Object[] myArray = stream.toArray();```
- of() - convert ANY GROUP OF VALUES to a stream
     - ```stream.of(1,11,111,1111,11111).forEach(x -> System.out.println(x));```
     - ```String[] name = "Alpha", "Bravo", Charlie", "Delta", "Echo"};```
     - ```stream.of(name).filter(x -> x.length() == 5).forEach(x -> System.out.println(x));```


```
public class StreamExample {
  public static void main(String[] args) {
  
    List<Integer> integerList = new ArrayList<>();
    integerList.add(2);
    integerList.add(15);
    integerList.add(25);
    
    List<Integer> streamList = new ArrayList<>();
    
    // creating a stream - .stream()
    // filter - keeps objects that match the criteria, e.g. anything that is >= 15
    // collect - creates a new list which is being returned to the streamList object
    streamList = integerList.stream().filter(x -> x >= 15).collect(Collectors.toList());
    streamList.stream().forEach(x -> System.out.println(x));
    
    // Create a stream object - Stream stream
    // Configuration stream - .filter();
    // Process stream - forEach, Collect, Count, Sorted, Min, Max, toArray, of()
    Stream stream = integerList.stream().filter(x -> i % 2 == 0);
    stream.forEach(x -> System.out.println(x));
    
    // or on one line, an example of chaining
    integerList.stream().filter(x -> i % 2 == 0).forEach(x -> System.out.println(x));
    
    // map - process the collection and create a new collection
    // Create a stream object - Stream stream
    // Configuration stream - .map();
    // Process stream - forEach, Collect, Count, Sorted, Min, Max, toArray, of()
    Stream stream = integerList.stream().map(x -> i * i);
    stream.forEach(x -> System.out.println(x));
    
    // or on one line, an example of chaining
    integerList.stream().map(x -> i * i).forEach(x -> System.out.println(x));
  }
}
```
