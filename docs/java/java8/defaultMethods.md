# Default Methods

- Modifying methods to an Interface without affecting Implementing Classes
- Overriding Default Methods, define the method in the Implementing Class
- default is not the access modifier or the switch case default
- default is a keyword for a method for default implementation in the Interface
- default keyword is public always in a Class
- TO REPEAT:  default keyword is only used in the interface not the class, so change class to public
- default implementation of ANY java.lang.Object Class method() cannot be used
- java.lang.Object class methods cannot be used as default methods
- all implemented classes have access to all java.lang.Object class methods

```
public interface InterfaceDemo {
  default void method1() {
    System.out.print("Default Method Example");
  }
}
```

## Duplicate Interface methods

- two or more Interfaces with the same method signatures can cause what is called the DIAMOND problem, to fix this, pick the Interface, e.g.
     - use Interface.super.method

```
public class DiamondProblemClass implements DiamondProblemInterface1, DiamondProblemInterface2 {
  @Override
  public void method1() {
    DiamondProblemInterface1.super.method1();
  }
}
```
