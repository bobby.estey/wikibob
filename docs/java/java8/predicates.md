# Function Interface Predicate<T> { boolean test(T t); }

- predefined Functional Interface having one abstract method
- input type is anything which always returns a boolean
- code reusability
- same conditions utilized multiple times

```
import java.util.function.Predicate;

public class PredicateFunctionDemo {

  public static void main(String[] args) {
  
    // s - string being passed in, s.length() get the length of s, compare and return boolean
    Predicate<String> checkLength = s -> s.length() >= 5;
    System.out.println("String Length > 5: " + checkLength.test("Some String"));
  }
}
```

## Predicate Joining

- three ways to join:  and, or, negate

```
import java.util.function.Predicate;

public class PredicateJoining {
  public static void main(String[] args) {
    
    Predicate<String> checkLength = s -> s.length() >= 5;
    System.out.println(checkLength.test("String Length greaterEqual to 5");
    
    // joining Predicates
    Predicate<String> checkEvenLength = s -> s.length() %2  == 0);
    System.out.println(checkEvenLength.test("String Length greaterEqual to 5");
    
    // joining Predicates with AND
    System.out.println(checkLength.and(checkEvenLength).test("String Length greaterEqual to 5");
    
    // joining Predicates with OR
    System.out.println(checkLength.or(checkEvenLength).test("String Length greaterEqual to 5");
    
    // joining Predicates with NEGATE
    System.out.println(checkLength.negate(checkEvenLength).test("String Length greaterEqual to 5");
  }
}
```
