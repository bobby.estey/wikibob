# [Method References (::)](https://youtu.be/oUdENE7ljjw?t=932)

- Replacement of Lambda Expressions
- Used for Code Reusability
- Use method references if there are abstract methods in the Functional Interface

```
// Previously before Method Reference, here's the Lambda call
FunctionalInterfaceExample functionalInterfaceExample = () -> System.out.println("methodWithinClass");
functionalInterfaceExample.singleAbstractMethod();

// HERE IS THE INTERFACE
@FunctionalInterface
public interface FunctionalInterfaceExample {
  void methodWithinClass();
}

// AnotherClass::methodWithinClass; - method reference
FunctionalInterfaceExample functionalInterfaceExample = AnotherClass::methodWithinClass;
functionalInterfaceExample.singleAbstractMethod();

class AnotherClass {
  public static void methodWithinClass() {
    System.out.println("methodWithinClass");
  }
}
```
