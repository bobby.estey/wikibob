# [Map vs FlatMap](https://www.youtube.com/watch?v=TM6TZvVoAko)

## Map Example

- function passed to map returns a single value
- used for transformation only

```List<Integer> ids = employeesList.stream().map(emp -> emp.getId()).collect(Collectors.toList());```

## FlatMap Example

- function passed to flatmap returns a Stream of values
- combination of Map and Flat
- apply Map first then flatten
- used for transformation and flattening
- returns a string of unique values
     - instead of [[4, 1, 2, 2], [4, 3, 3, 3], [4, 4, 4, 4]] only [4, 1, 2, 3] are returned

```
Set<String> set = list.stream().flatMap(x -> x.getMethod().stream()).collect(Collectors.toSet());
System.out.println(set);
```
