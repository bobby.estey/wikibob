# Static Methods

- Memory and Performance increase because
  - no Constructors
  - no static blocks
  - No need to create Class or object

- Call the Interface itself to access the method
- Static methods are not available to implementing classes
- Static methods are not default methods

```
public interface InterfaceStaticMethod {
  static void staticMethod() {
    System.out.println("Static Method");
  }
}

public class InterfaceStaticMethodImpl implements InterfaceStaticMethod {
  public static void main(String[] args) {
  
    // previous to JDK8
    InterfaceStaticMethodImpl interfaceStaticMethodImpl = new InterfaceStaticMethodImpl();
    interfaceStaticMethodImpl.staticMethod();
    
    // JDK8
    InterfaceStaticMethod.staticMethod();
  }
}
```  
