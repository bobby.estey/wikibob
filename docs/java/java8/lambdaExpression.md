# Lambda Expression - anonymous functions
- anonymous function, without: name, return type, access modifier, e.g.
- BiConsumer<Integer, Integer> biConsumer = (a, b) -> System.out.println(a + b);
     - (a, b) doesn't require (Integer a, Integer b) because of the generics specified on the left side
     
```
() -> System.out.println("receives no arguments");
(a, b) -> System.out.print(a + b);  // receives 2 arguments and prints the sum

// passing in s as String, compare length GE 5 and return boolean to checkLength
Predicate<String> checkLength = s -> s.length() >= 5;
```