# Optional

- created to deal with null values, NullPointerException (NPE), if a value is present or not
- the past extra code was required to handle NPE

- get() method - returns the value of an object if not null or throws no such element exception
     - e.g. Integer total = repository.getTotal();  // if returned value is null throw no such element exception
     - here's the fix, change to Optional
     
```
     Optional<Integer> total = repository.getTotal();
     if (total.isPresent()) {
     	return new ResponseEntity<>(total.get(), HttpStatus.OK);
     } else {
       return new ResponseEntity<>("Not Present", HttpStatus.NOT_FOUND);
     }
```

- isPresent(Consumer) - is the value present

```
object.ifPresent( o -> System.out.println("object is present " + o));
object.ifPresentOrElse(o -> System.out.println("object is present " + o), 
                      () -> System.out.println("object is not present)); 
```

- isEmpty - is the value empty
- of - argument cannot be null or NPE is thrown
- ofNullable - returns empty if value is null, not NPE

- Container / optional, container object, item or empty
- Optional<Integer> numExist = Optional.of(5);
- Optional<Integer> numNotExist = Optional.empty();
- Optional<String> name = Optional.ofNullable(employee.get().getName()); // get() returns name or empty, not NPE
