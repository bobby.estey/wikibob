# Streams2

## Parallel Stream

- Uses multiple processor cores, so code can execute in parallel on separate cores and result outcome combined.
  
# Intermediate Operation

- Operations that return a stream as a result
     - filter(), map(), distinct(), sorted(), limit(), skip(), peek()
     - peek() - helps debugging
     - reduce() - combine elements into a single value
          - ```System.out.println(list.stream().reduce((a,b) -> a+b).get());```
     - Lazily Loading - need a Terminal Operation to display results
     - operations can be chained
     - multiple operations are permitted
     - do not produce end result
     
- peek() 
      - used for debugging within a pipeline
      - stream.peek(System.out::println)
     
# Terminal Operation     

- Operations that return non-stream values (objects, primitives or void)
     - forEach(), toArray(), reduce(), collect(), min(), max(), count(), anyMatch(), allMatch(), noneMatch(), findFirst(), findAny()
     - Eagerly Loaded
     - operations cannot be chained
     - only one operation is permitted
     - produce end result
     - placed at the end
     
- reduce()
     - reduce number of elements in stream and produce a value
     - first argument (a) is return value of previous value, second argument (b) is the current element
     - List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
     - System.out.println(list.stream().reduce((a, b) -> a + b).get());
     - prints 15
      
     
