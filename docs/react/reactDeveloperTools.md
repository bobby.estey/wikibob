# React Developer Tools (RDT)

# Overview

- Inspects React Component Hierarchies
- Components Tab
     - Root React Components
     - Sub Components that were rendered
- Props, State are visible and editable

# Installation

[RDT Site](https://react.dev/learn/react-developer-tools)

# Implementation

- Open up Google Development Tools
- Select Components
