# Redux - Managing State across the Application

- [Redux in 100 seconds](https://www.youtube.com/watch?v=_shA5Xwe8_4)

# Overview

- Single Source reference to all the States (Applications Data) of JavaScript Applications
     - single source of truth - the application state is a single JS object (Store) that is READONLY
     - state does not change but a new state is created
     - state is centralized
- A FLUX pattern (Action -> Dispatcher -> Store -> View) / library, implementing complex state management requirements at scale
- A single IMMUTABLE object (Store) that stores all of the applications State, e.g. Client Side Database
- To change the State, an Event is fired (button click), Dispatching an Action (Named Event and Payload)
	- const action = { name: 'buttonPressed', payload: { text: "Helloworld" } }
- A reducer (pure function) is called creating a new object, returning the state and action to the Store
     - NOTE:  Store is IMMUTABLE
     - pure function - consistent output for input, consistency within the state management system
     - function reducer(state, action) { return { ... state, message:  action.payload } }
- Redux provides access to data without prop drilling or context
     
# Installation / Configuration

- npx create-react-app cv64App --template typescript
- npm install @reduxjs/toolkit react-redux
- npm i -D @types/react-redux    this allows the type and action to be recognized from REDUX
- [Install Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)
    - npm install @reduxjs/toolkit react-redux

## Providing and Creating the Store

```
// src/store.js - create the store
import { configureStore } from '@reduxjs/toolkit';
import cv64Reducer from './cv64Slice
export const store = configureStore( {
  reducer: {
    cv64Object:  cv64Reducer
  }, 
});

// src/main.js - provide the store
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
)
```

## Creating the Slice - contains a collection of reduce to change the state

```
// src/cv64Slice.js - create a slice - providing data into the store
import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  name: 'nebraska',
  teamColors: ['red', 'white'],
  winLoss: true
}

export const cv64Slice = createSlice({
  initialState,
  sport: 'football',
  
  // MUTATES HERE
  reducers: {
    toggleWinLoss: (state) => {
      state.winLoss = ! state.winLoss
    },
    changeName: (state, action) => {
      state.name = [...state.name, action.payload]
    },
})

export const { toggleWinLoss, changeName } cv64Slice.actions
export default cv64Slice.reducer
```

## Dispatching Actions

```
// src/App.js
function App() {
  const cv64App = useSelector(state => state.cv64);  use selector hook
  const dispatch = useDispatch();  // use dispatcher hook
  
  return (
    <>
      <h1>CV64 Application</h1>
      
      // get the reactive value or slice in the Store with use useSelector hook
      {cv64App.name.map(name => (
        <div key={name}>{topping}</div>
      ))}
      
      // name - action  payload 'arizona'
      <button onClick={() => dispatch(name('arizona'))}>Arizona</button>
    </>
  )
}

export default App
```

# Video

- [React Redux TypeScript Tutorial - Introduction for Beginners](https://www.youtube.com/watch?v=8DH7Ekp2PWI)
     - 17:07 - interesting, I usually have the following syntax:
       ```<Button color="red">```
     - WebStorm always places {} and on this video the format looks different:
     ```<Button color={'red'}>``` 
     - QUESTION:  Should this be the new syntax we should be using?

# Need to Heed
- [SAID SO PERFECTLY](https://youtu.be/8DH7Ekp2PWI?t=3743)

- This page was an attempt to share what I learned about REDUX - again the industry continues the practice of ABSTRACTION / CONVOLUSION and that's where we are headed.  Java, the Greatest Programming Language on the planet and truly the simplest, most sensible and easy language to program.  Why would anyone want to create CONVOLUTED pollution.  None of this is new, all the same technologies from the past, just a dumber approach.  Even the author of the recording, said so perfectly.

- BOTTOM LINE:  I learned so much from this authors code, than anything else.

# Resources

- [JSONPlaceholder](https://jsonplaceholder.typicode.com)
- ES7 React/Redux/GraphQL/React-Native snippets

# ![Redux Data Flow](reduxDataFlow.drawio.png)

- State - Object, primitive, etc. with a value
- View - React Components that can send an action through an interaction, e.g. submit, click, etc.
- Store - Contains the State (Immutable Object) - Client Side Database
- Component - subscribes to the Store and Dispatches an Action
- Action Creators - Dispatch and Action to the Store
``` const action = { name: 'Bobby', payload: { text: "Hello World" } }```
- Reducers - Functions that change the State in accordance with the received action from the Application.
     - Note: State is immutable so a new State is created, not modified, (in other words - not changing but creating a new state)
     ```function reducer(state, action) { return ...state, message: action.payload } }```
- Slice - Component
- Capability to log every user action and playback

# File Structure

- src
     - index.tsx (TypeScript Extension)
     - App.tsx - main start application
          - components
               - store - Contains the State and sends to the Component
               - rootStore.ts (rootStore.js) - creates the store
                    - ```<application>```
                    - models
