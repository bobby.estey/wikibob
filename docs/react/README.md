# React / [Redux](./redux/redux.md) / [Nexus](../nexus/nexus.md)

- index.html 
     - contains root div
- index.js
     - loads the App components
     - src/index.css - styling for entire application
- App.js
     - App.css - style for App component
     - App.test.js - unit test for App components

# State

- Object used to contain data or information about the component
     
## Document Object Model (DOM)

- HTML page is represented by the DOM
- Each element is a node in the DOM
- The DOM needs to be updated to update an Element
- Process can be slow

## React

- Single Page Application (SPA) - only what changed is updated, not the entire page
- Component Based - builds the page with one to many components
- React can also build Native applications, e.g. Android, iOS with React Native
- Virtual DOM - virtual representation of a User Interface (kept in memory)
- React updates Virtual DOM
- React creates Virtual DOM v1 on page load
- React creates Virtual DOM v2 as a result of an action
- React identifies changes between v1 and v2 and synchronizes with the HTML page
- React does not update the DOM directly and more efficient
- Virtual DOM -> (differences & updates) -> DOM -> (events) -> Application (creates) -> Virtual DOM

# Installation

|Command|Description|
|--|--|
|[WebStorm Instructions](./webStorm/README.md)|WebStorm Instructions|
|[excellent tutorial](https://www.jetbrains.com/webstorm/guide/tutorials/react_typescript_tdd/project_setup/)|https://www.jetbrains.com/webstorm/guide/tutorials/react_typescript_tdd/project_setup/|
|npm install -g create-react-app|- Node Package Manager (NPM) - Install React|
|npx create-react-app <b>projectName</b>|- Node Package Executor (NPX) - Creating a new React app in /home/cv64/git/cv64/react/fundamentals.<br>Installs node packages. This might take a couple of minutes<br>Installing react, react-dom, and react-scripts with cra-template...<br>Created git commit.<br>Success! Created fundamentals at /home/cv64/git/cv64/react/fundamentals<br>Inside that directory, you can run several commands:|
|npx create-react-app <br>projectName</b> --template typescript|Create React Typescript|
|React Router DOM|npm i react-router-dom|
|[React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)|Browser Plugin for Debugging React Code|
|React Icons|npm i react-icons|
|[npm start](npmStart.png)|Starts the development server and application|
|npm run build|Bundles the app into static files for production|
|npm test|Starts the test runner|
|npm run eject|Removes this tool and copies build dependencies, configuration files and scripts into the app directory. If you do this, you can’t go back!|
|React Version|npm view react version|
|install react test|npm install --save-dev @testing-library/react react-test-renderer @testing-library/jest-dom babel-jest @babel/preset-env @babel/preset-react enzyme @wojtekmaj/enzyme-adapter-react-17 @types/jest|

|Terminology|Description|
|--|--|
|asynchronous|non-blocking, code continues without waiting for completion or code freezing, callsbacks and promises|
|async|asynchronous - makes the function return a promise|
|await|asynchronous - must be inside an async function, wait for request completion|
|Callback Functions|Child components passes state variable back to parent component|
|[useCallback Hook](https://www.youtube.com/watch?v=IL82CzlaCys)|Return a memorized version of the callback function that only changes if one of the dependencies has changed|
|Class Component|maintains internal state, returns HTML (JSX / TSX), e.g. class Welcome extends React.Component { render() { return <h1>Hello, {props.name}</h1>; } }|
|Component|- JavaScript functions, work in isolation and return HTML, e.g. (App.js) root component, header, footer, navigation, mainContent<br>- There both Class Components and Functional Components (SFC)<br>- SFC now have Hooks<br>- Component names start was an Uppercase letter|
|Controlled Controller|- when React is synchronized with the DOM<br>- this is accomplished with state|
|Declarative|React is a declarative paradigm, tell React what you want and React builds the UI for you, alternative is Imperative|
|Empty Wrapper <>...</>|- React JSX / TSX will not allow multiple top level elements.<br>- JSX / TSX must have one parent element|
|.env|Environment File, e.g. SKIP_PREFLIGHT_CHECK=true|
|Functional Component|- Allows adding state to Functional Components<br>- returns HTML (JSX / TSX), e.g. function welcome(props) { return <h1>Hello, {props.name}</h1>|
|Imperative|Implements algorithms in specific steps, explicitly do all the work|
|import / export|React has modules, self contained units that expose assets to other modules using export and acquiring assets using import|
|Imports|- Default Import -> export default function<br>- Named Import -> export function|
|Interfaces|Have more features which goes against type safety, use for public API|
|JSX|JavaScript Extension or JavaScript XML, XML code for elements and components, transpiles to JS.  JSX uses React.createElement  JSX is not returning only HTML but dynamic JavaScript expressions|
|Hooks|React 16.8 - functions use hooks for states and lifecycle features from function components|
|Module|React Components inside their own file|
|[Properties](https://lucybain.com/blog/2016/react-state-vs-pros)|static variables passed from a parent component to a child component.  Props are immutable, pure functions|
|Render method|Returns JSX / TSX not HTML|
|Rerender|export default React.memo(Component)  Only rerender if properties or state has changed|
|Spread syntax (...)|Destructure an array or object into separate variables|
|Spread opposite|// Structure props coming in, e.g.  const {color, children, handleClick} = props;|
|[State](https://lucybain.com/blog/2016/react-state-vs-pros)|dynamic variable groups initialized by props|
|[State Hook Tutorial](stateHookTutorial.mp4)|Recording explaining state hooks|
|TSX|TypeScript Extension|
|Types|Use for React Props and States, types are more constrained|

# React Libraries - NOT FRAMEWORK

- React - is a JavaScript library, not a framework for developing user interfaces as a Single Page Application (SPA)
     - The React libraries required to execute a React program building User Interfaces (UI)
     - Platform Agnostic - React can be a web, mobile phone or native application that do not use the web DOM library
     - React is Declarative (see Terminology / Description)
     - React makes the UI easier by abstracting the difficult parts
     - React efficiently updates and renders components, called reusable components with their own state
     - DOM updates (expensive operations) are handled gracefully
     - React compiles to a JavaScript bundle that is loaded through the Browser
     - React is part of the MERN stack:
          - MongoDB (NoSQL Database)
          - Express - Backend Framework
          - React - JavaScript Library
          - NodeJS - JavaScript Runtime
     - JSX (JavaScript Extension) / TSX (TypeScript Extension) - dynamic markups, combining both logic and HTML
     - Interactive UIs with a Virtual Document Object Model (DOM), only what changes is redrawn, not the entire page
     - One way data binding, the data is immutable, requiring to recreate state
     
- ReactDOM 
     - Imports ES6+ load libraries and is used only in index.js file and applies only to web applications and websites
     - Renders the React Application to the DOM in the Browser
     - not imported in React Native

- React Native - framework for mobile development
     - React Framework to build native mobile applications which allows React developers to write programs on a mobile client without requiring knowledge of Android or iOS
     - does not import ReactDOM
  
- React Scripts
     - Development Server, Build Tools, Testing Libraries, etc.
     
- ES6+
     - let - declared before use, cannot be redeclared, local scope
     - const - declared before use, cannot be redeclared, local scope, cannot be updated
     - var - declared before use, cannot be redeclared, global scope
     - arrow functions
          - function welcome(props) { return <h1>Hello, {props.name}</h1>
          - const welcome = (props) => { return <h1>Hello, {props.name}</h1> }
     - template literals / strings - backtick (') characters allowing multiple line strings
     - default parameters - initialize parameters with default values, e.g. var = undefined
     - object literals - comma separated list of name-value pairs, e.g. JSON
     - rest operators - (...) allows a function to treat an indefinite number of arguments as an array function sum(...args) { let sum = 0;  for (let arg of args) sum += arg;  return sum;} let x = sum(4, 9, 16, 25, 29, 100, 66, 77);
     - spread operators - const numbersOne = [1, 2, 3]; const numbersTwo = [4, 5, 6]; const numbersCombined = [...numbersOne, ...numbersTwo];
     - destructuring assignment - const vehicles = ['mustang', 'f-150', 'expedition']; const [car, truck, suv] = vehicles;

# React Component (Reusable Code)

![React Component](react.component.drawio.png)

- Class Components
     - same as Functional Components with additional and more complex and interactive components
     - has properties and methods
     - adopting Object Oriented Features
     - maintain private state
     - provide lifecycle events (hooks)
- Custom Components
     - Reducing the quantity of code on a page
- Functional Components
     - Returns JSX Code
     - simple functions
     - absense of 'this' keyword
     
# React Component Example

![React Component Example](react.component.example.drawio.png)

# Fat Arrow Examples

|ES old|ES6+|
|--|--|
|function()|() =>|
|function(x)|x =>|
|function(x)|(x) =>|

# Code

- Hello React Program

```
import React from 'react';
import ReactDOM from 'react-dom';

/**
  The “App” function name is a commonly used starting point for many React applications.  
  The purpose is to display JSX code to the browser. 
  JSX looks like HTML, is a unique Javascript to React. 
  @see https://reactjs.org/docs/introducing-jsx.html
*/
const App = () => {
  return <div>Hello React</div>
}

/**
  Displays the entire application in the web page identified as #root. 
  Open public/index.html file and locate first element <div id=”root” />. 
  The standard name is "root" can be named differently.
*/
ReactDOM.render(<App />, document.querySelector("#root"));
```

# props

|no args|args|
|--|--|
|const App = () => { render() { return (<div></div>) } }|const App = () => { render(props) { return (<div>{this.props.attribute</div>) } }|

# hooks - React 16.8

|Hook|Description|
|--|--|
|createContext|returns an Authorization Context|
|useState|stateful value and function to update|
|useEffect|- tell React that the component needs to do something after render<br>- [] - stops infinite loop<br>- side effects in function components, e.g. HTTP Requests|
|useNavigate|incorporated into Routing|
|useParams|returns object of key/value pairs on the dynamic params from the current URL from the route path|

# events

|section|description|code|
|--|--|--|
|function|event passed in from render / return|handleMessage=(event) => { this.setState({message: event.target.value});}|
|render/return|// onChange passes an event object to handleMessage function|<input type="text" value="{this.state.message} onChange= {this.handleMessage}/><button onClick = {this.addOneClick}>Click Me</button>|
|async|await|

# Errors

## Code

|Error|Solution|Reference|
|--|--|--|
|react-jsx-dev-runtime.development.js:117 Warning: Each child in a list should have a unique "key" prop.|add key id to the element, e.g. key = {album.id} or key = {incrementer++}||
|error enospc system limit for number of file watchers reached watch '/snap/code'|sudo gedit /etc/sysctl.conf<br>add last line the following and save fs.inotify.max_user_watches=524288<br>test with the following:  sudo sysctl -p|https://stackoverflow.com/questions/55763428/react-native-error-enospc-system-limit-for-number-of-file-watchers-reached|
|TS2322: Type '(props: PropsWithChildren<DataGridProperties>) => void' is not assignable to type 'FC<DataGridProperties>'.  Type 'void' is not assignable to type 'ReactElement<any, any> \| null'.|- Calling program must pass parameters, e.g. ```MyComponent dataGridDetailedRecords={dataGridDetailedRecords}/>```<br>- Program being called must have return, e.g.  ```return(<div></div>);```||
|TS2322: IntrinsicAttributes does not exist on type IntrinsicAttributes|Called function property not defined||
|TS2322: Type 'boolean' is not assignable to type 'string | number | readonly string[]'.|Change from value={reminder} to value={"reminder"}||

## Testing

|Error|Solution|Reference|
|--|--|--|
|Error: Method “props” is meant to be run on 1 node. 0 found instead.|node was not found, fix with correct id, class, etc.||
|<--- Last few GCs --->[51482:0x67344e0]    33117 ms: Scavenge 2034.5 (2076.5) -> 2032.3 (2077.0) MB, 4.2 / 0.0 ms  (average mu = 0.151, current mu = 0.116) allocation failure FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory|Cannot find element||

## Building

- To build and deploy locally do the following:
- npm i -g serve
- serve -s build -p 3100
- 
# References

- [Debugging](https://egghead.io/lessons/react-render-an-object-with-json-stringify-in-react)
     - {JSON.stringify(attributeName)}
- [React calling REST Service](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/react/reactCallingREST.mp4)
- [JavaScript Commands](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements)
- [bablejs.io - |converts latest js/jsx to old js](https://babeljs.io/)
- [react axios](https://www.digitalocean.com/community/tutorials/react-axios-react)

# Tricks

|Trick|Example|
|Ternary without Else|{showAddTask && <AddTask onAdd={addTask}/>}|

# Cross Origin Resource Sharing (CORS) Policy

- Web Site calling another Web Site, e.g. localhost:8080 calling localhost:3000

```
// Application.java
@Bean
public WebMvcConfigurer corsConfigurer() {
	return new WebMvcConfigurer() {
		public void addCorsMappings(CorsRegistry corsRegistry) {
			corsRegistry.addMapping("/**").allowedMethods("*").allowedOrigins("*");
		}
	};
}

// Controller.java
@CrossOrigin(origins = "*", allowedHeaders = "*")
```

# Additional Products

- npm install formik 
     - instead of mapping the state to the form, form back to the state and onChange handlers on each of the fields 
     - using formik handles all this without the extra overhead
- npm install moment

# Troubleshooting

|Issue|Solution|
|--|--|
|React Troubleshooting Guide|https://create-react-app.dev/docs/troubleshooting/|
|React Application not created|- npm uninstall -g create-react-app<br>- npx clear-npx-cache<br>- [Search create react app troubleshooting](https://create-react-app.dev/docs/troubleshooting/)|
|A component is changing an uncontrolled input to be controlled.  This is likely caused by the value changing from undefined to a defined value.|- When working with DOM elements, React and DOM need to be synchronized, thus adding state will resolve the issue and the component is now a CONTROLLED Component.<br>- const [username, setUsername] =  useState("cv64");|
|You provided a value prop to a form field without an onChange handler.  This will render a read-only field.|- If the field should be mutable use defaultValue, otherwise use either onChange or readOnly|
|Each child in a list should have a unique key prop|<tr key={someObject.id}>|
|useHref may be used only in the context of a Router component|Move Link element inside BrowserRouter element|

