import React, { useState } from "react";
const StateHookTutorial = () => {
 const [counter, setCounter] = useState(0);  // useState initializes counter to 0
 const [name, setName] = useState("Bobby"); // initialize name to Bobby
 const increment = () => { // function increment
  //counter++;
  setCounter(counter + 1); // increment the count
  console.log(counter);
 };
 let updateName = (event) => { // function update the name field
  const newName = event.target.value;
  setName(newName);
 };
 return (
  <div>
   <h1>State Hook Tutorial</h1>
   <h2>Counter: {counter}</h2> // display the counter
   <button onClick={increment}>Increment</button>&nbsp;&nbsp; // increment the counter when button is clicked
   <input placeholder="Enter Name " onChange={updateName} /> // change the name when input is changed
   &nbsp;&nbsp;
   {name}
  </div>
 );
};

export default StateHookTutorial;