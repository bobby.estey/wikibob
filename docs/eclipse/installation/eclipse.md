# ![](../../docs/icons/cv64us50x50.png) eclipse Integrated Development Environment (IDE)

# WARNING - install git prior to installing eclipse

- The eclipse Integrated Development Environment (IDE) is a framework tool that assists software development in any programming / scripting language

- The eclipse IDE has several views (called perspectives) available by default, the two most popular:
     - The Java (development view)
     - Debug (debugging view)

# Download

- Navigate to the [eclipse download](https://www.eclipse.org/downloads/)
- Download the appropriate version

# Installation (PLEASE PAY ATTENTION HERE)

![Installation Process](eclipseInstaller.png)

- Execute the .exe file that was downloaded
     - (or)
- Extract the compressed file and copy to the appropriate folder
- Select Eclipse IDE for Java Developers
- **DO NOT INSTALL in "Program Files" and not in DEFAULT FOLDERS**
- Installing software in filenames with spaces is a terrible practice and you will have issues with configurations
- Only install software with filenames and directories without spaces
- The following table illustrates paths that should be utilized:

|Operating System|Path|
|--|--|
|Linux / Mac|/opt/eclipse|
|Windows|C:\opt\eclipse

- Press Install

# Configuration Parameters

![Launch](launch.png)

- Press Launch

# Workspace Directory Settings

![Workspace Directory](workspaceDirectory.png)

- Ensure workspace directory is correct and Press Launch

# Create New Java Project

![New Project](newProject.png)

- DO NOT GO WITH THE DEFAULTS
- Utilize the git directory that was configured previously

# Create Helloworld Program

```
public class Helloworld {
	public static void main(String[] args) {
		System.out.println("Helloworld");
	}
}
```

# Execute Helloworld

- Right Click in Code
- Run as Java Application