# Perspectives

![1](1.png)

- When starting the eclipse IDE the default perspective appears, this is usually the Java (development) perspective
- The image on the image above is the Java perspective
- The current perspective is shown as a depressed icon in the upper right corner
- The J character with a package and two balls is the icon for the Java Perspective
- Arrow 1 is pointing to the Java perspective icon, validating that the Java perspective is the current view
- Arrow 2 is the upper right corner zoomed in showing the Java Perspective icon selected

![2](2.png)

- The Java Perspective usually starts with 5 or more panels.  The only panels you will need at this time are:
     - Package Explorer (left panel) – the file system layout, circle 1
     - Development Panel (center panel) – the code being developed, circle 2
     - Outline Panel (right panel) – the architecture of the class, circle 3
     - Miscellaneous Panel (bottom panel) – several tabs are located here, e.g. Console, circle 4

![3](3.png)

- This is the Development Panel (center panel) – the code being developed from the previous slide zoomed in
- We are going to set breakpoints
- Breakpoints are where you want the code to stop in the debugger application
- To set a breakpoint is very simple:
     - Double click on the left side of the line number you want to set the breakpoint
     - Once a breakpoint has been established, a blue ball will appear
     - Arrow 1 has a breakpoint on line 12, arrow 1
     - Arrow 2 has a breakpoint on line 18, arrow 2
     - Arrow 3 has a breakpoint on line 24, arrow 3

![4](4.png)

- Depending on how you wish to execute the program, you can either Run (normal operation) or Debug (debugger operation)
- NOTE:  The first time a program executes in eclipse, you must inform eclipse which Class has the main method
- To do this right click anywhere in the Development Panel (center panel) – the code being developed and select either, circle 1:
     - Run As -> Java Application
     - Debug As -> Java Application
- When either is executed for the first time, you can start using the icons on the menu bar, arrow 2
- The icons on the left upper panel is zoomed in with arrow 3 (debug) and arrow 4 (run) icons

![5](5.png)

- If the Debug As or the Debug icon are pressed then the perspective changes from the Java to the Debug Perspective
- NOTE:  The first time you go into the Debug Perspective you will receive several notifications about leaving the Java Perspective
- Accept all notifications from the Operating System
- Check the box that asks if you want to use the Debug Perspective now and in the future
- The Debug Perspective icon is now depressed (arrow 1), zoomed in (arrow 2) and the Debug Perspective is now displayed:
     - Debug / Stack (left panel) – the programming stack, Class and line number is highlighted, circle 3
     - Development Panel (center panel) – the code being debugged, circle 4
     - Variables Panel (left panel) – Variables, Breakpoints and Expressions, circle 5
     - Miscellaneous Panel (bottom panel) – several tabs are located here, e.g. Console, circle 6
- NOTE:  Breakpoints tab was moved from the Variables Panel to the Miscellaneous Panel
- Default the Breakpoints are next to Variables

![6](6.png)

- Before continuing with the Debugger the Panels need to be explained in more detail:
- Arrow 1a (Stack Perspective) – Circle 1b (zoom in of Stack Perspective) – Arrow 1c – code that the stack is pointing
- Stack Perspective highlights line 12 with gray background
- Code highlights 12 with green background
- Arrow 2a (Breakpoint Status) – Circle 2b (zoom in of Breakpoint Status)
- Arrow 3a (Breakpoint Clear Buttons) – Circle 3b (zoom in of Breakpoint Clear Buttons)
- Single X – remove current breakpoint
- Double X – remove all breakpoints
- NOTE:  Breakpoints can be enabled / disabled by double clicking the blue ball to the left of the line number or pressing the X icons

![7](7.png)

- When in the Debug Perspective the Navigation Buttons will appear, we are going to study only 5 of the many buttons
- Arrow 1, points to the menubar showing the Navigation Buttons and below is a zoomed I view
- Buttons Defined:
     - Resume (Yellow Bar and Green Arrow) – go to the next breakpoint
     - If program execution is stopped, pressing this button goes to the next breakpoint
     - Terminate (Red Square) – end program execution
     - Step Into (Arrow Pointing Down) – navigate into a Constructor() or method()
     - Step Over (Arching Arrow) – navigate to the next line, if a Constructor() or method(), do not go into the Constructor() or method()
     - Step Return / Step Out (Arrow Point Up Right) – navigate out of a Constructor() or method()

![8](8.png)

- We have pressed the Resume icon and have advanced from line 12 to line 18
- Arrow 1 points to line 18 in the programming stack
- Arrow 2 points to line 18 in the code
- Arrow 3 to the Variables which now appear in memory and is zoomed in below
- NOTE:  The variable names and values

![9](9.png)

- We have pressed the Resume icon and Stepped Over until we have reached line 34, Arrow 1 and Arrow 2
- The variable at line 34 is currently 64, you can see this in the Variables both in setInteger and getInteger (circle 3)
- Eclipse also lets you change variables while the program is executing
- Selecting the value in Variables, you can change the value
- I change the value under setInteger to 13 and eclipse highlights anything that changes in Yellow (circle 4)
