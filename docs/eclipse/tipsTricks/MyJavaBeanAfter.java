package us.cv64;

import java.awt.Color;

public class MyJavaBeanAfter {

	private String name = "";
	private int height = 0;
	private int weight = 0;
	private Color eyes = null;
	private Color hair = null;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	/**
	 * @return the eyes
	 */
	public Color getEyes() {
		return eyes;
	}
	/**
	 * @param eyes the eyes to set
	 */
	public void setEyes(Color eyes) {
		this.eyes = eyes;
	}
	/**
	 * @return the hair
	 */
	public Color getHair() {
		return hair;
	}
	/**
	 * @param hair the hair to set
	 */
	public void setHair(Color hair) {
		this.hair = hair;
	}
}
