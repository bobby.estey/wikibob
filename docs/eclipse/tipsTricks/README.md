# eclipse Tips and Tricks

This document is about the eclipse Integrated Development Environment (IDE) and some of the available features

## Moving project under another folder

- Right click project
- Refactor -> Move...

#### Ctrl-Shift-F - reformat selected area

When you have a lot of code and the formatting is inconsistent, select the area or the entire file, press Ctrl-Shift-F and eclipse will autoformat instantly.  Comes in very handy when control structures, e.g. if / else, while, etc. get cumbersome

#### JavaBean - generating Constructors / getters and setters 

Java has a concept called a Java Bean.  A Java Bean and Enterprise Beans are not the same.  A Java Bean is simply setting up a Class's attributes with getters and setters.  Some Classes can be very large, let's say a Class has 20 attributes, how long is this going to take you to write up all the getters and setters???  In eclipse, 1 second, by simply doing the following:

- Right Click in the Class file, just below the last attribute 
- Press -> Source -> Generate Getters and Setters
- Select the attributes to generate the different types of methods and don't forget to select Generate method comments at the bottom to generate the Javadoc also :-)
- There are also features to Generate Constructors

See these files for example:

- [MyJavaBeanBefore](./MyJavaBeanBefore.java)
- [MyJavaBeanAfter](./MyJavaBeanAfter.java) 

#### Automated Javadoc

- Navigate above a Class, Constructor, method, etc.  Type /** and press enter
- Javadoc is generated with all the variables that are available
- Details are still required, however, this feature provides a template to quickly write Javadoc
