[Ranga Karanam / in28Minutes](https://in.linkedin.com/in/rangakaranam)

I have had so many Mentors and Ranga is one of my favorites.  You have to visit his courses.  Ranga is both Professional and Fun.  I cannot express my appreciation with what Ranga provides and in layman terms.  I have followed Ranga for years and finally caved in to pay for his site and WOW, WORTH more than I invested.  If you cannot afford what Ranga asks for his Tutorials, contact me, I will gladly pay, he is that GOOD.

# Course

- [Master Spring Boot 3 & Spring Framework 6 with Java](https://www.udemy.com/course/spring-boot-and-spring-framework-tutorial-for-beginners/learn/lecture/35017678#overview)
     - Fun, Precise, Accurate and a LOT
          - What is Fun, Ranga will say something like, "How does this happen?"  AHA <- SO GREAT!!!
          - Ranga intentionally makes things break to teach the solutions to the problems
     - Installs other products outside the course, gives training on those technologies, e.g. Docker, JSON Formatter, Amazon Web Service, etc.
     - Covers the History of these products with the [6 W's](https://library.wcc.hawaii.edu/c.php?g=35279&p=3073195)
     - Everything WORKED!!! - SO IMPORTANT, I go to sites, half way through, doesn't work and I have to ABANDON and look for the CORRECT Solution, SO FRUSTRATING
     - Date Format:  ISO Format, which we all should be using, nothing else, YYYYMMDDTHHMMSS
          - [Why use ISO Date Format](../misc/time/isoDateFormat.md)
