# Mongo Database

- No we are not talking about the Movie where MONGO was one of the major Players
     - [Mongo - Blazzing Saddles](https://www.youtube.com/watch?v=28khv-BydeY)
     - YES / NO on the Bull's Rear is SYMBOLOGY for YES / NO on Semi Trucks.  Ok to Pass on the Left, DO NOT Pass on the Right
     - [Mongo - Deleted Scenes](https://www.youtube.com/watch?v=kqOEvN_DQgA)
  
# References

- [New Installation Notes](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/mongodb/installation/README.md)
- [Traversy Media - MongoDB Crash Course](https://www.youtube.com/watch?v=2QQGWYe7IDU)

# History & Terminology

- Created in 2007 by DataClick for handling HU **MONGO** US Databases
- NoSQL (Not Only SQL) Document database and can store relational data
    - Stored in Binary Javascript Object Notation (BSON)
    - Document is a JSON data structure with field and value pairs
    - SQL has predefined structures (schemas, tables), Strict Rules, definitions must be defined ahead of tim
    - NOSQL definitions are optional, can be defined later and flexible (store any type of data)
- Collections are comparable to Tables and NoSQL provides both Flexible Schemas, Tables and Optional
    - Data Structures can evolve without executing complex Data migrations
    - Allows frequently accessed data to be stored in the same location, allowing fast read operations and JOINS are not required
- Sharding distributes data across a cluster of machines
    - Collections are self-contained (not coupled relationally)
    - Databases are called Clusters
    - User Accounts are configured through Database Access
    - Internet Protocol (IP) addresses are configured through Network Access
- Indexes and Secondary Indexes can be used for fast Queries
- Aggregation Pipeline:  
    - Collection -> $Match -> $Group -> Result
    - Processing a large number of Documents in a Collection passing through different stages (pipeline)
    - Pipeline - map, filter, sort, group, reshape and modify documents
- Mongo Database Atlas (Cloud Database Platform)
    - User Interface to Interact with Data
    - Full Text Search
- Triggers - Data Updates (Serverless Function) - server provisioning and management is abstracted from the user
- Realm (Firebase Pattern) - Sync up with a front end to the database in real time

# Commands

|Command|Description|
|--|--|
|Installation|[Installation](https://www.mongodb.com/docs/manual/administration/install-community/)|
|Commands|[Commands](https://docs.mongodb.com/manual/tutorial/getting-started)|
|Manage Mongo Database Server(Daemon)|sudo service mongod start \| stop \| restart|
|Mongo Sharding Router|mongos|
|Mongo Database Shell (interactive JavaScript)|mongo|
|mongosh "mongodb+srv://cluster0.ethity.mongodb.net/cv64Database" --username estey|Mongo Connection Example|
|BSON Example|{ name: "bobby", website: "cv64.us" }|
|mongosh --version|get the version of mongodb|
|db|displays current database|
|show dbs|displays all databases, NOTE:  Database must have at least one record to display|
|use <db>|change database, e.g. use cv64, would change to the cv64 database|
|upsert|update the Document, if Document doesn't exist the insert into database|

# Query Examples

|Command|Description|
|--|--|
|db.cv64.drop()|Deletes Collection|
|db.cv64.insertOne({ name: "bobby", website: "cva64.us"})|Insert into the database cv64 a single Document with the values inside the method call|
|db.cv64.insertMany([{ name: "bobby", website: "cv64.us"},{ name: "ester", website: "cv64.us"},{ name: "rebby", website: "cv64.us"}])|Insert Many Documents|
|db.cv64.find()|returns the entire collection|
|db.cv64.find({name: "bobby" }).sort({ website: -1})|Returns all names with value "bobby" sorted by website field(-1 descending order) 1 ascending order|
|db.cv64.find({name: "bobby" }).sort({ website: -1}).count()|Returns the number of Documents|
|db.cv64.find({name: "bobby" }).sort({ website: -1}).limit(3)|Returns the first 3 Documents|
|db.cv64.find({date: {$gte: new Date('1961-10-27'), $lt: new Date('2003-08-07')}})|Find Date Range|
|db.cv64.findOne({website: "cv64.us"})|Finds one Document|
|db.cv64.updateOne({name:"rebby"}, {$set: {website: "cvn71"}})|Updates a Document|
|db.cv64.updateOne({name:"max"}, {$set: {website: "cv1"}}, {upsert: true})|Update Document, if doesn't exist Insert new Document| |Update a Document field cruises incrementing the value by 2|
|db.cv64.updateOne({name:"max"}, {$inc: {cruises: 2}})|Update all Documents, incrementing cruises by 2|
|db.cv64.deleteOne({name: "bobby"})|Deletes one Document|
|db.cv64.deleteMany({name: "ester"})| Delete all Documents with the name "ester"|
|```const aggregation = [{$match:{date:{$gte: new Date('1961-10-27'), $lt: new Date('2003-08-07')}}}, {$group: {_id: '$item', total: {$sum:{$multiply:['$price', '$quantity']}}}}]```|Aggregation Example|
|db.cv64.aggregate(aggregation)|Execute const aggregation|

# Notes

## Ubuntu Installation Details

```
wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | sudo apt-key add -
sudo apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/6.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-database hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-mongosh hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
ps --no-headers -o comm 1
sudo systemctl start mongod
sudo systemctl status mongod
npm i mongodb
```

## MongoDB Atlas

- Cloud Deployment
- Management (Databases, Collections, Documents, Indexes) 
- Execution (Pipelines)
- Shared Global Clusters and Scalability

- User Interface
    - [Login](https://account.mongodb.com/account/login)
- Database Access
    - Select Password -> Create Password
    - Select Built-in Role -> Atlas admin
- Network Access
    - IP Address = 0.0.0.0/0
    - Status should be Active
- Database
    - Connect -> Connect with the MongoDB Shell -> I have the MongoDB Shell installed
    - Copy Connection String and change out the myFirstDatabase with your database
        - mongosh "mongodb+srv://cluster0.5szso3j.mongodb.net/cv64" --apiVersion 1 --username bobbyestey
        - if Connection successful the prompt will change ending with database, e.g.
            - Atlas atlas-qnavts-shard-0 [primary] cv64>
    - **NOTE:  The database is not created until data is inserted**
    - Browse Collections - display the collections in the database
        - Insert Document to manually insert a Document
        - All CRUD capabilities are available in Atlas
    - Load Sample Database <- go back to Database -> Press ... -> Load Sample Database
        - Sample Database is loaded
    
## MongoDB Compass - GUI

### Installation

```
wget https://downloads.mongodb.com/compass/mongodb-compass_1.35.0_amd64.deb
sudo dpkg -i mongodb-compass_1.35.0_amd64.deb
```

### Startup
```
mongodb-compass
```

- Database
    - Connect -> Connect using MongoDB Compass
    - Copy Connection String
    - Paste Connection String in Compass
    - Replace <password> to actual database password
    - Locate the database and collection, e.g. 
    - [View Database](mongoDBcompass.png)
    
- Aggregations
     - $match - FILTER - returns query results that MATCH the specified requirements, JSON, e.g. {hullNumber: {$gt: 58}, price: {$lt: 500}}
     - $sort - sorts by fields in particular order (-1 descending order) 1 ascending order ) JSON, e.g. {field1: 1, field2: -1}
     - $project - returns only the fields specified JSON, e.g. {field1: 1, field2: 2}
     - $limit - returns only the number of records, INTEGER, e.g. 3

- Export To Language - provides all the code required to access the database, including the Query

# Connections

## Java Connection Example

```
import java.util.Arrays;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.conversions.Bson;
import java.util.concurrent.TimeUnit;
import org.bson.Document;
import com.mongodb.client.AggregateIterable;

/*
 * Requires the MongoDB Java Driver.
 * https://mongodb.github.io/mongo-java-driver
 */

MongoClient mongoClient = new MongoClient(
    new MongoClientURI(
        "mongodb+srv://bobbyestey:<password>@cluster0.5szso3j.mongodb.net/cv64"
    )
);
MongoDatabase database = mongoClient.getDatabase("cv64");
MongoCollection<Document> collection = database.getCollection("cv64");

AggregateIterable<Document> result = collection.aggregate(Arrays.asList(new Document("$match", 
    new Document("hullnumber", 
    new Document("$gt", 4L))
            .append("price", 
    new Document("$lt", 500L)), 
    new Document("$sort", 
    new Document("$project", 
    new Document("name", 1L)
            .append("hullnumber", 1L)
            .append("price", 1L)), 
    new Document("$limit", 4L)));
```

## Node.js Connection Example

```
const { MongoClient } = require('mongodb');
const uri = "mongodb+srv://bobbyestey:<password>@cluster0.5szso3j.mongodb.net/cv64?retryWrites=true&w=majority";
const client = new MongoClient(uri, {useNewUrlParser: true, useUnifiedTopology: true});
client.connect(async err => { 
    const collection = client.db("cv64").collection("cv64");  // database = cv64, collections = cv64
    const pipeline = [ query ], e.g. { '$match': { ... }
    const aggregation = await collection.aggregate(pipeline).toArray();  // promise
    console.log(aggregation);
    client.close();
});
```

## Docker Installation

- sudo docker pull mongo // pulls from docker hub
- sudo chmod 777 docker.sock 
- sudo chmod 777 ~/docker
- IntelliJ -> Services -> Docker -> Images -> Create Container -> Right Click -> Create Docker Container -> Give Container name

