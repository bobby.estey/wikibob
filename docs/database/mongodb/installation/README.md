# MongoDB Installation and Configuration

## Create Account

- [MongoDB Atlas Project](https://www.mongodb.com)
- [Create Account](https://www.mongodb.com/cloud/atlas/register) - Press Try Free

## Configuration

- Press + New Project, press Next

![New Project](newProject.png)

- Create a Project, Enter Project Name, press Next

![Create Project](createProject.png)

- Add Members, can leave blank, press Create Project

![Add Members](addMembers.png)

- Create Cluster if none available, press Build a Cluster

![Create Cluster](createCluster.png)

#### VERY IMPORTANT - Select the FREE Database (M0)

- Deploy Cluster, select M0 Free Database, Cluster, Provider, Region, press Enter

![Deploy Cluster](deployCluster.png)

- Connect to Cluster, add Username and Password, press Create Database User

![Connect Cluster](connectCluster1.png)

- Press Choose a connection method

![Connect Cluster](connectCluster2.png)

- Configurations

![Configurations](configurations.png)

- Select Drivers, select Java, the highest Version, copy the URI for future use, press GoBack

![Drivers](drivers.png)

- MongoDB Shell, Operating Systems, select the Operating System, press GoBack

![Operating System](operatingSystem.png)

- Select Quick Start, press Create

![Quick Start](quickStart.png)

- Select Driver JDBC Driver, Select Database, press Done

![Database](database.png)

- Overview, press Connect

![Connect](connect.png)

- Press I don't have MongoDB Compass Installed

![Compass](compass.png)

## Compass Linux Installation and Startup

```
sudo dpkg -i mongodb-compass_1.45.2_amd64.deb
mongodb-compass
```

## Visual Studio Notes

- Press Extensions Icon

![Extensions](extensions.png)

- Search Marketplace, enter mongodb connect

![Marketplace](marketplace.png)

- Add Connection to VS

![Connection](connection.png)

## References / Notes

- [MongoDB Atlas](https://www.youtube.com/watch?v=084rmLU1UgA)
