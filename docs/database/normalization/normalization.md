# Data Normalization

This page only discusses the 1st through 3rd Data Normal Forms.  There are several others that are not mentioned, e.g. Boyce-Codd, 4th, 5th.

- 1st Normal Form (1NF) Eliminate Repeating Groups - Make a separate table for each set of related attributes, and give each table a primary key
- 2nd Normal Form (2NF) Eliminate Redundant Data - If an attribute depends on only part of a multi-valued key, remove the attribute to a separate table
- 3rd Normal Form (3NF) Eliminate Columns Not Dependent On The Key - If attributes do not contribute to a description of the key, remove them to a separate table

## 1st Normal Form (1NF) Eliminate Repeating Groups

Moving all attributes from a raw data source into seperate tables helps with concentration of the problem and reducing data redundancy.  Separating the repeating groups of data results in **first normal form**.  The example below illustrates that several attributes that are combined into a single data source can cause confusion and produce redundant data within each record.  Moving the attributes to three new tables shows how the designer can concentrate on a specific topic, e.g. company, department, employee.  Note also that the field names are much simpler, we don't need to repeat the names because the table name clarifies the meaning.

![Entity-Relationship Diagram (1NF)](1NF.gif)

![Raw Data Example](rawDataExample.png)

![1NF Tables](1nfTables.png)

## 2nd Normal Form (2NF) Eliminate Redundant Data

After we have moved all the attributes to three new tables we find redundancy in the key fields of the department table, the fields code and name.  We create a new table called department_code and eliminate the redundant key.  Using the 2NF prevents from having update and delete the anomalies.

A deletion anomaly example is due to a record being the only record with a particular reference that gets deleted.  When future records start being added the previous code
could possibly be not used anymore.  If a previous record is then added at a future date this record would have the original code and the current records in the database the incorrect code.  You can delete a record in the department table without having concern about losing the code and name because the department code table keeps these references.

![Entity-Relationship Diagram (2NF)](2NF.gif)

![2NF Tables](2nfTables.png)

## 3rd Normal Form (3NF) Eliminate Columns Not Dependent On The Key

Let's say that the skill fields increased in size and this is no longer relevant to the employee table.  We then further reduce the employee table with a skill code.

![Entity-Relationship Diagram (3NF)](3NF.gif)
