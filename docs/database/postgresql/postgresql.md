# postgresql

## videos

- [Ubuntu Postgresql setup](https://www.youtube.com/watch?v=-LwI4HMR_Eg)
- [Ubuntu Postgresql / pgadmin setup](https://www.youtube.com/watch?v=7tfPKDba1Jo)
- [Ubuntu pgadmin setup](https://www.pgadmin.org/download/pgadmin-4-apt/)
     - installation instructions should be as follows:
          - sudo apt install curl
          - sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
          - sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
          - sudo apt install pgadmin4

## installation postgresql

- sudo apt-get -y install postgresql postgresql-contrib
- Ubuntu Software Center - pgadmin
- [Logfile Example](postgresqlInstallation.log)

## notes

| Description | Syntax |
| ----------- | ------ |
| location of postgresql files  | /etc/postgresql/<version>                          |
| service postgresql            | start, stop, restart, reload, force-reload, status |
| sudo su postgres              | login as postgres                                  |
| manual psql                   | man psql                                           |
| psql prompt and documentation | psql                                               |
| psql status                   | systemctl status apache2                           |

#### service postgresql status / prompt

```
/etc/postgresql/14/main$ service postgresql status
● postgresql.service - PostgreSQL RDBMS
     Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
     Active: active (exited) since Tue 2022-08-30 17:47:02 PDT; 13min ago
    Process: 73655 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 73655 (code=exited, status=0/SUCCESS)
        CPU: 2ms

Aug 30 17:47:02 cv64-Inspiron-16-7610 systemd[1]: Starting PostgreSQL RDBMS...
Aug 30 17:47:02 cv64-Inspiron-16-7610 systemd[1]: Finished PostgreSQL RDBMS.

/etc/postgresql/14/main$ psql
psql (14.5 (Ubuntu 14.5-0ubuntu0.22.04.1))
Type "help" for help.

postgres=# 

```

## commands
 
| Description | Syntax |
| ----------- | ------ |
| list databases | \l |
| list tables | \dt |
| list users, role name, attributes | \du |
| ALTER USER postgres WITH PASSWORD 'password'; | change password |
| CREATE USER cv64 WITH PASSWORD 'password'; | create user |
| ALTER USER cv64 WITH SUPERUSER; | set user with superuser |
| DROP USER cv64; | remove user |

## pgadmin tool

- alias pgadmin4='/usr/pgadmin4/bin/pgadmin4 &'
- http://localhost/pgadmin4

## pgadmin

![Password Reset](passwordReset.png)
![General](pgadmin1.png)
![Connection Configuration](connectionConfiguration.png)
![Server](pgadmin3.png)
