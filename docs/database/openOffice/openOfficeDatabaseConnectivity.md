<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Open Office Database Connectivity</title>
  <meta http-equiv="content-type"
 content="text/html; charset=ISO-8859-1">
</head>
<body>
<div align="center"><font face="Helvetica, Arial, sans-serif"><big><big><b>Open
Office Database Connectivity</b></big></big><br>
</font></div>
<font face="Helvetica, Arial, sans-serif"><br>
This web page shows how to make database connections with various
database using the Open Office product.<br>
<br>
Database Examples<br>
</font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif"><a href="#msAccess">MSAccess</a></font></li>
  <li><font face="Helvetica, Arial, sans-serif"><a href="#MySQL">MySQL</a></font></li>
</ul>
<font face="Helvetica, Arial, sans-serif"><big><a name="msAccess"></a>MS
Access</big><br>
<br>
Download the latest release of&nbsp;<a
 href="http://msdn.microsoft.com/downloads/list/dataaccess.asp?frame=true#mdac"
 target="_self">Microsoft Data Access Components (MDAC)</a>, e.g.
&nbsp;2.7 RTM &#8211; Refresh or later version.<br>
<br>
Start up Open Office (Writer or Calc) and cl</font><font
 face="Helvetica, Arial, sans-serif">ick on Tools / Data Sources...<br>
<br>
</font>
<div align="center"><font face="Helvetica, Arial, sans-serif">&nbsp;<img
 src="openOffice1.png" alt="" height="310" width="339"> <br>
</font></div>
<font face="Helvetica, Arial, sans-serif"><br>
Press the New Data Source button and fill out the fields as follows:<br>
<br>
</font>
<table border="1" cellpadding="2" cellspacing="2" width="100%">
  <tbody>
    <tr>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">Key<br>
      </font></td>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">Value<br>
      </font></td>
    </tr>
    <tr>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">Name<br>
      </font></td>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">MS
Access<br>
      </font></td>
    </tr>
    <tr>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">Database
Type<br>
      </font></td>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">ADO<br>
      </font></td>
    </tr>
    <tr>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">Data
Source URL<br>
      </font></td>
      <td valign="top"><font face="Helvetica, Arial, sans-serif">PROVIDER=Microsoft.Jet.OLEDB.4.0;Data
Source=&lt;path_to_mdb_file&gt;<br>
      <br>
An example would be the following:<br>
      <br>
PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=c:\bobsDatabase.mdb<br>
      </font></td>
    </tr>
  </tbody>
</table>
<font face="Helvetica, Arial, sans-serif"><br>
</font>
<div align="center"><font face="Helvetica, Arial, sans-serif"><img
 src="openOffice2.png" alt=""> <br>
<br>
</font>
<div align="left"><font face="Helvetica, Arial, sans-serif">Now your
ready to manipulate the database. &nbsp;Tables and Queries can be
created, edited
or deleted. &nbsp;Queries can also be created and viewed using SQL
commands.
&nbsp;The Open Office Data Source Tool behaves simularily to actual MS
Access.<br>
<br>
<b><a name="MySQL"></a>MySQL - Under Construction</b><br>
<br>
<br>
<br>
</font>
<hr size="2" width="100%">
<div align="center">
<div align="center">
<div style="text-align: left;"><a href="mailto:information@cv64.us"><img
 src="../cv64.bmp" title="" alt="" style="width: 32px; height: 32px;"
 align="left"></a><small><font face="Arial,Helvetica"><a
 href="mailto:information@cv64.us">Web
Contact</a></font></small><br>
<a href="mailto:information@cv64.us"><small><font face="Arial,Helvetica">Last
modified:&nbsp; 2003 December 31</font></small></a></div>
</div>
<font face="Helvetica, Arial, sans-serif"><b></b><br>
</font> </div>
</div>
</div>
<p> </p>
<br>
<br>
</body>
</html>
