# H2 Structured Query Language (SQL) Database

- ZERO - installation, configuration, NO OVERHEAD
- in Memory Database
- Command Line Runner - Executes SQL commands

## Configuration - [resources/application.properties](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/maven/application.properties)

- URL - Look at Spring Logs for URL
- For Example:  

```
2024-11-27T16:30:09.144-08:00  INFO 600186 --- [geneaology] [  restartedMain] com.zaxxer.hikari.pool.HikariPool        : HikariPool-2 - Added connection conn10: url=jdbc:h2:mem:5bb62bbe-4816-4245-a42f-a3333ee732c7 user=SA
```

- Edit application.properties
     - spring.h2.console.enabled=true
     
## H2 Console

- localhost:8080/h2-console
- [localhost:8080/jdbc:h2:mem:3c4edf24-9de9-4ffe-834e-31d29b6a317a](../jpaHibernate/docs/h2Configuration.png)
- Set fields with parameters defined in application.properties
     - JDBC URL:  jdbc:h2:mem:cv64
     - User Name:  cv64
     - Password:  password

- Press Test Connection, should display Test successful

![Press Test Connection](./h2Console.png)

- Press Connect, should display database

![Press Connect](./h2Database.png)

## H2 SQL Example

- create src/main/resources/schema.sql
- schema.sql will be executed when the Spring Server is started, e.g. [Spring Log Example](#spring-log-example)

```
create table course (
	id bigint not null,
	firstName varchar(255) not null,
	lastName varchar(255) not null,
	primary key (id)
);
```

- reload H2 console
- the table should now created
- test with SQL, select * from course;

- SQL Commands to Execute in H2

```
insert into estey (id, firstName, lastName) values (1, 'bobby', 'estey');
select * from estey;
delete from estey where id=1;
```

## Spring Log Example

```
2024-12-04T12:49:03.596-08:00 DEBUG 534060 --- [geneaology] [  restartedMain] o.s.jdbc.datasource.init.ScriptUtils     : 0 returned as update count for SQL: create table estey ( id bigint not null, firstName varchar(255) not null, lastName varchar(255) not null, primary key (id) )
```
