# Java Persistence API (JPA) / Hibernate

## Videos

- [JPA / Hibernate](https://www.youtube.com/watch?v=MaI0_XdpdP8&t=5s)

## spring-jpa-hibernate-h2

- Java Database Connectivity (JDBC)
     - Java Code writing several queries
- Spring JDBC
     - Less Java Code writing several queries
- Java Persistence API (JPA)
     - Map Entities to Tables without concern about queries
- Spring Data JPA
     - Easier and Robust
     - Define an Interface with the Entity and the Id
     - Manages Everything

#### Terminology

- Criteria API - write JPQL with Java APIs
- Data Transfer Object - Bean (Data) - JPA
- Data Access Object 
     - defines the interface
     - provides a persistence mechanism
     - injects dependencies into implementations
- Entities
- Entity Manager - manage entities, e.g. get the tasks from a table and relate the data as a object
- JPQL - like SQL you will be using entities instead of table
- Mapping - maps Java Classes to Database tables
- Object Relational Mapping (ORM)
- Object Relational Impedence Mismatch - a good comparison Java stores fields whereas SQL stores columns
- Relationships

## Configuration - [resources/application.properties](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/maven/application.properties)

## Java Persistence API (JPA) / Hibernate

- JPA provides an ORM between Java Classes and Database Tables
- [JPA / Hibernate Directory Structure](./jpaHibernate.png)

- Building with Spring you need Web, JPA and H2 (in memory database)
     - [eclipseLink 1](springBootJpaHibernateH2_3.png)
     - [eclipseLink 2](springBootJpaHibernateH2_4.png)
     - [eclipseLink 3](springBootJpaHibernateH2_5.png)

## Configuration / Demo

- Copy zip file to the extraction directory, e.g.
     - cp ~/Downloads ~/git/cv64/java/maven
- Unzip files
- Open up IDE and Import existing Maven Project, select the folder and then the POM file
     - [Select Directory](selectDirectory.png)
     - [Select POM](selectPOM.png)
     - Press Finish
- Maven start downloading Maven Dependencies (JARs and POMs) required for the Project
- Create the following directories:
      - controller - Controllers
      - dao - Data Access Objects (Implementation)
      - model (Database Table)
           - dto - Data Transfer Object (Bean)
           - entity - Entities
      - service - Services
      Application
      
#### JPA - Java Persistence API - Interface - Defines the Specification - @Annotations

- JPA provides an interface between Java Classes and Database Tables
- defines specifications entities (@Entity, @EntityManager), maps, attributes (@Id, @Column), relationships between entities and manages entities (EntityManager)

#### Hibernate (Object Relational Mapper) - Implementation of JPA

- get (retrieves) / set (stores) values from / to the database
- Provides a persistence mechanism
- Injects dependencies into the implementations
- Communicates the binding between Java Objects and Database Tables

#### Annotations - mapping class attributes to database columns

- @Column(name = "ColumnName") - maps to a column in the database
- @DiscrimatorColumn(name = "ColumnName") - map only to one column
- @EnableJpaAuditing - tracking and logging events related to persistent entities
- @Entity - entity that exists in the database
- @Generated - generated value
- @Id - primary key
- @Inheritance(strategy = InheritanceType.SINGLE_TYPE) - map only to one table
- @ManyToMany - many to many relationship
- @Repository - interacts with the database, open / close transactions
- @Table(name = "TableName") - maps to a table in the database

## Spring JDBC

- [JDBCTemplate](https://youtu.be/MaI0_XdpdP8?t=540)
     - Spring JDBC provides the template, which is a layer on top of JDBC<br><br>

- Spring JDBC Template example
<pre>
jdbcTemplate.update("update bean set attribute1=?, attribute2=? where ..."), bean.getAttribute1(), bean.getAttribute2());
</pre><br>

- BeanPropertyRowMapper Example (not required if attributes match)
<pre>
jdbcTemplate.queryForObject(preparedStatement, new Object[] {attribute1, attribute2}, new Bean());<br><br>
class BeanMapper implements RowMapper<Bean> {<br>
    @Override<br>
    public Bean mapRow(ResultSet resultSet, int rowNum) throws SQLException {<br>
        Bean bean = new Bean();<br>
        bean.setAttribute1(resultSet.getInt("attribute1"));<br>
        bean.setAttribute2(resultSet.getString("attribute2"));<br>
        return bean;<br>
    }<br>
}<br>

public class Bean {
  private int attribute1;
  private String attribute2;
}
</pre>

## [myBatis](https://mybatis.org) 

- MyBatis Example
<pre>
@Mapper<br>
public interface BeanMyBatisService extends BeanDataService {<br>
    @Override<br>
    @Update("Update bean set attribute1=#{attribute1}, attribute2=#{attribute2}")<br>
    public void updateBean(bean) throws SQLException;<br><br>
    @Override<br>
    @Select("select * from table where id = #{id}")<br>
    public Bean retrieveTodo(int id) throws SQLException;<br>
}<br>
</pre>
