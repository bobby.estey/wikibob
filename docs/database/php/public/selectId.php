<?php

header('Content-Type: application/json');

require_once('../private/configStudent.php');

$databaseConnection=$config['databaseConnection'];

// Create connection
$connection = new mysqli($databaseConnection['servername'], $databaseConnection['username'], $databaseConnection['password'], $databaseConnection['database']);

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}
//echo "Connected successfully";

// The query we want to execute
// https://www.cv64.us/cv64/database/public/selectId.php?id=1
$sql = "SELECT * FROM table1 WHERE id = ?";

// Attempt to prepare the query
if ($statement = $connection->prepare($sql)) {

  // Pass the parameters
  $statement->bind_param("s", $_GET['id']);

  // Execute the query
  $statement->execute();

  if (!$statement->errno) {
    // Handle error here
  }

  // Get Results
  $rows = array();
  $results = $statement->get_result();
  while($row = $results->fetch_array(MYSQLI_ASSOC)) {
    $rows[] = $row;
  }

  //  print json_encode($rows);
  print json_encode($rows);

  // close
  $results->close();
  $statement->close();
  $connection->close();

} else {
  // Handle error here
}
?>

