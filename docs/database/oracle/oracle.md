# Oracle Database

## Python Connection to Oracle Database

```
#! /usr/bin/env python
import cx_Oracle
import os

"""
# -- READING ENVIRONMENT VARIABLES --

username = os.environ['DATABASE_USERNAME']
password = os.environ['DATABASE_PASSWORD']
server   = os.environ['DATABASE_SERVER']
port     = os.environ['DATABASE_PORT']
sid      = os.environ['DATABASE_SID']

connectionString = username + '/' + password + '@' + server + ":" + port + "/" + sid
connection = cx_Oracle.connect(connectionString)
cursor = connection.cursor()
print('Connection Version: ' + connection.version)
print('Username:           ' + os.environ['DATABASE_USERNAME'])
print('Password:           ' + os.environ['DATABASE_PASSWORD'])
print('Connection String:  ' + connectionString)
cursor.execute("select * from schema.table where field='something'")
for row in cursor:
    print(row[0])
connection.close()
"""

"""
# -- READING PROPERTIES FILE --

propertiesArray = []
records = open("/env/database.properties", "r")

for record in records:
    propertiesArray.append(record);
    
url = '@' + propertiesArray[0].strip();
username = '@' + propertiesArray[1].strip();
password = '@' + propertiesArray[2].strip();

print("url ~" + url + "~")
print("username ~" + username + "~")
print("password ~" + password + "~")

connectionString = username + "/" + password + url
# records.close()
"""

# -- READING ORACLE PROPERTIES --
connectionString = os.environ['ALIAS'] # builds the connection parameters
connection = cx_Oracle.connect(connectionString, encoding="UTF-8") # creates the connection
cursor = connection.cursor()
cursor.execute("select * from schema.table where field='something'")
print('Connection String:  ' + connectionString) # prints the connection as successful adn version
print('Connection Version: ' + connection.version) # displays the connection version

for row in cursor:
    print(row[0])
    
connection.close()
