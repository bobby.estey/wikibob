<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Enterprise Data</title>
  <meta http-equiv="content-type"
 content="text/html; charset=ISO-8859-1">
</head>
<body>
<div align="center"><font face="Helvetica, Arial, sans-serif"><big><big><b>Database
Management<br>
</b></big></big></font></div>
<font face="Helvetica, Arial, sans-serif"><br>
This week we will discuss 1) &nbsp;concurrency control and
transactions, ensuring that data transactions are correct and not
corrupting the database. &nbsp;2) &nbsp;Database reliability, uptime,
failovers,
connectivity of the database. &nbsp;3) &nbsp;Database Security</font><font
 face="Helvetica, Arial, sans-serif">, how the Database Management
System (DBMS) protects the data. &nbsp;4) &nbsp;Database management
roles and responsibilities.<br>
<br>
<b>Concurrency Control</b><br>
<br>
Concurrency Control means that the many transactions occurring on the
system do not adversely effect the data. &nbsp;If User A modifies a
record, how does this effect User B reading the same record. &nbsp;Did
User B read the record before of after User A's modification.
&nbsp;Concurrency Control are the rules that are followed to ensure
data is not corrupted. &nbsp;There are several different Concurrency
Control options and they all have their pros and cons.<br>
<br>
<a href="#figure1"> Figure 1</a>, illustrates the two extremes of
concurrency control. &nbsp;The strictest option would be to lock the
entire database, make the changes and then unlock for others to access.
&nbsp;Using strict controls, the costs are much higher, accessibility
is reduced, however, database integrity is increased.<br>
</font>
<div align="center">&nbsp; <img src="concurrencyControl.gif" alt=""
 title="" style="width: 303px; height: 152px;"> <br>
</div>
<div align="center"><font face="Helvetica, Arial, sans-serif"><a
 name="figure1"></a><b>Figure 1 - Concurrency Control</b><br>
</font><font face="Helvetica, Arial, sans-serif"> </font></div>
<font face="Helvetica, Arial, sans-serif"><b><br>
</b>Transactions are known as Logical Units of Work (LUW), tasks that
are performed. &nbsp;Transactions must be Atomic, Consistent, Isolated
and Durable (ACID).<br>
</font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Atomic, every
transaction must execute completely and commit the changes to the
database or the database must be returned to the original state,
rollback. &nbsp;<a href="#figure2a">Figure 2a</a>, shows how
transaction A successfully places the value 1 into the database.
&nbsp;Transaction B however could of encountered a link error, system
error or some other type of interference that prohibited the
transaction. &nbsp;Transaction B could have been half way done with the
transaction. &nbsp;Now all records that were added to the database are
rolled back and the database is at the same state prior to transaction
B.</font></li>
</ul>
<div align="center"><img src="atomic.gif" title="" alt="">&nbsp;&nbsp; <br>
<font face="Helvetica, Arial, sans-serif"><b><a name="figure2a"></a>Figure
2a - Atomic Transaction</b></font><br>
<font face="Helvetica, Arial, sans-serif"> </font></div>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Consistent, integrity
of the application programs and transactional system. &nbsp;The
programs must be free of error. &nbsp;<a href="#figure2b">Figure 2b</a>,
illustrates that there is an adder program where two tasks have the
different values. &nbsp;If the adder program works correctly then the
result should be correct and the result should be placed in the
database.&nbsp; The first example has transactions A=1 and B=1, the
adder program failed and the result is 3, thus this transaction is not
consistent.&nbsp; The second example has transactions A=1 and B=2, the
added program is successful and the result is 3, thus a consistent
transaction.&nbsp; If there is
an error with the program either the application or the database, the
transaction would be rolled back.</font></li>
</ul>
<div align="center"><img src="consistent1.gif" alt=""
 style="width: 351px; height: 161px;">&nbsp;<br>
<img src="consistent2.gif" alt="" style="width: 351px; height: 161px;"><br>
<font face="Helvetica, Arial, sans-serif"><b><a name="figure2b"></a>Figure
2b - Consistent Transaction</b></font><br>
<font face="Helvetica, Arial, sans-serif"> </font></div>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Isolated, thread safe,
a transaction must execute without interference from other
transactions. &nbsp;Locking mechanisms and architectures that lock
records or fields
from others while one transaction is performing an update. &nbsp;This
is the most difficult of all transaction types. &nbsp;<a
 href="#figure2c">Figure 2c</a>, illustrates two transactions are
competing
with each other for the same location on the database. &nbsp;If both
transactions
are read operations then there is no transaction error, however, if
either
or both transactions are writes then database integrity could be
compromised.
&nbsp;The next example, transaction A does a read and B does a write.
&nbsp;If
A does the read before B then A's data is outdated. &nbsp;If we switch
the
order the problem can get worse. &nbsp;Let's have B do a read from the
database
and get the value 2. &nbsp;Then A does a read of 2 doubles the amount
and
immediately writes 4 to the database before transaction B returns.
&nbsp;B adds 1 to the value 2 to get the result of 3. &nbsp;B wants to
write 3
to the database, who's data is correct, A's or B's. &nbsp;Was B suppose
to
wait for A's value 4 to increment or not?</font></li>
</ul>
<div align="center"><img src="isolated.gif" alt="" title=""
 style="width: 332px; height: 501px;"> <br>
</div>
<div align="center"><font face="Helvetica, Arial, sans-serif"><b><a
 name="figure2c"></a>Figure 2c - Isolated Transaction</b></font><br>
<font face="Helvetica, Arial, sans-serif"> </font></div>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Durable, data must be
written to physical storage. &nbsp;If a system crashes the data in
memory is lost and so the data is not durable. &nbsp;Uninterruptable
Power
Supplies (UPS) probably help in this area the most, however, systems
still
do go down and if a transaction occurred during the unexpected event,
did
the data get written to the database? &nbsp; <a href="#figure2d">Figure
2d</a>, illustrates a banking example. &nbsp;If you deposited $100 and
this wasn't written to physical storage, would this transaction of
occurred.
&nbsp;Of course you can show your statement to the Bank, this is a
written
transaction, because the deposit slip is the physical storage, however,
with
E-Commerce this is an important consideration.</font></li>
</ul>
<div align="center"><img src="durable.gif" alt="" title=""> <br>
</div>
<ul>
</ul>
<div align="center"><font face="Helvetica, Arial, sans-serif"><b><a
 name="figure2d"></a>Figure 2d - Durability Transaction</b></font><br>
</div>
<font face="Helvetica, Arial, sans-serif"><b> Resource Locking<br>
<br>
</b>Concurrent processing problems are prevented by locking the
data. &nbsp;This can be an entire database, a table, record, field or
even a cell locked, called Lock Granularity. &nbsp;Large Lock
Granularity
is the strictest level and prevent anyone with the exception of the
Database
Administrator (DBA) to access and make database modifications.
&nbsp;There
are several types of locks:<br>
</font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Implicit locks are
performed by the DBMS set up either by the vendor or the DBA.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Explicit locks are
performed by commands given to the DBMS.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Exclusive locks
prevent any access, DBA's use this option when doing maintenance.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Shared locks allow one
process to perform the update and other processes can perform reads.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Two Phased Locking
means that when a lock has been given to a process the lock isn't
released until that process has completed with a rollback or commit
event.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Deadlock occurs
when processes are waiting for other resources to free up.
&nbsp;Usually
one of the processes must be destroyed to allow work to continue.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Optimistic locking, no
conflict will occur. &nbsp;Transactions are repeated until a conflict
occurs, then the lock is issued.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Pessimistic locking,
conflicts will occur. &nbsp;First process requesting the lock gets the
lock and frees the lock when the process has completed.</font></li>
</ul>
<font face="Helvetica, Arial, sans-serif">Looking back at <a
 href="#figure2c">Figure 2c</a>, the Isolated Transaction could have
been resolved with locking. &nbsp;There was a question on what was the
correct order of transactions. &nbsp;By using locks we could have
prevented this and the data would have been correct. &nbsp;<a
 href="#figure3">Figure 3</a>, illustrates the same transactions being
performed in the correct order. &nbsp;Process B was to perform the read
first, get the number 2 add 1 and write 3 to the database. &nbsp;Then
process A was suppose to read the number 3, double
the value and write the result 6. &nbsp;The burden of how the locks are
managed are really on the developer and database administrator.
&nbsp;Application programs have the ability to lock out methods,
records, etc. and this ensures that no other transactions can occur
until the Unit of Work has been completed. &nbsp;So the answer to the
question was the result suppose to be 3 or 4,
the real answer is 6. &nbsp;Yes, this is a very simple example but you
can
see how data can loose integrity.<br>
</font>
<div align="center"><br>
</div>
<div align="center"><img src="resourceLocking.gif" alt="" title=""
 style="width: 332px; height: 161px;"> <br>
</div>
<div align="center"><font face="Helvetica, Arial, sans-serif"><b><a
 name="figure3"></a>Figure 3 - Resource Locking</b></font><br>
</div>
<font face="Helvetica, Arial, sans-serif"><b> <br>
Transaction Isolation Levels<br>
</b><br>
The transaction isolation has several levels:<br>
</font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Exclusive Use, the
transaction can lock out any part of the database not giving use to any
other process.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Repeatable Read, if the
transaction does an update an exclusive lock is issued, if the
transaction performs a read a shared lock is issued.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Cursor Stability,&nbsp;
a CurSor (Current Set of Records) is a pointer into a recordset,
sometimes called a result set.
&nbsp;For example, a query is performed, the results are returned and
the cursor points at the first row of the results. &nbsp;The cursor is
used to navigate through the result set. &nbsp;Cursor stability
isolation means that the row gets a lock on the row the cursor is
pointing.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Dirty Read, transaction
locks are ignored and no locks are issued to the transaction.</font></li>
</ul>
<font face="Helvetica, Arial, sans-serif"><b>Cursor Type<br>
<br>
</b>Cursors are results that use memory and system overhead. &nbsp;If
you have many cursors and your system lacks memory your assured to have
performance issues. &nbsp;Cursors can be small, returning a few records
of results or very large, returning thousands of records and cursors
come in many types, which also affect the system performance:<br>
</font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Forward only, the
cursor can only move forward through the result set. &nbsp;If a
transaction is changed after the cursor has passed that row, the result
set will
not see the change.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Static, the data result
is static. &nbsp;Only the changes made by that particular cursor are
seen the cursor, all other data is static.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Keyset, the cursor uses
the primary key for each row to navigate through the result set.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Dynamic, all changes
are visible.</font></li>
</ul>
<font face="Helvetica, Arial, sans-serif"><b>Database Recovery</b><br>
<br>
Database Recovery are the procedures that are followed in the
event of a disaster. &nbsp;Computer system failure, catastrophic
events,
human error are the primary reasons why Database Recovery procedures
are a requirement for business, not a recommendation.<br>
<br>
The first task for a business is to stay in business and get back
online. &nbsp;I don't know of any large company that can do without
their computing resources. &nbsp;When systems are down the plan is to
get them back up as soon as possible. &nbsp;The approach to restoring
operations depends on the type of outage. &nbsp;If the situation is
catastrophic,
then you have to go with your disaster recovery plan. &nbsp;If a system
has an unscheduled shutdown, get the system back online. &nbsp; This is
primarily the hardware stage. &nbsp;Computer systems and networks
returning
back online.<br>
<br>
The second task after hardware and networks are restored, all the
software and databases need to be brought back online or restored if
the database has been destroyed or data integrity is a question.<br>
<br>
The final task is send out the word that systems are back online.
&nbsp;Customers both internal and external are going to want to know
if their data is safe and all the transactions that were entered are
correct. &nbsp;The bottom line is when a disaster occurs hardware and
networks are easily restored or replaceable, intangible assets like
data
are not.<br>
<br>
<b>Transaction Files</b><br>
<br>
Transaction files are the intermediate step that provide both security
and restoration schemes to enable database integrity. &nbsp;Transaction
files keep logs of all the transactions that change the database state.
&nbsp;The transaction files record the transaction before being applied
to
the database, <a href="#figure4">Figure 4</a>. &nbsp;They can also
keep logs
of all access to the database, e.g. reads, database configurations,
etc.
&nbsp;If a system crashes the transaction files primary purpose is to
get
the database back up-to-date if need be by reapplying all the
transactions that occurred since the last full database backup.<br>
</font><span style="font-family: helvetica,arial,sans-serif;"><span
 style="font-weight: bold;"><br>
</span></span><font face="Helvetica, Arial, sans-serif"><b>Reprocessing</b><br>
<br>
If the database cannot be brought back to the previous state, then
reprocessing the transactions is a very common alternative.
&nbsp;Reprocessing is typically done by restoring the database to the
last know state, e.g. the last full backup and then applying
transactions to that database using the Transaction files that occurred
after the full backup. &nbsp;This technique will require the same
computing resources as before and this usually gets most of the
transactions depending on your environment.<br>
<br>
<b>Rollback / Rollforward</b><br>
<br>
This restoration procedure for rollback / roll forward is the entire
database is copied, saved and a log of the transactional changes is
also saved. &nbsp;When a failure occurs a rollforward process restores
the
database from the database copy and all transactions that made changes
to the database are applied, not all transactions are applied. &nbsp;A
rollback process is actually the easiest where all invalid or partially
processed transactions are removed from the database. &nbsp;Then the
transactions
that were rolled back can be reapplied. </font><font
 face="Helvetica, Arial, sans-serif"><b><br>
</b></font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Before / After Images,
transaction files contain records that are of two types, before and
after images. &nbsp;Before image transactions are used to undo
transactions, rollback transactions that were applied to the database.
&nbsp;After image transactions redo the transactions, reapplying the
changes using the rollforward principal.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Checkpoints are
primarily used to speed up the recovery process. &nbsp; Database
transactions that are currently in progress will be given the
opportunity to complete their transaction and all other outstanding
transactions will be placed on a queue waiting for database access.
&nbsp;The transaction file and database will be synchronized and verify
that all outstanding requests have been processed. &nbsp;Then the
checkpoint record is written to the log. &nbsp;When checkpoints are
taken the system overhead is not noticeable to the standard user and
checkpoints should be taken at shorter intervals if many transactions
occur to the database or larger intervals if the database is mostly
static.</font></li>
</ul>
<font face="Helvetica, Arial, sans-serif">If your system has a crash
using the Checkpoints to restore your transactions are much easier.
&nbsp;The system automatically knows where to restart (The checkpoint
record) and what transactions need to be applied. &nbsp;Much faster
than restoring the entire database and applying the transaction files.<br>
</font><font face="Helvetica, Arial, sans-serif"> <br>
</font><font face="Helvetica, Arial, sans-serif"><b>Restoration Example</b><br>
<br>
There are database products on the market that make database
restoration very feasible, e.g. the PROMIS database. &nbsp;PROMIS, see <a
 href="#figure4">Figure 4</a>,&nbsp; would use two servers with two
instances of the data (not mirrored). &nbsp;The system has a primary
and secondary database and a primary and secondary transactional file.
&nbsp;The transaction would be submitted to the primary database from
the primary transaction file but not committed. &nbsp;The secondary
transaction file would also perform the same task to the secondary
database. &nbsp;The databases communicate to the transaction files that
the data was successful and the transaction files confirm with each
other that the transactions can be marked completed and the databases
commit the changes. put after the record on the primary system was
submitted. &nbsp;If at anytime there was an error, rollbacks would
occur. &nbsp;In the event there was a disaster you could always depend
on either site to have the correct and up-to-date data, provided the
systems are not in the same room, building, campus, etc.<br>
</font>
<div align="center"><img src="promis.gif" alt="" title=""
 style="width: 313px; height: 218px;"> <font
 face="Helvetica, Arial, sans-serif"><br>
</font></div>
<div align="center"><font face="Helvetica, Arial, sans-serif"><b><a
 name="figure4"></a>Figure 4 - PROMIS</b><br>
</font> </div>
<font face="Helvetica, Arial, sans-serif"><b>Actual Disaster Recovery
Example<br>
<br>
</b></font><font face="Helvetica, Arial, sans-serif">During the early
1990's I was Manager, Computer Operations for five Semiconductor Wafer
Fabrication facilities (Fabs), Motorola, Mesa, AZ, (ACT, MOS5, MOS6,
MOS17 &amp; MOS21), part of Computer Integrated Manufacturing
(CIM).&nbsp; Computing Hardware was not as reliable as they are today
so this was a major strain for the CIM team.&nbsp; We had two sites, a
Primary and Secondary.&nbsp; The Primary site would perform only the
processing for the actual manufacturing of Semiconductors, nothing
else.&nbsp; The Secondary site would duplicate the transactions that
occurred on the Primary and perform all the overhead processing, e.g.
reporting.&nbsp; <br>
<br>
Instructions were clear to all employees to only trust and rely on the
PROMIS Database, if the process was not followed, this would cause
Scrap of Wafers.&nbsp; There are many steps to produce a wafer
correctly, MOS6 the steps could range between 400 and 600 processing
steps, thus the importance of the Database, taking the Human out of the
loop.&nbsp; The average loss of $1.2 million dollars (1992 - 1994) per
month was MOS6's record, we are not talking minor dollars.&nbsp; The
costs would be much higher if the process was executed by humans.&nbsp;
The reliability of the systems and uptime were also crucial.&nbsp; The
average cost of $50K dollars (1992 - 1994) per hour of actual loss of
profit per fab was also a factor to point out to Motorola, that
reliability and disaster recovery were measures that must be documented
and implemented.<br>
<br>
The CIM group generated a Disaster Recovery Committee (DRC) to
establish, create and execute a Disaster Recovery Plan (DRP).&nbsp; The
DRP was not only a plan but a living document.&nbsp; The DRC generated
the plan and <span style="font-weight: bold;">PROCEDURES</span> in the
event of a disaster.&nbsp; The most important part of the plan, <span
 style="font-weight: bold;">PRACTICE PRACTICE PRACTICE</span>.&nbsp;
Most companies have a DRP, however, that document is shelfed and never
opened again.&nbsp; Our DRP was PRACTICED monthly, the most important
reason why,&nbsp; <span style="font-weight: bold;">Building
Habits.&nbsp; </span>To accomplish this and not affecting production,
CIM had to make the DRP uneventful and invisible to Wafer
Production.&nbsp; This was a very hard task and we were told this
wasn't possible by all the experts, including other Motorola Fabs and
the computer vendor at the time, Digital Equipment Corporation (DEC).<br>
<br>
PROMIS is a database and databases all have the same problem,
Fragmentation.&nbsp; Monthly procedures of defragmenting the database
was common.&nbsp; The process was to take down the Secondary PROMIS
database, defragment, bring back online, catch up the transactions from
the Primary PROMIS database, do a takeover and the Secondary is now the
Primary.&nbsp; Take down the original Primary database, defragement,
bring back online, catch up the transactions and become the new
Secondary database.<br>
<br>
DRC saw that we could take advantage of PROMIS maintenance and create
the DRP, document the processes.&nbsp; The team was ready and our first
test.&nbsp; Getting permission from all fabs, we were given time to
test out our plan.&nbsp; The fabs downloaded all the data they needed
for the next one hour of processing that they needed.&nbsp; This meant
we had 30 minutes to prove out the plan or abort.&nbsp; Yes, there were
glitches, things unforeseen, however, lessons learned and processes
missed were documented.<br>
<br>
The next month, again we were given one hour to perform the test, with
updated documentation and processes, CIM's first 100% successful
disaster plan worked flawlessly.&nbsp; CIM proved that we had the
knowledge, skills and training to perform in the event of a
disaster.&nbsp; This Distaster Plan Process didn't stop, but every
month the Primary and Secondary sites would be switched, seamlessly
without the fabs even realizing what was happening.&nbsp; CIM was also
performing maintenance on the databases without the fabs even
aware.&nbsp; Every month and every switch over built habits and ensured
that if there was an actual disaster, the CIM team could execute
successfully without hesitation.<br>
<br>
A true Disaster Plan must be generated, up-to-date but most of all
PRACTICED, EXECUTED and PERFORMED OVER AND OVER.<br>
<br>
</font><font face="Helvetica, Arial, sans-serif"><b>Security</b><br>
<br>
Databases have always been a major intangible asset to an organization
(business, military, academic). &nbsp;The standalone databases from the
1960's were centrally located by the Information Systems (IS)
department. &nbsp;The only access to these first databases were
terminals, tape drives and you had to be physically in the space to
alter the configurations. &nbsp;The first computer hackers and
break-ins were from internal. &nbsp;The database architecture was
simple (usually flat files) and security was managed by the operating
system, not the database.<br>
<br>
With networks becoming popular in the 1980's computer systems were now
communicating to each other. &nbsp;This also required that security
would require major enhancements because not only are you protecting
the system at your internal site but now you have to protect the data
from others
outside your organization. &nbsp;This was one problem the other was
databases
also grew into more complex architectures. &nbsp;The databases during
this
time were also flat files, Hierarchical Databases and the new
Relational
Database.<br>
<br>
The 1990's with the emergence of the Internet, security concerns grew
to another level. &nbsp;The database's security could be compromised by
anyone anywhere in the world due to anyone having access to the
Internet. &nbsp;The Secure Socket Layer (SSL), HyperText Transfer
Protocol over Secure Socket Layer&nbsp; (HTTPS), Certificates, etc.
have emerged because of the Internet. &nbsp;While these protocols are
providing security for computer systems they are not necessarily
protecting the database.<br>
<br>
<b>Database Management Security</b><br>
<br>
Databases previously used only the operating systems security schemes
to protect the data. &nbsp;When computers started networking using
relational database technologies the database systems started providing
their own
security. &nbsp;Database security adopted many system security concepts
and terminology.<br>
<br>
Usernames and passwords, these were adapted from system security.
&nbsp;To access a database you must provide a database username and
password not the operating system username and password. &nbsp;The
database and system password files are separate entities. &nbsp;There
are tools and utilities where the operating system can pass the
username / password to the&nbsp;</font><font
 face="Helvetica, Arial, sans-serif">Database Management System (DBMS).<br>
</font> <font face="Helvetica, Arial, sans-serif"><br>
Groups also adopted from systems are how you can create a collection of
users. &nbsp;The reason for groups is to make management easier to
maintain. &nbsp;Instead of micro managing accounts you can place people
from one department in the group Department A and the other group in
Department B. &nbsp;When
giving access to database resources all you need to do is give the
group
access, not each individual account.<br>
<br>
Permissions are what protect the database objects from outside access.
&nbsp;Permissions are the limits of who can access the database objects
and what they can do with the objects. &nbsp;<a href="#figure5">Figure
5,</a> illustrates that users, Max and Gus are in group A and Users Bud
and Buddy are in group B. &nbsp;Group A has permission to read the
object,
Group B has permission to read and write to the object and all others
have
no access. &nbsp;When any of these users log into the DBMS they have to
enter their username and password. &nbsp;Once the DBMS attains their
account
information, permissions are given to the particular resources they
have
the appropriate access. <br>
</font>
<div align="center"><img src="permissions.gif" alt="" title=""
 style="width: 313px; height: 198px;"> <br>
</div>
<div align="center"><font face="Helvetica, Arial, sans-serif"><b><a
 name="figure5"></a>Figure 5 - Permissions</b></font><br>
</div>
<font face="Helvetica, Arial, sans-serif"> <br>
Roles are another way to group accounts. &nbsp;Lets say there is
another group called Group C that has a user named Luna. &nbsp;From
Group A Max is the manager, Group B, Bud is the manager and Luna is the
manager for
Group C. &nbsp;All of their roles are manager and the manager has
particular
role that others can't access, e.g. performance evaluations. &nbsp;You
can
create a role called manager. &nbsp;The managers now has access to
update
the evaluations of their group while all other accounts can view their
personal
evaluation only. &nbsp;Today's DBMS system can be configured to provide
a view of the database from the entire database all the way down to a
cell.<br>
<br>
<b>Database Administration</b><br>
<br>
A database is managed by a person usually called the Database
Administrator (DBA). &nbsp;For a large database or data warehouse there
will be several DBA's. &nbsp;The DBA is responsible for the databases
integrity and security. &nbsp;The DBA's role is to provide:<br>
</font>
<ul>
  <li><font face="Helvetica, Arial, sans-serif">Backups, ensuring that
the database is backed up along with the transaction files that have
been checkpointed.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Performance tuning
and enhancements, resource allocations, adjusting system parameters,
assess impact on users.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Schedule maintenance,
take a database down to perform maintenance, e.g. reindexing tables,
modifying data structures.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Security, ensure that
the accounts and permissions are set up correctly, ensure data
integrity.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Managing the database
structures, including the database designs, schema, maintain
documentation, managing change, proactively finding alternatives and
managing the Database Management System software.</font></li>
  <li><font face="Helvetica, Arial, sans-serif">Evaluate and implement
new DBMS features.<br>
    </font> </li>
</ul>
<font face="Helvetica, Arial, sans-serif"><b>Database Design</b><br>
<br>
Database design is more important than most understand. &nbsp;With
a poorly designed database, the application programs are harder to
develop and as time goes on making changes to the database are very
hard and very expensive to change. &nbsp;There are a lot of designs
sent out to early
&nbsp;to meet the corporate schedule with the attitude we can fix the
problem
later. &nbsp;This is a problem that will eventually haunt the business.
&nbsp;Databases are essentially Concrete Objects. &nbsp;Once set in
stone
they are extremely hard to change. &nbsp;The first obstacle is the
database,
changing the schema, this is hard enough but can be done. &nbsp;The
second
and hardest part are all the applications that were written for that
particular
architecture now all have to be changed. &nbsp;This is why when making
a
design, proper planning is essential.<br>
<br>
<b>Documentation</b><br>
<br>
A task sometimes overlooked is documentation. &nbsp;Most databases
have reporting software that can produce schemas, models, e.g.
Entity-Relationship (ER) Diagrams, Unified Modeling Language (UML)
documents. &nbsp;These documents being present in the planning stages
are vital to a successful deployment and maintenance of a database
system.<br>
<br>
</font> <font face="Helvetica, Arial, sans-serif"><b> Summary</b><br>
<br>
</font><font face="Helvetica, Arial, sans-serif"> This weeks discussion
was on database management and the Database Administrator (DBA) roles
and responsibilities. &nbsp;Concurrency Control, Transaction
Management, Lock Management and Cursor types. &nbsp;There are the
processes and configurations that the DBA sets when administering the
database. &nbsp;Database recovery, transaction files, rollback and
rollforward the many processes of restoring a database from an outage.
&nbsp;Finally, the DBA is the person responsible for the performance,
data integrity, security and the documentation of the database.</font><font
 face="Helvetica, Arial, sans-serif"><br>
</font>
<hr size="2" width="100%">
<p><font face="Helvetica, Arial, sans-serif"><b> Discussion Questions:</b>
&nbsp;</font><font face="Helvetica, Arial, sans-serif">Please
read the Discussion Question appendix in the syllabus</font><font
 face="Helvetica, Arial, sans-serif">.<br>
</font></p>
<p><font face="Helvetica, Arial, sans-serif"> Discussion Question 1:
&nbsp;Discuss ACID and the other types of transactions and resource
locks.<br>
</font></p>
<p><font face="Helvetica, Arial, sans-serif">Discussion Question 2:
&nbsp;Discuss the Database Administrators (DBA) roles and
responsibilities.<br>
</font></p>
<font face="Helvetica, Arial, sans-serif">Discussion Question 3: &nbsp;</font><font
 face="Helvetica, Arial, sans-serif"> When and why should a business
invest in Security and Database Recovery Procedures.</font> <font
 face="Helvetica, Arial, sans-serif"><font
 face="Helvetica, Arial, sans-serif"><font
 face="Helvetica, Arial, sans-serif"></font></font><br>
</font>
<hr size="2" width="100%">
<p><font face="Helvetica, Arial, sans-serif"><big>References</big><br>
</font> </p>
<font face="Helvetica, Arial, sans-serif"><a name="text"></a>David M.
Kroenke, Database Processing: &nbsp;Fundamentals, Design &amp;
Implementation, Seventh Edition, 2000<br>
<a name="ado"></a><a href="http://whatis.com">whatis.com</a>,
http://whatis.com, Accessed: &nbsp;17 May 2003</font><span
 style="font-family: helvetica,arial,sans-serif;"><br>
</span>
<hr style="width: 100%; height: 2px;"><span
 style="font-family: helvetica,arial,sans-serif;"></span><img
 src="cv64.bmp" alt="" style="width: 32px; height: 32px;" align="left"><small><font
 face="Arial,Helvetica"><a href="mailto:information@cv64.us">Web
Contact</a></font></small><br>
<small><font face="Arial,Helvetica">
Last modified:&nbsp; 2007 April 21</font></small><br>
<br>
<br>
<br>
<br>
<br>
</body>
</html>
