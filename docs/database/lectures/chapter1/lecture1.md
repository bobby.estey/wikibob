# Unified Modeling Language (UML)

The [Object Management Group's](http://www.omg.org/) website has the current specifications, papers and articles on the [Unified Modeling Language](https://en.wikipedia.org/wiki/Unified_Modeling_Language). UML was founded by three authors known as the "Three Amigos (Booch, Jacobson, Rumbaugh)".  UML is a modeling language that focuses on the conceptual and physical representation of a system.  UML is used for both analysis and design of software and produces several types of artifacts required for a software project, e.g. requirements, architecture, design, source code, project plans, tests, prototypes and releases.  Grady Booch's original work described objects and their relationships, James Rumbaugh provided the Object-Modeling Technique (OMT) and Ivar Jacobson provided the use case methodology approach.  Together these founding authors and others combined their work into what is known today as UML.   UML 1.0 was offered for standardization to the Object Management Group (OMG) in January 1997.

The UML has the following standard diagrams and we will go into each of these in this order throughout the course:

**Use Case Diagram - this is the first diagram used when building a system.  The Use Case Diagram contains the Actors (interacts with the system, such as humans, computer systems, databases, etc.  Actors take on different roles depending on what the goal is at any given moment, drawn as a stick figure) and Use Cases (sequence of actions that provide something of measurable value, drawn as a horizontal ellipse) and the relationships between them.  The Use Case Diagram the most important of all UML diagrams enables both customers, stakeholders, designers and developers to visualize how a software project will be constructed and what is expected from all stakeholders.**

Basics about Use Cases:
- Use Cases accomplishs a goal for a  problem
- Use Case Names should start with an action verb
- Use Cases have distinct start and stop points
- Use Cases may have pre and post conditions
- Use Cases always have a flow of events
- Use Cases may have a secondary actor to accomplish the goal

![Figure 1 - Use Case Diagram](useCase.jpg)
 
**Please see an example of a Use Case Scenario below**

**Activity Diagram - models the logic of a use case.  Activity Diagrams enable you to depict both standard and alternate flows of a program.  Activity diagrams can also be drawn that cover several use cases or a small portion of a use case.  You can also use activity diagrams without use cases being involved at all, e.g. a larger business process.  Remember Activity diagrams while providing simular services like the Sequence Diagram show how the logic flows, not the specific sequence between objects.**

![Figure 2 - Activity Diagram](activityDiagram.jpg)
  
**Sequence Diagram - validate the logic of a scenario, usually the scenarios that were developed from a use case.  Sequence Diagrams provide a way to visualize the operations defined by your classes and detect bottlenecks within an object-oriented design.  The Sequence Diagram is a popular diagram because they are the preliminary diagrams used just before the Class Diagrams and identify all the Interfaces, Messaging required for the software program.  Sequence Diagrams have the objects aligned horizontally with lines going down from the object.  Messaging is displayed by drawing a line between the object lines, messaging is displayed in order from top to bottom.**

![Figure 3 - Sequence Diagram](sequenceDiagram.jpg)

**Communication Diagram - formerly known as the Collaboration Diagram is very simular to the Sequence Diagram, both visually and functionality.  The only difference is each object is defined in a square and all messaging is defined using arrows with numbers identifying the message sequence.**

**Class Diagram - this diagram contains the class icon, which is a rectangular shape with three sections.  The top section is the name of the class.  The middle section contains the attributes (variables, states) of the particular Object.  The bottom section is the methods (procedures, behavior) of the Object.  Class diagrams are developed primarily for the developer to write software that conforms exactly how to build class files in Object-Oriented languages, e.g. Java, C++.  Class diagrams also show inheritance and relationships between different classes.**

![Figure 4 - Class Diagram](classDiagram.jpg)
 
Additional notes on Class diagrams, stereotypes.  These are used to categorize the type of class your diagramming.  There are three very popular stereotypes, Bounday, Entity and Controller.
The Boundary stereotype, represents the interface between the use case and the actor.  When an actor interacts with the use case, this would be through a boundary class.  There should be at least one for every actor – use case interaction.
The Entity stereotype represents classes that are persistent.  They would actually represent the tables, records, etc. in the database.
The Controller stereotype represents classes that delegate work.  They don’t actually carry out true functionality and manage the use case.  If a class sends a message to it, it would call another class.

**State Diagram - this diagram helps the developer to understand the details, modes and transitions that an Object can go through with the occurrence of any event or action.  State diagrams (also called State Chart diagrams) are used to help the developer better understand any complex/unusual functionalities or business flows of specialized areas of the system.  State diagrams depict the dynamic behavior of the entire system, sub-system, or a single object in a system.   State diagrams consist of states, transitions, events and activities.**

![Figure 5 - State Diagram](stateDiagram.jpg)
 
**Component Diagram - models the high-level software components and the interfaces to those components.  Once the interfaces are defined, component diagrams organize the development effort between subteams.  When there are new requirements or changes to the system design, the Component Diagram enables the appropriate implementation between teams.**

**Deployment Diagram - this diagram shows the hardware, the software that is installed and the middleware used to connect the disparate nodes.  Deployment Diagrams are a static view of the run-time configuration of processing nodes and the components that run on those nodes.  Deployment Diagrams are used for applications that are deployed on several machines, e.g. Enterprise Software Applications using Java 2 Enterprise Edition (J2EE).**

![Figure 6 - Deployment Diagram](deploymentDiagram.gif)
 
# Databases

- Databases have been a popular medium for accessing, managing and storing data.  There are several types of databases Network, Hierarchial, Relational, Object-Oriented,  Object-Relational, etc.  A DBMS must support persistence, secondary storage management, concurrency, recovery and an ad hoc query facility.
- Relational (RDBMS) - the relational database was invented by E. F. Codd in 1970 and is the most common type of database that provides access and organization of data using tables.  Tables are row / column (two dimensional) structures that contain data or reference other tables, tables are sometimes called relations.  Relational databases use the Structured Query Language (SQL) to gather information from the database.
- Object-Oriented (OODBMS or ODBMS) - these databases have become popular with the acceptance of Object-Oriented Programming Languages (OOPL).  OOPL trace their roots back to Simula in 1962 but didn't catch on until Smalltalk in 1980, C++ a few years later and then finally Java in 1995.  With the reemergence of OOPL the data has changed from a state only database, e.g. relational to state and behavior database, e.g. Object-Oriented.  OODBMSs are required to be a DBMS and Object-Oriented meaning the system must support complex objects, object identity,  encapsulation, types or classes, inheritance, overriding combined with late binding, extensibility and computational completeness.
- Object-Relational (ORDBMS) - traditional relational databases have been converted to the new Object Relational Database Management Systems.  This is a combination of both Relational and Object-Oriented practices that allow scalability and capabilities of both technologies.  ORDBMSs provide both a relational or Object-Oriented interface to the relational backend database.  Most relational providers have already released their versions of ORDBMS, e.g. Oracle, IBM.

# Summary

- The Unified Modeling Language (UML) presents both data and behavior using an Object-Oriented architecture.  UML covers the entire lifecycle of a project thus the popularity and world acceptance of the model.  UML is a model that inherited and encapsulates the best of previous models into a single product.
- We have also discussed briefly the differences between the database types, Relational, Object-Oriented and Object-Relational.  Relational still has market share but will be passed up with the Object-Relational type.

## Discussion Questions:  Please read the Discussion Question appendix in the syllabus.

- Discussion Question 1:    Discuss at least two of the Unified Modeling Language's diagrams in detail.
- Discussion Question 2:    Which model have you seen used more Entity-Relationship Data Model, Unified Modeling Language or another model?  Why do you think this model is being used currently.  (Please remember, no proprietary / classified discussions).
- Discussion Question 3:    Discuss which model Entity-Relationship Data Model or Unified Modeling Language in your opinion is the best model and why.

## Individual Assignment

- Write a 2 - 3 page paper (at least 300 words per page, Arial Font, Size 10) that explains in detail the topic of discussion.  Make sure your resources are declared.
- Object-Oriented Analysis, Design and Programming are now popular today.  However, most of our databases are Relational.  Discuss your research / findings on the database future.  
- In your opinion, are databases going to transition from Relational to Object-Oriented, become a hybrid solution or some other alternative.  What types of analysis, design and development are being targeted in the future?

# Acronyms

|Acronym|Description|
|--|--|
|ER|Entity Relationship|
|OMG|Object Management Group|
|OODBMS|Object-Oriented Database Management System|
|OOPL|Object-Oriented Programming Language|
|ORDBMS|Object-Relational Database Management System|
|RDBMS|Relational Database Management System|
|SQL|Structured Query Language|
|UML|Unified Modeling Language|

# References

- UML, http://www.handaweb.com/anthony/portfolio/emarketplace/usecase.html, Accessed:  20 March 2003
- UML, http://www.visual-paradigm.com/UseCaseModeling/, Accessed:  20 March 2003
- UML, http://www.developer.com/design/article.php, Accessed:  20 March 2003
- E.F. Codd, "Extending the Relational Model to Capture More Meaning, "ACM Transactions on Database Systems, December 1976, pp. 397 - 424
- Cardinal, http://whatis.techtarget.com/definition/0,,sid9_gci214506,00.html, Accessed:  20 March 2003
- Cardinality, http://whatis.techtarget.com/definition/0,,sid9_gci498885,00.html, Accessed:  20 March 2003
- Grady Booch, James Rumbaugh, Ivan Jacobson, "The Unified Modeling Language User Guide", ISBN 0-201-57168-4, 1999, p: xx, 15, 16
- Object Management Group, http://www.omg.org/, Accessed:  11 May 2003
- Object-Relational Database Systems - The Road Ahead, http://www.acm.org/crossroads/xrds7-3/ordbms.html, Accessed:  25 June 2003


# Get Credit Card Information - Use Case / Scenario Example

- Description
     - The Get Credit Card Information Use Case defines the process of how a customer submits their credit card to the merchant

- Actors
     - Customer
     - Merchant

- Pre Conditions
     - Customer must have a credit card or cash
     - Customer has already decided on an item(s) to purchase

- Basic Flow
     1.  This use case begins with a customer performing a checkout, ordering the items that were selected
     2.  The customer is presented a payment form, e.g. credit card data
     3.  The customer fills out the credit card data
     4.  The customer is presented a mailing and billing form

- Alternate Flow
     A1-1.  At basic flow step 3, the customer wants to pay cash on delivery (COD)
     A1-2.  The customer is presented a COD form.
     A1-3.  Continue at basic flow 4.

- Exceptions
     E1.  At any step, an error occurs, this use case ends
     E2.  At basic flow step 4, the customer's fails to fill out the form correct, return to basic flow 3.  After three (3) tries, this use case ends.
     E3.  At alternate step A1-2, the customer's fails to fill out the form correct, return to alternate step A1-1.  After three (3) tries, this use case ends.

- Post Conditions
     - The customer's credit card and billing information have been taken.

- Notes
     - A customer is a non-employee of the merchant company.

- Owners
     - (U)  Author:  Robert Estey (estey@cv64.us)
     - (U)  Organization: cv64.us
     