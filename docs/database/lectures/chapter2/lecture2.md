# Database Application Design

This page discusses the fundamentals of Database Application Design and the primary steps Processing Views, Formatting Views, Enforcing Constraints, Security & Control and Application Logic.  Table 1 illustrates the Database Application design process.

|Processing Views|Formatting Views
|--|--|
|- Create|- Forms|
|- Read|- Reports|
|- Update|- Inter-process|
|- Delete|- OLAP cubes|

|Enforce Constraints|Provide Security and Control|
|- Domains|- Execute Application Logic|
|- Uniqueness||
|- Relationship Cardinality||
|- Business Rules||

Table 1 - Database Application Design

# Semantic Object Views

- Semantic Object Views or Views are structures of data and how an object is accessible to an outside source, including the attributes.  This is a common practice in databases today.  A view that has been initialized with data is known as a view instance.  One example would be a customer can log on a website with their personal login name and password.  Then the system will display attributes that only the customer should have access to view.  While being in the same database the vendor can only work on system settings and not look into others personal information.  Views are structured attributes that are dynamically easy to setup and can provide an indirect form of security.  The simplest way of thinking of a view is executing one or more Structured Query Language (SQL) statements that produce a structure for later use.  Figure 1 shows that the customer and vendor information are separate from each other, however, the corporate view encapsulates both customer and vendor.  Notice that the corporate view is also limited and doesn't have total access to the database.

![Figure 1 - Database Views](databaseViews.gif)

# Processing Views

- There are four processing types of views.
     - Creating views is performed by retrieving values usually through an interface, e.g. forms, other programs.  Then SQL statements are executed and placed in the new view.
     - Reading views is performed by executing one or more SQL statements and place the results into a view.
     - Updating views contain several types of transactions.  Updating views can involve simple changes, e.g. changing address or sales tax for every record.  Modifying relationships between tables.  Adding additional rows.
     - Deleting views is removing rows from tables that make up the view.

# Formatting Views

     - Kroenke discusses that forms are screen objects used to enter data into a database.  They can also be used for retrieving query information.  A form is a Graphical User Interface (GUI) Widget container that encapsulates data from a client, formats the data for transmission usually bound for a server.  Again, a form can post or retrieve data.  Forms should represent the actual view that you designed in the database.  Our example from Figure 1 would have 3 views, customer, vendor and corporate.  The customer view would only contain information that are relevant to the customer, e.g. items purchased, accounts payable, etc.
     - Forms should be well thought out before producing.  A badly planned form can cause performance issues both as wasted computer and human resources.  When designing a form you want to keep all the related information in one contiguous section.  This not only helps visibly to the client but the programmer can easily tie the back end database to the front end form.  Use the appropriate widgets, e.g. pull downs versus lists and radio buttons versus checkbooks.  Even the littlest concern of tab order can be detrimental on a form.
     - There are always exceptions to the rule.  Sometimes placing fields on a form just doesn't make any logical sense. One example would be to place a grand total for all items purchased in the middle of a form.

# Report Views

     - Reports are similar to forms, however, reports can only retrieve data and then produce information as output.  Forms are bi-directional where as reports are unidirectional (output only).  Reports like forms do contain some calculated data versus the same data stored in a database as an attribute.

# Enforcing Constraints

     - Enforcing Constraints is a technique no only keeping your data cleansed but also helps make a developers job a lot easier.  Let's use a light bulb for an example.  The bulb can only be on or off and these are the only values that we will accept.  If someone was to enter data into the database they could accidentally type any character as input.  If they didn't understand the form and placed a 1 for on and 0 for off (which are the actual values) this might be stored correctly.  If you force constraints on the form that they can only enter "yes" or "no" then your assured that the database has the correct data.  True this form could have been fixed with a 2 selection radio box, e.g. on | off, which is another form of constraint.
     - Where to put constraints depends on the type of program.  If the entire system needs the constraint then you would use the Database Management System's (DBMS) constraint features.  If only the program executing needs the constraint then define the constraints locally.  Setting up global constraints is not only easier to manage but  also is better for performance.  The global constraints are only checked once where as the local constraints are always checked anytime they are executed.  The DBMS system also was developed with database performance, GUIs already installed and uses general business rules.  The local program is written for specific purposes, no really concerned about how the data is being managed, more concerned about the results.  

# Constraint Types

     - Domain Constraints are a set of values a constraint may have, e.g. person's address: 1313 Mockingbird Lane, 50 or less characters.
     - Required Constraints are fields that are required to process data.  Without this data, the reports we produce would be invalid.
     - Uniqueness Constraints ensure that duplicate data are not entered into the database.  An example would be a database that only contained a first and last name as the primary key, duplicates records are possible.  Using this database structure we have two people, Mary A Smith and Mary B Smith.  Having only those two fields for their name would cause a duplicate record error.  Adding a third name column would make the records unique.
     - Relationship Cardinality Constraints are matching the relationships that were designed originally.
     - Business Rules Constraints are matching procedures and policies of an organization.

# Security and Control

     - Most DBMS have security of which they follow similar standards, 128 bit encryption, username, password, etc.  Horizontal Security is limiting access to certain rows (records) and is usually obtained through application programs.  An example, a department manager can only view their direct reports and subordinates.  The department manager can't view outside of their organization.  Vertical Security is limiting access to certain columns (fields) and is usually obtained through the DBMS.  An example, a department manager can view there subordinates name, pay grade, pay rate the department manager cannot view the subordinates social security number, home address, etc.
     - Control is how the client can manage the DBMS usually done through GUIs.  To set up security, granting & revoking access and performance tuning are a few of several options using the control feature of a DBMS.
     - Then there is Data Perturbation which is out of scope and if you wish to study more about this subject please go to the Internet.  Data Perturbation is, Information attained without exposing the data.  Inference is a technique to get information indirectly.

# Application Logic

     - Application Logic is the site specific requirements being solved.   A program designed to perform a specific function directly for the client or another program.

# Summary

     - This page discussed briefly the fundamentals of Database Application Design and the primary steps Processing Views, Formatting Views, Enforcing Constraints, Security & Control and Application Logic.  These are the foundations of Database Application Design.

# Discussion Questions:  Please read the Discussion Question appendix in the syllabus.

     - Discussion Question 1:  Why has the use of databases increased dramatically?  I your own opinion where do you see the future of database design and development, e.g.  are databases moving more to a centralized role?  What type of new architectural and design tools are being implemented.  What languages are going to be the leaders in database technology, e.g. SOAP
     - Discussion Question 2:  Discuss what make a good form, what was the author trying to convey, e.g.
          - the form structure should reflect the view structure
          - the semantics of the data should be graphically evident
          - the form structure should encourage appropriate action
          - cursor movement and pervasive keys
     - Discussion Question 3:  Discuss some of the security schemes that are possible for a database both for a standalone database and web enabled.

# Individual Assignment

     - Write a 2 - 3 page paper (at least 300 words per page, Arial Font, Size 10) that explains in detail the topic of discussion.  Make sure your resources are declared.
     - Why are databases today requiring extensive planning and architectural design?  Include performance, security, availability, ease of use, standards and other attributes on this question.

# Acronyms

|Acronym|Description|
|--|--|
|DBMS|Database Management System's|
|UI|Graphical User Interface|
|OLAP|On Line Analytical Processing|
|SQL|Structured Query Language|

# References

- David M. Kroenke, Database Processing:  Fundamentals, Design & Implementation, Seventh Edition, 2000
- OLAP, http://searchdatabase.techtarget.com/sDefinition/0,,sid13_gci214137,00.html, Access 18 April 2003
- Widget, http://whatis.techtarget.com/definition/0,,sid9_gci213364,00.html, Accessed 15 April 2003
- Data Perturbation, http://databases.about.com/library/weekly/aa111702a.htm, Accessed 18 May 2004
