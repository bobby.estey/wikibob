create database if not exists cv64;
show databases;
use cv64;

show tables;
drop table if exists example;

create table example (primaryIndex varchar(5), name varchar(20));
insert into example (primaryIndex, name) values ('1', 'bobby');
