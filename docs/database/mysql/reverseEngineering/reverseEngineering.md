# MySQL Reverse Engineering (RE)

- This page explains how to Reverse Engineering (RE) a MySQL database.

## Process

- Open up MySQL Workbench
- Select Database -> Press Reverse Engineering...
- Select [Stored Connection](reveseEngineering1.png)
- Press [Next](reverseEngineering2.png)
- Select Schema, Press [Next](reverseEngineering3.png)
- Retrieve and Reverse Engineering Schema Objects, Press [Next](reverseEngineering4.png)
- Select Objects to Reverse Engineering, Check Place imported objects on a diagram, Press [Execute](reverseEngineering5.png)
- Reverse Engineeringing Progress, Press [Next](reverseEngineering6.png)
- Reverse Engineeringing Results, Press [Close](reverseEngineering7.png)
- [EER Diagram](reverseEngineering8.png) is displayed
