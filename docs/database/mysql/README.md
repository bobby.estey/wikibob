# MySQL / MariaDB

- MySQL Relational Database (RDBC) was an awesome database until Oracle purchased in 2010
- The pioneers and creators of the MySQL Relational Database (RDBC) did not like the direction or decisions for the future of MySQL
- Thus MariaDB was developed.

## Documentation

- [Ubuntu - Reset root password](https://studygyaan.com/blog/reset-root-mysql-password-on-ubuntu)
- [MySQL Reset root password](https://dev.mysql.com/doc/refman/8.4/en/resetting-permissions.html)

## Videos

- [Ubuntu - Reset root password](https://www.youtube.com/watch?v=L86Z4r4orUs)
- [JDBC Example Mac](jdbcExampleMac.mp4)
- [MariaDB setup](mariadb.mp4)
- [bind variables](https://youtu.be/MnISfllmK74)

## Installation

- [how to install MariaDB](https://linuxize.com/post/how-to-install-mariadb-on-ubuntu-18-04/)
- [default password to MariaDB](https://stackoverflow.com/questions/20270879/whats-the-default-password-of-mariadb-on-fedora)
    - quick answer:  leave blank
- [reset root password](https://www.digitalocean.com/community/tutorials/how-to-reset-your-mysql-or-mariadb-root-password)
- [SQLDeveloper connection to MySQL / MariaDB](https://alvinbunk.wordpress.com/2017/06/29/using-oracle-sql-developer-to-connect-to-mysqlmariadb-databases/)
- [MariaDB maven repository](https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client)
- [MySQL maven repository](https://mvnrepository.com/artifact/mysql/mysql-connector-java)
- HeidiSQL: sudo snap install heidisql-wine --beta

### Apache Installation

- sudo apt update
- sudo apt install apache2
- sudo gedit /etc/apache2/apache2.conf
     - last line append:  ServerName 127.0.0.1
- sudo apache2ctl configtest
     - output should be "Syntax OK"
- sudo systemctl restart apache2
     - verify Apache is working:  http://localhost
     - [Apache2 Ubuntu Default Page](apache2UbuntuDefaultPage.png)

### Remove MySQL

```
sudo systemctl stop mysql
sudo apt-get purge mysql-server mysql-client mysql-common mysql-server-core-* mysql-client-core-*
sudo apt-get remove --purge mysql*
sudo rm -rf /etc/mysql /var/lib/mysql
sudo apt-get autoremove
sudo apt-get autoclean
```

### Remove and reinstall MySQL 

```
sudo apt-get remove --purge mysql*
sudo apt-get purge mysql*
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt-get remove dbconfig-mysql
sudo apt-get dist-upgrade
sudo apt-get install mysql-server
```

### set plugin to mysql_native_password and password to blank

```
mysql -u root -p
mysql -h 127.0.0.1 -P 3306 -u <user> -p <database>
mysql> select user, host, plugin from mysql.user;
mysql> update user set plugin='mysql_native_password' where user='root';
mysql> alter user 'root'@'localhost' IDENTIFIED BY '';
mysql> flush privileges;
mysql> exit;
service mysql restart
```

### MySQL Shell (mysqlsh)

- [mysqlsh](https://dev.mysql.com/doc/mysql-shell/8.0/en/mysqlsh.html)

- Install mysqlsh 
     - sudo apt-get install mysql-shell
     - sudo snap install mysql-shell

|Command|Description|
|--|--|
|mysqlsh|Execute MySQL Shell|
|\sql|Change to SQL commands|
|\connect cv64@localhost:3306|Connect to the cv64 database on port 3306|
|\use cv64|Utilize cv64 database|
|select * from user_details;|Enter SQL commands|
|exit|exit out of mysql|
|Control-D|exit out of mysqlsh|

```
Example Commands:

mysqlsh
\connect sqluser@localhost:3306
\sql
show databases;
show tables;
use cst339;
show tables;
select * from cst339.ORDERS;
exit
<CTRL-D>
```

- [mysqlsh example](mysqlsh.txt)

### MySQL Workbench


```
sudo snap install mysql-workbench-community
```

#### TOOK ME FOREVER TO FIND THIS - disable the app armor


```
sudo snap connect mysql-workbench-community:password-manager-service :password-manager-service
```

### PHP working with MySQL

- sudo apt install php libapache2-mod-php php-mysql
- sudo gedit /etc/phpmyadmin/config.inc.php
     - uncomment two lines with the following:  ```$cfg['Servers'][$i]['AllowNoPassword'] = TRUE;```
- sudo systemctl restart apache2
- sudo systemctl status apache2
- apt-cache search php- | less
- sudo gedit /var/www/html/info.php
     - add test php code -> <?php phpinfo(); ?>
- sudo systemctl restart apache2
     - http://127.0.0.1/info.php
     - [PHP Info Test](phpInfo.png)
     
### phpMyAdmin Installation

- sudo apt install phpmyadmin
- Configure database for phpmyadmin with dbconfig-common? Yes
- enter password, <cr>
- sudo cp /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
- sudo a2enconf phpmyadmin
- sudo service apache2 restart
- [http://127.0.0.1/phpmyadmin/](phpMyAdmin.png)

## Notes / Troubleshooting

|Description|Syntax|
|--|--|
|Reset Password|alter user 'root'@'localhost' identified with mysql_native_password by \'password\';|
|Reset Password|alter user 'root'@'localhost' identified by \'password\';|
|Install MariaDB|sudo apt install mariadb-server
|Start MariaDB|sudo mysqld & (run in background)|
|Start MariaDB|sudo systemctl restart or start|
|MariaDB Status|sudo systemctl status mariadb|
|Login MariaDB|mysql -u root -p|
|Login MariaDB|mariadb -u root -p|
|Start HeidiSQL|/snap/bin/heidisql-wine &|
|Set Timezone|SET @@global.time_zone = '+00:00'; SET @@session.time_zone = '+00:00'; SELECT @@global.time_zone, @@session.time_zone;|

### Reset root password (Approach 1)

```
sudo /etc/init.d/mysql stop
sudo mkdir /var/run/mysqld
sudo chown mysql /var/run/mysqld
sudo mysqld_safe --skip-grant-tables&
sudo mysql --user=root mysql
UPDATE mysql.user SET authentication_string=null WHERE User='root';
FLUSH PRIVILEGES;
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Password@123';
FLUSH PRIVILEGES;
exit;
sudo killall -KILL mysql mysqld_safe mysqld
sudo /etc/init.d/mysql start
mysql -u root -p
```

### Reset root password (Approach 2)


```
mysql --version
sudo systemctl stop mysql.service
sudo systemctl status mysql.service
sudo systemctl set-environment MYSQLD_OPTS="--skip-networking --skip-grant-tables"
sudo systemctl start mysql.service
sudo systemctl status mysql.service
sudo mysql -u root
mysql> flush privileges;
mysql> use mysql;
mysql> alter user 'root'@'localhost' identified by '<newPassword>';
mysql> quit;
sudo systemctl unset-environment MYSQLD_OPTS
sudo systemctl revert mysql
sudo killall -u mysql
sudo systemctl restart mysql.service
sudo mysql -u root -p
enter new password
```

## Log Example

* 2020-07-16  7:59:13 0 [Note] mysqld (mysqld 10.4.13-MariaDB-1:10.4.13+maria~eoan-log) starting as process 62336 ...
* 2020-07-16  7:59:13 0 [Note] InnoDB: Using Linux native AIO
* 2020-07-16  7:59:13 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
* 2020-07-16  7:59:13 0 [Note] InnoDB: Uses event mutexes
* 2020-07-16  7:59:13 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
* 2020-07-16  7:59:13 0 [Note] InnoDB: Number of pools: 1
* 2020-07-16  7:59:13 0 [Note] InnoDB: Using SSE2 crc32 instructions
* 2020-07-16  7:59:13 0 [Note] mysqld: O_TMPFILE is not supported on /tmp (disabling future attempts)
* 2020-07-16  7:59:13 0 [Note] InnoDB: Initializing buffer pool, total size = 256M, instances = 1, chunk size = 128M
* 2020-07-16  7:59:13 0 [Note] InnoDB: Completed initialization of buffer pool
* 2020-07-16  7:59:13 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
* 2020-07-16  7:59:13 0 [Note] InnoDB: 128 out of 128 rollback segments are active.
* 2020-07-16  7:59:13 0 [Note] InnoDB: Creating shared tablespace for temporary tables
* 2020-07-16  7:59:13 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
* 2020-07-16  7:59:13 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
* 2020-07-16  7:59:13 0 [Note] InnoDB: Waiting for purge to start
* 2020-07-16  7:59:13 0 [Note] InnoDB: 10.4.13 started; log sequence number 107969; transaction id 157
* 2020-07-16  7:59:13 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
* 2020-07-16  7:59:13 0 [Note] Plugin 'FEEDBACK' is disabled.
* 2020-07-16  7:59:13 0 [Note] InnoDB: Buffer pool(s) load completed at 200716  7:59:13
* 2020-07-16  7:59:13 0 [Note] Server socket created on IP: '127.0.0.1'.
* 2020-07-16  7:59:13 0 [Note] Reading of all Master_info entries succeeded
* 2020-07-16  7:59:13 0 [Note] Added new Master_info '' to hash table
* 2020-07-16  7:59:13 0 [Note] mysqld: ready for connections.
* Version: '10.4.13-MariaDB-1:10.4.13+maria~eoan-log'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  mariadb.org binary distribution

## Bind variable

* parsing
    - setting up the query, memory management, process creation, all the overhead, etc.  very expensive
    - is the query systematically correct
    - is the statement correct
        - columns, tables, grants, views, etc.
        - optimizer - the best approach to execute the query |

* bind variable
    - reuse the parsing
    - temporary variable as place holders, later replaced with values
    - utilize a query multiple times
        - sql - bind variable are defined with the := character
        - java - PreparedStatement, CallableStatement
        