# k6 page

# References

- [k6 Website](https://k6.io/)
- [Tony Teaches Tech k6](https://www.youtube.com/watch?v=ukoC319npUw)

# Installation k6

- sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 379CE192D401AB61
echo "deb https://dl.bintray.com/loadimpact/deb stable main" | sudo tee -a /etc/apt/sources.list

- sudo apt-get update

- sudo apt-get install k6
     - if this doesn't work try:  snap install k6
     
# Test Programs

|Program|Link|
|--|--|
|First k6 program|[firstK6.js](./k6/scripts/firstK6.js)|
|Complex k6 program|[complexK6.js](./k6/scripts/complexK6.js)|

# Execution Examples

|Command|Description|
|--|--|
|k6 run script.js|execute load test on file firstK6.js with one virtual user|
|k6 run --vus 10 --duration 30s firstK6.js|execute load test on file firstK6.js with 10 virtual users for 30 seconds|
|k6 run complexK6.js|execute load test on file complexK6.js ramping up from 0 to 100 virtual users in 15s, run 100 virtual users for 30 seconds and then ramp down from 100 to 0 users in 15s|
