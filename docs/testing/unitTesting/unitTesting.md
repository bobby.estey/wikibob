# Unit Testing

# Terminology

|Term|Description|
|--|--|
|act|call method to test|
|arrange|setup project and variables, mocking|
|assess|results as expected|
|beforeEach(async(() =>||
|beforeEach(() =>||
|compileComponents|compiles components HTML and CSS|
|debugElement|the components rendered HTML|
|emit||
|expect|expectation of the test|
|describe|what you are testing|
|fdescribe - test the specific description|
|fit|test the specific test|
|fixture|test environment for the component and provides access to the component|
|fixture.detectChanges()|before testing check for changes|
|it|test block, also known as spect|
|mock|instance (object) created to simulate a classes behavior used for testing purposes, e.g. a fake class with the same method signature as the real class|
|spy|- Jasmine spies provide custom return values, stub and check functions|
|TestBed|specific tests of the component|
|toBeTruthy()|something evaluates to true, not the primitive true value|

# Jasmine - JavaScript Unit Testing

- Jasmine - protractor.conf.js - (e2e directory) end to end testing within the browser, decoupled from main application code

# Karma - Angular Unit Testing

- Karma - karma.conf.js - sets up a web server and tests code automatically once code is modified
- src/test.ts - used by Karma to set up the spec files
- app.component.spec.ts - since we generated our project prior to Testing was available we need to create a new app.component.spec.ts file, e.g.  ng g component AppComponent --spec-only

# Karma Testing
- Press DEBUG will bring up DEBUG RUNNER and Router: 
- Component toBeTruthy passed

![Karma](karma.png)
![Jasmine](jasmine.png)

# Angular Unit Testing Configuration and Execution
|Angular Unit Testing Configuration and Execution||
|--|--|
|add JQuery and Bootstrap<br>- karma.conf.js|files: [<br>'./jquery.min.js',<br>'./bootstrap.min.js'<br>],|
|component.spec.ts|import MODULES both at the beginning of the file and in beforeEach|
|- cd ~/git/perfectprocure-ui/NewUI<br>- ng test|- change directory where the configuration files are located<br>- compiles the test and brings up Karma|

# Definitions inside package.json

- npm run test --code-coverage --watch  (default true) otherwise --watch=false
- ng test --code-coverage

# Code Coverage

- located in the project folder coverage
- open up coverage/index.html to review code coverage

|Term|Definition|
|--|--|
|Functional|Method Testing|
|Statement|Statement, e.g. if (x > y)|
|Branch|Conditional Testing, e.g. if/then/else, case, various combinations of paths|
|Line|Line of Code|

# Testcases

- describe(string, function)
    - string - explain the testing suite, "Component Name Test Suite"
    - function - the function / method being tested
    - can contain nested describes
    - container for tests within the describe suite
    
- its(string, function)
     - string explain the test, "should do ..."
     - function - the function / method being tested
     - implementation of the testcase

# Sample Testcase

```
describe('Testing Angular or React Program', () => {

	// execute the beforeEach function, setting up each test
	beforeEach(() => {
		let someVariable = 'abc';
	}

	// execute the afterEach function, closing, cleaning up after each test
	afterEach(() => {
		let someVariable = '';
	}

	// example of a test
	it('testing someVariable is not null', () ={
		expect(someVariable).toBe('abc');
	});
});
```
