# Cascading Style Sheets (CSS) Properties

- 2015 [Custom Properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties) were added to CSS.  Please select the link and read further details.

WHAT DO I REALLY LIKE ABOUT THIS SITE ARE THE DEFINITIONS :-)  element <- Many people interchange the words TAGS and ELEMENTS.  This is incorrect, the correct term is ELEMENT.

What are Custom Properties, they are simply defined attributes that you can reuse within CSS.  Instead of repeating a value, a property is in place.  First example of a custom property starts with two dashes, then the property and value, for example:

```
--myProperty: myValue;
```

The site provides the overall syntax of "root" - root contains property definitions throughout the CSS file, here is an example of three property definitions:


## [CSS](properties.css)

- Looking at the properties.css file the root properties are defined as follows, the team colors for various Universities are defined and width ,height, font, font-size are also defined:

```
/* root CSS properties */
:root {
  --nebraska-bg-color: #E41C38;
  --nebraska-fg-color: #FDF2D9;
  --gcu-fg-color: #FFFFFF;
  --gcu-bg-color: #522398;
  --ua-fg-color: #CC0033;
  --ua-bg-color: #003366;
  --asu-fg-color: #FFC627;
  --asu-bg-color: #8C1D40;
  --main-width: 400px;
  --main-height: 50px;
  --main-font: arial;
  --main-font-size: 36px;
}
```

- Looking at the class definitions, note the variables referencing the root CSS properties, e.g. font: var(--main-font);  This is replaced with the property 36px.

```
/* Nebraska Huskers */
.nu {
  color: var(--nebraska-fg-color);
  background-color: var(--nebraska-bg-color);
  width: var(--main-width);
  height: var(--main-height);
  font: var(--main-font);
  font-size: var(--main-font-size);
}
```

## [HTML](properties.html)

- The HTML references the nu class that is defined in the CSS file.

```
<textarea class="nu">     Nebraska Huskers</textarea>
```

## Final Display

![Final Display](properties.png)
