# Cascading Style Sheets (CSS)

- There are 3 approaches to CSS insertions (Inline, Internal and External)
- Below are several examples of each and the [pros and cons](#insertion-types) below

## Inline

- Style attribute inside HTML elements
- Specific element style definition

#### inline.html

```html
<body>
	<h1 style="color: red; text-align: center;">Inline Style</h1>
</body>
```

## Internal

- Style element in the head section
- Many elements can share the same styles

#### internal.html

```html
<head>
  <style>
    h1 {color: red; text-align: center;}
    table td {border-style: solid; border-width: thin; color: red;}
  </style>
</head>
```

## External 

- Link element to an external CSS file
- Many elements can share the same styles

#### external.html

```html
<head>
  <link rel="stylesheet" href="styles.css">
</head>
```

##### styles.css

```css
h1 h4 {color: red; text-align: center;}
h2 h5 {color: white; text-align: center;}
h3 h6 {color: blue; text-align: center;}
table td {border-style: solid; border-width: thin; color: red;}
```

## Insertion Types

|Insertion<br>Type|Pros|Cons|
|--|--|--|
|Inline|- Simple pages and small projects<br>- Overrides all forms of styles for a specific element|- Less Maintainable<br>- Burdensome on Updates<br>- No Reuse<br>- Specific element styles increases size and performance degradation|
|Internal|- Simple pages and small projects<br>- Styling for a Single Page|- Less Maintainable<br>- Burdensome on Updates<br>- No Reuse|
|External|- Complex Architectures<br>- PREFERRED APPROACH<br>- Reusable Styles for multiple pages|- A simple CSS change effect several pages|

# CSS History
 
|Year|Specification|Details|
|--|--|--|
|1996|CSS 1.0 Released|Basic styling capabilities introduced, focusing on layout and fonts|
|1998|CSS 2.0 Specification|Enhanced features positioning, z-index, and media types|
|1999|CSS Mobile Profile - (no longer supported)|Mobile devices and features|
|2001|CSS 2.1 Working Draft|Refinement of CSS 2.0, fixing inconsistencies and adding features|
|2005|CSS 3.0 Proposal|Introduced modularization, aiming to split CSS into smaller modules|
|2011|CSS3 Selectors Recommendation|Expanded selector capabilities for more versatile styling|
|2012|CSS Flexible Box Layout Module (Flexbox)|New layout model for more efficient and responsive design|
|2014|CSS Grid Layout Module (CSS Grid)|Enhanced layout system for two-dimensional grid-based layouts|
|2015|CSS Custom Properties (Variables)|Allows the definition of variables for reuse in style sheets|
|2017|CSS Containment Module|Improved performance by isolating styles to specific subtrees|
|2019|CSS Scroll Snap Module|Scroll snapping for better control over scroll behavior|
|2021|CSS Color Module|Extended color features, new syntax and color values|

# References

- [W3 Schools](https://www.w3schools.com/html/html_css.asp)
