# HTML Canvas 2D Example

```
<!DOCTYPE html>
<html>
<body>

<!-- canvas1 being defined as 200 x 200 with a solid red border 20 pixels wide -->
<canvas id="canvas1" width="200" height="200" style="border:20px solid #ff0000;">
Your browser does not support the HTML canvas tag.
</canvas>

<!-- canvas2 being defined as 100 x 100 with a dashed blue border 10 pixels wide -->
<canvas id="canvas2" width="100" height="100" style="border:10px dashed #0000ff;">
Your browser does not support the HTML canvas tag.
</canvas>

<script>
  // drawing on canvas1
  var canvas1 = document.getElementById("canvas1");  // define canvas 1 variable
  var canvas1Context = canvas1.getContext("2d");  // define canvas 1 as a 2 dimensional structure
  canvas1Context.moveTo(50,50); // move the drawing instrument to coordinates (50,50)
  canvas1Context.lineTo(250,150); // prepare a line from current coordinate to new coordinate (250,100)
  canvas1Context.moveTo(50,150);
  canvas1Context.lineTo(250,150);
  canvas1Context.lineTo(250,50);
  canvas1Context.lineTo(50,50);
  canvas1Context.lineTo(50,150);
  canvas1Context.stroke();  // draw the lines
  
  // drawing on canvas2
  var canvas2 = document.getElementById("canvas2");  // define canvas 2 variable
  var canvas2Context = canvas2.getContext("2d");  // define canvas 2 as a 2 dimensional structure
  canvas2Context.moveTo(25,25); // move the drawing instrument to coordinates (50,50)
  canvas2Context.lineTo(75,75); // prepare a line from current coordinate to new coordinate (250,100)
  canvas2Context.moveTo(25,75);
  canvas2Context.lineTo(75,75);
  canvas2Context.lineTo(75,25);
  canvas2Context.lineTo(25,25);
  canvas2Context.lineTo(25,75);
  canvas2Context.stroke();  // draw the lines
</script>

</body>
</html>
```

## Canvas 2D Example

![Canvas 2D Example](canvas2Dexample.png)
