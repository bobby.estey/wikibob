# Syntactically Awesome StyleSheet (SASS)

- SASS is CSS with Functionality.  That is all :-)  CSS is strictly a properties files, when adding functionality, that is SASS.

SASS JavaScript (JS) Single Page Applications (SPAs), such as Angular and React.  These languages utilize both JS and TypeScript (TS) and the TS is compiled into JS so that Web Browsers can interpret the code.  The same with SASS, that is compiled into CSS.

- TS compiled into JS
- SASS compiled into CSS

## References

- [Quick Video showing Awesomeness of SASS](https://www.youtube.com/watch?v=akDIJa0AP5c)
- [SASS Tutorial](https://www.youtube.com/watch?v=_kqN4hl9bGc&list=PL4cUxeGkcC9jxJX7vojNVK-o8ubDZEcNb)

- Variables - referenced with a "$" are a symbolic name for information.  For example:
     - $fg-color : #e41c38;
     - $bg-color : #fdf2d9;
     - NOTE:  CSS has this same feature today

- Nesting - reducing duplication of CSS properties
     - [Bootstrap Button Colors](https://getbootstrap.com/docs/5.0/components/buttons/)

- Mixins - encapsulate many properties into a single reference, very much like Object Oriented Constructors
     - Mixins also can provide control structures, functional capabilities and more

|Description|CSS|SCSS|
|--|--|--|
|Not Nesting compared to Nesting|![Not Nesting](notNested.png)|![Nesting](nested.png)|
|Constructors compared to Mixins|![3 Constructors](constructors.png)|![3 Mixins](mixins.png)|
|Function / Computation Example|![functionExample](functionExample.png)|![computationExample](computationExample.png)|
