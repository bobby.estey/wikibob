# Material User Interface (MUI)

##  Comparison between MUI and Bootstrap

- container, wrapper that encapsulates the row and column classes
- row, the row class defining the row for responsiveness

The following are the same structure for a webpage with the different technologies, Material UI and Bootstrap.  Please note the differences and similarities:
        
#### Material UI

```
<Grid container>
  <Grid item xs={12} md={3}>
    <Item>xs=12 md=3</Item>
  </Grid>
  <Grid item xs={12} md={4}>
    <Item>xs=12 md=4</Item>
  </Grid>
</Grid>
```

#### Bootstrap

```
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-md-3">
      <p>col-xs-12 col-md-3</p>
    </div>

    <div class="col-xs-12 col-md-4">
      <p>col-xs-12 col-md-4</p>
    </div>
  </div>
</div>
```
        