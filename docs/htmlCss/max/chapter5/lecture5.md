  Week Five Lecture  Introduction to HyperText Modeling Language

**Week Five Lecture**

**Multimedia**

This is our final week for this class.  We will learn how to add multimedia characteristics to our web page and all the tags are very easy to write.  Multimedia even though is easy to write, Browsers need to have a plug-in to execute multimedia.  

* * *

**Multimedia Overview**

Multimedia is a combination of sound and/or video added to your web site versus still images.  The term, "A picture is a thousand words" is just what Multimedia is all about.  Displaying information actively is more effective than just textual information.  Multimedia can also be used interactively, thus getting clients involved.  Some examples of Interactive Multimedia are:  voice command, mouse manipulation, text entry, touch screen, video capture of the client or live participation.  There is a big drawback on Multimedia and that is bandwidth.  If you have high speed Internet access then Multimedia sites are excellent resources to visit.  If you have a 56K connection then Multimedia can be horrifying.

There is a limit to Multimedia, clients do not always want to spend minutes of download time to get to a specific piece of information.  That is why you should set up your site so that the client has a choice to view your Multimedia talent.  Your starting to see this now at sites with Flash.  There is usually a button that states, Bypass Flash Presentation.  Even Multimedia Vendor web sites load quickly without polluting your web bandwidth, [Macromedia](http://macromedia.com) is a good example.

**When not to use Multimedia**

If your page is a reference document then Multimedia shouldn't be used at all.  A good example of referential information is search engines, phone books and dictionaries.  Clients will stop returning to your site if you have banners, videos popping up, getting in the way of the information they wish to view.

**When to use Multimedia**

Tutorials, News, Weather are excellent examples of when to use Multimedia.  Learning online, with examples that you can interact always helps with learning.  Displaying a news story, viewing video and listening to audio online, very much like [CNN](http://cnn.com) current news events happening around the world.  

* * *

**Plug-ins**

Web Browsers require the use of a plug-in when working with Multimedia content.  Plug-ins allow the browser to communicate with that particular technology, whether that technology be Java, Macromedia Flash or Shockwave, Photoshop, Apple Quicktime, etc.  There are many plug-ins available, such as for web browsing, audio, video, and other application tools such as Adobe Acrobat Reader.  There are three types of plug-ins available:  hidden, embedded or full screen.  Hidden plug-ins execute in the background, unknown to the user.  Embedded plug-ins are executed within the webpage as part of an HTML document.  Full screen plug-ins execute within their own environment, e.g.  separate window, audio and video players are examples of a full screen plug-in.  One of the more popular plug-ins is the Java 2 Runtime Environment (JRE), which allows execution of Java Byte Code Applets.  Plug-ins sole purpose is to enhance Web Browsers capabilities, such as Multimedia and Data Processing.

* * *

**Web Hosting**

The is a Web Programming class requiring Web Hosting.  Please review the Web Hosting Instructions Thread in the Course Materials Newsgroup for detailed information.  **Reminder:  All assignments are not to be generated using WYSIWYG tools.  I can verify this by viewing your source.**  

* * *

**HTML Tags in Detail**

The following sections discuss new HTML tags and code.  We will go into each of the tags individually.  Each example will build upon the previous example.  All new material that is added will contain this **font and color.**  

* * *

**Sound**

Sound on a web page is easy to write.  Sound can be either a link to a sound page, sound application or background.  We will not discuss background sound.  Here is the syntax for both:  
 

<a href="soundFile">text</a>

The href tag creates a link to a sound file.  The soundFile property is the file name, e.g. max.wav

<embed src="URL" width="value" height="value" autostart="startvalue" />

The embed places a sound application on the web page.  Browsers need a plug-in to execute the embed tag.  The src property is the URL of where the sound file is located.  The width and height properties are the size of the sound application.  The autostart property is a boolean tag.  If you wish the sound to start up automatically when a client loads your page you would set this to true.  Setting autostart to false the client is required to press the play button on the sound application.

* * *

**Video**

Video on a web page is easy to write.  Video can be either a link to a video page or video application.  Here is the syntax for both:  
 

<a href="videoFile">text</a>

The href tag creates a link to a video file.  The videoFile property is the file name, e.g. max.avi

<embed src="URL" width="value" height="value" autostart="startvalue" />

The embed places a video application on the web page.  Browsers need a plug-in to execute the embed tag.  The src property is the URL of where the video file is located.  The width and height properties are the size of the sound application.  The autostart property is a boolean tag.  If you wish the video to start up automatically when a client loads your page you would set this to true.  Setting autostart to false the client is required to press the play button on the video application.

* * *

**Multimedia Example**

Here's our Resume from week 2 adding multimedia.

    <table border="5" width="675">  
      <caption align="top"><font size="+2" color="#006600">Max's Multimedia</font></caption>  
      <th width="230"><font color="#800080">Image</font></th>  
      <th width="100"><font color="#f08080">Sound</font></th>  
      <th width="300"><font color="#f08080">Video</font></th>

      <tr>  
        <td width="230">  
          <map name="max">  
            <area shape="circle" coords="95, 35, 25" href="max.jpg">  
          </map>  
          <img src="max.jpg" alt="Max The Dog" align="middle" height="110" width="140" usemap="#max">  
          <a href="mailto:restey@email.uophx.edu">Send me mail</a>  
        </td>

        <td width="100">  
       **<embed src="max.wav" width="100" height="20" autostart="false" />**  
        </td>

        <td width="300">  
       **<embed src="max.mov" width="300" height="200" autostart="false" />**  
        </td>  
      <tr>  
    </table>  
 

 ![](resume5-1.jpg)

Figure 5-1 Multimedia Page

* * *

**Summary**

This lecture gave examples of adding multimedia to our HTML web page.  Multimedia makes the pages more exciting, however, we don't want to make our web pages to slow that clients will leave.  Remember, content on a web page is very important and adding a little multimedia makes the web page nice.  If your writing a documentation / reference web page, you do not want to add multimedia.

Make sure you visit the many sites that I have referenced, including the real true HTML reference, [http://www.w3c.org.](http://www.w3c.org)  This site is the W3 consortium and where the Internet standards are based, referenced and agreed upon.  

* * *

**Discussion Question 1**

Why are Web delivered multimedia presentations gaining popularity over standalone presentations?

**Discussion Question 2**

Discuss the advantages and disadvantages of multimedia.  When should you use / not use multimedia?

**Discussion Question 3**

Find a web site that talks about the future of multimedia.  Where is multimedia going?  What is the newest in technology?

* * *

**References**

*   Carey, Patrick. (2001).  New Perspectives on Creating Web Pages with HTML - Comprehensive, 2nd edition.  Cambridge, MA: Course Technology ISBN 0-619-01968-9
*   Whatis.com Information Technology Web Site, Retrieved 2002 May 13 from [http://whatis.com](http://whatis.com)  
    W3C, HTML Home Page, Modified 2002 May 10, [http://www.w3.org/MarkUp/](http://www.w3.org/MarkUp/)

* * *

![](../../../../../images/cv64logo.gif)[Web Contact](mailto:estey@cv64.us)  
Last modified:  2003 December 31