   Weekly Summary 2

Small versus Large

**There are several web sites that have an image made up of several files versus a single large image file.  Which page would load faster the several smaller images or one large image, e.g. 10 files of 10KB or 1 file size of 100KB?**

The single large image will generally load faster because there is a single source of overhead and compression is more effective with the larger file.  If a file is to small the compression benefits are next to null.  Below is a diagram that shows this hypothetically.  The first example shows the 10-10KB files with each files overhead.  The next example shows the 1-100KB file with one overhead.  The third example shows that the larger file taking advantage of compression thus using less network bandwidth.

Remember this is not at all accurate because the rates, network delay, compression factors, etc. could affect the results.  Don't forget to clear out your cache.  Once these files are loaded into your browser cache, you will see faster responses.  

I have seen in some cases where the opposite is true and the 10-10KBs load faster but the 100KB file should load faster.

![](performance.jpg)

Do the following experiment at your hosted web sites.

**1)  Small File Test**

*   Create a 10KB file, make a text file not graphics.
*   Make ten copies of this file, giving them a different name.
*   Create a web page that loads the ten files.
*   Time how long the page loads.

**2)  Large File Test**

*   Create a 100KB file, can be a text file not graphics.
*   Create a web page that loads this file.
*   Time how long the page loads.

Report your findings to your classmates and lets discuss why we observed different behavior.

You can also try this site where I have several links you can compare.  [Performance](../chapter2/performance/performance.html)[](../chapter2/performance/performance.html)  

* * *

![](../../../../../images/cv64logo.gif)[Web Contact](mailto:estey@cv64.us)  
Last modified:  2003 December 31