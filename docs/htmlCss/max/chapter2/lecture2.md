Week Two Lecture Colors and Backgrounds This week we will learn how to enhance our web pages with colors and backgrounds. We will also learn about Tables and Frames enabling your web page to become exciting for clients to view. From our previous discussion questions we must remember that performance is an important attribute. Make sure you use these features conservatively so that the web page download is tolerable. Let's get started. Web Hosting The is a Web Programming class requiring Web Hosting. Please review the Web Hosting Instructions Thread in the Course Materials Newsgroup for detailed information. Reminder: All assignments are not to be generated using WYSIWYG tools. I can verify this by viewing your source. HTML Tags in Detail The following sections discuss new HTML tags for colors, backgrounds, tables and frames. We will go into each of the tags individually. Each example will build upon the previous example. All new material that is added will contain this font and color. Color and Backgrounds Reading the text we find that there are 16.7 million distinct colors available to the viewer. Colors enhance a web page's appearance. These colors are defined with a color value (hexadecimal number defining a color's appearance) that are 6 digits in size. The first two digits represent Red intensity, the next two digits Green intensity and the last two digits Blue intensity. Combining these three intensities results in a distinct different color. The hexadecimal value is encapsulated by quotes and start with an octothrop / sharp character (#), e.g. #ffffff To specify a color on your web page you use the tag as follows: Property Descriptions bgcolor background color text color of the text link color of links vlink color of previous links alink color of active links There are times when you wish your text to be of a different font size, color and font type. The tag is used to change the values of sections, paragraphs or even individual letters. All the properties can be used individually, any combination or all. The following is the font tag: font size="12" color="#0000ff" face="arial"> Adding a background to your web page is achieved by using the background property, also part of the tag. Colors & Background Example The web page has been enhanced with colors. We now have various colors for our background, text and links. We even have an image as a background. The following is an example of tags we learned so far. ...

Max's Resume
------------

#### _Background_

I am a Mixed Breed Dog that was found on the streets as a stray. I was taken to the Arizona Human Society where they cleaned and feed me. I was purchased by my current owners who continuously show their love for me.  My owners have a large yard and other animals, e.g. _[Goats, Chickens and Cats.] I get along with all of the animals with the exception of **[Rabbits] unliked. I play to rough with the Rabbits so now they are off limits.

#### _Skills_

... Figure 2-1 shows our updated web page with the various HTML tags. Notice the link colors are different. The Goats, Chickens and Cats link has never been executed so the color is dark blue. The Rabbits link has previously been visited, thus the Rabbits link change to a Fuchsia color. Figure 2-1 - Colors, Fonts and Background Image Placement and Size There are several examples covered in the text discussing image placement and size. This is using the  tag we have seen before and using new properties. There are several options to display an image. Here are some of the properties discussed. alt Places text in your image area. When the image area is activated the text will appear align The location of the surrounding text appearance hspace Spaces from the left and right of image in pixels vspace Spaces from the top and bottom of image in pixels height Height of image in pixels width Width of image in pixels Here is an example in Figure 2-2. Notice that the "Max The Dog" label appears, the alignment of the text is in the middle and the image is 50% the size before. Here is the  tag modified in the code: ![Max The Dog](max.jpg) Figure 2-2  Tag Example Image Maps Using past experience surfing the web you may have notice that when web pages are loaded they have links in certain areas of an image. These areas on an image are called hot spots. A hot spot is a geometric area on the web page that references a link. To set up hot spots on your web pages you use what is called an image map using the tag. Image maps are easy to write but can be very tasking. Here is an example of Max's web page. The tag starts with the name of the map, in our case "max". Then you identify the coordinates using one of three shapes, rectangle (rect), circle or poly (polygon). If a mouse pointer enters the area that is defined by the shape and coordinates of the image the link will be activated, see Figure 2-3. The mouse was moved over Max's head and the link to this area is being displayed at the bottom of the browser. If I press Max's tail I will get his tail image, e.g. maxTail.jpg alone (the reference). If I press Max's head I will get his head image, e.g. maxHead.jpg alone (the reference). The image must reference the map by using the property usemap. Note: Image Maps origins are based off of the image not the web page.   ![Max The Dog](max.jpg) Figure 2-3 Map Example Tables Tables are structures that give order and display referential information. Let us give some more details to Max's resume. Below is the code that will form Max's Characteristics Table which I will describe in detail. To create a table you need the and

tags. This informs the Browser that there are specific tags that will be appearing to structure the table. There are many properties you can place in the table tag. I only used the border property defining the width of the table border as 10 pixels. The next tags I like to use are the and tags. The caption tags define what the table is about, in our case Max's Characteristics. border the width of a table border in pixels cellspacing the width of the cell border in pixels cellpadding the gap between the data and border align horizontal alignment of the data valign vertical alignment of the data width tables width in pixels or percentage height tables height in pixels or percentage bgcolor tables background color Table Headers are defined with the and tags. The table header tag informs the Browser that the headings for this particular table will be what is encapsulated between the and tags. For each header title you must use a and combination. Our example has two header columns, Attributes and Value. Table Rows are defined with the and tags. The table row is informing the browser to display a new row of information. Table rows are for the structure of the table not data. Table Data are defined with the and tags. To enter data into a table you need to use the table data tags. We have two columns in our example so we can use two and sets to define the data. This is not the only option to display data. There are several schemes to produce information to your client. Note: Tables can have other tags within the cells, in our case below I am using the tag from our previous discussion.

Max's Characteristics

Attributes

Value

Max Speed

20 MPH

Color

Black and Brown

Age

est. 10 Human Years

Figure 2-4 Max's Characteristics Table Frames A Frame is an individual windowing area containing a HTML file. Frames are a technique that allow several independent frames to be displayed, controlled and managed from a frame set. The frame set has references to all the individual frames. A very popular frame layout uses three frames. There is a logo frame (titles, main links, banners), a links frame (links to general and specific information) and a documents frame (general HTML web pages), see Figure 2-5. Figure 2-5 Popular Frame Layout The frame set defines your frame structure, there is no body. You need to use the and tags. Frameset comes with two properties rows and columns (cols). Rows defines the number of frames and their sizes vertically. Columns define the number of frames and their sizes horizontally. Note: The tag is not required in the frame set HTML file, because there is no body content. A tag inside the tag is . This is the frame that is being encapsulated by the frame set. The tag has several properties, the most common is the source (src) property. The src property defines the HTML file that the frame is representing. Below are the and definitions. defines the number of frames vertically and their heights defines the number of frames horizontally and their widths document source location for frame reference to display Now we can look at a simple example. The file maxsFrameSet.html is the frame set HTML file. This HTML file contains the and tags. This file contains a header and the frameset structure, no body. The maxsFrameSet.html file contains three frames (maxsLogo.html, maxsLinks.html and his resume (resume2-4.html)) Looking at maxsFrameSet.html we can see that maxsLogo.html is being defined on at the top of the frame set and will be 60 pixels in height and the remaining frames below (maxLinks.html and resume2-4.html) will use the remaining pixels. Then another frameset within the first frame set contains two more frames (maxsLinks.html and resume2-4.html). The first frame (maxLinks.html) is 140 pixels wide, the next frame (resume2-4.html) uses the remaining pixels. Notice that the frames (maxLogo.html, maxsLinks.html and resume2-4.html) are the standard HTML files that we are all familiar with, there is nothing new. The only new information is the frameset and one line in maxLinks.html. The  tag is telling the Browser when a reference has been selected to display the reference (target) in the frame that is named documents (this was defined in the maxsFrameSet.html file). The resume2-4.html tags that are green are only there because this material was contained in the lecture. maxsFrameSet.html Max's World maxsLogo.html Max's Logo

Max's World

maxsLinks.html Max's Logo 

Max's Links

[Resume](resume2-4.html)

[Goats](goats.html)  
[Chickens](chickens.html)  
[Cats](cats.html)  

[Rabbits](rabbits.html)  
[Possum](possum.html)  
[Racoon](racoon.html)

resume2-4.html ...

Max's Characteristics

Attributes

Value

Max Speed

20 MPH

Color

Black and Brown

Age

est. 10 Human Years

* * *

  ![Max The Dog](max.jpg) ... Figure 2-6 Max's Frame Finally there is an option for Browser's that don't have frame capability or do no wish to display frames. The maxsFrameSet.html is then modified with the

,

, and tags. This gives the client an option to view frames or not to view frames. maxsFrameSet.html (With or Without Frames) Max's World

<body> <a href="resume2-4.html">Max's Resume</a> </body>

Summary This lecture gave some pointers on making a web page more spicy. We added color, backgrounds, tables and frames. Learning these few tags we have saw the power of HTML. We didn't go into all of the tags, properties that are available to make a web page very impressive, however, with time and effort you can design some fascinating web pages. Make sure you go to the many sites that I have referenced, including the real true HTML reference, http://www.w3c.org. This site is the W3 consortium and where the Internet standards are based, referenced and agreed upon. Discussion Question 1 Discuss the advantages and disadvantages between client side and server side image maps. Locate a web site that discusses image maps and share with the class the web site content that was different from the text. There are several web sites that have an image made up of several files versus a single large image file. Which page would load faster the several smaller images or one large image, e.g. 10 files of 10KB or 1 file size of 100KB? Discussion Question 2 Discuss the advantages and disadvantages between fixed and dynamic tables. Locate a web site that discusses these types of tables and share with the class the web site content that was different from the text. When is defining table size by pixels preferred compared to percentage? and percentage compared to pixels? Discussion Question 3 Web developers have debated over the pros and cons regarding the usage of framesets and frames. Provide comments on what you feel are the advantages and disadvantages of using frames? If you need help, you can do an Internet search on "HTML Frames" to see how others feel about this topic. Some areas you might want to think about include: For what purposes are frames used for? How can multiple frames on a web page affect page scrolling? Is the effect good or bad, in your opinion? Browser support for frames Maintenance issues for web pages inside frames You may include any other areas of concern related to HTML frames References Carey, Patrick. (2001). New Perspectives on Creating Web Pages with HTML - Comprehensive, 2nd edition. Cambridge, MA: Course Technology ISBN 0-619-01968-9 Whatis.com Information Technology Web Site, Retrieved 2002 May 13 from http://whatis.com W3C, HTML Home Page, Modified 2002 May 10, http://www.w3.org/MarkUp/ Another Image Map Example Copy the html source to a file called imageMap.html and the image of the United States to usa.gif. Then open the imageMap.html in a browser. Move the mouse over the states Arizona and the arizona.html link appears, move over Virginia and the virginia.html link appears. Again, I was born in Arizona and now live in Virginia. Max map   ![](usa.gif) Web Contact Last modified: 2003 December 31