  Week Three Lecture  Introduction to HyperText Modeling Language

**Week Three Lecture**

**Forms and Cascading Style Sheets**

This week we will learn how to make our web pages more active and how to define the style of an element in an HTML document.  Forms are used to collect information from a client and then the information is sent to a server.  The server then processes the information that was filled out on the web page.  Cascading Style Sheets (CSS) are used to give your web page elements consistency and using formats that you set, not using the standard Browser defaults.  

* * *

**Web Hosting**

The is a Web Programming class requiring Web Hosting.  Please review the Web Hosting Instructions Thread in the Course Materials Newsgroup for detailed information.  **Reminder:  All assignments are not to be generated using WYSIWYG tools.  I can verify this by viewing your source.**  

* * *

**HTML Tags in Detail**

The following sections discuss new HTML tags for Forms and Cascading Style Sheets (CSS).  We will go into each of the tags individually.  Each example will build upon the previous example.  All new material that is added will contain this **font and color.**  

* * *

**Common Gateway Interface (CGI) Scripts - Dynamic Web Pages**

HTML is a static approach at displaying information.  So how do we create interaction and dynamic web pages?  The Common Gateway Interface (CGI) was the first approach to answer that question.  Read the text for details but here is the architecture in a nutshell.

*   Web designers create a HTML web page with a form
*   Clients enter information onto the form
*   Once the form has been completed the form is submitted to a destination system (server)
*   When a form is submitted there is an address that is contained in the HTML file, this is the address of the destination system (server) that receives the submitted input
*   The destination system (server) processes the information and returns the output back to the client system

The destination system (server) has a CGI program awaiting input to process information.  There are also other technologies that are in use today that enable web pages to become dynamic.  

* * *

**Forms**

Web page forms enable clients to submit information and interact with a web page.  To create a form you must use the <form> and </form> tags.  The <form> tag has several properties and inner tags, we will discuss a few of them.  Forms can have various types of Widgets.  Widgets are buttons, boxes, text areas, pull down menus, etc.  Your form can have any type of Widget and can be placed anywhere you wish on the web page.  To create a form is very simple, encapsulate all information between the <form> and </form> tags.  Let's look at some Widgets and create a simple web page using some of the examples.

* * *

**Input Boxes**

The first Widget we will work with is the input box also known as a text field.  This is a simple rectangular box that receives input.  Below is the syntax for input box and an example:

name

name of the field

value

default value, text to place in the screen when the web page is first presented

size

the width of the input box in number of characters

maxlength

the maximum number of characters allowed in the field

Below is an example of our input boxes.  We are using a table to format our input boxes, however, tables are not a requirement for forms.  The input box names are petName, ownerName and breed with various column widths.  The breed field will display with the word "Mutt" by default using the value property.

<html>  
  <head>  
    <title>Vetranarian Library</title>  
  </head>  
  <body bgcolor="#ccffff">  
    <div align="center"><h1>Vetranarian Library</h1></div>  
    <form name="vetLib">  
      <table>  
        <tr>  
          <td width="100">Pet's Name</td>  
          <td>**<input name="petName" size="10" />**  
        </tr>  
        <tr>  
          <td width="100">Owner's Name</td>  
          <td>**<input name="ownerName" size="20" />**  
        </tr>  
        <tr>  
          <td width="100">Breed</td>  
          <td>**<input name="breed" value="Mutt" size="10" />**  
        </tr>  
      </table>  
    </form>  
  </body>  
</html>  

* * *

**Selection Lists**

Selection Lists also known as pull downs are another type of Widget.  They allow the client to make a multiple choice selection without having to type input.  Below is the selection list syntax and an example, which we will incorporate to our previous code:

<select name="nameOfField">  
  <option>option value</option>  
</select>

selection box with only one value selectable

<select multiple>

selection box with multiple values selectable, use Ctrl key to select individual items and Shift key to select all

<select size="value">

selection box number of items being displayed, default 1

**...**  
  <tr>  
    <td width="100">Animal</td>  
    <td>  
      **<select name="animal">**  
        **<option>Cat</option>**  
        **<option>Dog</option>**  
        **<option>Goat</option>**  
        **<option>Other</option>**  
      **</select>**  
    </td>  
  </tr>  
**...**  

* * *

**Radio Button**

The Radio Button Widget is very much like a selection list however instead of a pull down the mouse clicks a circular area to select the item.  Below is the syntax for a Radio Button and an example:

<input type="radio" name="nameOfField" value="valueOfField">

radio box syntax

<input type="radio" checked>

place checked for the default value

Radio Boxes can have one and only one selection.  Thus the name of the field has the same name for all radio buttons, only one selection is allowed.  Note that the medium value will always be the default because the checked parameter is in the input type syntax.  
**...**  
  <tr>  
    <td width="100">Size of Animal</td>  
    <td>  
      **<input type="radio" name="size" value="small">Small</input><br>**  
      **<input type="radio" name="size" value="medium" checked>Medium</input><br>**  
      **<input type="radio" name="size" value="large">Large</input>**  
  </td>  
  </tr>  
**...**  

* * *

**Check Boxes**

The Check Box Widget is not the same as a Radio Button Widget and a lot of people make this mistake.  Here is the difference.  Radio Buttons can have only one selection, Check Boxes can can have zero to many selections.  Below is the syntax and example:  
 

<input type="checkbox" name="fieldName">

check box syntax

<input type="checkbox" name="checkup" value="yes">

have the checkup field initially checked

Note that the checkup value will always be the default because the checked parameter is in the input type syntax.  
**...**  
  <tr>  
    <td width="100">Type of Visit</td>  
    <td>  
      **<input type="checkbox" name="checkup" checked>Checkup</input><br>**  
      **<input type="checkbox" name="operation">Operation</input><br>**  
      **<input type="checkbox" name="shots">Shots</input>**  
  </td>  
  </tr>  
**...**  

* * *

**Text Areas**

The Text Area Widget is just like an input box that has a larger area to enter information.  The Text Area Widget is popular to add detailed and miscellaneous information that cannot be categorized, like an input box.  Below is the syntax and example:

<textarea>default text</textarea>

simple and standard text area.  If you wish default text to be inside the text area just type between the textarea tags

<textarea rows="number of rows" cols="number of columns" name="nameOfTextArea">default text</textarea>

text area that is a specific row height and column width

<textarea wrap="wrapOption">

refer to the text on the various options

**...**  
  <tr>  
    <td width="100">Comments</td>  
    <td>  
      **<textarea rows="3" cols="80" name="comments" wrap="virtual">**  
        **Patient Input**  
      **</textarea>**  
  </td>  
  </tr>  
**...**

* * *

**Buttons**

Buttons enable action to occur on a web page and are used with forms frequently.  The most popular buttons are Submit and Reset.  Below is the syntax and example:

<input type="submit" value="text">

Creates a button with the text that is inside the value property is displayed on the button, the word Submit is default.  When pressed the values of fields are sent to a server for processing.

<input type="reset" value="text">

Creates a button with the text that is inside the value property is displayed on the button, the word Reset is default.  When pressed the form is cleared and fields are restored with preset values.

<input type="button" value="text">

Creates a button with the text that is inside the value property is displayed on the button.  When pressed a program within the web page is executed.

**<input type="submit" value="Send Message">**  
**<input type="reset" value="Reset Form">**

There is another way to create buttons without submission of form data using the <button> tag.  Below is the syntax and example:

<button name="text" type="button">  
  <img src="image"> text  
</button>

Creates a button.  The name property is the name of the button, src is the image to place on the button and text is the text to be written on the button.

**<button name="Top" type="button">**  
  **<img src="top.gif">Go To Top**  
**</button>**

Creates a button with the name Top, top.gif is the image and Go To Top is displayed on the button.

* * *

**Image Fields**

Inline images are the same as submit buttons without buttons, another words, the image itself will do the submission, the image is the button.  You probably have seen web pages where the United States are displayed and you can click on your state and your are sent to that state.  This is what is an inline image.  Below is the syntax and example:

<input type="image" src="imageFile" name="text" value="text">

Creates an inline image, with the src being the location of an image, e.g. gif, jpg.  The name property is the name of the field and value is the value of the field.

**<input type="image" src="arizona.gif" name="state" value="AZ">**

When pressing the image of Arizona will submit to the server program the field state with the value AZ.

* * *

**Hidden Fields**

Forms can also pass hidden information to a server, this is known as hidden fields.  Below is the syntax and example:

<input type="hidden" name="text" value="text">

Creates a hidden field.  The name property is the name of the field and value is the value of the field.

**<input type="hidden" name="client" value="maxDomain">**

When making a submission the field client will have the value maxDomain.

* * *

**Form Properties**

We have most of the form elements defined.  Now we need to give our form action.  We will discuss only the Action and Method form properties.  Below is the syntax and example:

<form action="URL" method="type">

The action property identifies the URL where a server program is located to process your form.  The method property controls how the Browser sends your information.

method="get"

This is the default value, which concatenates your fields and values in the URL.  Very ugly and I discourage everyone from doing this.

method="post"

This method sends the fields and values as a separate data stream as standard input to the server.  I prefer this method because you don't have a URL that is exceedingly long.

**<form action="mailto:youremailAddress" method="post">**

When making a submission all the fields of the form will be sent to a server to execute, in this case your settings will be sent to your email address.

<form action="mailto:youremailAddress?subject= some subject text" method="post">  

You can also append certain subject matter to the mailto command to automatically display in the mail message.  This message puts "some subject text" in the Subject textfield.  

* * *

  
**Form Review**

Now let's look at our code and sample output:

<html>  
  <head>  
    <title>Veterinary Library</title>  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>Veterinary Library</h3></div>

    **<form name="vetLib" action="mailto:restey@email.uophx.edu" method="post">**  
      <table>  
        <tr>  
          <td width="200">Pet's Name</td>  
          <td>**<input name="petName" size="10">**

          <td width="200">Owner's Name</td>  
          <td>**<input name="ownerName" size="20">**  
        </tr>  
        <tr>  
          <td width="200">Breed</td>  
          <td>**<input name="breed" value="Mutt" size="10">**  
          <td width="200">Animal</td>  
          <td>  
       **<select name="animal">**  
              **<option>Cat</option>**  
              **<option>Dog</option>**  
              **<option>Goat</option>**  
              **<option>Other</option>**  
            **</select>**  
          </td>  
        </tr>  
        <tr>  
          <td width="200">Size of Animal</td>  
          <td>  
       **<input type="radio" name="size" value="small">Small</input><br>**  
            **<input type="radio" name="size" value="medium" checked>Medium</input><br>**  
            **<input type="radio" name="size" value="large">Large</input>**  
          </td>  
          <td width="200">Type of Visit</td>  
          <td>  
       **<input type="checkbox" name="checkup" checked>Checkup</input><br>**  
            **<input type="checkbox" name="operation">Operation</input><br>**  
            **<input type="checkbox "name="shots">Shots</input>**  
          </td>  
        </tr>  
      </table>

      Comments  
      **<textarea rows="3" cols="80" name="comments" wrap="virtual">Patient Input</textarea>**

      <div align="center">  
      **<input type="submit" value="Send Message">**  
      **<input type="reset" value="Reset Form">**  
      </div>

      **<input type="hidden" name="client" value="maxDomain">**  
    **</form>**  
  </body>  
</html>

Figure 3-1 shows the form with the default values.  Figure 3-2 shows the form being updated.  After submission of the form the results are sent to my email address and the results are enclosed in a text file.  Figure 3-3 shows the results.

Looking at the results you can see how HTML distinguishes the parameters and values, e.g. petName=Max&ownerName=Bob.  This is saying that the field name petName has the value Max.  The ampersand means next field which is ownerName with the value Bob.  
 

 ![](form3-1.jpg)

Figure 3-1 Default Form

 ![](form3-2.jpg)

Figure 3-2 Filled out Form

petName=Max&ownerName=Bob&breed=Mutt&animal=Dog&size=medium&checkup=on&shots=on&comments=Max+is+a+medium+black+and+brown+dog+having+a+check+up+and+receiving+his+rabie+shots.&client=maxDomain

Figure 3-3 Submission Results

* * *

**Cascading Style Sheets (CSS)**

HTML is limited in formatting and this lead to Cascading Style Sheets (CSS).  A style is a rule that defines the appearance of an element in the document.  Instead of having a Browser define your web page appearance you can override the Browser defaults with your own style.  CSS was developed by W3C and was released in 1996.  There are three ways to employ CSS in your web page, Inline, embedded or global, link or external.  We will go over a few examples of CSS.

Inline style, you add the style property to the HTML tag.  The problem is only for one <h1> tag.

<tag style="style declarations">

Inline style.

<h1 style="color:red; font-family:arial">

This h1 tag will be red with the arial font, all other h1 tags will be the Browser default.

If you wish all tags of a type to have the same characteristics you would define an embedded or global style.

<style type="style sheet language">  
  style declarations  
</style>

Embedded or global style.

<style>  
  h1 {color: red; font-family: arial}  
</style>

All h1 tags will be red with the arial font.

<style>  
  h1, h2, h3 {color:red; font-family: arial}  
  h4, h5, h6 {color:blue; font-family: helvetica}  
</style>

Grouping selectors.  All h1, h2 and h3 tags will be red and arial, All h4, h5 and h6 tags will be blue and helvetica.

Finally, you can set up a link to your style sheet definitions using the link or external style sheet.

<link href=URL rel="relation type" type="link type">

Link or external style.

<link href="maxs.css" rel="stylesheet" type="text/css">

This defines a file maxs.css that contains the style definitions.

Below are all three examples with the exact same output shown in Figure 3-8:

<html>  
  <head>  
    <title>Cascading Style Sheets (CSS) - Inline Example</title>  
  </head>

  <body bgcolor="#ccffff">  
    <align="center">  
    <h1 **style="color:red;   font-family:arial**">Red and Arial</h1>  
    <h2 **style="color:green; font-family:arial"**\>Green and Arial</h2>  
    <h3 **style="color:blue;  font-family:arial**">Blue and Arial</h3>  
    <h4 **style="color:red;   font-family:times new roman,times"**\>Red and Times</h4>  
    <h5 **style="color:green; font-family:times new roman,times"**\>Green and Times</h5>  
    <h6 **style="color:blue;  font-family:times new roman,times"**\>Blue and Times</h6>   
    </div>  
  </body>  
</html>

Figure 3-4 Inline Example

<html>  
  <head>  
    <title>Cascading Style Sheets (CSS) - Embedded / Global Example</title>  
  </head>

  **<style>**  
    **h1, h3, h5 {color:red;  font-family:arial}**  
    **h2, h4, h6 {color:blue; font-family:times new roman,times}**  
  **</style>**

  <body bgcolor="#ccffff">  
    <div align="center">  
    <h1>Red and Arial</h1>  
    <h2>Blue and Times</h2>  
    <h3>Red and Arial</h3>  
    <h4>Blue and Times</h4>  
    <h5>Red and Arial</h5>  
    <h6>Blue and Times</h6>  
    </div>  
  </body>  
</html>

Figure 3-5 Embedded / Global Example

<html>  
  <head>  
    <title>Cascading Style Sheets (CSS) - Link / External Example</title>  
  </head>

  **<link href="maxs.css" rel="stylesheet" type="text/css">**  
  **</link>**

  <body bgcolor="#ccffff">  
    <div align="center">  
    <h1>Red and Arial</h1>  
    <h2>Blue and Times</h2>  
    <h3>Red and Arial</h3>  
    <h4>Blue and Times</h4>  
    <h5>Red and Arial</h5>  
    <h6>Blue and Times</h6>  
    </div>  
  </body>  
</html>

Figure 3-6 Link / External Example

h1, h3, h5 {color:red;  font-family:arial}  
h2, h4, h6 {color:blue; font-family:times new roman,times}

Figure 3-7 maxs.css file

 ![](style3-1.jpg)

Figure 3-8 Cascading Style Sheets (CSS)

Cascading Style Sheets (CSS) have several other properties to format your information.  Please refer to the text for the several ways to display your information.

* * *

**Summary**

This lecture gave examples of web pages being interactive.  We created Forms and Cascading Style Sheets (CSS).  While HTML is very flexible and powerful for displaying information, you will learn that HTML is limited and mostly static.  There are new dynamic standards that are very popular in the industry, one such technology is Java Server Pages (JSP).  JSP uses the Java Programming Language to create HTML web pages dynamically.

Make sure you visit the many sites that I have referenced, including the real true HTML reference, [http://www.w3c.org.](http://www.w3c.org)  This site is the W3 consortium and where the Internet standards are based, referenced and agreed upon.  

* * *

**Discussion Question 1**

When would you use a FORM?  Make suggestions on the benefits to both the client and the owner.  Discuss a web page that you believe uses forms effectively and why their site will have return customers.

**Discussion Question 2**

We discussed CGI scripts.  What other technologies are being used today to effectively submit client information to a server and return results to the client?  Why are several companies still using CGI and not the newest scripting technology?

**Discussion Question 3**

We studied Cascading Style Sheets (CSS) basics.  Discuss with your fellow classmates why and when you would use CSS.  Do you feel that CSS is getting more popular or is there new technology that is in current use today that is replacing CSS?  List the new technologies with references.

* * *

**References**

*   Carey, Patrick. (2001).  New Perspectives on Creating Web Pages with HTML - Comprehensive, 2nd edition.  Cambridge, MA: Course Technology ISBN 0-619-01968-9
*   Whatis.com Information Technology Web Site, Accessed 2002 May 13 from [http://whatis.com](http://whatis.com)  
    W3C, HTML Home Page, Modified 2002 May 10, [http://www.w3.org/MarkUp/](http://www.w3.org/MarkUp/)
*   MailTo Syntax, Accessed 2004 February 21, [http://www.ianr.unl.edu/internet/mailto.html](http://www.ianr.unl.edu/internet/mailto.html)

[](http://www.ianr.unl.edu/internet/mailto.html)

* * *

![](../../../../../images/cv64logo.gif)[Web Contact](mailto:estey@cv64.us)  
Last modified:  2003 December 31