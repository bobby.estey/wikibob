  Week One Lecture  Introduction to HyperText Modeling Language

**Week One Lecture**

**Introduction to HyperText Modeling Language (HTML)**

You are about to embark on a five-week course where you will learn how to create and design Web Pages using the HyperText Markup Language (HTML), which is used on the World Wide Web (W3).  Some of our topics will include structure, presentation format, lists, links, images, tables, frames and forms.

* * *

**Web Hosting**

**This is a Web Programming class requiring Web Hosting.  Please review the Web Hosting Instructions Thread in the Course Materials Newsgroup for detailed information.**  

* * *

  
This week we will focus on Web Fundamentals and HTML (The Language of the Web).  We will learn the history, terminology, versions and basic tools for authoring web pages.  We will then learn the details about HTML tags.

**Information Technology Terms**

Below are terms that are described on the [http://whatis.com](http://whatis.com) Information Technology site.  View each of these to familiarize yourself with the Internet and W3 terminology.  I also recommend this site for information on any computing technology.

*   Internet
*   World Wide Web (Web or WWW)
*   World Wide Web Consortium (W3C)
*   Hypertext Documents (Document)
*   Web Server (Server)
*   Web Browser (Browser), e.g. Netscape Navigator, Internet Explorer
*   HyperText Markup Language (HTML)
*   eXtensible HyperText Markup Language (XHTML)
*   eXtensible Markup Language (XML)

**HTML:  The Language of the Web**

HyperText Markup Language (HTML) is the Internet standard for publishing web pages on the W3.  HTML is a non-proprietary format based upon the Standard Generalized Markup Language (SGML) and can be created and processed by a wide range of tools, from simple plain text editors to sophisticated HTML tools, e.g. Converters and Editors.  HTML uses tags to structure text, import images and create hypertext links. HTML is a set of markup symbols or codes inserted in a file intended for display on a Browser page. The Browser interprets the HTML code and displays the content.  If you have had any contact with the Internet then the majority of web pages will be written in HTML.

**To learn more about HTML go to the official site:**  [http://www.w3.org/MarkUp/](http://www.w3.org/MarkUp/)

**HTML Editing**

There are many ways you can create a HTML page.  The most popular web authoring tool is the What You See Is What You Get (WYSIWYG) Web Editor Tools.  These tools enable anyone with no HTML knowledge to create web pages very quickly.  This course is about learning HTML, so we will not produce any of our pages using these tools.

Using HTML Editors helps creates web pages quickly but they are limited.  By understanding HTML you can enhance your web page by manually editing the HTML file and adding tags that are not part of the HTML Editor library.  There are also extraneous tags that are placed in an HTML file from the HTML Editor that you would wish to be removed.

> **Note:  All assignments are not to be generated using WYSIWYG tools.  I can verify this by viewing your source.**

**HTML Syntax**

HTML uses tags (code) that describes to a Browser how to display information.  Figure 1 displays how a tag is syntactically written.  All tags have brackets (<>) enclosing the tag name.  Tags are not case sensitive, however, good practice and the new XHTML standard, is to make all tags lowercase.

> **Note:**  XHTML calls tags, elements, they have the same meaning

<tagName properties> document content </tagName>

<tagName properties />

**Figure 1 - HTML Tag Syntax**

Tags can contain zero or more properties.  Figure 2a illustrates the tag title with no property and document content of My Resume.  Figure 2b illustrates the tag h1 with the property name align and property value center.  
 

<title>My Resume</title>

<h1 align="center">Your Name</h1>

Tags with no properties, title is the tag name.

Tags with properties, h1 is the tag name with the property name align and property value center.

**Figure 2a - Tag No Properties**

**Figure 2b - Tag with Properties**

Your first web project due Day 5 (not graded) is to create a Web Page with your Resume.  This is a simple exercise to perform and will help you acquire the basic knowledge on Web Pages.  Figure 3 - Resume Example has most of the tags you will need to accomplish the exercise.  We will in a future date make our web pages more impressive.

**Creating your first Web Page**

*   Open up a new document, call the document (resume.html) using a text editor, (vi - Unix, Wordpad - Windows)
*   Copy all the tags of Figure 3, including <html> and </html> into your new document, again resume.html
*   Save the file, you don't need to close the file

<html>  
  <head>  
    <title>My Resume</title>  
  </head>

  <body>  
    My Resume Information  
  </body>  
</html>

**Figure 3 - Resume Example - resume.html**

**Displaying your first Web Page**

*   Start your Browser
*   Drag your file onto the browser.  Your file should display automatically.  The browser title bar should display My Resume and the text area should display My Resume Information, see Figure 4
*   You don't need to close the browser window

![](resume1-1.jpg)

**Figure 4 - Resume Example Interpreted by a Web Browser**

**More Editor Work**

Make some modifications to your document, repeating the steps (Creating and Displaying your first Web Page) and notice how the Browser is interpreting the HTML code.

> **Note:**  When making changes in an HTML document you must reload/refresh the Browser to show your new work.  The Browser only displays the last information received since the original load.
> 
> **Important:**  Sometimes the Browser will only display what is in cache, what does this mean?  You can continue to modify your HTML web page and your performing a refresh and the page looks exactly the same as before.  To force Netscape Navigator or Internet Explorer press the Shift key and the refresh button on the Browser at the same time.  This action will force the Browser to reload the newly changed file and not the cached version.

* * *

  
**HTML Tags in Detail**

The following sections discuss some HTML tags that you can use for Web Pages.  We will go into each of the tags individually.  Each example will build upon each previous example.  We will start with the basic infrastructure of an HTML file and then learn some new tags and apply them.  All new material that is added will contain this **font and color.**  

* * *

**Infrastructure**

*   HTML has the infrastructure tags of <head>, <body>, <html> and corresponding end tags.  These tags form the basic HTML web page.  The <html> and </html> tags encapsulate a HTML document and inform the Browser that the content that is between these two tags contains HTML.  The <head> and </head> tags encapsulate the web page information.  The <body> and </body> tags encapsulate the display information (content).  The following example is what we have seen in the previous example.

**<html>**

**<head>**

**The area between the <head> and </head> tags contains information about the web page, e.g. name, author, metadata**

**</head>**

**<body>**

**The area between the <body> and </body> contains the display information**

**</body>**

**</html>**

*   <title> and </title> are encapsulated by the <head> and </head> tags.  The title tag contains the title of the web page and places this information on the title bar of the web page.  Our previous example My Resume was the title of our web page.  The words My Resume Information will appear in the text area or body of the web page.  Remember the heading area is for web page information and the body area is for displaying information.

*   <meta> Meta is a definition or description of information.  The meta tag has two parameters, name and content.  You can have any number of meta tags, we will use the most commonly used description and keyword parameters.  These two tags are used by search engines to categorize your information.  The description describes the page for other programs to decipher.  The keyword parameter contains any keywords that you would wish to report.

<html>

<head>

**<title>Max's Resume</title>**  
**<meta name="description" content="Max's Resume" />**  
**<meta name="keywords" content="max, resume" />**

</head>

<body>

**Max's Resume**

</body>

</html>

The HTML code above would display as shown in Figure 5.  Note the Title Bar contains what was defined between the <title> and </title> tags.  The Text that was entered between the <body> and </body> tags appears in the text area of the web page.  
 

![](resume1-2.jpg)

**Figure 5 - Max's Resume**  

* * *

**Headers**

*   Headers are very much the same as a word processors header fonts.  There are six levels on headers that are support in HTML.  All headers start with the letter "h" and are immediately followed by a number ranging from 1 to 6, 1 being the largest font and 6 being the lowest font setting, e.g. <h1> ... <h6>.  The header tags also have an alignment property so you can center your text if you wish.  Here are some examples of headers:

<h1>Header</h1>

Header
======

<h2 align="center">Header</h2>

Header
------

<h3 align="right">Header</h3>

### Header

<h4>Header</h4>

#### Header

<h5 align="center">Header</h5>

##### Header

<h6 align="right">Header</h6>

###### Header

* * *

**Centering**

*   The <div align="center"> and </div> tags center anything that these tags encapsulate, e.g. text, tables, buttons, etc.  Don't confuse this tag with the properties alignment in the header example, they are not the same.  Here is an example of Header 5 using the <div> and </div> tags instead of the alignment property:

<div align="center"><h5>Header</h5></div>

##### Header

* * *

**Paragraph**

*   The <p> and </p> tags encapsulate paragraph information and these tags are what you use to separate paragraph information.  The paragraph tags also have an alignment property.

<p>Paragraph 1</p>  
<p align="center">Paragraph 2</p>

Paragraph 1

Paragraph 2

* * *

**Line Break**

*   The <br /> tag inserts a line break and does not add extra space before the new line.

* * *

**Resume Part 2 Example**

The following is an example of tags we learned so far.

<html>  
    <head>  
        <title>Max's Resume</title>  
        <meta name="description" content="Max's Resume" />  
        <meta name="keywords" content="max, resume" />  
    </head>

    <body>  
        **<div align="center">**  
            **<h2>Max's Resume</h2>**  
        **</div>**

        **<p>**  
            **<h4>Background</h4>**  
        **</p>**

        **<p>**  
            **I am a mixed breed dog that was found on the streets as a stray.  I was taken to the Arizona Human Society where they cleaned and feed me.  I was purchased by my current owners who continuously show their love for me.**  
        **</p>**

        **<p>**  
            **My owners have a large yard and other animals, e.g. Goats, Chickens and Cats.  I get along with all of the animals with the exception of Rabbits.  I play to rough with the Rabbits so now they are off limits.**  
        **</p>**

        **<p>**  
            **<h4>Skills</h4>**  
        **</p>**

        **<p>**  
            **I like to chase after other animals, protect my owners property and show love towards my owners.**  
        **</p>**

    </body>  
</html>  
 

![](resume1-3.jpg)

**Figure 6 - Resume Example 2  
**

* * *

**Comments**

*   To add comments use the tag, <!-- comments -->

* * *

**Formatting**

*   Here are some formatting tag examples showing how to make your text bold, italic, underscored or a combination:

<b> bold </b>

**bold**

<i> italic </i>

_italic_

<u> underscore </u>

underscore

<b><i><u>combined</u></i></b>

**_combined_**

* * *

**Resume Part 3 Example**

The following is an example of tags we learned so far.

<html>  
    <head>  
        <title>Max's Resume</title>  
        <meta name="description" content="Max's Resume" />  
        <meta name="keywords" content="max, resume" />  
    </head>

    <body>  
        <div align="center">  
            <h2>Max's Resume</h2>  
        </div>

        <p>  
            <h4>**<i>**Background**</i>**</h4>  
        </p>

        <p>  
            I am a **<b>**Mixed Breed Dog**</b>** that was found on the streets as a stray.  I was taken to the **<b>**Arizona Human Society**</b>** where they cleaned and feed me.  I was purchased by my current owners who continuously show their love for me.  
        </p>

        <p>  
            My owners have a large yard and other animals, e.g. **<i>**Goats, Chickens and Cats.**</i>**  I get along with all of the animals with the exception of **<b><u>**Rabbits**</u></b>**. **<u>**I play to rough with the Rabbits**</u>** so now they are off limits.  
        </p>

        <p>  
            <h4><i>Skills**</i>**</h4>  
        </p>

        <p>  
            I like to **<u>**chase after other animals, protect my owners property and **<b>**show love towards my owners**</b></u>**.  
        </p>  
    </body>  
</html>  
 

![](resume1-4.jpg)

**Figure 7 - Resume Example 3**

* * *

**Lists**

*   Lists are easily accomplished in HTML, there are two types.  Unordered <ul> and </ul> and Ordered <ol> and </ol>.

*   Ordered lists are either in numeric (default) or alpha by specifying the property type, e.g. <ol type="a">
*   Unordered lists are list items that show up in a bullet list
*   Both types of lists use the <li> and </li> (list item tags) for the listed items in the list
*   Below are some examples:

Ordered List Example

<h1>Grades</h1>  
  <ol>  
    <li>A = 4.0</li>  
    <li>B = 3.0</li>  
    <li>C = 2.0</li>  
  </ol>

Grades
======

1.  A = 4.0
2.  B = 3.0
3.  C = 2.0

Unordered List Example

<h1>Grades</h1>  
  <ul>  
    <li>A = 4.0</li>  
    <li>B = 3.0</li>  
    <li>C = 2.0</li>  
  </ul>

Grades
======

*   A = 4.0
*   B = 3.0
*   C = 2.0

* * *

**Entities**

*   Entities enable other types of Characters and symbols to be displayed on a Web Page.  Here are some examples, note that all entities  begin with an ampersand "&" and end with semicolon ";"  
    

Symbol

Non Breaking Space

&

©

<

\>

Code Name

&nbsp;

&amp;

&copy;

&lt;

&gt;

Code

&#160;

&#39;

&#169;

&#60;

&#62;

* * *

**Horizontal Lines**

*   Horizontal lines are represented by the <hr> tag.  The horizontal line tag has several properties that can be set.  The following table displays some of these property options:

*   **Notes:**

*   Size is in pixels or 1 pixel is 1/72-inch height
*   hr default width is 100% and size is 2 pixels
*   width can be in percentage or pixels

<hr>

* * *

<hr align="center" size="10" width="50%">

* * *

<hr align="left" size="20" width="25%">

* * *

* * *

**Resume Part 4 Example**

The following is an example of tags we learned so far.

<html>  
    <head>  
        <title>Max's Resume</title>  
        <meta name="description" content="Max's Resume" />  
        <meta name="keywords" content="max, resume" />  
    </head>

    <body>  
        <div align="center">  
            <h2>Max's Resume</h2>  
        </div>

        <p>  
            <h4><i>Background</i></h4>  
        </p>

        <p>  
            I am a <b>Mixed Breed Dog</b> that was found on the streets as a stray.  I was taken to the <b>Arizona Human Society</b> where they cleaned and feed me.  I was purchased by my current owners who continuously show their love for me.  
        </p>

        <p>  
            My owners have a large yard and other animals, e.g. <i>Goats, Chickens and Cats.</i>  I get along with all of the animals with the exception of <b><u>Rabbits</u></b>.  <u>I play to rough with the Rabbits</u> so now they are off limits.  
        </p>

        <p>  
            <h4><i>Skills</i></h4>  
        </p>

        <p>  
            I like to <u>chase after other animals, protect my owners property and <b>show love towards my owners</b></u>.  
        </p>

       **<hr align="center" size="10" width="50%">**

        **<h5>Animals I Like</h5>**  
          **<ol>**  
            **<li>Goats</li>**  
            **<li>Chickens</li>**  
            **<li>Cats</li>**  
        **</ol>**

        **<h5>Animals I Don't Like</h5>**  
          **<ul>**  
            **<li>Rabbits</li>**  
            **<li>Possum</li>**  
            **<li>Racoon</li>**  
        **</ul>**

        **<hr>**  
        **Copyright &copy; 2002, Max The Dog**  
    </body>  
</html>  
 

![](resume1-5.jpg)

**Figure 8 - Resume Example 4**

* * *

**Images**

*   Images are one of the greatest features of HTML.  Images are easily placed into documents and do not require to much manual labor to import.  HTML supports several types of images, e.g. GIF, JPEG, PNG, etc.  
    

*   To import an image into a Web Document only requires the following tag, e.g. <img src="filename.filenameExtension">
*   The system must be able to find the file or a broken image shows in the screen
*   Images can be anywhere in the directory tree structure and must adhere to the correct path or the image will not display
*   Images also have several properties, e.g. align="center"  
    

<img src="max.gif">

* * *

**Anchors**

*   Anchors are text or images that create a reference for your own page or other systems to locate information on a Web Page.  Anchors become the destination for a link.  Anchors are represented by the <a> and </a> tags.  To place an anchor on a Web Page you use the following syntax, <a name="nameOfTheAnchor">Some Text</a>.  This says that your creating an anchor with the name nameOfTheAnchor and Some Text which will display on the Web Page is the location of the anchor.

*   Anchors using the name property are only destinations not sources
*   Anchors can be files, locations and images

<a name="nameOfTheAnchor">Some Text</a>

* * *

**Links**

*   Links are sources on a web page once clicked will be routed to an anchor.  Links also use the <a> and </a> tags, however they use the href property, **Hypertext Reference**, that references a destination anchor.  Destination anchors can be locations within a document or external references (other files or systems).  The following are some examples how to use links:

<a href="#anchorWithinPage">Within Page</a>

This is an example of an anchor that is within the page.  To reference anchors within a page use the "#" (octothorp, sharp) symbol in front of the anchor destination name.

<a href="path/filename.html">Path Example</a>

This is an example of an anchor that is on the current system.  The system must be able to find the correct path to communicate with the document.

<a href="http://w3c.org">Web Example</a>

This is an example of an anchor that is communicating with an external system's web page.

<a href="ftp://w3c.org">FTP Example</a>

This is an example of an anchor that is communicating with an ftp server site.

<a href="mailto:yourname@email.uophx.edu">yourname</a>

This is an example of an anchor that is going to send mail to your account.

* * *

**Resume Part 5 Example**

The following is an example of tags we learned so far.

<html>  
    <head>  
        <title>Max's Resume</title>  
        <meta name="description" content="Max's Resume" />  
        <meta name="keywords" content="max, resume" />  
    </head>

    <body>  
        <div align="center">  
            <h2>Max's Resume</h2>  
        </div>

       <p>  
            <h4><i>Background</i></h4>  
        </p>

        <p>  
            I am a <b>Mixed Breed Dog</b> that was found on the streets as a stray.  I was taken to the <b>Arizona Human Society</b> where they cleaned and feed me.  I was purchased by my current owners who continuously show their love for me.  
        </p>

        <p>  
            My owners have a large yard and other animals, e.g. <i>**<a href="#liked">**Goats, Chickens and Cats.</i>**</a>**  I get along with all of the animals with the exception of <b><u>**<a href="#unliked">**Rabbits**</a>**</u></b>.  <u>I play to rough with the Rabbits</u> so now they are off limits.  
        </p>

        <p>  
            <h4><i>Skills</i></h4>  
        </p>

        <p>  
            I like to <u>chase after other animals, protect my owners property and <b>show love towards my owners</b></u>.  
        </p>

        <hr align="center" size=10 width=50%>

   **<a name="liked">**<h5>Animals I Like</h5>**</a>**  
          <ol>  
            <li>Goats</li>  
            <li>Chickens</li>  
            <li>Cats</li>  
        </ol>

   **<a name="unliked">**<h5>Animals I Don't Like</h5>**</a>**  
          <ul>  
            <li>Rabbits</li>  
            <li>Possum</li>  
            <li>Racoon</li>  
        </ul>

        <hr>  
   **<img src="max.jpg">**  
        **<br></br>**  
        **<a href="mailto:restey@email.uophx.edu">Send me mail</a>**

        Copyright &copy; 2002, Max The Dog  
    </body>  
</html>  
 

![](resume1-6.jpg)

**Figure 9 - Resume Example 5**

* * *

**Well Formed**

There are certain tags that can be used as one-sided tags in HTML, e.g. <p>, <br>, <li>.  This is not good practice and should be discouraged.  Previous versions of web editors still don't close out tags or force well formed syntax.  With the emergence of XML you will see that well formed documents will be a requirement not a recommendation.  XHTML the next release of HTML requires that you use both opening and closing elements (this is known as well-formed syntax) and that all elements be in lower case. Also being well formed means you can only have proper structure.  The following table illustrates how HTML is a less rigorous and how XHTML is very structured:  
 

HTML (No Structure)

<tag1><tag2>content</tag1></tag2>  
<tag1><tag2>content

XHTML (Structure)

<tag1><tag2>content</tag2></tag1>

Here's another comparison between not well formed and well formed syntax.  Figure 10a, the new paragraphs are not ended with </p> tag.  Also the first paragraph has the <b> and <i> tags unnested.  Figure 10b, is well formed.  
 

<html>  
  <head>  
    <title>Not Well Formed</title>  
  </head>

  <body>  
    <div align="center">  
      <p>  
        <b><i>  
          First Paragraph  
        </b></i>  
      <p>  
        Next Paragraph  
  </body>  
</html>  
   
 

<html>  
  <head>  
    <title>Well Formed</title>  
  </head>

  <body>  
    <div align="center">  
      <p>  
        <b><i>  
          First Paragraph  
        </i></b>  
      </p>  
      <p>  
        Next Paragraph  
      </p>  
    </div>  
  </body>  
</html>

**Figure 10a - Not Well Formed Example**

**Figure 10b - Well Formed Example**

* * *

**Summary**

This is the first lecture of five and should give you good knowledge to get started with basic HTML programming.  The next few weeks we will learn a lot more tags, however, these infrastructure and basic tags that were covered are used almost all the time.

Make sure you go to the many sites that I have referenced, including the real true HTML reference, [http://www.w3c.org.](http://www.w3c.org)  This site is the W3 consortium and where the Internet standards are based, referenced and agreed upon.  

* * *

**Discussion Question 1**

List 5 major categories that a web page must comply to be ranked among the best.  Write details on each category and why you feel they are important.  Also give an order and percentage ranking, e.g.

*   Performance 50% - I don't enjoy email taking several minutes to download.  Sometimes this occurs because of the contents that are being sent, for example Images.  Web pages should be well designed, uncluttered and succinct.  Web pages should get the visitor the information as quick as possible.  The faster the response the more a client will return. [http://google.com](http://google.com), [http://yahoo.com](http://yahoo.com), [http://altavista.com](http://altavista.com), etc.  These search engines would not be in business if their performance was inadequate.

*   Content 30% - There is nothing more upsetting than being lead to a web page that has nothing to do with what your searching.  Once finding a website with the perfect advertisement there is less than a paragraph of content (information).  The World Wide Web Consortium (W3C) has realized this potential downfall so a new technology called eXtended Markup Language (XML) was created.  XML can describe the information that is contained in a web page versus HTML which could only describe TEXTUAL information.  Everyone wants the answer to their SIMPLE QUESTION and they don't want to spend the entire day searching.  Will this fix the problem, maybe not because the same websites that send their metadata to the search engines will also put the same metadata in their XML files.

**Discussion Question 2**

There are many web search engines on the Internet.  Locate a web site that contains a good tutorial on HTML.  Discuss why you feel this web site has the content that you feel could help you with this course.

**Discussion Question 3**

What are the major differences between HTML and XHTML?  What are the major reasons to move to XHTML, both professionally and business impact.  

* * *

**References**

*   Carey, Patrick. (2001).  New Perspectives on Creating Web Pages with HTML - Comprehensive, 2nd edition.  Cambridge, MA: Course Technology ISBN 0-619-01968-9
*   Whatis.com Information Technology Web Site, Retrieved 2002 May 13 from [http://whatis.com](http://whatis.com)
*   W3C, HTML Home Page, Modified 2002 May 10, [http://www.w3.org/MarkUp/](http://www.w3.org/MarkUp/)

[](http://developer.netscape.com/docs/manuals/htmlguid/index.htm)

* * *

![](../../../../../images/cv64logo.gif)[Web Contact](mailto:estey@cv64.us)  
Last modified:  2003 December 31