  Week Four Lecture  Introduction to HyperText Modeling Language

**Week Four Lecture**

**JavaScript**

This week we will learn how to write client side programs in our web pages using the scripting language, JavaScript.  Programming and scripting languages present the client with an active web page.  

* * *

**Web Hosting**

The is a Web Programming class requiring Web Hosting.  Please review the Web Hosting Instructions Thread in the Course Materials Newsgroup for detailed information.  **Reminder:  All assignments are not to be generated using WYSIWYG tools.  I can verify this by viewing your source.**  

* * *

**HTML Tags in Detail**

The following sections discuss new HTML tags and code.  We will go into each of the tags individually.  Each example will build upon the previous example.  All new material that is added will contain this **font and color.**  

* * *

**Java and JavaScript**

[Java](http://searchsolaris.techtarget.com/sDefinition/0,,sid12_gci212415,00.html) and [JavaScript](http://searchwebmanagement.techtarget.com/sDefinition/0,,sid27_gci212418,00.html) are not the same.  Java is a very powerful Object-Oriented Programming Language that is robust, stable, secure, distributed and Java's key feature PORTABLE.  Java uses the Java Virtual Machine which is an Interface between Java code and the hardware.  Thus developers can write programs that can execute on any platform that is executing the JVM.

JavaScript is a scripting language, using a small subset of Java executables.  Scripts are easy to learn, no Developer's Kit required and scripts are written directly with HTML.  JavaScript is used for simple tasks.

Remember Java is a programming language and JavaScript is a scripting language.  Here are a couple websites for additional information on these technologies:  
 

Java

[http://java.sun.com](http://java.sun.com)

JavaScript

[http://netscape.com](http://netscape.com)

* * *

**JavaScript Elements**

There are four categories of elements that appear in JavaScript.  They are:

*   Objects
    *   Properties
    *   Methods
*   Functions
*   Statements
*   Events

**Objects** are a window, document, image, page, form, table, button, graphic, or URL, to name a few.  Everything that can be controlled in a Web browser is an object.  Objects contain properties (or attributes) and methods (or operations).

**Properties** identify the condition of the object (e.g. “green”).  For example, length or name are JavaScript properties.  Properties are defined by using the object’s name, a period and then the name of the property.

**Methods** are the action behind the changing of the properties.  For example fontcolor(“green”) sets the color of the text to “green”.

**Functions** are simply methods (operations) that run outside of an object.

**Statements** are the part of the program that control the length of the object and how the program flows (e.g. if, else).

**Events** are things that trigger the program to react in a certain fashion.  A good example of this is when a user clicks the mouse on a portion of a Web page.  The event is what occurs when the mouse button is clicked.  Events always happen in conjunction with an object.  

* * *

**JavaScript <script> tag**

JavaScript is written using the two sided <script> tag.  The <script> tag can be placed anywhere in the <head> and/or <body> sections of an HTML file.  The table below shows some examples of the <script> tag:  
 

<script>  
  JavaScript Commands  
</script>

This is the simplest use of <script>

<script src="URL" language="language">  
  JavaScript Commands  
</script>

The script tag has additional attributes, e.g. URL is the location of an external file to store your information.  Language is the language that is inside the script tag, by default the language is JavaScript, e.g. <script language="JavaScript">

**Note:  JavaScript uses the comment tag to hide Browsers that can't interrupt JavaScript.**

<html>  
  <head>  
    <title>JavaScript Example</title>  
    **<script>**  
      **<!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript**  
        **JavaScript Commands**  
      **// Stop Hiding JavaScript Commands -->**  
    **</script>**  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>JavaScript Example</h3></div>  
    **<script>**  
      **<!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript**  
        **JavaScript Commands**  
      **// Stop Hiding JavaScript Commands -->**  
    **</script>**  
  </body>  
</html>  

* * *

**JavaScript Commands**

JavaScript Commands are the syntax that JavaScript uses to execute within an HTML web page.  We will first start with the document object.  The document object is the web page itself, the body.  To write on the web page you need to use the document object.  The document object has several methods.  We will start with the write and writeln methods.  
 

document.write("text");

Writes the text that is inside the double quotes, including HTML tags, e.g. document.write("<h1>text</h1> <br />");

document.writeln("text");

Writes the text that is inside the double quotes and appends a carriage return <cr>, ctrl-m at the end of the text.  Note:  the writeln needs to have the <pre> preformatted tag enabled.  My suggestion is to use write and not writeln and put <br /> tags that give the same effect as writeln.

The following code is a basic example of how to write text using JavaScript.  Notice that the HTML tags are included and display as if we were not using JavaScript.

<html>  
  <head>  
    <title>JavaScript Example</title>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>JavaScript Example</h3></div>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      **document.write("JavaScript document.write");**  
      **document.write("<h5>Header 5 Line with a carriage return</h5><br />");**  
      **document.write("<i>Another Line that is Italic.</i>");**  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </body>  
</html>  
 

 ![](script4-1.jpg)

Figure 4-1 document.write

* * *

**Debugger**

Most Browsers have a JavaScript built-in debugger.  To execute the debugger just type **javascript:** , include the colon in your URL and the debugger will appear.  There are many websites that will instruct you how to use the javascript tool.  
 

 ![](script4-2.jpg)

Figure 4-2 javascript:

 ![](script4-3.jpg)

Figure 4-3 JavaScript Debugger

* * *

**Variables**

JavaScript supports Variables much like any programming language.  We will write a simple program to display computations and use some of the built-in JavaScript objects.  Variables are defined using the var object.  Variables are declared only, assigned values only or both declared and assigned, see the table below:  
 

var myVariable;

The variable myVariable is declared and not assigned any value.

myVariable = "Max";

The variable myVariable is not declared and assigned the value Max.

var myVariable = "Max";

The variable myVariable is declared and assigned the value Max.

Variables can be Strings, Numeric, Boolean, Null and other types.  The variable myVariable in the table above is a String variable.  The following table shows some of the basic data types.  The next example we will generate a Date variable using a built-in JavaScript objects.  
 

Strings

Strings are any group of characters, e.g. "Hello", "Hello Class"

Numeric

Numerics can be any number, e.g. 13, 13.13, -13, 13E13

Boolean

Booleans are either True or false, e.g. true, false

Null

Nulls have no value

There are several built-in JavaScript objects.  To create an object you must use the **new** reserved word.  **new** creates an object instance.  We are going to declare a variable called maxAdoptionDate and assign the current value into maxsAdoptionDate.  The following command performs this function.  
 

var maxsAdoptionDate =   
  new Date("November 4, 1984");

Declare variable maxsAdoptionDate and assign the current value into maxsAdoptionDate.  new Date creates an object instance using the values November 4, 1984.  The object instance returns the date format.

<html>  
  <head>  
    <title>JavaScript Example</title>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>JavaScript Example</h3></div>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      **var maxsAdoptionDate = new Date("November 4, 1984");**  
      **document.write("Max's Adoption Date:  " + maxsAdoptionDate);**  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </body>  
</html>  
 

 ![](script4-4.jpg)

Figure 4-4 Date Object

* * *

**Expressions and Operators**

Expressions are commands that assign values to your variables using operators.  We have already worked an expression when we assigned a value to Max's Adoption Date, e.g. var maxsAdoptionDate = new Date("November 4, 1984");

Operators are elements that perform actions within expressions.  Operators can be arithmetic, assignment or logical.  The following table has some examples:  
 

arithmetic, plus sign would mean add

1 + 2 results as 3

assignment, plus sign would mean concatenate

"a" + "b" results as "ab"

logical, double equals would mean return true if equal

a == b results as true if a and b are equal else false

Here's an example using Expressions and Operators:

<html>  
  <head>  
    <title>JavaScript Example</title>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>JavaScript Example</h3></div>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript

      **var maxsAdoptionDate = new Date("November 4, 1984");**  
      **var today = new Date();**  
      **var cats = 2;**  
      **var dogs = 2;**  
      **var goats = 7;**  
      **var chickens = 50;**  
      **var moreCats = false;**

      **var totalAnimals = cats + dogs + goats + chickens;**

      **if (cats > dogs) {**  
          **moreCats = true;**  
      **}**

      **document.write("Max's Adoption Date:  " + maxsAdoptionDate + "<br />");**  
      **document.write("Today's Date:  " + today + "<br /><hr />");**

      **document.write("Total Animals: " + totalAnimals + "<br />");**  
      **document.write("Are there more cats than dogs?  " + moreCats + "<br />");**  
      **document.write("Do cats equal dogs?  " + (cats == dogs) + "<br />");**

      // Stop Hiding JavaScript Commands -->  
    </script>  
  </body>  
</html>  
 

 ![](script4-5.jpg)

Figure 4-5 Expressions and Operators

* * *

**Flow Control**

JavaScript like many programming languages has flow control.  If-Then-Else, For Loop and While Loop.  The following table contains the syntax for each:  
 

If-Then-Else

if (condition) {  
  JavaScript commands if true  
} else {  
  JavaScript commands if false  
}

For Loop

for (start; condition; update) {  
  JavaScript commands  
}

While Loop

while (condition) {  
  JavaScript commands  
}

Here's a new web page showing all 3 flow controls:

<html>  
  <head>  
    <title>JavaScript Example</title>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>JavaScript Example</h3></div>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript

      var cats = 2;  
      var dogs = 2;  
      var goats = 7;  
      var chickens = 50;  
      var moreCats = false;  
      var i = 0;

      document.write("<br />" + "If-Then-Else Example:" + "<br />");  
      **if (cats > dogs) {**  
          **document.write("There are more cats than dogs" + "<br />");**  
      **} else {**  
          **document.write("Cats equal dogs, or there are more dogs than cats" + "<br />");**  
      **}**

      document.write("<br />" + "For Example:" + "<br />");  
      **for (i=0; i<10; i++) {**  
          **document.write(i + " ");**  
      **}**  
      document.write("<br />");

      document.write("<br />" + "While Example:" + "<br />");  
      **i = 10;**  
      **while (i > 0) {**  
          **document.write(i-- + " ");**  
      **}**

      // Stop Hiding JavaScript Commands -->  
    </script>  
  </body>  
</html>  
 

 ![](script4-6.jpg)

Figure 4-6 Flow Control

* * *

**Functions**

Functions (methods) are a series of commands that perform an action or compute a value.  Functions can return values.  Functions are useful for segmenting code that is repeatable.  Here is one example.  If you have a web page that is an estore then you would like to keep a subtotal of all purchases.  To do this a function would make the JavaScript code a lot easier.  Let's  create our first function, the following is the syntax for a function.  
 

function functionName(parameters) {}

The word function notifies your HTML web page that the follow block of code is a function.  The functionName is the name of your function.  You can have zero to many input parameters.  Parameters are values going into a function.

return variable;

To return a value from a function use return with the variable that is being returned.  Only one value can be returned.

Here's our web page using a counter of animals as a function.  You can place functions in either the <head> or <body> section.  Functions must be declared before called.  A standard practice is to place functions in <head> section since most functions only compute not display information.  The code below call the adder function which is performing a running total.  The last call, adder(0) returns the accumulated total.  
 

function

accumulated total

adder(cats);

2

adder(dogs);

4

adder(goats);

11

totalAnimals = adder(chickens);

61

<html>  
  <head>  
    <title>JavaScript Example</title>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
      **var subTotal = 0;**  
      **function adder(accumulator) {**  
        **return subTotal += accumulator;**  
      **}**  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </head>

  <body bgcolor="#ccffff">  
    <div align="center"><h3>JavaScript Example</h3></div>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript

      var maxsAdoptionDate = new Date("November 4, 1984");  
      var today = new Date();  
      var cats = 2;  
      var dogs = 2;  
      var goats = 7;  
      var chickens = 50;  
      var moreCats = false;

      if (cats > dogs) {  
          moreCats = true;  
      }

      document.write("Max's Adoption Date:  " + maxsAdoptionDate + "<br />");  
      document.write("Today's Date:  " + today + "<br /><hr />");

      **document.write("Calling Function adder:  " + adder(cats) + "<br />");**  
      **document.write("Calling Function adder:  " + adder(dogs) + "<br />");**  
      **document.write("Calling Function adder:  " + adder(goats) + "<br />");**  
      **document.write("Calling Function adder:  " + adder(chickens) + "<br /><hr />");**  
      **document.write("Total Animals: " + adder(0) + "<br />");**  
      document.write("Are there more cats than dogs?  " + moreCats + "<br />");  
      document.write("Do cats equal dogs?  " + (cats == dogs) + "<br />");

      // Stop Hiding JavaScript Commands -->  
    </script>  
  </body>  
</html>  
 

 ![](script4-7.jpg)

Figure 4-7 Functions

* * *

**JavaScript with Date and** **Page Counter** **Information**  
The following code illustrates several date formats and a page counter:  
  
<html>  
  <head>  
    <title>JavaScript Example</title>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
  
      expireDate = new Date();  
      expireDate.setMonth(expireDate.getMonth() + 6);  
     
      pageCounter = parseInt(cookieVal("pageHit"));  
      pageCounter++;  
      document.cookie = "pageHit=" + pageCounter + "; expires=" + expireDate.toGMTString()  
         
      function cookieVal(cookieName) {  
        thisCookie = document.cookie.split("; ")  
         
        for (i = 0; i < thisCookie.length; i++) {  
         if (cookieName == thisCookie\[i\].split("=")\[0\]) {  
            return thisCookie\[i\].split("=")\[1\];  
          }  
        }  
        return 0;  
      }  
  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </head>  
  
  <body bgcolor=#ccffff>  
    <center><h3>JavaScript Example</h3></center>  
    <script language="JavaScript">  
      <!-- Hide JavaScript Commands from Browsers that can't interrupt JavaScript  
  
      var today = new Date();  
  
      document.write("Today's Date:  " + today + "<br /><hr />");  
  
      document.write("Current Time:  " + today.getHours());  
      document.write("  Minutes:  "    + today.getMinutes());  
      document.write("  Seconds:  "    + today.getSeconds() + "<br /><hr />");  
  
      document.write("Page Counter:  " + pageCounter);  
  
      // Stop Hiding JavaScript Commands -->  
    </script>  
  </body>  
</html>  

![](script4-8.jpg)

Figure 4-8 Date and Page Counter Example  

* * *

**JavaScript Tips**

When writing JavaScript use good programming habits:

*   Indentation, makes reading JavaScript easy to understand and maintain
*   Proper case - JavaScript is case sensitive
*   Enclose JavaScript commands within comments for Browsers that do not support JavaScript
    *   Most Browsers support JavaScript, however, they could have their JavaScript interpreter turned off
*   Test JavaScript along with your HTML programs
    *   JavaScript is interpreted differently with the various Browsers

* * *

**Java Script Examples**

The following are good examples of some simple, small JavaScript programs:

[http://www.devx.com/gethelp/GHpast10min.asp?Area=IDASGPPB&MenuID=276](http://www.devx.com/gethelp/GHpast10min.asp?Area=IDASGPPB&MenuID=276)  
[http://www.htmlgoodies.com/stips/](http://www.htmlgoodies.com/stips/)  
[http://www.javascript.com](http://www.javascript.com)  
[http://www.htmlgoodies.com/beyond/js.html](http://www.htmlgoodies.com/beyond/js.html)  
[http://www.webteacher.org/windows.html](http://www.webteacher.org/windows.html) (and click on the JavaScript link)  
[http://www.pagetutor.com/javascript/](http://www.pagetutor.com/javascript/)  
[http://hotwired.lycos.com/webmonkey/programming/javascript/tutorials/tutorial1.html](http://hotwired.lycos.com/webmonkey/programming/javascript/tutorials/tutorial1.html)

**Don’t reinvent the wheel.**

The nice thing about JavaScript is there is so much in cyberspace and so many people are willing to share their script.  There is no reason to reinvent the wheel if someone has a script that will do what you need.  If they allow, take the code, change things around and paste your outcome for use on your site.

As beginners, you don’t have to know much to be able to add JavaScript enhancements to your Web site.  Like I said above, there are a ton of free scripts out there just waiting for you to test drive them.  Take the time to review those scripts, add them to your site, change them around if you feel confident enough to and just learn from them.

Once you have the basics down you can start creating your own scripts and adding more functionality to them.  However, in order to do this, you will need to have some working knowledge of JavaScript.  

* * *

**Summary**

This lecture gave examples of using the JavaScript scripting language.  JavaScript is a nice tool for client side computing, reducing the network bottleneck.  JavaScript might not be as powerful as a programming language but there are a lot of features and uses that can make your web page more dynamic.

Make sure you visit the many sites that I have referenced, including the real true HTML reference, [http://www.w3c.org.](http://www.w3c.org)  This site is the W3 consortium and where the Internet standards are based, referenced and agreed upon.  

* * *

**Discussion Question 1**

What are the benefits of using JavaScript in addition to HTML?

**Discussion Question 2**

What are the major differences between Java and JavaScript?  What is the difference between a programming language and a scripting language?

**Discussion Question 3**

What other scripting or programming languages are available to make a web page dynamic, e.g. Java Applets.  Discuss how they work.

* * *

**References**

*   Carey, Patrick. (2001).  New Perspectives on Creating Web Pages with HTML - Comprehensive, 2nd edition.  Cambridge, MA: Course Technology ISBN 0-619-01968-9
*   Whatis.com Information Technology Web Site, Retrieved 2002 May 13 from [http://whatis.com](http://whatis.com)  
    W3C, HTML Home Page, Modified 2002 May 10, [http://www.w3.org/MarkUp/](http://www.w3.org/MarkUp/)[](http://www.w3.org/MarkUp/)

[](http://www.w3.org/MarkUp/)

* * *

![](../../../../../images/cv64logo.gif)[Web Contact](mailto:estey@cv64.us)  
Last modified:  2003 December 31