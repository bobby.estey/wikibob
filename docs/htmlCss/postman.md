# Postman

# SingleStone Demo
- [get](localhost:8118/contacts) - localhost:8118/contacts/1 - contact by id
- [post](localhost:8118/contacts)
- [put](localhost:8118/contacts/5) - update by id
- [delete](localhost:8118/contacts/1) - delete by id
- [get](localhost:8118/contacts/call-list) - get the entire call list

- Payload - raw / JSON
{
    "name": {
        "first": "Harold",
        "middle": "Francis",
        "last": "Gilkey"
    },
    "address": {
        "street": "8360 High Autumn Row",
        "city": "Cannon",
        "state": "Delaware",
        "zip": "19797"
    },
    "phone": [
        {
            "type": "home",
            "number": "302-611-9148"
        },
        {
            "type": "mobile"
        }
    ],
    "email": "harold.gilkey@yahoo.com"
}

# Spring Boot
- [get](http://localhost:8080/students/Student1/courses/Course1)
- [post](http://localhost:8123/students/Student1/courses?Username=user1&Password=admin1)
