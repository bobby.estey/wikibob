# HyperText Markup Language (HTML) / Cascading Style Sheets (CSS)

- [Max the Dog Website](https://cv64.us/cv64/wikibob/htmlCss/max/)
     - **Must See**
- [Hypertext Transfer Protocol (HTTP)](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)

# HTTP Methods and SQL Actions
|Method|Action|
|--|--|
|delete|delete|
|get|select|
|post|insert into|
|put|update|

# Links
- [hangman](https://codepen.io/cathydutton/pen/ldazc)

## [@media](media.md)
- @media standards for various displays

## [Syntactically Awesome StyleSheet (SASS)](./sass/sass.md)
