# MySQL Installation Example

- [Recording](https://www.youtube.com/watch?v=9yNZspAzHlc)

|Command|Description|
|--|--|
|docker pull mysql|- Downloads the latest version MySQL image from Repository|
|docker images|- Lists all the Docker images|
|docker run --name mysql -p 3306:3306 -v mysql_volume:/var/lib/mysql/ -d -e "MYSQL_ROOT_PASSWORD=password" mysql|- Start up image|
|docker ps|- Show all containers executing|
|docker exec -it mysql bash|- Execute MySQL image|
|mysql -u root -p|- Login to MySQL|
|mysql> use mysql;|- Use mysql database|
|mysql> select * from user;|- Select all records in user table|

## MySQL Notes

```
docker run --name mysql -p 3306:3306 -v mysql_volume:/var/lib/mysql/ -d -e "MYSQL_ROOT_PASSWORD=password" mysql
```

- name - name of the Container
- p - maps port 3306 on host to port on container
- v - mounts a volume named to persist data
- d - executes the container in detached mode (background process)
- e - sets the root password for MySQL

# MySQL Workbench

```
docker pull linuxserver/mysql-workbench
```

## MySQL Workbench Notes

- mysql configuration path - /home/cv64/.mysql/workbench

```
docker run -d --name=mysql-workbench --cap-add=IPC_LOCK -e PUID=1000 -e PGID=1000 -e TZ=Etc/UTC -p 3000:3000 -p 3001:3001 -v /home/cv64/.mysql/workbench:/config --restart unless-stopped lscr.io/linuxserver/mysql-workbench:latest

```
