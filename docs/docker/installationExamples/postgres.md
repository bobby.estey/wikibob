## PostgresSQL Installation Example

- [Recording](https://www.youtube.com/watch?v=9yNZspAzHlc)

|Command|Description|
|--|--|
|
|docker pull postgres|- Downloads the latest version PostgresSQL image from Repository|
|docker images|- Lists all the Docker images|
|docker run --name mysql -p 3306:3306 -v mysql_volume:/var/lib/mysql/ -d -e "MYSQL_ROOT_PASSWORD=password" mysql|- Start up image|
|docker ps|- Show all containers executing|
|docker exec -it mysql bash|- Execute MySQL image|
|mysql -u root -p|- Login to MySQL|

## Notes

```
docker run --name mysql -p 3306:3306 -v mysql_volume:/var/lib/mysql/ -d -e "MYSQL_ROOT_PASSWORD=password" mysql

--name - name of the Container
-p - maps port 3306 on host to port on container
-v - mounts a volume named to persist data
-d - executes the container in detached mode (background process)
-e - sets the root password for MySQL
