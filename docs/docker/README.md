# Docker

- Starts up a Container that enables testing and deploying applications by packaging up into images, then executing images
- Prior to Docker, deployment process, set up hardware, operating system, software, applications
- Standardized Application Packaging - same packaging for applications
- Multi-Platform Support - Local, Data Center, Cloud (AWS, Azure, GCP)
- Isolation - containers are separated from each other

## Docker Links / Terms

- [Docker](https://docker.com)
- [Docker Hub](https://hub.docker.com) - a registry to host Docker images
- Docker Registry - Docker Images are stored in the Registry
- Docker Repository - Docker Images for a specific application version
- Dockerfile - Docker Image creator
- [Archive](./archive/README.md)

## [Installation Linux](https://www.youtube.com/watch?v=Gkh-M2QkJaE)

```
sudo apt update
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
```

## [Installation](https://www.youtube.com/watch?v=38q5YRLzqD8)

- https://docs.docker.com/
- Select the Operating System and Download the executable
- There are other prerequisits required before installing Docker, e.g.

# Add Docker's official GPG key:

```
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
```

# Add the repository to Apt sources:

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "${UBUNTU_CODENAME:-$VERSION_CODENAME}") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

#### Installing Docker

```
sudo apt-get install ./docker-desktop-amd64.deb
```

#### Create Docker Account

- Creating a Docker Account allows access to the Repository, pushing and pulling code
- Select Create an account

![CreateDockerAccount](createDockerAccount.png)

## Testing Installation

|Command|Description|
|--|--|
|docker --version|- Displays the version of Docker|
|docker run hello-world|- Executes an image<br>- If image is not already loaded, Docker pulls down the image from the Repository|
|docker pull <image>|- Pulls down the image from the Repository|

## Docker Installation Examples

- [MySQL](./installationExamples/mysql.md)

## Troubleshooting

|Issue|Solution|
|--|--|
|Invalid Key|sudo gpg --keyserver pgpkeys.mit.edu --recv-key ACTUAL_KEY|

#### Docker Permission Denied

```
sudo service docker restart
service docker status
docker images
sudo docker run hello-world
sudo groupadd -f docker
sudo usermod -aG docker $USER
newgrp docker
groups
```

#### Remove Docker - snap

- which docker
     - /snap/bin/docker
- sudo snap disable docker
     - docker disabled
- sudo snap remove --purge docker
     - docker removed

#### Remove Docker - apt

```
sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
sudo rm /etc/apt/sources.list.d/docker.list
sudo rm /etc/apt/keyrings/docker.asc
```
