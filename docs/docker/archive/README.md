# ARCHIVE - old stuff

## docker container run -d -p 6464:6464 cv64/genealogy:0.0.1.RELEASE

- d - detached mode - get terminal prompt back, e.g. spawned process
- p - hostPort:containerPort 
     - maps internal docker port (containerPort) to the hostPort
     - only developer can change containerPort
- https://hub.docker.com/r/cv64/genealogy
- Image - Set of Bytes, specific version of application
- Container - Runtime instance of Docker Image
- Repository Name - cv64/genealogy
- Release - 0.0.1.RELEASE - version
- Docker has an internal network called bridge network
- Clients use host port to access the application

# Docker Installation

### Docker and Docker compose prerequisites

```
sudo apt install -y ca-certificates curl gnupg lsb-release
```

### Download the docker gpg file to Ubuntu

```
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

### Add Docker and docker compose support to the Ubuntu's packages list

```
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Add Docker and docker compose support to the Ubuntu's packages list

```
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-pluginsudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-pluginlinux/ubuntu   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Install docker and docker compose on Ubuntu

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

### Test docker setup

```
sudo docker --version
docker compose version
sudo docker run hello-world
sudo docker images
sudo docker ps -a

Hello from Docker!
This message shows that your installation appears to be working correctly.
```

# Running Docker

sudo docker run -d -p 27017:27017 --name cv64_mongodb mongo:latest
- (d) detach - run container in background and print container ID
- (p) ports - publish containers ports to the host (current:host) are 27017:27017
- name - name of the container
- image name

# Install Docker Desktop

- [Docker Desktop on Ubuntu](https://docs.docker.com/desktop/install/ubuntu/)
- Download DEB package
- sudo apt update
- sudo apt install ./docker-desktop-<version>-<arch>.deb

```
NOTE:  Ignore Error:  Download is performed unsandboxed as root as file docker-desktop-<version>-<arch>.deb couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
```

# Docker Desktop Commands

```
systemctl --user start docker-desktop
systemctl --user enable docker-desktop
systemctl --user stop docker-desktop

```

# Configuration

- [Docker Configuration 1](dockerConfigurationIntelliJ1.png)
- [Docker Configuration 2](dockerConfigurationIntelliJ2.png)
- [Docker Configuration 3](dockerConfigurationIntelliJ3.png)
- [Docker Configuration Mongo](dockerConfigurationIntelliJMongo.png)
- Docker -> Images -> Image to pull:  mongo
- Default Tag:  latest
- Right Click mongo -> Create Docker Configuration
    - Change Name to MongoDB Config
    - Server Docker
    - Container Name:  MongoDBConfig
    - Press Modify Options
    - Select Environment Variables -> Enter Environment Variables
        - Username
        - Password
    - Port Bindings
        - Host port:  3306
        - Container port:  3306
        - Protocol:  tcp
    - Press Run, will create the Container

## MongoDB

- sudo docker pull mongo
     - download and install the latest version of docker
    
- sudo docker images
     - check the versions of databases available, not mongo is the latest version as of two weeks ago
     
|REPOSITORY|TAG|IMAGE ID|CREATED|SIZE|
|--|--|--|--|--|
|mongo|latest|9a5e0d0cf6de|2 weeks ago|646MB|
|hello-world|latest|feb5d9fea6a5|18 months ago|13.3kB|
|mysql|5.7.8|adedf30d6136|7 years ago|358MB|
    
- sudo docker run --name cv64 -d mongo
     - create a container, e.g. cv64
    
- sudo docker ps -a
     - check if the container is executing
    
|CONTAINER ID|IMAGE|COMMAND|CREATED|STATUS|PORTS|NAMES|
|--|--|--|--|--|--|--|
|6e0b666f51a1|mongo:latest|"docker-entrypoint.s…"|2 minutes ago|Up 2 minutes|27017/tcp, 0.0.0.0:27018->27018/tcp, :::27018->27018/tcp|cv64_mongodb|
|07475007d6de|mysql:8-oracle|"docker-entrypoint.s…"|5 seconds ago|Up 4 seconds|0.0.0.0:3306->3306/tcp, 33060/tcp|mysql|

## MySQL

```
docker run --detach --env MYSQL_ROOT_PASSWORD=password --env MYSQL_USER=cv64 --env MYSQL_PASSWORD=password --env MYSQL_DATABASE=cv64 --name mysql --publish 3306:3306 mysql:8-oracle
```

|Field|Description|Example|
|--|--|--|
|docker run --detach|Execute Docker Container in the Background|
|(env) MYSQL_ROOT_PASSWORD|root password|password|
|(env) MYSQL_USER|user account|cv64|
|(env) MYSQL_PASSWORD|user password|password|
|(env) MYSQL_DATABASE|database|cv64|
|name|name of the container|mysql|
|publish|execute on port|3306|
|image||mysql:8-oracle|

# Miscellaneous

```
service docker status
    - displays the docker status
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2023-03-30 01:48:04 PDT; 37min ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 3154 (dockerd)
      Tasks: 22
     Memory: 73.7M
        CPU: 778ms
     CGroup: /system.slice/docker.service
             └─3154 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Mar 30 01:48:03 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:03.644484278-07:00" level=info msg="ccResolverWrapper: sending update to cc: {[{unix:///run/containerd/containerd.sock  <nil> 0 <nil>}] <nil> <nil>}" module=grpc
Mar 30 01:48:03 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:03.644493601-07:00" level=info msg="ClientConn switching balancer to \"pick_first\"" module=grpc
Mar 30 01:48:03 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:03.649203816-07:00" level=info msg="[graphdriver] using prior storage driver: overlay2"
Mar 30 01:48:03 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:03.656567509-07:00" level=info msg="Loading containers: start."
Mar 30 01:48:03 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:03.981217921-07:00" level=info msg="Default bridge (docker0) is assigned with an IP address 172.17.0.0/16. Daemon option --bip can be used to set a preferred IP address"
Mar 30 01:48:04 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:04.036479324-07:00" level=info msg="Loading containers: done."
Mar 30 01:48:04 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:04.061304558-07:00" level=info msg="Docker daemon" commit="20.10.21-0ubuntu1~22.04.2" graphdriver(s)=overlay2 version=20.10.21
Mar 30 01:48:04 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:04.061427390-07:00" level=info msg="Daemon has completed initialization"
Mar 30 01:48:04 cv64-Inspiron-16-7610 systemd[1]: Started Docker Application Container Engine.
Mar 30 01:48:04 cv64-Inspiron-16-7610 dockerd[3154]: time="2023-03-30T01:48:04.076296842-07:00" level=info msg="API listen on /run/docker.sock"
```

# Commands

```
docker compose up -d
docker compose ps
docker logs postgres -f
docker version get the full version
docker --version get the docker version and make sure installed (brief)
```

# Notes

```
sudo chmod 755 /var/lib/mongodb/mongod.lock
docker run -d -p 80:80 docker/getting-started
```

## Remove Docker Desktop Ubuntu

```
sudo apt remove docker-desktop
rm -r $HOME/.docker/desktop
sudo rm /usr/local/bin/com.docker.cli
sudo apt purge docker-desktop
```

# Docker Build

```
cd <project>, e.g. ~/git/cv64/springboot-mongodb-docker
sudo docker build -t springboot-mongodb-docker:1.0 .
sudo docker images
sudo docker run -p 8080:8080 --name springboot-mongodb-docker --link cv64_mongodb:mongo -d springboot-mongodb-docker:1.0
sudo docker ps -a
```

|CONTAINER ID|IMAGE|COMMAND|CREATED|STATUS|PORTS|NAMES|
|--|--|--|--|--|--|--|
|3318c4a856c4|springboot-mongodb-docker:1.0|"java -jar app.jar"|About a minute ago|Exited (1)|About a minute ago|springboot-mongodb-docker|
|6e0b666f51a1|mongo:latest|"docker-entrypoint.s…"|5 hours ago|Up 5 hours|27017/tcp, 0.0.0.0:27018->27018/tcp, :::27018->27018/tcp |cv64_mongodb|
|d865e7d1e4ce|mysql:5.7.8|"/entrypoint.sh mysq…"|18 hours ago|Up 5 hours|3306/tcp, 0.0.0.0:3307->3307/tcp, :::3307->3307/tcp|cv64|

```
sudo docker logs springboot-mongodb-docker  (shows the logs and port 8080 should show up as a context path)
```

|REPOSITORY|TAG|IMAGE ID|CREATED|SIZE|
|--|--|--|--|--|
|springboot-mongodb-docker|1.0|264da936a655|3 minutes ago|551MB|
|mongo|latest|9a5e0d0cf6de|2 weeks ago|646MB|
|hello-world|latest|feb5d9fea6a5|18 months ago|13.3kB|
|mysql|5.7.8|adedf30d6136|7 years ago|358MB|

## Docker Build Log

- [Docker Build Log](dockerBuild.log)
