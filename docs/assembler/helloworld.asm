.386                                  ; use the 386 instruction set 
.model flat, stdcall                  ; .model specifies the memory model of the program
									  ; flat is windows
                                      ; stdcall - parameters are pushed from left to right
option casemap :none                  ; labels are case sensitive

include \masm32\include\kernel32.inc  ; include for ExitProcess
include \masm32\include\masm32.inc    ; include for StdOut 
includelib \masm32\lib\kernel32.lib   ; include the libraries above 
includelib \masm32\lib\masm32.lib

.data                                 ; all initialized data must follow the .data directive
    message db "Hello World!", 0      ; define byte named 'message' to be the string 'Hello World!'
                                      ; followed by the NULL character, ASCII 0

.code                                 ; all code must follow the .code directive
    main:							  ; start the main code
      invoke StdOut, addr message	  ; 
      invoke ExitProcess, 0			  ; 
    end main                          ; end main code