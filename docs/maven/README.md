# Maven

- Software Project Management and Comprehension Tool
     - How projects are built and the management of dependencies
- Project Object Model (POM)
     - eXtensible Markup Language (XML) configuration file

# References

- [resources/applications.properties](application.properties)
- [Maven Apache Project](https://maven.apache.org/)
- [Maven Central Repository](https://mvnrepository.com/repos/central)
- Maven Local Repository - /home/cv64/.m2/repository
- [Maven Reference Manual](https://maven.apache.org/archives/maven-1.x/maven.pdf)

# Lifecycle

Validate -> Compile -> Test -> Package -> Integration Test -> Verify -> Install -> Deploy

# Dependencies List

- Edit POM file
- Select Dependency
- Press Ctrl + Click -> Provides the Dependencies for that one Dependency

# Maven Release Versioning

- MAJOR.MINOR.PATCH[-MODIFIER]
- Modifiers
     - M - Milestone
     - RC - Release Candidate
     - Snapshots - Snapshot (Under Development)

# Troubleshooting

|Issue|Solution|
|--|--|
|Error in XML Prolog|[forceDownload](forceDownload.png)|
