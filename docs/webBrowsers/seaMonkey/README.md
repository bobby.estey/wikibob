# Seamonkey Browser

## Installation

- [Download](https://www.seamonkey-project.org/)

#### Linux

```
unzip .bz file
sudo su
cd /opt
cp -R <download folder>/seamonkey/ .
vi ~/.bashrc
alias seamonkey='/opt/seamonkey/seamonkey &'  # seamonkey
```
