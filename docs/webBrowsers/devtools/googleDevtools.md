# [Google Development Tools](https://developers.google.com/web/tools/)

- Google Development Tools (The Inspector) assists a developer with designing and developing a Web Page.  Please go here for additional details about the product:
- To start Google Development Tools, right click the webpage and select Inspect
- There are also shortcuts, for example on:
     - Linux or Windows Operating System, press F12 or Control + Shift + Uppercase I (the letter “I”)  
     - A Mac computer is Command + Option + Uppercase I
      
## ![Docking](docking.png)

- Here is a webpage and the Inspector displaying above:

- Step 1, select the type of docking you want the Inspector to display, this screen is set to DOCK TO BOTTOM, below the webpage.

- Step 2, there are 4 dock side settings, Press the docking you wish by selecting the icon:
     - Undock, separate window
     - Dock to Bottom (current setting shown)
     - Dock to Left
     - Dock to Right

## ![Elements / Styles](elementsStyles.png)

- Elements and Styles – the Elements are the HyperText Markup Language (HTML) Elements
- HTML Elements are the components on a webpage that contain the CONTENT.  For example, the text, images, videos, links, etc.  
- HTML elements are usually stored in a .html file
- Styles are the appearance and formatting of the components.  For example, font family, font size, font color, background color, etc.
- Styles are usually stored in a .css file meaning Cascading Style Sheet (CSS)

- Arrow 1 is pointing to the Elements Panel
- Arrow 2 is pointing to the Styles Panel
- Arrow 3, Right Click on the web page and the Elements and Styles will navigate to the Element and CSS automatically.  
- Arrow 4, this example the link Say Hello was right clicked and the HTML is highlighted light blue in the Elements Panel.  
- Arrow 5, the Styles shows that the background for this page is red.

## ![Sources1](sources1.png)

- Console Panel, shows what is being logged to the console.
- Errors
- Warnings
- Information

## ![Sources2](sources2.png)

- Arrow 1 – Pointing to the Sources Panel
- Arrow 2 – file is highlight as Gray meaning the file is displayed in the center panel
- Arrow 3 – confirmation of the file that is being displayed
- Arrow 4 – the debugger was implemented and line 7 has a breakpoint set.
     - To enable breakpoints, click the line number once, to disable click the line number again
- Arrow 5 – debugger navigation buttons
     - Resume – Blue Bar and Arrow
     - Step Over – Arrow arching over the dot
     - Step Into – Arrow pointing to the dot (vertically)
     - Step Out – Arrow pointing away from the dot
     - Step – Arrow point to dot (horizontally)
     - Deactivate Breakpoints – Arrow with slash
     - Pause on Exceptions – Step sign with two bars
- Arrow 6 – Variable and Values, the can be changed while program is running

## ![Network](network.png)

- Network Panel – shows requests and responses between the Website and Server

|File Types|Media|
|--|--|
|Doc|JSP|
