# markdown Presentation

## [README.md](./README.md)

- git uses Markdown as a preferred documentation tool
- Discuss README.md
- Markdown View versus to Raw View
     
## File System Comparisons

- Web Browser
- File Browser
- Command Line
     
## Demos

#### Convert any file to Markdown Demo

- Utilize git to check in the code
- Show the History

#### [Table of Contents Demo - Hamburger](tableOfContentsDemo.md)

#### Search Demo

- Immediate Response
- Search Works
- Try this on m$Office or Sharepoint, DOESN'T WORK

#### Performance Demo

- Immediate Response

## Conclusion

- Markdown is superior to m$ Office
- Work Smarter with Markdown and not Harder with m$ Office
