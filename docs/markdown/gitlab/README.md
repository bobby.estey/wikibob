# gitlab instructions

## Project Member Management

- Select Project

- Select Manage -> Members -> Invite members

![Members](members.png)

- Select Invite members

![Invite members](invite.png)

- Fill out form

     - Enter Username, name or email address, e.g. **Bobby Estey**
     - Select a role, e.g. **Reporter**
     - Access expiration date (optional)
     - Press Invite

![Invite Members](inviteMembers.png)

## Change username

- https://gitlab.com/-/profile/account

![Change Username](changeUsername.png)

