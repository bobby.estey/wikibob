# Transition to Markdown (.md) and say good bye to Office

## References

#### Overview

- [Quick Examples Markdown File](markdownQuickExamples.md)
- [Quick Examples Recording](https://rumble.com/v3dlpb5-markdown-quick-examples.html)

#### Markdown - Table of Contents Example

- [Table of Contents Demo](tableOfContentsDemo.md)
- [Table of Contents Recording](https://rumble.com/v5euet9-markdowntableofcontents.mp4.html)

#### Markdown - Git History Example

- [Git History / Markdown Recording](https://rumble.com/v5euen0-markdowngitexample.mp4.html)

#### Other References

- [More git Stuff](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/git/README.md)
- [Markdown Tutorial](https://www.w3schools.io/file/markdown-introduction/)
- [Markdown to HTML Converter](https://codebeautify.org/markdown-to-html)
- [Test Markdown Knowledge](https://www.markdowntutorial.com/)

## [History](https://en.wikipedia.org/wiki/Markdown)

## Why should you not use Office

- Office Products are proprietary, heavyweight, archaic, obsolete, expensive, static, slow, searches do not work and SEVERAL other negatives
- If you are still living in the dark ages, then Office is for you:
     - [Eight Track Tapes](https://en.wikipedia.org/wiki/8-track_cartridge)
     - [CB Radios](https://en.wikipedia.org/wiki/Citizens_band_radio) 
     - [Rotary Dial Phones](https://en.wikipedia.org/wiki/Rotary_dial)
     - [Being like your Parents with Dr Rick](https://www.youtube.com/results?search_query=dr+rick+progressive+commercials) commercials 
- THAT'S RIGHT, OFFICE IS OLD, VERY OLD.  Your **GRAND PARENTS** know Office better than you.  Notice I said **GRAND PARENTS**

## [Why Markdown](https://www.markdownguide.org/getting-started/#)

- A freeware technology, delivering a professional approach towards developing dynamic, fully functional, configuration managed Documentation
- Interfaces nicely with any GIT repository
- Easy to learn and works with many Integrated Development Environments (IDEs) and Cloud Based products
     - these products already have Markdown rendering software which allow a "What You See Is What You Get (WYSIWYG)" interface
- There are many document artifact instances that utilize Markdown today

## git commands

- These are the git commands used often and must be used within a git directory

|Command|Example|Description|
|--|--|--|
|clone|git clone URL|- Copy from the repository all contents to the local machine|
|status|git status|- Get the Status between the local machine and the repository|
|add|- git add file(s)<br>- git add .|- Add files to the repository<br>- **git add .** means add all files|
|commit|git commit -m "text message"|- Prepare the files to send to the repository and provide a message about the commit|
|push|git push|- Push up from the local machine to the repository|
|pull|git pull|- Pull down from the repository to the local machine|

#### README.md - is like HTML index.html

- I will be glad to assist anyone willing to learn this new approach, transitioning from Office to Markdown

## Example Sites

- This is one example of my WikiBob site which utilizes Markdown:  [WikiBob](https://gitlab.com/bobby.estey/wikibob/-/blob/master/README.md)
- Former Student who took the Challenge, HIRE HER:  [Kimberly Hernandez](https://gitlab.com/kghernandez9/repositorypage/-/blob/main/README.md)

## Git Servers are not Git

- [Git Servers](https://www.git-tower.com/blog/git-hosting-services-compared/)

## Markdown Examples

|Description|Example|
|--|--|
|Hamburger Icon|[Hamburger Icon](hamburgerIcon.png)|
|Table of Contents|[TOC] - or select the Hamburger Icon which automatically displays the Table of Contents|
|Anchor Link|```[Anchor Name](#anchorLink)```|
|Custom Anchor Link|```# <a name="custom-anchor"></a>Custom Anchor Link```|
|[Cell Within Cell](cellWithinCell.md)|- Use HTML within the Markdown File|
|display less than and greater than in markdown|&lt;display less than and greater than in markdown &gt;|

## NOTE TO JOB APPLICANTS (Students)

You are at a Job Interview and want that job so how do you market yourself?  How to do WOW an Employer:

- Option 1:  Provide a paper resume and discuss your capabilities

- Option 2:  Inform the employer about your Markdown website (profile)
     - Present the employer your Markdown website and go over the details, e.g.
          - Here is my online resume
          - Here is my documentation, my code, my application executing, my recordings and much more
          - BLAH BLAH BLAH
     
Who do you think is going to get the job?

## [Gitlab - Instructions](./gitlab/README.md)
