# Cell Within Cell

## References

- [merge-table-rows-in-markdown](https://stackoverflow.com/questions/46621765/can-i-merge-table-rows-in-markdown)

## Using HTML

<table>
    <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
            <th>Column 3</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4>Column 1 Row 1</td>
            <td rowspan=2>Column 2 Row 1</td>
            <td>Column 3 Row 1</td>
        </tr>
        <tr>
            <td>Column 3 Row 2</td>
        </tr>
        <tr>
            <td rowspan=2>Column 2 Row 2</td>
            <td>Column 3 Row 3</td>
        </tr>
        <tr>
            <td>Column 3 Row 4</td>
        </tr>
    </tbody>
</table>
