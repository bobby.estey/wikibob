# ![](../../docs/icons/cv64us50x50.png) Formula Translater ForTran

## installation instructions

sudo apt-get install gfortran

## notes

* code starts in column 8

| command | syntax              | description                              | 
| ------- | ------------------- | ---------------------------------------- | 
| compile | gfortran <filename> | gfortran inputOutputData.f outputs a.out | 
| execute | ./a.out             | execute the compile file a.out           | 


