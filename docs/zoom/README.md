# ZOOM Meeting Software

## Technical Support

- [GCU Zoom Technical Support](https://support.gcu.edu/hc/en-us/articles/360052151393-Zoom-FAQ)

## Installation

- sudo apt update && sudo apt upgrade
- sudo snap install zoom-client
- zoom-client

## New Meeting 

- Press New meeting -> Use my personal meeting ID (PMI) -> Copy invitation
- Press New meeting

![PMI](newMeetingPMI.png)

- Select Open xdg-open

![xdg-open](xdg-open.png)

## Zoom Workplace

- Select Chat
- Select Show captions -> Save
- Select Share -> Desktop #
     - Select Share sound
     - Select Optimize for video sharing
     - Press Share
- **TAKES TIME TO INITIALIZE** - Then the Shared Screen appears

![Window](window.png)

- Press Show meeting
- Select My screen

![My Screen](myScreen.png)

- Select View Fullscreen

![Full Screen](fullScreen.png)

## Recording

- Select Recording -> Select Record to this Computer
- Select End -> End meeting for all
- **Recording Location** - /home/cv64/snap/zoom-client/current/Documents/Zoom/

![Video Conversion](videoConversion.png)

## Meeting Email / Invitation

## Convert Zoom File to MP4 after a Technical Issue

- This approach works with all OSes (Linux, Mac, Windows)
- Start a new meeting and recording, play for 5 seconds and stop, DO NOT END THE MEETING
- Copy the 2 previous unconverted files to new recording directory, replacing the 2 new unconverted files (the 5 second recording)
- End the meeting
- ZOOM will give you a warning as ZOOM is confused about the new files in the new recording.  Ignore and select Convert Anyway

![Convert Anyway](convertAnyway.png)

- [VIDEO](https://www.youtube.com/watch?v=3z6-wn-oS3E)

## Remove Files from Zoom

- Navigate to Recordings & Transcripts
- Select All
- Delete
