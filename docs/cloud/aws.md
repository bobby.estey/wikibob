# Amazon Web Services (AWS)

# [AWS WebSite](https://aws.amazon.com/)

- Global Cloud Platform (Hosting Service - executing several services)
- Hosting and Manage Web Services on Internet
- Infrastructure Service - Backups and Power Supplies of the Service
- Platform Service - Java, Ruby, PHP and more (binaries provided)
- Software as a Service (SaaS) - (email, queueing)
- Cloud Storage Platform

# Infrastructure

- Region, e.g. San Francisco
- Availability Zone, 3 locations in San Francisco

# Services

- Elastic Compute Cloud (EC2)
    - Servers (Hardware) to execute software, Services, Virtual Private Servers
    - Compute Storage and Networking, there are various options for different tasks and capabilities
- Virtual Private Cloud (VPC) 
    - Small Segment Private Cloud, not full Amazon Cloud
    - Create Networks and execute services in the VPC
- Simple Storage Service (S3)
    - File Storage and Sharing Service
- Relational Database Service (RDS)
    - Oracle, MySQL, Postgres, SQLServer, Aurora
- Route 53
    - Dynamic Name Service (DNS)
    - Global and Dynamic DNS Service
- Elastic Load Balancing (ELB)
- Autoscaling

