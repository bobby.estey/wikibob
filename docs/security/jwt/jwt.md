# JavaScript Object Notation (JSON) Web Token (JWT)

- [Website](https://jwt.io)

Used to validate a route

# Anatomy

- Header (Algorithm and Token Type)
- Payload (Data)
     - iat (Issued At Time)
     - exp (Expires At Time)
- Verify Signature

# Dependencies

- npm i bcryptjs (used to hash password)
- npm i jsonwebtoken


# Project MERN

- Testing Tokens
     - Login - post-api-users-login - get Token
     - User - get-api-users-get - postman -> authorization -> Type -> Bearer Token -> Paste in Token
     