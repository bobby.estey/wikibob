# Security

## Securing Browsers / Software and Hardware

- AdBlock Extension
- Virtual Private Network (VPN)
- Remove Untrusted or Unused Extensions
- Two Factor Authentication
- Stegnography
- Software / Firmware Updates
- Implement Strong Malware Scanners and Spam Filters
- Rotating Passwords, e.g. word-word-word-word
- Avoid email over free WiFi
- Use encrypted email
- Connect HTTPS / TLS 1.2+
- Use EV validated sites
- Clear Browser History
- Never reply or unsubscribe from Spam
- Utilize Browser Security
- Never Store Passwords in Browser

## Terminology / Glossary

|Term|Description|
|--|--|
|[Attribute-Based Access Control (ABAC)](https://www.nextlabs.com/products/technology/abac/)|Operations are granted or denied based on assigned attributes|
|[eXtensible Access Control Markup Language (XACML)](https://www.nextlabs.com/a-business-users-guide-to-xacml/)|Attribute-based access control policy language|
|[Identity and Access Management (IAM)](https://auth0.com/blog/what-is-iam/)|Ensures the correct roles / individuals can access the tools they need|
|[Policy Administration Point (PAP)](https://www.nextlabs.com/what-is-a-policy-administration-point-pap/)|Administration of the Policies|
|[Policy Decision Point (PDP)](https://www.nextlabs.com/what-is-a-policy-decision-point-pdp/#:~:text=A%20Policy%20Decision%20Point%20(PDP)%20is%20mechanism%20that%20evaluates%20access,user%20who%20issued%20the%20request.)|Evaluates access requests to resources to authorization policies|
|[Policy Enforcement Point (PEP)](https://www.nextlabs.com/what-is-a-policy-enforcement-point-pep/#:~:text=A%20Policy%20Enforcement%20Point%20(PEP)%20protects%20an%20enterprise's%20data%20by,eXtensible%20Access%20Control%20Markup%20Language%E2%80%9D.)|Receives authorization requests sent to the PDP for evaluation|
|[Policy Information Point (PIP)](https://www.nextlabs.com/what-is-a-policy-information-point-pip/)|Additional attributes needed for Authorization|
|[Policy Retrieval Point (PRP)](https://curity.io/resources/learn/entitlement-management-system/#:~:text=in%20authoring%20policies.-,The%20Policy%20Retrieval%20Point,each%20PDP%20that%20requires%20them)|Central distribution point where policies are pushed or pulled from each PDP|

## Links

- [JavaScript Object Notation (JSON) Web Token (JWT)](./jwt/jwt.md)
- [Symantec SiteMinder](./siteMinder/siteMinder.md)
