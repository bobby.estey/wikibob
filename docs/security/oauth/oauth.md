# OAUTH

# Links

- [OAUTH Example Google Cloud](https://www.youtube.com/watch?v=zHfR96IZECQ)
- [OAUTH Playground](https://www.oauth.com/playground/index.html)
- [OAUTH Postman](https://www.youtube.com/watch?v=pxD9e2fk9fE)
- [OAUTH Spring](https://www.youtube.com/watch?v=ouQ8GI-owqs)
- [OAUTH Debugger](https://oauthdebugger.com/)
- [OAUTH Client Examples](https://stackoverflow.com/questions/34917822/mock-oauth-server-for-testing)

# Process

![Create Client Account](oauth1.png)

|Key|Value|
|--|--|
|client_id|BSQ6knNpUNdG4e0M77hEU3_l|
|client_secret|fVhR57v58i49hLw4-Wf5JWd3nXGKUqKnS5InTpLwF3IylHZM|
|login|fantastic-donkey@example.com|
|password|Better-Quagga-71|

![Build Authorization URL](oauth2.png)
![Validate States Match](oauth3.png)
![Setting up the POST](oauth4.png)
![Access Token Received](oauth5.png)
