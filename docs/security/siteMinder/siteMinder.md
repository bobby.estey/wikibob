Symantec SiteMinder

# Terminology / Glossary

|Term|Description|
|--|--|
|||

# Links

- [Symantec SiteMinder](https://techdocs.broadcom.com/us/en/symantec-security-software/identity-security/siteminder/12-8/getting-started.html)
- [Programming in C](https://techdocs.broadcom.com/us/en/symantec-security-software/identity-security/siteminder/12-8/programming/sdks/programming-in-c.html)
- [Programming in Java](https://techdocs.broadcom.com/us/en/symantec-security-software/identity-security/siteminder/12-8/programming/sdks/programming-in-java.html)

# Recordings

- [Symantec SiteMinder 12.8.06](https://www.youtube.com/watch?v=Av16ppjwtTE)

# Questions

- Administration is performed outside our group and the Admin UI is inexcessible
- Is CA? Computer Associates and is Symantec and CA the same
- Do we have access to OneView Monitor
- Do we have access to other applications, e.g. policy server, auditing, logging, accounts, etc
- I just scanned the documentation and only saw Session management, are there others
- User sessions, who manages these
- What is the architecture utilized?  Unix, Oracle, JEE, LDAP?  Can we get the links to the architecture?
- Do we have access to SQL Query Schemes
- Are we just client side
