# ![](../../docs/icons/cv64us50x50.png) Apache Software Foundation

[Apache Software Foundation (ASF)](https://apache.org)

- Amazing Products that are available for free
- ASF products are freeware and are supported on several platforms, not just two

#### History

- ASF was established in 1999 and is an organization that provides many software products for free.  Before becoming ASF, Apache had one product that most utilized in the past and still today, that product is called the [Apache HTTP Server](https://httpd.apache.org)
- The HTTP Server was initially available to the public back in 1995 and still today freeware.  You can download the HTTP Server, write up HTML pages and start serving your webpages.  Again, Apache was the pioneer and still very active today.

#### Popular Technologies

- Ant
     – in the past the C/C++ programming languages would compile several files into a single executive, the tool was called make.  Ant is the Java make equivalent and kidding aside, Ant was also called make without the wrinkles because make was very hard and painful
- Maven
     – is a Project Builder, can compile, package and deliver products
- Geronimo
     – is a Java Enterprise Edition software product, provides frontend look and feel software, middleware business logic and backend database connections
     - Enterprise software is utilized by everyone who utilize the Internet today
- OpenOffice
     - if you do not want to pay money for proprietary Office Software you have several choices OpenOffice, LibreOffice or Google Docs
     - OpenOffice is a client side application and Google Docs is a server side application
     - To install OpenOffice, go to [OpenOffice](http://www.openoffice.org/)
     – select your Operating System and download
     - Once installed you can open up and use just like any word processor, spreadsheet, presentation software, etc.
     - NOTE:  That I never call technologies word, excel or powerpoint
     - These are not technologies but product offerings
     - The correct terminology is word processor, spreadsheet or presentation software
- POI – is a Java Application Programming Interface for Office Document File Manipulation.  POI can read or write Office Formatted Files
