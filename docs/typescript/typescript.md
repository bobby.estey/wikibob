# TypeScript

- compiles to JavaScript
- static types (optional)
- works with front end JavaScript and back end Node.js
- includes features from ES6/7
    - classes, arrow functions, etc.
    - third party libraries
    - var is ES5, let / const is ES6/7
- dynamic typed languages, e.g JavaScript
     - types are associated at runtime
     - static types explicitly assigned in code
- npx create --template typescript (ts / tsx)

|Pros|Cons|
|--|--|
|more robust|more code|
|easy to spot bugs|more to learn|
|predictability (consistency|requires compiling, e.g. TypeScript to JavaScript|
|Readability|Not True Static Type|
|Popular|Transpiled|

## TypeScript Compiler (TSC)

- tsconfig.js - configuration file for TSC, e.g. tsc --init
- sudo npm -i -g typescript
- tsc -v
- index.ts
- tsc index -> index.js
- tsc --watch // shows with every change

# Examples

```
let id: number = 5;  // type inference
let ids: number[] = [1, 2, 3];
ids.push(4);
let arr: any[] = [1, true, 'Hello']; // legal
let person: [number, string, boolean] = [1, 'Bobby', true]; // tuple
let id: string | number = 22; // union
```

## Enumerations

```
enum Direction {up, down, left, right};
console.log(Direction.up);
enum DirectionInt {up = 1, down, left, right} // start with 1 for up 2 down, ...
enum DirectionString {up='Up', down='Down', left='Left', right='Right'};
```

## Object

```
const user : {id: number, name: string} = {id: 1, name: 'Bobby'}
```

## Types - Used with Primitives and Unions

type User = {id: number, name: string];
const user: User = {id: 1, name='Bobby'}
type Point = number | string; // union

## Type Assertion

```
let cid: any = 1; let custId = <number>cid;  or let custId = cid as number;
```

## Functions

```
function addNumber(x, y) { return x + y } // implicit
function addNumber(x: number, y: number): number { return x + y } // explicit
```

## Interfaces - use with Objects (like a Type)

```
interface UserInterface {id: number, name: string}
const user1: UserInterface = {id:1, name: 'Bobby'}
const point1: Point = 1; // Interface would be an error
interface Optional Interface { id: number, readonly name: string, age?: number } // optional interface example

interface MathFunction { x: number, y: number) : number => x + y;
const add: MathFunction = (x: number, y: number) : number => x + y;
const sub: MathFunction = (x: number, y: number) : number => x - y;
```

## Classes

- access modifiers / data modifiers
    - private - class access only
    - protected - class and extended classes access only
    - public - access for all
    

```
class Person { 
  private id: number;
  name: string; // implied public
  
  constructor() {
    console.log(123);
  }
  
  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
  
  register() { return `${this.name} is now registered`}
  console.log(bobby.register());
  
  interface PersonInterface { id: number, name: string, register(): string }
  class Person implements PersonInterface;  // 40:10
    
}
```

## Extending Classes

```
class Employee extends Person {
  position: string;
  constructor (id: number, name: string, position: string) {
    super(id, name);
    this.position = position;
  }
}

const emp = new Employee(1, 'Bobby', 'Developer');
console.log(emp.name, emp.register());
```

## Generics - Build reusable components


```
function getArray(items: any[]): any[] {
  return new Array().concat(items);
}

let numberArray = getArray([1, 2, 3]);
let stringArray = getArray(['Bobby', 'Ester']);
numberArray.push('Rebby'); // no error, accepts any

function getArray<T>(items: T[]): T[] {
  return new Array().concat(items);
}

let numberArray = getArray<number>([1, 2, 3]);
numberArray.push('Rebby'); // error, expecting a number type

```

# React Examples / unary operation

```
export interface Props {
  title: string;
  color?: string
}

export const Header = (props: Props) => {
  return (
    <header>
      <h1>{props.title}</h1>
      <h1 style={{color: props.color ? props.color : 'red'}}>{props.title}</h1>
    </header>
  )
}


// App.tsx

import Header from './Header';

function App() {
  return (
    <div classname='App'>
      <Header title = 'Helloworld'/>
    </div>
  )
}
```
