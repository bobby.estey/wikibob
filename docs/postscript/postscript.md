# Postscript

## code

- [Postscript.java](https://gitlab.com/bobby.estey/wikibob/-/tree/master/java/postscript)
- [Postscript.c](Postscript.c)

## ps examples

- [chrysler.ps](chrysler.ps)
- [circle.ps](circle.ps)
- [compass.ps](compass.ps)
- [meanader.ps](meanader.ps)
- [meanader2.ps](meanader2.ps)
- [chrysler.ps](chrysler.ps)
- [snowflake.ps](snowflake.ps)
- [star5.ps](star5.ps)
- [test.ps](test.ps)
- [yinyang.ps](yinyang.ps)