/* ECE660 PostScript Driver in C for lines. fsh...9/21/93 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DBL double
#define TRUE 1
#define FALSE 0
/*<<<<<<<<<<<<<<<< globals >>>>>>>>>>>>>>>*/
FILE *Postfil;
int PostOn = FALSE;
/****************** prototypes ***********************/
void Init_Post(int vportWid, int vportHt);
void Exit_Post(void);
void Write_Post(int x, int y, int which);
/* <<<<<<<<<<<<<<<<<< Init_Post >>>>>>>>>>>>>>>>>*/
void Init_Post(int vportWid,int vportHt)
{ /* 1. Prompts user for a PostScript file name. If none given, exit.
   2. Prompts user for a title to be printed at bottom of PS page.
   3. Puts proper stuff in PS file to print title, if any.
   4. Defines m for moveto, l for lineto, s for stroke for brevity.
   5. Sets PS coord system with origin near upper left of landscape page,
	  x-axis to right, y-axis downward, leaving margin around page.
   6. Uses vportWid,vportHt (the width and height of viewport in pixels),
	  to find viewport's aspect ratio. Then compares it with aspect
	  ratio of PS drawing area, and sets PS scaleFactor so viewport maps
	  to largest region possible having same aspect ratio as viewport,
	  tucked in upper left corner of PS page.  */

	int H, W;
	DBL vportAspect, scaleFactor;
	char fmt[]= "/Helvetica findfont 12 scalefont setfont 144 36 m ( %s ) show\n";
	char fname[30], picname[80];
	PostOn = FALSE;

	if (vportWid == 0 || vportHt == 0){
		puts("degenerate viewport !! I'm outa here.."); return;}
	vportAspect = (DBL)vportHt / (DBL)vportWid;

	puts("\nName of Postscript file to build <CR> for none: ");
	gets(fname);
	if (strlen(fname) == 0){ /* nothing to do */
		puts("\n ok, no PostScript file ..");
		return;
	}
	if(!(Postfil = fopen(fname,"w"))){ 	/* open file for writing */
		puts("\n can't open file!!! "); return;}
	PostOn = TRUE;
	#define pageheight 792 /*11 inch x 72 */
	#define pagewidth 612  /* 8.5 inch x 72 */
	fprintf(Postfil," %% fsh, file = %s \n",fname); /* put comment in PS file */
	fprintf(Postfil,"/m {moveto}def /l {lineto}def /s {stroke} def\n");
	fprintf(Postfil,"%d 0 translate 90 rotate\n",pagewidth);
	/* landscape, origin at bottom left for title */

	puts("\n Please give title of picture, <CR> for none:");
	gets(picname);
	if(strlen(picname)!=0) fprintf(Postfil,fmt,picname); /* print it */

	/* shift and invert coord syst so origin is at top, and y-axis downward */
	#define margin 54
	/* 3/4 inch border * 72 is 54 points, to be left around page */
	fprintf(Postfil," %d %d translate 1 1 neg scale\n",margin,pagewidth-margin);

	/* set proper scale for largest drawing area */
	H = pagewidth - 2 * margin;  /* drawable height of page in points */
	W = pageheight - 2 * margin; /* drawable width of page in points */

	scaleFactor = (vportAspect > (DBL)H / W ? 
			H / (DBL)vportHt : W / (DBL)vportWid);
	fprintf(Postfil," %8.4lf %8.4lf scale\n", scaleFactor, scaleFactor);
} /* end of Init_Post */

/********************* Exit_Post() **************/
 void Exit_Post(void)  /* close up postscript file */
{
	if (!PostOn) return; /* nothing to do */
	fprintf(Postfil,"\nstroke showpage\n");
	fclose(Postfil);
	PostOn = FALSE;
} /* end of Exit_Post() */
/****************** Write_Post *****************/
void Write_Post(int x, int y, int which)
/* if which = 1 then write m for moveto(); else write l for lineto(), and handle count before
stroke */
{
	#define NumBeforeStroke 100
	static int countForStroke = 0; /* init'd first time only */
	static int countOnLine = 0;
	/* countForStroke: # of lineto's so far since last stroke*/
	/* countOnLine: # of entries so far on current line of PS file*/
	countForStroke++;
	countOnLine++;
	if(countOnLine > 8){
		countOnLine = 0;
		fprintf(Postfil,"\n"); /* write <CR> */
	};
	if(which == 1) { /* about to do moveto() */
		if(countForStroke > NumBeforeStroke) /* put stroke before next moveto */
		{
			countForStroke = 0;
			fprintf(Postfil," s %d %d m ", x, y);
		}
		else /* no stroke needed */
			fprintf(Postfil,"%d %d m ", x, y);
	}
	else /* which <> 1, so do a lineto */
		fprintf(Postfil,"%d %d l ", x, y);
} /* end of Write_Post() */

/*################ end of driver  ##############*/
/*<<<<<<<<<<<< prototypes >>>>>>>>>>>>>>*/
void myMoveTo(int x, int y); /* my local tool ..not part of driver */
void myLineTo(int x, int y); /* ditto */

/*<<<<<<<<<<< myMoveTo >>>>>>>>>>>*/
void myMoveTo(int x, int y)
{ /* imbed Write_Post() with sysMoveTo */
	moveto(x,y);
	if(PostOn)Write_Post(x,y,1); /* put in PS file too */
}
/*<<<<<<<<<<<<<<<<<<< myLineTo >>>>>>>>>>>*/
void myLineTo(int x, int y)
{  /* imbed Write_Post() with sysLineTo */
	lineto(x,y);
	if(PostOn)Write_Post(x,y,2);
}
/*<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>>*/
main(){
 /* example of 'application' to test PS driver */
	int vWid = 600, vHt = 400;	/* choose viewport size */
/*	int Gd= DETECT,Gm=0;   	/* DEV DEP! Turbo C */	*/
/*	initgraph(&Gm,&Gd,""); 	/* DEV DEP! Turbo C */	*/
	Init_Post(vWid,vHt);		
	myMoveTo(100,100);   	/* draw 200 by 200 pixel square */
	myLineTo(300,100);
	myLineTo(300,300);
	myLineTo(100,300);
	myLineTo(100,100);
	myMoveTo(0,0);   		/* draw box around viewport */
	myLineTo(vWid,0);
	myLineTo(vWid,vHt);
	myLineTo(0,vHt);
	myLineTo(0,0);
	getch();			/* pause to admire: DEV DEP! Turbo C */
	Exit_Post();
}
