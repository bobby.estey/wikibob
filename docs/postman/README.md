# Postman

- What is Postman?

     - Allows developers to easily create and share API requests and collections
     - Automate testing, mock APIs, and monitor performance

|Issue|Solution|
|--|--|
|Postman moving from Web to App|- modify .bashrc<br>- alias postman='/opt/postman/Postman &'  #/snap/bin/postman|
