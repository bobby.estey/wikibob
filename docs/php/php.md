# php development and applications page

Start XAMPP - Start Apache

- http://localhost/php/phpVersion.php
- http://localhost/php/basic/intro2php.html
- http://localhost/php/database/database.html

## phpmyadmin installation

- [download](https://www.phpmyadmin.net/downloads/)
- copy to the apache htdoc directory, e.g.  /htdocs

    - linux location:  /opt
    - windoze location:  c:/opt/XAMPP/htdocs
    
- rename myPhpAdmin folder to myphpadmin
- rename myPhpAdmin folder to phpmyadmin

## startup instructions

See image below and follow the instructions: 
- start XAMPP
- start Apache
- start MySQL

Press MySQL -> Admin button -> opens up in the Browser at the URL:  localhost/phpmyadmin

![phpMyAdmin User Interface](images/phpMyAdmin.png)

## JavaScript Object Notation (JSON) Examples

 - [test connection](http://cv64.us/cv64/database/public/testConnectionStudent.php)
 - [select all](http://cv64.us/cv64/database/public/selectAll.php)
 