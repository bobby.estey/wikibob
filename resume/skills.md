# Phone 775.513.7587 - Interview Schedule:  

# Apply List

|Years|Skill|
|--|--|
|20|RDBMS, Relational Database, SQL|
|10|Design Patterns, Hibernate, Java, JDBC, JPA, Kanban, Microservices, REST, SAAS, Spring, Ubuntu|
|5|Angular, API Integration, Cloud, JIRA, NodeJS, Responsive, SPA, Testing (JUnit, Mockito), TypeScript|
|3|React, SOAP|
|2|MongoDB, NoSQL, PHP, Python|

# Don't Apply List

- .NET, C, C++, C#, ABAP, AEM, Casandra, Django, Elastic Search, GCP, GIS, Go, IOS, Kafka, Kotlin
- Laravel, Map Reducer, Mulesoft, NESSUS, PCI, Presto, RabbitM2, REDIS, Ruby, Rust, 
- Scala, S3, SAFE, Slas, Spark, Splunk, Swagger, Tableau, TSQL, VBA
