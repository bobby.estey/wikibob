# Courses Facilitated

### Current Online Courses

##### Grand Canyon University (December 2020 - Present)

|Course|Description|
|--|--|
|CST 120|Web Development|
|CST 339|Programming in Java III|
|CST 391|JavaScript Web Application Development (Node, Express, Angular, React)|
|SDD 630|Mobile Software Development|
|SDD 680|Software Maintenance and Testing|

### Past Online Courses

##### DeVry University, Online (January 2006 - December 2023)

|Course|Description|
|--|--|
|COMP 100|Computer Applications for Business with Lab|
|COMP 129|PC Hardware and Software with Lab|
|COMP 230|Introduction to Scripting and Database with Lab|
|COMP 274|Application Programming (Java)|
|CIS 206|Architecture and Operating Systems|
|CIS 355A|Business Application Programming with Lab|
|CIS 407A|Web Application Development|
|CEIS 295|Data Structures and Algorithms|
|CEIS 308|Computer Aided Design (Autodesk Inventor)|
|CEIS 320|Introduction to Mobile Device Programming (Android)|
|CEIS 400|Software Engineering II|
|CEIS 420|Programming Languages and Advanced Techniques|

##### University of Phoenix, Online (March 2002 - December 2016)

|Course|Description|
|--|--|
|WEB 410|Web Programming I|
|DBM 380 / 500|Database Concepts|
|DBM 405 / 502|Database Management|
|POS 407|Computer Programming (Java)|
|POS 440|Introduction to C++|
|IT 205N|Management of Information Systems|
|IT 215 / 307|Java Programming|
|IT 220 / 312 / 320|Internet Concepts|
|IT 300|Management Information Systems|

##### Thomas Nelson Community College (January 2002 - May 2003)

|Course|Description|
|--|--|
|IST 133|Database Management Software|
|IST 139|Microcomputer Integrated Software|

##### Rappahannock Community College (September 2001 - December 2001)

|Course|Description|
|--|--|
|IST 149|Java Programming|
