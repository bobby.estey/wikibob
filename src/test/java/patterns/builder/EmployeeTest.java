package patterns.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeTest {

//	@Test
	public void builderPatternTest() {

		String dateString = "2013/05/28";
		Date date = null;

		try {
			date = new SimpleDateFormat("yyyy/MM/dd").parse(dateString);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(dateString + "\t" + date);

		Employee bobby = new Employee.EmployeeBuilder(false, "Bobby").dateHired(date).id(123).build();
		Employee ester = new Employee.EmployeeBuilder(true, "Ester").dateHired(date).id(124).build();
		Employee rebby = new Employee.EmployeeBuilder(false, "Rebby", date, 125, true).build();

		System.out.println(bobby);
		System.out.println(ester);
		System.out.println(rebby);
	}
}
