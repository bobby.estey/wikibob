/**
 * GuestBook.java
 * This applet allows people to enter in comments
 * and save them to my Microsoft Access 7.0
 * database.
 */
package stql;

import java.awt.*;
import java.applet.*;

public class GuestBook extends Applet {
  private TextField email;
  private Label label1;
  private Label label2;
  private TextArea comments;
  private TextField name;
  private Label label3;
  private Button save_button;
  private Label status;
  private boolean error_disable;

  /**
   * Sets up the applet's look.
   */
  public void init() {
    System.out.println( "init" );
    super.init();
    setLayout(null);
    addNotify();
    resize(351,230);
    setBackground(new Color(16777215));
    email = new java.awt.TextField();
    email.reshape(67,4,157,22);
    add(email);
    label1 = new java.awt.Label("Email ");
    label1.reshape(11,7,45,15);
    add(label1);
    label2 = new java.awt.Label("Name");
    label2.reshape(11,40,48,15);
    add(label2);
    comments = new java.awt.TextArea();
    comments.reshape(11,87,325,109);
    add(comments);
    name = new java.awt.TextField();
    name.reshape(67,37,157,23);
    add(name);
    label3 = new java.awt.Label("Comments");
    label3.reshape(11,68,70,15);
    add(label3);
    save_button = new java.awt.Button("Save");
    save_button.reshape(252,4,87,26);
    add(save_button);
    status = new java.awt.Label("");
    status.reshape(12,202,322,19);
    add(status);
    checkButton();
    error_disable = false;
    try {
      System.out.println( "odbc" );
      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    }
    catch( Exception e ) {
      status.setText("A Java error occurred.");
      error_disable = true;
    }
  }

  /**
   * Checks to make sure enough information exists to
   * allow saving to the database.  This is something
   * you cannot do in CGI-land.
   */
  private boolean validate_value(String str, boolean flag) {
    if( str.length() < 1 ) return false;
    if( flag ) {
      int i = str.indexOf("@");

      if( i == -1 ) return false;
      else return ((i != 0) && (i != str.length()-1));
    }
    return true;
  }

  /**
   * Performs the actual database save.
   */
  private void save() {
    status.setText("Saving to the database, this will take a minute...");
    try {
      //String url = "jdbc:odbc:db_web";
      String url = "jdbc:odbc:db1";
      java.sql.Connection connection;
      java.sql.Statement statement;
      java.sql.ResultSet result;
      int id = -1;
	System.out.println( "save - a" );
      connection = java.sql.DriverManager.getConnection(url, "Admin", "test");
	System.out.println( "save - b" );
      statement = connection.createStatement();
	System.out.println( "save - c" );
      result = statement.executeQuery("SELECT next_id FROM sys_gen, comments " +
                                      "WHERE id = comment_id");
      if( !result.next() ) {
        throw new java.sql.SQLException("Failed to generate id.");
      }
      id = result.getInt(1) + 1;
      result.close();
      statement.close();
      statement = connection.createStatement();
      statement.executeUpdate("UPDATE sys_gen, comments SET next_id = " + id +
                              " WHERE id = comment_id");
      statement.close();
        status.setText("...generated the update command");
      statement = connection.createStatement();
      statement.executeUpdate("INSERT into comments " +
                              "VALUES (" + id +", '" + getEmail() + "', '" +
                              getName() + "', '" + (new java.util.Date()).toString() + "', '" + 
                              getComments() + "')");
      statement.close();
        status.setText("...generated the insert command");
      connection.close();
      email.setText("");
      name.setText("");
      comments.setText("");
      checkButton();
      status.setText("Saved comment id " + id + ".");
    }
    catch( java.sql.SQLException e ) {
      status.setText("A database error occurred: " + e.getMessage());
      error_disable = true;
      checkButton();
    }
  }

  /**
   * Looks for key presses and other fun events.
   */
  public boolean handleEvent(Event event) {
    if( event.target == comments ||
        event.target == name || event.target == email ) {
      if( event.id == Event.KEY_PRESS ) {
        if( event.key == '\t' ) {
          if( event.target == email ) {
            name.requestFocus();
          }
          else if( event.target == name ) {
            comments.requestFocus();
          }
          else {
            email.requestFocus();
          }
          return true;
        }
        else {
          checkButton();
          return super.handleEvent(event);
        }
      }
      if( event.id == Event.ACTION_EVENT ) {
        if( !save_button.isEnabled() ) {
          return super.handleEvent(event);
        }
        save();
        return true;
      }
    }
    if (event.target == save_button && event.id == Event.ACTION_EVENT) {
      save();
    }
    return super.handleEvent(event);
  }

  /**
   * Enable or disable the save button as appropriate.
   */
  void checkButton() {
    if( error_disable ) {
      save_button.enable(false);
      return;
    }
    save_button.enable(validate_value(comments.getText(), false) &&
                       validate_value(email.getText(), true) &&
                       validate_value(name.getText(), false));
  }

  public String getEmail() {
    String str = email.getText();

    return str.replace('\'', '"');
  }

  public String getName() {
    String str = name.getText();

    return str.replace('\'', '"');
  }

  public String getComments() {
    String str = comments.getText();

    return str.replace('\'', '"');
  }
}
