// $Id: Database.java,v 1.2 1997/10/20 20:30:49 rp4370 Exp $

package stql;

//**************************************************************************
// Purpose:
//   Represent a database - specific for visgenic
//
// Author: ekr
// Date: 8-14-97
//
//**************************************************************************

import java.sql.*;

public class Database
{
  // constructor
  public Database( String server, String dsn, String user, String passwd )
    throws ClassNotFoundException {
    this.server = server;
    this.dsn = dsn;
    this.user = user;
    this.passwd = passwd;
    // Try loading VisiChannel for JDBC driver
    try { 
      Class.forName( driver );
    }
    catch ( ClassNotFoundException e ) {
      throw e;
    }
}
  // accessors
  public String server() { return server; }
  public String dsn() { return dsn; }
  public String user() { return user; }
  public static String driver() { return driver; }
  public String connectionURL() { return URLPrefix + server + "/" + dsn; }


  public Connection getConnection() throws SQLException {
    
    // try to make a connection
    Connection connection = null;
    try{
      connection = DriverManager.getConnection( connectionURL(), user, passwd );
    }
    catch ( SQLException e ) {
      throw e;
    }
    return connection;
  }
  
  // vars
  private String server;
  private String dsn;
  private static String driver = "visigenic.jdbc.sql.VisiChannelDriver";
  private String user;
  private String passwd;
  private static String URLPrefix = "jdbc:visichannel://";
};

