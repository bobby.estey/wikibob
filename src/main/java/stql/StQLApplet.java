package stql;

import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;
import java.net.*;
import java.sql.*;

import swing.gridBagLayout.Constraint;

public class StQLApplet extends Applet implements ActionListener, KeyListener
{
  GridBagLayout gridBagLayout = new GridBagLayout();
  Constraint c          = new Constraint();
  StQL stql;
  
  String dsn            = null;
  String server         = null;
 
  Panel panel1          = new Panel();
  Panel panel2          = new Panel();
  Panel panel3          = new Panel();
  Panel panel4          = new Panel();
  
  Font font             = new Font( "Courier", Font.BOLD, 10 );
  Image images[]        = new Image[5];

  TextArea ta           = new TextArea( "", 20, 80 );

  Label sqlLabel        = new Label( "SQL> " );
  Label usernameLabel   = new Label( "Username:" );
  Label passwordLabel   = new Label( "Password:" );
  Label dsnLabel        = new Label( "DSN:" );
  Label serverLabel     = new Label( "ORB Server:" );
  
  TextField sqlTF       = new TextField( 80 );
  TextField usernameTF  = new TextField( 20 );
  TextField passwordTF  = new TextField( 20 );
  TextField dsnTF       = new TextField( 20 );
  TextField serverTF    = new TextField( 20 );
  
  Button loginButton    = new Button( "Login" );
  Button logoutButton   = new Button( "Logout" );
  Button executeButton  = new Button( "Execute" );
  Button commitButton   = new Button( "Commit" );
             
  public void init() {
    try {
      URL url   = new URL( getCodeBase().toString() + "stql/" );
      images[0] = getImage( url, "duke.gif" );
      dsn       = this.getParameter( "dsn" );
      server    = this.getParameter( "server" );
    
      dsnTF.setText( dsn );
      serverTF.setText( server );
      loginButton.setVisible( true );
      logoutButton.setVisible( false );
      executeButton.setVisible( false );
      commitButton.setVisible( false );
      
      ta.setFont( font );
      ta.setEditable( false );
      passwordTF.setEchoChar( '*' );
      appletLayout();
      addListeners();
    }
    catch( MalformedURLException e ) {
      ta.append( "MalformedURLException\n\n" + e.getMessage() );
    }
  }
  
  private void addListeners() {
    loginButton.addActionListener( this );
    logoutButton.addActionListener( this );
    executeButton.addActionListener( this );
    commitButton.addActionListener( this );
    sqlTF.addKeyListener( this );
  }
  
  public void actionPerformed( ActionEvent e ) {
    if( e.getSource() instanceof Button ) {
      String buttonLabel = e.getActionCommand();
      
      if( buttonLabel.equals( "Login" )) {
        loginButton.setVisible( false );
        logoutButton.setVisible( true );
        executeButton.setVisible( true );
        commitButton.setVisible( true );
        panel4.validate();
        startSQL();
      }
      
      else if( buttonLabel.equals( "Execute" )) {
        stql.executeStQL( sqlTF.getText() );
      }

      else if( buttonLabel.equals( "Commit" )) {
        stql.executeStQL( "commit" );
      }     
            
      else if( buttonLabel.equals( "Logout" )) {
        loginButton.setVisible( true );
        logoutButton.setVisible( false );
        executeButton.setVisible( false );
        commitButton.setVisible( false );
        panel4.validate();
        stql.stopStQL();      }
    }
  }
  
  public void keyPressed( KeyEvent e ) {}
  public void keyReleased( KeyEvent e ) {}
  public void keyTyped( KeyEvent e ) {
    if( e.getKeyChar() == '\n' ) stql.executeStQL( sqlTF.getText() );
  }
  
  public void startSQL() {
    stql = new StQL( ta );
    stql.startStQL( dsnTF.getText(), serverTF.getText(), 
                    usernameTF.getText(), passwordTF.getText() );
  }
  
  public void stop() {
    stql.stopStQL();
  }
    
  public void appletLayout() {
    this.setLayout( gridBagLayout );
    
    panel1.setBackground( Color.cyan );
    panel2.setBackground( Color.lightGray );
    panel3.setBackground( Color.lightGray );
    panel4.setBackground( Color.lightGray );
    
    panel1.setLayout( gridBagLayout );
    panel2.setLayout( gridBagLayout );
    panel3.setLayout( gridBagLayout );
    panel4.setLayout( gridBagLayout );
                    
    c.constrain( this, panel1, 0, 0, 1, 1, 
                GridBagConstraints.BOTH, GridBagConstraints.CENTER,
                1.0, 1.0, 5, 5, 5, 5 );

    c.constrain( this, panel2, 0, 1, 1, 1,
                GridBagConstraints.BOTH, GridBagConstraints.CENTER,
                1.0, 1.0, 5, 5, 5, 5 );

    c.constrain( this, panel3, 0, 3, 1, 1,
                GridBagConstraints.BOTH, GridBagConstraints.CENTER,
                1.0, 1.0, 5, 5, 5, 5 );

    c.constrain( this, panel4, 0, 2, 1, 1,
                GridBagConstraints.BOTH, GridBagConstraints.CENTER,
                1.0, 1.0, 5, 5, 5, 5 );
                                                
    c.constrain( panel1, ta, 0, 0, 1, 1,
                GridBagConstraints.BOTH, GridBagConstraints.CENTER,
                1.0, 1.0, 5, 5, 5, 5 );
                       
    c.constrain( panel2, sqlLabel,       0, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel2, sqlTF,          1, 0, 1, 1, 1.0, 1.0 );
                           
    c.constrain( panel3, usernameLabel,  0, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, usernameTF,     1, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, passwordLabel,  2, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, passwordTF,     3, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, dsnLabel,       0, 1, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, dsnTF,          1, 1, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, serverLabel,    2, 1, 1, 1, 1.0, 1.0 );
    c.constrain( panel3, serverTF,       3, 1, 1, 1, 1.0, 1.0 );
    
    c.constrain( panel4, executeButton,  0, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel4, commitButton,   1, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel4, loginButton,    2, 0, 1, 1, 1.0, 1.0 );
    c.constrain( panel4, logoutButton,   3, 0, 1, 1, 1.0, 1.0 );
  }
}
