package stql;

import java.applet.Applet;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import com.estey.awt.Constraint;
//import com.estey.exception.ExceptionHandler;

public class Estey extends Applet {

//  Constraint constraint;
  static private Connection connection;

  public void init() {

    super.init();
    
    try {
      Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" ); // load the JDBC-ODBC bridge driver
      open();   // open the database
      select(); // SQL Selection
      close();  // close the database
    }
    catch( Exception ex ) { new ExceptionHandler( ex ); }
  }

  static void open() throws SQLException {

    String dsn = "jdbc:odbc:estey"; // estey database
    String username = "Admin"; 
    String password = "";

    try {
      connection = DriverManager.getConnection(dsn, username, password); // Connect to the database 
      connection.setAutoCommit(false);
    }
    catch( SQLException ex ) { new ExceptionHandler( ex ); }
  }

  static void close() throws SQLException {

    try {
      connection.commit(); 
      connection.close();
    }
    catch( SQLException ex ) { throw( ex ); }
  }

  static void select() throws SQLException {

    Statement statement; // SQL statement object 
    String query;   // SQL select string 
    ResultSet resultSet;   // SQL query results 
    boolean more;   // more rows found switch

    query = "select * from estey where geneology='gab'"; // select from the estey table

    statement = connection.createStatement();
    resultSet = statement.executeQuery( query );

    more = resultSet.next(); // check to see if any rows were read 
    if( !more ) {
      System.out.println("No rows found."); 
      return;
    }

    while( more ) { // loop through the rows retrieved from the query

      System.out.println( "Misc1: " + resultSet.getString( "MISC1" ) );
	System.out.println( "Misc2: " + resultSet.getString( "MISC2" ) );
	System.out.println( "Misc3: " + resultSet.getString( "MISC3" ) );
	System.out.println( "Misc4: " + resultSet.getString( "MISC4" ) );
	System.out.println( "Misc5: " + resultSet.getString( "MISC5" ) );
      more = resultSet.next();
    }

    resultSet.close(); 
    statement.close();
  }
}