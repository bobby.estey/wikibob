package stql;

import java.sql.DriverManager;
import java.awt.TextArea;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class <code>StQL</code> performs SQL commands through Java Database
 * Connection (JDBC).
 * 
 * @version 1.0
 * @author Robert Estey
 */

public class StQL {

	private Connection connection = null;
	private String driver = "winnt.system32.Odbcjt32.dll";
	private String dsn = null;
	private String server = null;
	private String password = null;
	private String username = null;

	private Statement statement = null;
	private ResultSet rs = null;
	private String user;
	private String passwd;

	public StQL(TextArea ta) {
		// TODO Auto-generated constructor stub
	}

	public void start() {
		try {
			connection = DriverManager.getConnection(connectionURL(), user, passwd);
			connection.setAutoCommit(false);
			statement = connection.createStatement();

			System.out.println(statement.getUpdateCount());
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	private String connectionURL() {
		// TODO Auto-generated method stub
		return null;
	}

	public void stop() {
		try {
			if (rs != null) {
				rs.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			sqlError(e);
		}
	}

	public void sqlError(SQLException e) {

		System.out.println("SQLException: \n");
		System.out.println("Message: " + e.getMessage() + "\n");
		System.out.println("SQL State: " + e.getSQLState() + "\n");
		System.out.println("Error Code: " + e.getErrorCode() + "\n\n");
	}

	public void executeStQL(String text) {
		// TODO Auto-generated method stub
		
	}

	public void stopStQL() {
		// TODO Auto-generated method stub
		
	}

	public void startStQL(String text, String text2, String text3, String text4) {
		// TODO Auto-generated method stub
		
	}
}