/**
 * GuestBookView.java
 * A look at all the comments listed in the guest book
 * through the GuestBook applet.
 */
package stql;

import java.awt.*;
import java.applet.*;

public class GuestBookView extends Applet {
  private GridBagLayout applet_layout;
  private GridBagConstraints applet_constraints;
  private Button retrieve;
  private Button next;
  private Button previous;
  private Panel main_panel;
  private Panel comment_panel;
  private Panel status_panel;
  private TextArea comment;
  private TextField email;
  private TextField name;
  private Label status;
  int current_comment = 1;
  int total_comments = 0;

  /**
   * Initialize the appearance of the applet.
   * For the sake of speed, do not download comments until they
   * explicitly perform a retrieval.
   */
  public void init() {
    super.init();
    setLayout(applet_layout = new GridBagLayout());
    applet_constraints = createDefaultConstraints();
    addNotify();
    setBackground(Color.white);
    main_panel = new Panel();
    { // Set up the main panel
      GridBagConstraints constraints;
      GridBagLayout layout;
      Label tmp;

      main_panel.setLayout(layout = new GridBagLayout());
      constraints = createDefaultConstraints();
      constraints.insets = new Insets(2, 10, 2, 10);
      layout.setConstraints(tmp = new Label("Email"), constraints);
      main_panel.add(tmp);
      constraints.gridx = 1;
      layout.setConstraints(email = new TextField(30), constraints);
      main_panel.add(email);
      constraints.gridx = 2;
      layout.setConstraints(retrieve = new Button("Retrieve"), constraints);
      main_panel.add(retrieve);
      constraints.gridx = 0;
      constraints.gridy = 1;
      layout.setConstraints(tmp = new Label("Name"), constraints);
      main_panel.add(tmp);
      constraints.gridx = 1;
      layout.setConstraints(name = new TextField(30), constraints);
      main_panel.add(name);
    }
    applet_constraints.gridy = 1;
    applet_constraints.insets = new Insets(4, 2, 4, 2);
    applet_layout.setConstraints(main_panel, applet_constraints);
    add(main_panel);
    { // Set up the comments panel
      comment_panel = new Panel();
      comment_panel.setLayout(new CardLayout());
      comment_panel.add("1", getPanel(0, "", "", "", ""));
    }
    applet_constraints.gridy = 2;
    applet_layout.setConstraints(comment_panel, applet_constraints);
    add(comment_panel);
    { // Set up the status panel
      GridBagLayout layout;
      GridBagConstraints constraints;

      status_panel = new Panel();
      status_panel.setLayout(layout = new GridBagLayout());
      constraints = createDefaultConstraints();
      // Extra spaces in label for proper spacing in layout
      status = new Label("Ready.                                  " +
                         "                                              ");
      previous = new Button("<<");
      next = new Button(">>");
      previous.enable(false);
      next.enable(false);
      constraints.anchor = GridBagConstraints.SOUTHWEST;
      constraints.insets = new Insets(4, 2, 4, 2);
      layout.setConstraints(previous, constraints);
      status_panel.add(previous);
      constraints.gridx = 1;
      layout.setConstraints(next, constraints);
      status_panel.add(next);
      constraints.gridx = 2;
      layout.setConstraints(status, constraints);
      status_panel.add(status);
    }
    applet_constraints.gridy = 3;
    applet_constraints.anchor = GridBagConstraints.SOUTHWEST;
    applet_layout.setConstraints(status_panel, applet_constraints);
    add(status_panel);
    applet_constraints.anchor = GridBagConstraints.SOUTH;
    try {
      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    }
    catch( Exception e ) {
      status.setText("An error occurred finding database drivers.");
    }
  }  

  /**
   * Handle the clicking on any of the buttons.
   * For retrieve, go to the database.
   * For previous, go to the previous comment.
   * For next, go to the next comment.
   * For previous or next, make sure to enable and disable as appropriate.
   */
  public boolean action(Event event, Object arg) {
    if( event.target instanceof Button ) {
      if( (Button)event.target == retrieve ) {
        retrieve();
        return true;
      }
      if( (Button)event.target == next ) {
        if( current_comment == total_comments ) {
          return super.action(event, arg);
        }
        ((CardLayout)comment_panel.getLayout()).next(comment_panel);
        current_comment++;
        if( current_comment == total_comments ) {
          next.enable(false);
        }
        if( current_comment > 1 ) {
          previous.enable(true);
        }
        resetStatus();
        return true;
      }
      if( (Button)event.target == previous ) {
        if( current_comment < 2 ) {
          return super.action(event, arg);
        }
        ((CardLayout)comment_panel.getLayout()).previous(comment_panel);
        current_comment--;
        if( current_comment < 2 ) {
          previous.enable(false);
        }
        if( current_comment < total_comments ) {
          next.enable(true);
        }
        resetStatus();
        return true;
      }     
    }
    return super.action(event, arg);
  }

  /**
   * Retrieve data from the database based on any selection
   * criteria.  A user can specify either email address or
   * name.
   */
  private synchronized void retrieve() {
    remove(comment_panel);
    comment_panel = new Panel();
    comment_panel.setLayout(new CardLayout());
    current_comment = 1;
    total_comments = 0;
    try {
      String url = "jdbc:odbc:db_web";
      java.sql.Connection connection;
      java.sql.Statement statement;
      java.sql.ResultSet result;
      String sql;
      int i = 0;

      status.setText("Retrieving from the database, " +
                     "this may take a minute...");
      connection = java.sql.DriverManager.getConnection(url, "admin", "");
      statement = connection.createStatement();
      sql = "SELECT comment_id, email, name, comment, date " +
            "FROM comments " + getWhere();
      result = statement.executeQuery(sql);
      // For each row, add to card layout a panel representing the row
      while(result.next()) {
        Panel tmp;
        
        i++;
        tmp = getPanel(result.getInt(1),
                       result.getString(2),
                       result.getString(3),
                       result.getString(4),
                       result.getString(5));
        comment_panel.add("" + i, tmp);
      }
      // redo the applet layout
      remove(status_panel);
      applet_constraints.gridy = 2;
      applet_layout.setConstraints(comment_panel, applet_constraints);
      add(comment_panel);
      applet_constraints.gridy = 3;
      applet_constraints.anchor = GridBagConstraints.SOUTHWEST;
      applet_layout.setConstraints(status_panel, applet_constraints);
      add(status_panel);
      applet_constraints.anchor = GridBagConstraints.SOUTH;
      total_comments = i;
      resetStatus();
    }
    catch( java.sql.SQLException e ) {
      remove(status);
      comment_panel = getPanel(0, "", "", "", "");
      applet_constraints.gridy = 2;
      applet_layout.setConstraints(comment_panel, applet_constraints);
      add(comment_panel);
      applet_constraints.gridy = 3;
      applet_constraints.anchor = GridBagConstraints.SOUTHWEST;
      applet_layout.setConstraints(status_panel, applet_constraints);
      add(status_panel);
      applet_constraints.anchor = GridBagConstraints.SOUTH;
      status.setText("A database error occurred: " + e.getMessage());
      total_comments = 0;
      current_comment = 1;
      next.enable(false);
      previous.enable(false);
    }
    validate();
  }

  /**
   * Provides a WHERE clause with an ORDER BY as an extra bonus
   * Needs to make sure user-entered fields do not have database
   * sensitive characters.
   */
  private String getWhere() {
    String e = email.getText().replace('\'', '"');
    String n = name.getText().replace('\'', '"');
    String where = "WHERE ";
    
    if( e.length() < 1 && n.length() < 1 ) return "ORDER BY comment_id";
    if( e.length() > 0 ) {
      where += "email = '" + e + "'";
      if( n.length() > 0 ) {
        where += " AND ";
      }
    }
    if( n.length() > 0 ) {
      where += "name = '" + n + "'";
    }
    return where + " ORDER BY comment_id";
  }
  
  /**
   * Creates a panel for database data.
   */
  private Panel getPanel(int id, String mail, String nom, String text,
                         String day) {
    Panel panel =  new Panel();
    GridBagConstraints constraints;
    GridBagLayout layout;
    TextArea cmt = new TextArea(5, 50);
    TextField eml = new TextField(30);
    TextField nme = new TextField(30);
    
    panel.setLayout(layout = new GridBagLayout());
    constraints = createDefaultConstraints();
    constraints.gridx = 2;
    constraints.anchor = GridBagConstraints.NORTHWEST;
    constraints.insets = new Insets(2, 2, 2, 2);
    cmt.setText(text);
    eml.setText(mail);
    nme.setText(nom);
    cmt.setEditable(false);
    eml.setEditable(false);
    nme.setEditable(false);
    layout.setConstraints(eml, constraints);
    panel.add(eml);
    constraints.gridy = 1;
    layout.setConstraints(nme, constraints);
    panel.add(nme);
    constraints.gridy = 3;
    constraints.anchor = GridBagConstraints.CENTER;
    layout.setConstraints(cmt, constraints);
    panel.add(cmt);
    return panel;
  }

  /**
   * A way to avoid redoing the creation of constraints
   * being used everywhere in this applet.  This creates
   * a GridBagConstraints object and sets some defaults.
   */
  private GridBagConstraints createDefaultConstraints() {
    GridBagConstraints constraints = new GridBagConstraints();
    
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.gridheight = 1;
    constraints.gridwidth = 1;
    constraints.weightx = 0.0;
    constraints.weighty = 0.0;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.SOUTH;
    return constraints;
  }

  private void resetStatus() {
    // Extra spaces in setText() below for layout reasons
    if( total_comments == 0 ) {
      status.setText("No comments found.                     " +
                     "                                      ");
      next.enable(false);
    }
    else if( total_comments == 1 ) {
      status.setText("1 of 1 comment.                        " +
                     "                                       ");
      next.enable(false);
    }
    else {
      status.setText(current_comment + " of " + total_comments +
                     " comments.                             " +
                     "                      ");
      if( current_comment < total_comments ) {
        next.enable(true);
      }
      else {
        next.enable(false);
      }
    }
    if( current_comment < 2 ) {
      previous.enable(false);
    }
    else {
      previous.enable(true);
    }
  }
}
