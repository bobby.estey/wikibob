package swing;

import java.awt.BorderLayout;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class ButtonSample extends JFrame {

	private static final long serialVersionUID = -1225153522858815807L;

	public ButtonSample() {

		super("Button Sample");

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create a Button with the value Select Me
		JButton button = new JButton("Select Me");

		// This is an inner Class - ButtonSample.class -
		// ButtonSample$MouseListener.class
		MouseListener mouseListener = new MouseAdapter() {
			@SuppressWarnings("deprecation")

			// when a mouse is pressed this method gets called
			public void mousePressed(MouseEvent mouseEvent) {

				int modifiers = mouseEvent.getModifiers();

				if ((modifiers & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) {
					System.out.println("Left button pressed.");
				}
				if ((modifiers & InputEvent.BUTTON2_MASK) == InputEvent.BUTTON2_MASK) {
					System.out.println("Middle button pressed.");
				}
				if ((modifiers & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK) {
					System.out.println("Right button pressed.");
				}
			}

			// when a mouse is released this method gets called
			public void mouseReleased(MouseEvent mouseEvent) {

				if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
					System.out.println("Left button released.");
				}
				if (SwingUtilities.isMiddleMouseButton(mouseEvent)) {
					System.out.println("Middle button released.");
				}
				if (SwingUtilities.isRightMouseButton(mouseEvent)) {
					System.out.println("Right button released.");
				}
				System.out.println();
			}
		};

		// get the button object and add a Mouse Listener (Listener is a method that is
		// called when an event occurs) if the button is pressed and the event is a
		// mouse event,
		// then call the methods that listen for mouse event.
		button.addMouseListener(mouseListener);

		// add Button Class with button object and place in the Frame
		// add the Button to the Frame using the BorderLayout on the SOUTH of the Layout
		add(button, BorderLayout.SOUTH);

		setSize(300, 100);
		setVisible(true);
	}

	public static void main(String args[]) {
		new ButtonSample();
	}
}