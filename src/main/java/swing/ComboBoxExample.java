// https://www.tutorialspoint.com/swing/swing_jcombobox.htm
package swing;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ComboBoxExample {
	private JFrame frame;
	private JLabel headerLabel;
	private JLabel statusLabel;
	private JPanel panel;

	public ComboBoxExample() {
		prepareGUI();
	}

	private void prepareGUI() {
		frame = new JFrame("USN Aircraft");
		frame.setSize(400, 400);
		frame.setLayout(new GridLayout(3, 1));

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		headerLabel = new JLabel("", JLabel.CENTER);
		statusLabel = new JLabel("", JLabel.CENTER);
		statusLabel.setSize(350, 100);

		panel = new JPanel();
		panel.setLayout(new FlowLayout());

		frame.add(headerLabel);
		frame.add(panel);
		frame.add(statusLabel);
		frame.setVisible(true);
	}

	private void showComboboxDemo() {
		headerLabel.setText("Control in action: JComboBox");
		final DefaultComboBoxModel<String> aircraftType = new DefaultComboBoxModel<>();

		aircraftType.addElement("F14 Tomcat");
		aircraftType.addElement("F18 Hornet");
		aircraftType.addElement("F35 Lightning");

		final JComboBox<String> aircraftCombo = new JComboBox<>(aircraftType);
		aircraftCombo.setSelectedIndex(0);

		JScrollPane aircraftListScrollPane = new JScrollPane(aircraftCombo);
		JButton showButton = new JButton("Show");

		showButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String data = "";
				if (aircraftCombo.getSelectedIndex() != -1) {
					data = "Aircraft Selected: " + aircraftCombo.getItemAt(aircraftCombo.getSelectedIndex());
				}
				System.out.println("data: " + data);
				statusLabel.setText(data);
			}
		});
		panel.add(aircraftListScrollPane);
		panel.add(showButton);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		ComboBoxExample comboBoxExample = new ComboBoxExample();
		comboBoxExample.showComboboxDemo();
	}
}