package swing.borderLayout;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class BorderLayoutExample extends JFrame {

	private static final long serialVersionUID = -162491729407893592L;

	public BorderLayoutExample() {

		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Border Layout");
		
		// instruct the JFrame that the contents inside are utilizing the BorderLayout
		new BorderLayout();

		Image image = null;
		try {
			URL url = new URL("https://www.cv64.us/cv64/images/cv64logo.gif");
			image = ImageIO.read(url);
		} catch (IOException e) {
			e.printStackTrace();
		}

		JLabel center = new JLabel(new ImageIcon(image));

		setSize(300, 300);
		add(new Button("North"), BorderLayout.NORTH);
		add(new Button("South"), BorderLayout.SOUTH);
		add(new Button("East"), BorderLayout.EAST);
		add(new Button("West"), BorderLayout.WEST);
		add(center, BorderLayout.CENTER);
	}

	public static void main(String[] args) {
		new BorderLayoutExample();
	}
}