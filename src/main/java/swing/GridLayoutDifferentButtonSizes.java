/* https://stackoverflow.com/questions/2536873/how-can-i-set-size-of-a-button
 * 
 * GridLayout take up all space in its container, and BoxLayout seams to take up all 
 * the width in its container, so I added some glue-panels that are invisible and just 
 * take up space when the user stretches the window. I have just done this horizontally, 
 * and not vertically, but you could implement that in the same way if you want it.
 *
 * Since GridLayout make all cells in the same size, it doesn't matter if they have a 
 * specified size. You have to specify a size for its container instead, as I have done.
 */
package swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutDifferentButtonSizes extends JFrame {

	private static final long serialVersionUID = 5239090756373519521L;

	public GridLayoutDifferentButtonSizes() {

		setTitle("GridLayoutDifferentButtonSizes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		JPanel firstPanel = new JPanel(new GridLayout(4, 4));
		firstPanel.setPreferredSize(new Dimension(4 * 100, 4 * 100));
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j <= 4; j++) {
				firstPanel.add(new JButton());
			}
		}

		JPanel firstGluePanel = new JPanel(new BorderLayout());
		firstGluePanel.add(firstPanel, BorderLayout.WEST);
		firstGluePanel.add(Box.createHorizontalGlue(), BorderLayout.CENTER);
		firstGluePanel.add(Box.createVerticalGlue(), BorderLayout.SOUTH);

		JPanel secondPanel = new JPanel(new GridLayout(13, 5));
		secondPanel.setPreferredSize(new Dimension(5 * 40, 13 * 40));
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= 13; j++) {
				secondPanel.add(new JButton());
			}
		}

		JPanel secondGluePanel = new JPanel(new BorderLayout());
		secondGluePanel.add(secondPanel, BorderLayout.WEST);
		secondGluePanel.add(Box.createHorizontalGlue(), BorderLayout.CENTER);
		secondGluePanel.add(Box.createVerticalGlue(), BorderLayout.SOUTH);

		mainPanel.add(firstGluePanel);
		mainPanel.add(secondGluePanel);
		getContentPane().add(mainPanel);

		// frame.setSize(400,600);
		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
		new GridLayoutDifferentButtonSizes();
	}
}