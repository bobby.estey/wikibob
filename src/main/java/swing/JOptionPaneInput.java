// https://www.mkyong.com/swing/java-swing-how-to-make-a-simple-dialog/
package swing;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class JOptionPaneInput extends JFrame {

	private static final long serialVersionUID = -5792574017255929347L;

	public JOptionPaneInput() {

		getContentPane().setBackground(Color.DARK_GRAY);
		setTitle("Input Dialog in Frame");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		setSize(400, 300);
		getContentPane().setLayout(null);
	}

	private void closeIt() {
		this.getContentPane().setVisible(false);
		this.dispose();
	}

	public static void main(String[] args) {

		JOptionPaneInput frame = new JOptionPaneInput();
		String message = JOptionPane.showInputDialog(frame, "Question?");
		if (message.isEmpty()) {
			frame.closeIt();
		}

		System.out.println("The message is: " + message);
	}
}