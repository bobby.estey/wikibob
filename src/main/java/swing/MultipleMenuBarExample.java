/**
 * This Class is an example of a Java Swing Application with a Multiple MenuBar example
 */
package swing;

import java.awt.GridLayout;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class MultipleMenuBarExample {

	public static final String CVA64 = "CVA-64";
	public static final String CV64 = "CV-64";
	public static final String CONSTELLATION_LOGO = "images/cv64logo.png";
	public static final String CONSTELLATION_SEAL = "images/cv64seal.png";

	public JMenuBar createMenuBar(String icon, String title) {

		ImageIcon imageIcon = createImageIcon(icon);
		JMenuItem menuItem = new JMenuItem(CVA64, imageIcon);
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu(title);
		menuBar.add(menu);
		menu.add(menuItem);
		menuItem = new JMenuItem(CV64, imageIcon);
		menu.add(menuItem);
		return menuBar;
	}

	protected static ImageIcon createImageIcon(String filename) {

		URL imageURL = MultipleMenuBarExample.class.getResource(filename);
		if (imageURL != null) {
			return new ImageIcon(imageURL);
		} else {
			System.err.println("File not found: " + filename);
			return null;
		}
	}

	private static void createAndShowGUI() {

		JFrame frame = new JFrame("Multiple MenuBar Example");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MultipleMenuBarExample multipleMenuBarExample = new MultipleMenuBarExample();

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 2));
		panel.add(multipleMenuBarExample.createMenuBar(CONSTELLATION_LOGO, "JMenuBar 1 - JMenu 1")); // add MenuBar 1
		panel.add(multipleMenuBarExample.createMenuBar(CONSTELLATION_SEAL, "JMenuBar 2 - JMenu 2")); // add MenuBar 2

		frame.add(panel);
		frame.setSize(500, 100);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}