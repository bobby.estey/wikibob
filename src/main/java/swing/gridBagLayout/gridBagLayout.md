# GridBagLayout Manager

- The most complex Graphical User Interface (GUI) Layout Manager in the Java Programming Language
- The most versatile and simplified utilizing a generated [Constraint Class](./Constraint.java)
- [Overloading Constraint Class Constructors and methods](./overloadingConstraint.png)
- [GridBagExample.java](./GridBagExample.java)

This class demonstrates why the GridBagLayout manager object should be used
over all the other layouts for large and complex projects. Several
institutions of education teach the other layouts but not the GridBagLayout,
this is sad.

THERE IS ONLY ONE LAYOUT REQUIRED WHEN USING THE GRIDBAGLAYOUT MANAGER, NOT
SEVERAL WITH THE OTHER LAYOUTS. THE GRIDBAGLAYOUT MANAGER KEEPS TRACK OF EACH
COMPONENT.

This demonstration application is intended as a descriptive tour of how to
implement the GridBagLayout manager. The GridBagLayout manager is the most
flexible and powerful layout manager. To fully exploit this layout manager,
you must really exercise the code and write your own. Another important point
is that this layout manager works very closely with the GridBagConstraints
object.

In general, the GridBagLayout manager divides the container into a series of
grids arranged in rows an columns. Each of the grids does not have to be the
same size. The manager IMPLICITLY determines the number of rows and columns.

Parameters are specified as constraints, constraining the components to
certain sizes, locations, reactions to being resized, borders around the
components and more. The GridBagConstraints class is used to specify these
constraints.

This example demonstrates how to use the layout manager and its associated
constrains to manage a complex application. This application creates four
panels, each panel has a number of buttons. Each panel has a default
background color, and the buttons on the panel can be used to change the
background color. The four panels are added to the frame and are managed by a
GridBagLayout manager and the associated GridBag constraints. Each panel in
turn, manages their own buttons with the same GridBagLayout manager, but with
a different GridBagConstraint for each button.

Clicking on a button results in the title of the frame informing the client
the name of the button pressed in addition to changing the color of the
panels background.

NOTE: This code is for educational purposes only. There are many places where
there are hard coded variables and the code could be far more modular and
other classes could be added.

# References

- @author Bobby Estey - BE
- @author Dan Mazzola - DM

# Versions

- @version 19990511BE - uses Constraints class
- @version 19980210DM - uses new JDK 1.1 events and AWT features
- @version 19970330DM - used JDK 1.0 event handling
