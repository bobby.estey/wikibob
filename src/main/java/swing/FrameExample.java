package swing;

import javax.swing.JFrame;

public class FrameExample extends JFrame {

	private static final long serialVersionUID = 1L;

	public FrameExample() {

		// super - calling the SUPER CLASS Constructor
		// in this case the Super Class Constructor that accepts a String
		// and set the Frame Title
		super("Frame Example");

		// once the user selects the exit button, close the Frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// sets the size of the Frame
		setSize(300, 100);

		// make the Frame visible
		setVisible(true);
	}

	public static void main(String args[]) {
		new FrameExample();
	}
}