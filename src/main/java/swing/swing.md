Swing Basics - Swing is Java's User Interface Application Programming Interface (API) that contains a toolset of Widgets.  Think of Widgets as things you can apply to a webpage, e.g.  buttons, checkboxes, images, videos, etc.

# Model / View / Controller (MVC)
 - model - data - database or file
 - controller - manager - server
 - view - user interface

# SwingSet

All Swing Classes start with J

The fundamental Widget to Swing is the Frame.  The Frame is like a picture Frame which encapsulates the User Interface (UI).  Below is an Example of a Frame.  Frames only contain:
 - title bar - the title of the UI Application, in this case "Frame Example"
 - minimize button - the dash to minimize the UI Application
 - maximize button - the open square to maximize the UI Application
 - close button - the circle X - to close the UI Application
 - container - the outer boundary of the title bar, buttons and the blank space that is encapsulated
 - [code example](./FrameExample.java)

 ![frame example](./frameExample.png)

Adding a Widget to the Frame.  The Button is one of several Widgets in the Swing Toolset

# Layouts

# Events / Listeners

|Event Class|Listener Interface|Methods|Description|
|--|--|--|--|
|ActionEvent|ActionListener|- actionPerformed()|Component action, e.g. Button Click, Menu Item Selected|
|AdjustmentEvent|AdjustmentListener|- adjustmentValueChanged()|Adjustable component, e.g. Scrollbar|
|ComponentEvent|ComponentListener|- componentResized()<br>- componentShown()<br>- componentMoved()<br>-componentHidden()|Component moved, e.g. size changed or visibility|
|ContainerEvent|ContainerListener|- componentAdded()<br>- componentRemoved()|Component added or removed from a Container|
|FocusEvent|FocusListener|- focusGained()<br>- focusLost()|Focus event, e.g. focus, focusin, focusout, blur|
|ItemEvent|ItemListener|- itemStateChanged()|Item selected, e.g. Menu Item|
|KeyEvent|KeyListener|- keyTyped()<br>- keyPressed()<br>- keyReleased()|Key presses on the keyboard|
|MouseEvent|MouseListener|- mousePressed()<br>- mouseClicked()<br>- mouseEntered()<br>- mouseExited()<br>- mouseReleased()|Interaction with the mouse|
|MouseEvent|MouseMotionListener|- mouseMoved()<br>- mouseDragged()|Interaction with the mouse|
|MouseWheelEvent|MouseWheelListener|- mouseWheelMoved()|Mouse wheel was rotated in a component|
|TextEvent|TextListener|- textChanged()|Object’s text changes|
|WindowEvent|WindowListener|- windowActivated()<br>- windowDeactivated()<br>- windowOpened()<br>- windowClosed()<br>- windowClosing()<br>- windowIconified()<br>- windowDeiconified()|Window has changed its status|
