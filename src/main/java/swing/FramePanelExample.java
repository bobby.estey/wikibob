/**
 * Good Swing Coding Practice is add only a Panel or Canvas to a Frame, instead of multiple lower level Components.
 * The Class shows an example where a mainPanel is the overall Container of which we add two panels (panel1 and panel2).
 * Buttons are added to Panel 1 and CheckBoxes are added to Panel 2.
 */
package swing;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FramePanelExample {
	public static void main(String[] args) {
		
		// Create the JFrame with Default Close Operation
		JFrame frame = new JFrame("Frame Panel Example");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create JPanels as containers.  mainPanel is the overall Container for this application
		JPanel mainPanel = new JPanel();
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();

		// Setting the layouts of the Panels
		mainPanel.setLayout(new GridLayout(0, 1));
		panel1.setLayout(new GridLayout(2, 2));
		panel2.setLayout(new GridLayout(0, 4));

		// Creating JButtons
		JButton button1 = new JButton("Button 1");
		JButton button2 = new JButton("Button 2");
		JButton button3 = new JButton("Button 3");
		JButton button4 = new JButton("Button 4");

		// Creating JCheckBoxes
		JCheckBox checkbox1 = new JCheckBox("CheckBox 1");
		JCheckBox checkbox2 = new JCheckBox("CheckBox 2");
		JCheckBox checkbox3 = new JCheckBox("CheckBox 3");
		JCheckBox checkbox4 = new JCheckBox("CheckBox 4");

		// Adding Components to Panels
		// Panel 1, buttons are being added
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel1.add(button4);

		// Panel 2, checkboxes are being added
		panel2.add(checkbox1);
		panel2.add(checkbox2);
		panel2.add(checkbox3);
		panel2.add(checkbox4);

		// Add Panels to Main Panel
		mainPanel.add(panel1);
		mainPanel.add(panel2);

		// Add the Main Panel to the Frame
		frame.getContentPane().add(mainPanel);
		frame.setSize(600, 200);
		frame.setVisible(true);
	}
}