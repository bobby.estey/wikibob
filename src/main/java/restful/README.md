#Representational state transfer (RESTful)

RESTful are services that are available between Client and Server systems

- [RESTful Services explained](https://en.wikipedia.org/wiki/Representational_state_transfer)
- [Simple RESTful example](RESTfulExample.java)

## Postman - API Development Software

 - [application download](https://www.postman.com/)
 
 ![postman example](postman.png)
 
 1. Create GET request, e.g. RESTfulExample
 2. Select GET in Dropdown
 3. Enter Server / Application URL, [RESTful Test Path](https://cv64.us/cv64/database/public/selectAll2.php)
 4. Press Send, sending a Request to the Server
 5. Receive Response from Server, see Results in Body

