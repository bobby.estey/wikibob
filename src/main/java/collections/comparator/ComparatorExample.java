package collections.comparator;

import java.util.Arrays;

// Comparator is how to sort a collection of objects. 
public class ComparatorExample {

	private static void oldArrayStyle() {
		String[][] array2d; // Creating the array
		User[] users = new User[10];

		array2d = new String[10][]; // Defining the parameters of the array

		// Add data to the array
		array2d[0] = new String[] { "0", "Admin", "Password1" };
		array2d[1] = new String[] { "1", "Vale.Vicky", "1VJ4lmIq" };
		array2d[2] = new String[] { "2", "Lane.Lois", "i4Bo7St9W" };
		array2d[3] = new String[] { "3", "Kent.Clark", "8JwnSt99" };
		array2d[4] = new String[] { "4", "Wayne.Bruce", "J5vi0Vx4" };
		array2d[5] = new String[] { "5", "Parker.Peter", "6xInK42J" };
		array2d[6] = new String[] { "6", "Rogers.Steve", "B5t8Ne92" };
		array2d[7] = new String[] { "7", "Luther.Lex", "n8UtgD90" };
		array2d[8] = new String[] { "8", "Osborn.Harry", "1eTn9Jkl" };
		array2d[9] = new String[] { "9", "Prince.Diana", "uThb9Ijk" };

		User user0 = new User("0", "Admin", "Password1");
		User user1 = new User("1", "Vale.Vicky", "1VJ4lmIq");
		User user2 = new User("2", "Lane.Lois", "i4Bo7St9W");
		User user3 = new User("3", "Kent.Clark", "8JwnSt99");

		users[0] = user0;
		users[1] = user1;
		users[2] = user2;
		users[3] = user3;

//		for (User user : users) {
//			System.out.print(user.getUsername() + " ");
//		}
//		
//		for (int i = 0; i < users.length; i++) {
//			System.out.print(users[i].getUsername() + " ");
//		}

		for (String[] user : array2d) {
			System.out
					.println("before the sort ->>>> index: " + user[0] + " name: " + user[1] + " password: " + user[2]);
		}

		Arrays.sort(array2d, new ColumnComparator(1));

		for (String[] user : array2d) {

			// u.getUserId(), u.getUsername(), u.getPassword()
			/*
			 * if (u[1].equals("Luther.Lex")) { System.out.println(u[1]); }
			 */

			System.out
					.println("after the sort ->>>> index: " + user[0] + " name: " + user[1] + " password: " + user[2]);
		}
	}

	private static void stillOldStyle() {

	}

	public static void main(String[] args) {

		ComparatorExample.oldArrayStyle();
		ComparatorExample.stillOldStyle();
	}
}
