package collections.comparator;

import java.util.Comparator;

public class SortByName implements Comparator<Student> {

	// Comparator is simply (-1 less than 0 equal 1 greater than)
	public int compare(Student studentA, Student studentB) {
		return studentA.name.compareTo(studentB.name);
	}
}