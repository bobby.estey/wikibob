package collections.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CarDriver {

	public static void main(String[] args) {

		Car superbird = new Car("Plymouth", "Superbird", 1970, 5);
		Car daytonaCharger = new Car("Dodge", "Daytona Charger", 1969, 3);
		Car metro = new Car("Geo", "Metro", 1983, 15);

		List<Car> cars = new ArrayList<Car>();
		cars.add(superbird);
		cars.add(daytonaCharger);
		cars.add(metro);

		Collections.sort(cars);

		for (int i = 0; i < cars.size(); i++)
			System.out.println(cars.get(i));
	}

}