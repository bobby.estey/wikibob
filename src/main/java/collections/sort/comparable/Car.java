package collections.sort.comparable;

public class Car implements Comparable<Car> {

	private int year = 0;
	private double miles = 0;
	private String name = "";

	// Used to sort by year - compareTo method that compares the year
	// car.year is the Car (outside Class of Car) that is being passed in to the
	// Class (this)
	public int compareTo(Car car) {

		// return negative or positive or zero
		return this.year - car.year;
	}

	// Constructor creating the Car object
	public Car(String name, double miles, int year) {
		this.name = name;
		this.miles = miles;
		this.year = year;
	}

	public double getMiles() {
		return miles;
	}

	public String getName() {
		return name;
	}

	public int getYear() {
		return year;
	}
}
