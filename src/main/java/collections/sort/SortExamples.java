package collections.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortExamples {

	public static void insertionSort(Integer[] simpleArray) {

		int n = simpleArray.length;
		for (int i = 1; i < n; ++i) {
			int key = simpleArray[i];
			int j = i - 1;

			/*
			 * Move elements of arr[0..i-1], that are greater than key, to one position
			 * ahead of their current position
			 */
			while (j >= 0 && simpleArray[j] > key) {
				simpleArray[j + 1] = simpleArray[j];
				j = j - 1;
			}
			simpleArray[j + 1] = key;
		}
	}

	// https://www.geeksforgeeks.org/bubble-sort/
	public static void bubbleSort(Integer[] simpleArray) {
		int n = simpleArray.length;
		for (int i = 0; i < n - 1; i++)
			for (int j = 0; j < n - i - 1; j++)
				if (simpleArray[j] > simpleArray[j + 1]) {
					// swap arr[j+1] and arr[i]
					int temp = simpleArray[j];
					simpleArray[j] = simpleArray[j + 1];
					simpleArray[j + 1] = temp;
				}
	}

	/* A utility function to print array of size n */
	public static void printArray(Integer[] simpleArray) {
		int n = simpleArray.length;
		for (int i = 0; i < n; ++i) {
			System.out.print(simpleArray[i] + " ");
		}

		System.out.println();
	}

	// https://www.geeksforgeeks.org/collections-sort-java-examples/
	public static void collectionSortExample() {
		List<Integer> collection = new ArrayList<>();
		collection.add(12);
		collection.add(3);
		collection.add(7);
		collection.add(4);

		/* Unsorted List */
		System.out.println("collectionsSort - Before Sorting:");
		for (Integer element : collection) {
			System.out.print(element + " ");
		}

		/* Collections Class sort method is called and sorts the elements */
		Collections.sort(collection);

		/* Sorted List */
		System.out.println("\ncollectionsSort - After Sorting:");
		for (Integer element : collection) {
			System.out.print(element + " ");
		}
	}

	public static void main(String[] args) {

		Integer[] simpleArray = { 12, 14, 17, 2, 3, 7 };
		SortExamples.insertionSort(simpleArray);
		SortExamples.bubbleSort(simpleArray);
		SortExamples.collectionSortExample();
	}
}
