package collections.stream;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMap {

	public static void main(String[] args) {

		Function<String, Integer> func = x -> x.length();

		List<Integer> list = Arrays.asList(1, 2, 2, 3, 3, 3, 4, 4, 4, 4);

		Stream<Integer> stream = list.stream().map(x -> x);
		stream.forEach(x -> System.out.print(x));

		List<String> list2 = Arrays.asList("node", "c++", "java", "javascript");

		// lambda
//        Map<String, Integer> map = func.apply(list2);

		System.out.println(list2); // {node=4, c++=3, java=4, javascript=10}

		// https://www.techiedelight.com/create-frequency-map-java-8/
		String[] chars = { "A", "B", "C", "A", "C", "A" };

		Map<String, Long> freq = Stream.of(chars)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		System.out.println(freq);

//        Map<String, Long> freq = new HashMap<>();
//        for (String s: chars) {
//            freq.merge(s, 1L, Long::sum);
//        }
// 
//        System.out.println(freq);

//	    Integer sumResult = Stream.of(list).reduce(0, (a, b) -> a + b);

//	    long a = stream.count();
//	    
//	    System.out.println(a);

//	    List list2 = Arrays.asList(1, 2, 3, 4, 5);
//	    System.out.println(list2.stream().reduce((a, b) -> a* b).get());
//	    
//	    list.stream().flatMapToInt(x -> x).;
//	    Set<String> set = list.stream().flatMap(x -> x).collect(Collectors.toSet());

//		Set<String> set = list.stream().flatMap(x -> x.getMethod().stream()).collect(Collectors.toSet());
//		System.out.println(set);
	}

}
