package collections.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExamples {

	private static int[] intArray = { 1, 2, 3, 4, 5 };

	public static void boxed() {
		System.out.println("boxed");
		Stream.of(1, 2, 3).forEach((n) -> System.out.print(n + "\t"));

		List<Integer> list = Arrays.stream(intArray) // IntStream
				.boxed() // Stream<Integer>
				.collect(Collectors.toList());

		list.forEach((n) -> System.out.print(n + "\t"));
		System.out.println();
	}

	public static void streamRange() {
		System.out.println("streamRange");
		// Creating an IntStream
		IntStream stream = IntStream.range(3, 8);

		// Creating a Stream of Integers
		// Using IntStream boxed() to return
		// a Stream consisting of the elements
		// of this stream, each boxed to an Integer.
		Stream<Integer> stream1 = stream.boxed();

		// Displaying the elements - method reference ::
		// stream1.forEach(x -> System.out.println(x));
		stream1.forEach(System.out::print);
	}
	
	public static void streamFilterForEach1() {
		System.out.println("streamFilterForEach1");
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		List<Integer> newList = list.stream().filter(x -> x > 3).collect(Collectors.toList());
		newList.stream().forEach(x -> System.out.print(x + " "));
	}
	
	public static void streamFilterForEach2() {
		System.out.println("streamFilterForEach2");
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		list.stream().filter(x -> x % 2 == 0).forEach(x -> System.out.print(x + " "));
	}
	
	public static void streamMap() {
		System.out.println("streamMap");
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		Stream<Integer> stream = list.stream().map(x -> x * x);
		stream.forEach(x -> System.out.print(x + " "));
		System.out.println();
		
		// list.stream() - creates stream
		// map(x -> x * x) - configuration
		// forEach(x -> System.out.print(x + " ")) - processing
		list.stream().map(x -> x * x).forEach(x -> System.out.print(x + " "));
	}
	
	public static void streamCollection() {
		System.out.println("streamCollection");
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
		Stream<Integer> stream = list.stream().map(x -> x * x);
//		stream.forEach(x -> System.out.print(x + " "));
		List<Integer> newListFromStream = stream.collect(Collectors.toList());
		Object[] objectArray = list.stream().toArray();
		
		newListFromStream.stream().forEach(x -> System.out.print(x +  " "));
//		System.out.println(stream.count());
		
		for(Object o : objectArray) {
			System.out.print(" objectArray: " + o);
		}
		
		// array filtered
		Object[] arrayFilter = list.stream().filter(x -> x > 3).toArray();
		for(Object o : arrayFilter) {
			System.out.print(" arrayFilter: " + o);
		}
	}
	
	public static void streamSortNatural() {
		System.out.println("streamSortNatural");
		List<Integer> list = Arrays.asList(5, 2, 1, 4, 3);
		Stream<Integer> stream = list.stream().sorted();
		stream.forEach(x -> System.out.print(x + " "));
	}
	
	public static void streamSortComparator() {
		System.out.println("streamSortComparator");
		List<Integer> list = Arrays.asList(5, 2, 1, 4, 3);
		Stream<Integer> stream = list.stream().filter(x -> x > 3).sorted((i1, i2) -> i2.compareTo(i1));
		// Stream<Integer> stream = list.stream().sorted((i1, i2) -> i2.compareTo(i1));
		stream.forEach(x -> System.out.print(x + " "));
	}
	
	public static void streamMinMax() {
		System.out.println("streamMinMax");
		List<Integer> list = Arrays.asList(5, 2, 1, 4, 3);
		Integer min = list.stream().min((i1, i2) -> i1.compareTo(i2)).get();
		Integer max = list.stream().max((i1, i2) -> i1.compareTo(i2)).get();
		System.out.print("min: " + min + " max: " + max);
	}
	
	public static void streamOf() {
		System.out.println("streamOf");
		Stream.of(1,22,333,4444,55555).forEach(x -> System.out.print(x + " "));
		
		String[] name = { "ONE", "TWO", "THREE", "FOUR", "FIVE" };
		Stream.of(name).filter(x -> x.length() > 3).forEach(x -> System.out.print(x + " "));
	}

	public static void main(String args[]) {

		StreamExamples.boxed();
		StreamExamples.streamRange();
		System.out.println();
		StreamExamples.streamFilterForEach1();
		System.out.println();
		StreamExamples.streamFilterForEach2();
		System.out.println();
		StreamExamples.streamMap();
		System.out.println();
		StreamExamples.streamCollection();
		System.out.println();
		StreamExamples.streamSortNatural();
		System.out.println();
		StreamExamples.streamSortComparator();
		System.out.println();
		StreamExamples.streamMinMax();
		System.out.println();
		StreamExamples.streamOf();
		
	}
}