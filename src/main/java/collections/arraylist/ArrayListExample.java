// https://crunchify.com/how-to-iterate-through-java-list-4-way-to-iterate-through-loop/

/*
 * List Collections are preferred over Arrays, here are some reasons why:
 *   1)  Dynamic - Lists change sizes automatically, the developer can concentrate on the logic 
 *                   and not worry about the management of the list.
 *   2)  methods - there are several methods available, e.g. add, remove, sort and much more.
 *                   Arrays, the developer has to write ALL methods
 *   3)  null pointer - most likely never to have a null pointer or index out of range
 *   4)  interface / class - Arrays are very much like primitives, limited
 *   5)  type safe - <nothing else to add>
 */
package collections.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ArrayListExample {

	public ArrayListExample() {

		// Create an ArrayList as an empty set of type String, e.g. <String> and put
		// into a List Interface
		List<String> list = new ArrayList<>();
		
		// Create an ArrayList with predefined values, e.g. 1, 2, 3
		List<String> initializedList = new ArrayList<>(Arrays.asList("1", "2", "3"));

		// add <String> elements to the List
		list.add("a");
		list.add("b");
		list.add("c");

		// loop through the list utilizing the standard for loop
		System.out.print("for: ");
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + ":" + initializedList.get(i) + " ");
		}

		// loop through the list utilizing the standard while loop
		System.out.print("\nwhile: ");
		int i = 0;
		while (i < list.size()) {
			System.out.print(list.get(i) + ":" + initializedList.get(i) + " ");
			i++;
		}

		// loop through the list (String temp : list), the right side list is the list
		// and this loops through the list pulling an element at a time
		System.out.print("\nforList: ");
		for (String element : list) {
			System.out.print(element);
		}

		// utilizing an iterator. Get the iterator from the list and while the iterator
		// has an element, execute the code inside the block
		System.out.print("\niterator: ");
		Iterator<String> iterator = list.iterator();

		// while the iterator has an element
		while (iterator.hasNext()) {
			System.out.print(iterator.next());
		}

		// loop through the list utilizing lambda (big arrow ->)
		System.out.print("\nforEach: ");
		list.forEach((temp) -> {
			System.out.print(temp);
		});
	}

	public static void main(String[] args) {
		new ArrayListExample();
	}
}