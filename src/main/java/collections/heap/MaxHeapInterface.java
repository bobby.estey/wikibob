// https://www.geeksforgeeks.org/max-heap-in-java/
package collections.heap;

public interface MaxHeapInterface<T> {
	public void add(T newEntry);

	public T removeMax();

	public T getMax();

	public boolean isEmpty();

	public int getSize();
}