package collections.btree;

public class StackUnderflowException extends RuntimeException {

	private static final long serialVersionUID = 3929421548478378329L;

	public StackUnderflowException() {
		super();
	}

	public StackUnderflowException(String message) {
		super(message);
	}
}