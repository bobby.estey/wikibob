package collections.btree;

public class LinkedListNode<T> {
	protected LinkedListNode<T> link;
	protected T info;

	public LinkedListNode(T info) {
		this.info = info;
		link = null;
	}

	public void setInfo(T info) {
		this.info = info;
	}

	public T getInfo() {
		return info;
	}

	public void setLink(LinkedListNode<T> link) {
		this.link = link;
	}

	public LinkedListNode<T> getLink() {
		return link;
	}
}
