//https://www.geeksforgeeks.org/convert-base-decimal-vice-versa/
package cs;

import java.util.Arrays;

public class Nano {
	public static char[] digit = { '#', '!', '%', '@', '(', ')', '[', ']', '$' };
	private static final int BASE = 9;

	/*
	 * ========================================================== Return the 2's
	 * complement binary representation for the Nano number given in String s
	 * ==========================================================
	 */
	public static int parseNano(String s) {
		/*
		 * ------------------------------------------------------------------ This loop
		 * checks if the input contains an illegal (non-Nano) digit
		 * ------------------------------------------------------------------
		 */
		for (int i = 0; i < s.length(); i++) {
			int j = 0;
			while (j < digit.length) {
				if (s.charAt(i) == digit[j] || s.charAt(i) == '-') {
					break;
				}

				j++;
			}

			if (j >= digit.length) {
				System.out.println("Illegal nano digit found in input: " + s.charAt(i));
				System.out.println("A Nano digit must be one of these: " + Arrays.toString(digit));
				System.exit(1);
			}
		}

		// Write the parseNano() code here
		return convertBase9toBase10(s);
	}

	/*
	 * ========================================================== Return the String
	 * of Nano digit that represent the value of the 2's complement binary number
	 * given in the input parameter 'value'
	 * ==========================================================
	 */
	public static String toString(int value) {

		// Write the toString() code here
		return convertBase10toBase9(value);
	}

	private static int convertBase9toBase10(String s) {

		String string = "";
		int computedValue = 0;
		boolean negative = false;

		for (int i = 0; i < s.length(); i++) {
			for (int j = 0; j < digit.length; j++) {
				if (s.charAt(i) == digit[j]) {
					string += Integer.toString(j);
					break;
				} else if (s.charAt(i) == '-') {
					negative = true;
				}
			}
		}

		// computedValue = Integer.parseInt(string, 9);
		computedValue = HexBaseConverter.toDecimal(string, BASE);

		if (negative) {
			computedValue = computedValue * -1;
		}

		if (string.isEmpty()) {
			string = "0";
		}

		return computedValue;
	}

	private static String convertBase10toBase9(int decimal) {

		String s = "";
		String string = "";
		boolean negative = false;

		if (decimal < 0) {
			decimal *= -1;
			negative = true;
		}

		string = HexBaseConverter.fromDecimal(9, decimal);

		for (int i = 0; i < string.length(); i++) {
			int index = Character.getNumericValue(string.charAt(i));
			s += digit[index];
		}

		if (negative) {
			s = "-" + s;
		}

		if (s.isEmpty()) {
			s = "#";
		}

		return s;
	}
}