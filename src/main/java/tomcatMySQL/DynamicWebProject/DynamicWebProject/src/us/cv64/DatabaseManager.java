package tomcatMySQL.DynamicWebProject.DynamicWebProject.src.us.cv64;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * database manager
 * 
 * @author rwe001
 *
 */
public class DatabaseManager {

	/** jdbc:mysql://localhost/table */
	private static final String URL = "jdbc:mysql://localhost/cv64";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "welCome1!2@";

	@SuppressWarnings("unused")
	private HttpServletRequest httpServletRequest = null;
	
	@SuppressWarnings("unused")
	private HttpServletResponse httpServletResponse = null;
	
	private Connection connection = null;

	/**
	 * DatabaseManager two argument Constructor
	 * 
	 * @param httpServletRequest
	 * @param httpServletResponse
	 */
	public DatabaseManager(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

		System.out.println("DatabaseManager() - two arg Constructor");
		this.httpServletRequest = httpServletRequest;
		this.httpServletResponse = httpServletResponse;
	}

	/**
	 * DatabaseManager no argument Constructor
	 */
	public DatabaseManager() {

		this(null, null);
		System.out.println("DatabaseManager() - no arg Constructor");
	}

	/**
	 * gets a database connection
	 * 
	 * @throws ServletException
	 */
	public void getConnection() throws ServletException {

		System.out.println("DatabaseManager.getConnection()");

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

		} catch (SQLException exception) {
			System.err.println(exception.getMessage());
			System.out.println("SQLState: " + exception.getSQLState());
			System.out.println("ErrorCode: " + exception.getErrorCode());
			throw new ServletException(exception);
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
			throw new ServletException(exception);
		}
	}

	/**
	 * executes SQL query
	 * 
	 * @param query
	 * @throws ServletException
	 */
	public void executeQuery(String query) throws ServletException {

		System.out.println("DatabaseManager.executeQuery(" + query + ")");

		Statement statement = null;
		ResultSet resultSet = null;
		String results = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				results = resultSet.getString(1);
				System.out.println("DatabaseManager.executeQuery - results: " + results);
			}

		} catch (SQLException exception) {
			System.err.println(exception.getMessage());
			System.out.println("SQLState: " + exception.getSQLState());
			System.out.println("ErrorCode: " + exception.getErrorCode());
			throw new ServletException(exception);
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
			throw new ServletException(exception);
		} finally {
			if (null != resultSet) {
				resultSet = null;
			}
			if (null != statement) {
				statement = null;
			}
		}
	}

	/**
	 * closes database connection
	 * 
	 * @throws ServletException
	 */
	public void closeConnection() throws ServletException {

		System.out.println("DatabaseManager.closeConnection()");
		try {
			if (connection != null)
				connection.close();

		} catch (SQLException exception) {
			System.err.println(exception.getMessage());
			System.out.println("SQLState: " + exception.getSQLState());
			System.out.println("ErrorCode: " + exception.getErrorCode());
			throw new ServletException(exception);
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
			throw new ServletException(exception);
		}
	}
}
