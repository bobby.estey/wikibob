package threads;

public class ImplementsRunnable implements Runnable {

	private static final int NUMBER_THREADS = 10;

	@Override
	public void run() {
		System.out.print("ImplementsRunnable.run: ");
		for (int i = 0; i < NUMBER_THREADS; i++) {
			System.out.print(i + "r ");
		}
	}

	public static void main(String[] args) {

		ImplementsRunnable implementsRunnable = new ImplementsRunnable();
		Thread thread1 = new Thread(implementsRunnable);
		Thread thread2 = new Thread(implementsRunnable);
		thread1.start(); // calls the run method
		thread2.start(); // calls the run method

		RunMethodOnly runMethodOnly = new RunMethodOnly();
		Thread thread3 = new Thread(runMethodOnly);
		thread3.start(); // calls the run method

		System.out.println("End Program");
	}

}
