/**
 * This Class extends the Thread Object in Java and produces and example of Thread Technology
 */
package threads;

public class ExtendsThread extends Thread {

	// total of 10 threads
	private static final int NUMBER_THREADS = 10;

	// run method that is called by the start() method
	public void run() {

		System.out.print("ExtendsThread.run: ");
		for (int i = 0; i < NUMBER_THREADS; i++) {
			System.out.print(i + "r ");
		}
	}

	public static void main(String[] args) {

		ExtendsThread extendsThread = new ExtendsThread();
		extendsThread.start(); // calls the run method

		System.out.print("ExtendsThread.main: ");
		for (int i = 0; i < NUMBER_THREADS; i++) {
			System.out.print(i + "m ");
		}
		
		System.out.println("End Program");
	}
}
