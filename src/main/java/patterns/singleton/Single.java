// https://www.youtube.com/watch?v=KUTqnWswPV4
// A static (class) instantiation, the Constructor is private and the getInstance method is public. 
// The getInstance method returns a single instance through the static instance
// Singleton - only create a single instance
package patterns.singleton;

public class Single {
	static Single single = new Single();

	// Constructor is private not accessible
	private Single() {
	}

	public static Single getInstance() {
		return single;
	}
}
