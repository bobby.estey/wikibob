package patterns.factory;

abstract class Vehicle {

	protected double hours;

	abstract void getHours();

	public void calculateHours(int vehicles) {
		System.out.println(vehicles * hours);
	}
}
