# Factory Pattern

## [Source](../factory/)

- Factory Method Design Pattern defines an interface for creating an object
- Factory method in the Interface, defers the instantiation to one or more concrete subclasses

![Factory Pattern](factory.drawio.png)
