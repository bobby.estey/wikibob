package patterns.factory;

class VehicleFactory {

	public Vehicle getHours(String vehicleType) {

		if (vehicleType.equalsIgnoreCase("AIRCRAFT")) {
			return new Aircraft();
		} else if (vehicleType.equalsIgnoreCase("BOAT")) {
			return new Boat();
		} else if (vehicleType.equalsIgnoreCase("CAR")) {
			return new Car();
		}
		return null;
	}
}
