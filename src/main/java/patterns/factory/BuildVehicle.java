package patterns.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class BuildVehicle {
	
	public static void main(String args[]) throws IOException {
		
		VehicleFactory vehicleFactory = new VehicleFactory();

		System.out.print("Enter the type of Vehicle to be built (AIRCRAFT, BOAT, CAR): ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

		String vehicleType = bufferedReader.readLine();
		System.out.print("Enter the number to build: ");
		int vehicles = Integer.parseInt(bufferedReader.readLine());

		Vehicle vehicle = vehicleFactory.getHours(vehicleType);

		System.out.print("The number of Hours to build " + vehicles + " " + vehicleType + "(s) is: ");
		vehicle.getHours();
		vehicle.calculateHours(vehicles);
	}
}
