/*
 * You are modifying a human resources application. The current Employee class looks like this:
 * Currently everyone is an Employee and some of those are Managers, which is determined by calling the isManager() method. 
 * The company has now started hiring part-time employees, so the system must be modified to keep track of them.
 * Create a new implementation of the Employee class which provides the flexibility required to support the new part-time 
 * employee notion. You are free to refactor this class as required and add additional classes if needed. 
 */
package patterns.builder;

import java.util.Date;

public class Employee {
	
	// All final attributes
	private final boolean managerFlag;
	private final String name;
	private final Date dateHired;
	private final int id;
	private final boolean partTime;

	private Employee(EmployeeBuilder builder) {
		this.managerFlag = builder.managerFlag;
		this.name = builder.name;
		this.dateHired = builder.dateHired;
		this.id = builder.id;
		this.partTime = builder.partTime;
	}

	// All getter, and NO setter to provide immutability
	public boolean isManager() {
		return managerFlag;
	}

	public String getName() {
		return name;
	}

	public Date getDateHired() {
		return dateHired;
	}

	public int getID() {
		return id;
	}

	public boolean getPartTime() {
		return partTime;
	}

	@Override
	public String toString() {
		return "Employee - Manager: " + this.managerFlag + " Name: " + this.name + " Date Hired: " + this.dateHired
				+ " Id: " + this.id + " Part Time: " + this.partTime;
	}

	public static class EmployeeBuilder {
		private final boolean managerFlag;
		private final String name;
		private Date dateHired;
		private int id;
		private boolean partTime;

		public EmployeeBuilder(boolean managerFlag, String name, Date dateHired, int id, boolean partTime) {
			this.managerFlag = managerFlag;
			this.name = name;
			this.dateHired = dateHired;
			this.id = id;
			this.partTime = partTime;
		}

		public EmployeeBuilder(boolean managerFlag, String name) {
			this.managerFlag = managerFlag;
			this.name = name;
		}

		public EmployeeBuilder dateHired(Date dateHired) {
			this.dateHired = dateHired;
			return this;
		}

		public EmployeeBuilder id(int id) {
			this.id = id;
			return this;
		}

		// Return the finally constructed Employee object
		public Employee build() {
			Employee employee = new Employee(this);
			validateEmployeeObject(employee);
			return employee;
		}

		private void validateEmployeeObject(Employee employee) {
			// Do some basic validations to check
			// if employee object does not break any assumption of system
		}
	}
}