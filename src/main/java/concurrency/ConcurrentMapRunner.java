package concurrency;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.LongAdder;

public class ConcurrentMapRunner {

	public static void main(String[] args) {

		String string = "ABCD ABCD ABCD";

		// not thread safe
		// Map<Character, LongAdder> occurances = new Hashtable<>();

		// thread safe
		ConcurrentMap<Character, LongAdder> occurances = new ConcurrentHashMap<>();

		for (char character : string.toCharArray()) {
			occurances.computeIfAbsent(character, ch -> new LongAdder()).increment();

			// not thread safe
//			LongAdder longAdder = occurances.get(character);
//			if (longAdder == null) {
//				longAdder = new LongAdder();
//			}
//
//			longAdder.increment();
//			occurances.put(character, longAdder);
		}

		System.out.println(occurances);
	}
}
