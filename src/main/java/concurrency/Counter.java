package concurrency;

public class Counter {
	int i = 0;

	// get i, increment, set i - not thread safe
	// must add synchronized
	public void increment() {
		i++;
	}

	public int getI() {
		return i;
	}
}
