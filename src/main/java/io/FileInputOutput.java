// https://mkyong.com/java/java-how-to-create-and-write-to-a-file/
package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileInputOutput {

	// java 6 input and prior example
	public void java6input() {

		try {
			BufferedReader inbuffer = new BufferedReader(new FileReader("java6input.txt")); // read input file
			String inputString = "";
			System.out.println("Reading Input File");

			inputString = inbuffer.readLine(); // read the first row

			while (inputString != null) // check for not End Of File (EOF)
			{
				System.out.println(inputString); // print line
				inputString = inbuffer.readLine(); // reading next row
			}

			inbuffer.close();

		} catch (Exception e) {
			System.err.println("Java 6 Input Exception: " + e.toString());
		}
	}

	// java 6 output and prior example
	public void java6output() {

		try {
			BufferedWriter outputFile = new BufferedWriter(new FileWriter("java6output.txt")); // create output file
			outputFile.write("Java 6 Output\n");
			outputFile.write("All statements are within the try block\n\n");
			outputFile.close();

		} catch (IOException e) {
			System.err.println("Java 6 Output Exception: " + e.toString());
		}
	}

	// java 7 output example
	public void java7Output() {
		
		try (FileWriter fileWriter = new FileWriter("java7output.txt");
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
			bufferedWriter.write("Java 7 Output\n");
			bufferedWriter.write("All statements are within the try block\n\n");
		} catch (IOException e) {
			System.err.println("Java 7 Output Exception: " + e.toString());
		}
	}

	// java 11+ output example
	public void java11Output() {
		
		try {
			String content = "Java 11 Output\n";
			Files.write(Paths.get("java11output.txt"), content.getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			System.err.println("Java 11 Output Exception: " + e.toString());
		}
	}

	public static void main(String[] args) {
		FileInputOutput fileInputOutput = new FileInputOutput();
		
		fileInputOutput.java6input();
		fileInputOutput.java6output();

		fileInputOutput.java7Output();

		fileInputOutput.java11Output();
	}
}
