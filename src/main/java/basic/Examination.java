package basic;
import java.util.*; 
import java.io.*;

class Examination {
	
	private static double matrixDimension = 0;

	public static String MatrixChallenge(String string) {
    // code goes here  

    // OK DOING THIS THE OLD FASHIONED WAY, NUTS AND BOLTS - NO JDK STUFF HERE, e.g. Stream
    // I am not cheating moving to my other screen so that methods appear to speed up the test

	matrixDimension = Math.sqrt(string.length());
	int dimension = (int) matrixDimension * 2;
	int[][] matrix = new int[dimension][dimension];
	
	
	for (int i = 0; i < matrixDimension; i++) {
		for (int j = 0; j < matrixDimension; j++) {
			matrix[i][j] = Integer.valueOf(string.charAt(i + j));
		}
	}
	
    return string;
  }

  public static void main (String[] args) {  
    // keep this function call here     
    Scanner s = new Scanner(System.in);
    System.out.print(MatrixChallenge(s.nextLine())); 
  }

}