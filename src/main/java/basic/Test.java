/*
 * Given an array A[] consisting 0s, 1s and 2s. The task is to write a function that sorts the given array. The functions should put all 0s first, then all 1s and all 2s in last.

Examples:

Input: {0, 1, 2, 0, 1, 2}
Output: {0, 0, 1, 1, 2, 2}

Input: {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1}
Output: {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2}
 */
package basic;

public class Test {

	public static void main(String[] args) {

	//	int A[] = { 0, 1, 2, 0, 1, 2 };
		int A[] =  {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1};
		int temp = 0;

		for (int j = 0; j < A.length - 1; j++) {
			for (int i = 0; i < A.length - 1; i++) {
				if (A[i] > A[i + 1]) {
					temp = A[i];
					A[i] = A[i + 1];
					A[i + 1] = temp;
				}
			}
		}

		System.out.println("End Test");

	}

}
