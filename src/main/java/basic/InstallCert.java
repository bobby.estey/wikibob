/*
 * Copyright 2006 Sun Microsystems, Inc.  All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Sun Microsystems nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * Originally from:
 * http://blogs.sun.com/andreas/resource/InstallCert.java
 * Use:
 * java InstallCert hostname
 * Example:
 *% java InstallCert ecc.fedora.redhat.com
 */
package basic;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Class used to add the server's certificate to the KeyStore with your trusted
 * certificates.
 */
public class InstallCert {

	private static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();
//	private static final String JAVA_HOME = "java.home";
//	private static final String LIB = "lib";
//	private static final String SECURITY = "security";
	private static final String CACERTS = "cacerts";
	private static final String TLS = "TLS";
	private static final String PASS_PHRASE = "changeit";

	@SuppressWarnings("deprecation")
	public static void addCertificates(String host, int port, String passPhrase, String certificate) throws Exception {

		if (passPhrase.isEmpty()) {
			passPhrase = PASS_PHRASE;
		}

		char[] passphrase = passPhrase.toCharArray();

		File file = new File(certificate);
		if (file.isFile() == false) {
//			char SEP = File.separatorChar;
			// File dir = new File(System.getProperty(JAVA_HOME) + SEP + LIB + SEP +
			// SECURITY);
			File dir = new File("/media/rwe001/34CCF751CCF70BBC/opt/java/jdk1.6.0_191/jre/lib/security");
			file = new File(dir, certificate);
			if (file.isFile() == false) {
				file = new File(dir, CACERTS);
			}
		}
		System.out.println("Loading KeyStore " + file + "...");
		InputStream in = new FileInputStream(file);
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(in, passphrase);
		in.close();

		SSLContext context = SSLContext.getInstance(TLS);
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(keyStore);
		X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
		SavingTrustManager savingTrustManager = new SavingTrustManager(defaultTrustManager);
		context.init(null, new TrustManager[] { savingTrustManager }, null);
		SSLSocketFactory factory = context.getSocketFactory();

		System.out.println("Opening connection to " + host + ":" + port + "...");
		SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
		socket.setSoTimeout(10000);
		try {
			System.out.println("Starting SSL handshake...");
			socket.startHandshake();
			socket.close();
			System.out.println();
			System.out.println("No errors, certificate is already trusted");
		} catch (SSLException e) {
			System.out.println();
			e.printStackTrace(System.out);
		}

		X509Certificate[] chain = savingTrustManager.x509Certificates;
		if (chain == null) {
			System.out.println("Could not obtain server certificate chain");
			return;
		}

		System.out.println();
		System.out.println("Server sent " + chain.length + " certificate(s):");
		System.out.println();
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		for (int i = 0; i < chain.length; i++) {
			X509Certificate cert = chain[i];
			System.out.println(" " + (i) + " Subject " + cert.getSubjectDN());
			System.out.println("   Issuer  " + cert.getIssuerDN());
			sha1.update(cert.getEncoded());
			System.out.println("   SHA1    " + toHexString(sha1.digest()));
			md5.update(cert.getEncoded());
			System.out.println("   MD5     " + toHexString(md5.digest()));
			System.out.println();

			X509Certificate x509Certificate = chain[i];
			String alias = host + "-" + (i);
			keyStore.setCertificateEntry(alias, x509Certificate);

			OutputStream outputStream = new FileOutputStream(certificate);
			keyStore.store(outputStream, passphrase);
			outputStream.close();

			System.out.println();
			System.out.println(x509Certificate);
			System.out.println();
			System.out.println("Added certificate to keystore " + certificate + " using alias '" + alias + "'");
		}
	}

	private static String toHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 3);
		for (int b : bytes) {
			b &= 0xff;
			sb.append(HEXDIGITS[b >> 4]);
			sb.append(HEXDIGITS[b & 15]);
			sb.append(' ');
		}
		return sb.toString();
	}

	private static class SavingTrustManager implements X509TrustManager {

		private final X509TrustManager x509TrustManager;
		private X509Certificate[] x509Certificates;

		SavingTrustManager(X509TrustManager x509TrustManager) {
			this.x509TrustManager = x509TrustManager;
		}

		public X509Certificate[] getAcceptedIssuers() {

			/**
			 * This change has been done due to the following resolution advised for Java
			 * 1.7+ http://infposs.blogspot.kr/2013/06/installcert-and-java-7.html
			 **/

			return new X509Certificate[0];
			// throw new UnsupportedOperationException();
		}

		public void checkClientTrusted(X509Certificate[] x509Certificates, String authType)
				throws CertificateException {
			throw new UnsupportedOperationException();
		}

		public void checkServerTrusted(X509Certificate[] x509Certificates, String authType)
				throws CertificateException {
			this.x509Certificates = x509Certificates;
			x509TrustManager.checkServerTrusted(x509Certificates, authType);
		}
	}

	public static void main(String[] args) {

		try {
			InstallCert.addCertificates("portal-ws.uat.hubwoo.com", 443, "changeit", "tbn");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
