package basic;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestExample {
	
	private static void printPalindromes(String[] words) {
		
		List<String> listWords = Arrays.asList(words);
		
		for (int i = 0; i < words.length -1; i++) {
			System.out.print(words[i] + words[i + 1] + ":");
		}
		
		System.out.print(words[words.length-1] + words[0]);
	}

	public static void main(String[] args) {

		// Given a list of unique words, return all the pairs of the distinct indices
		// (i, j) in the given list, so that the concatenation of the two words words[i]
		// + words[j] is a palindrome. Example 1: Input: words =
		// ["abcd","dcba","lls","s","sssll"] Output: [[0,1],[1,0],[3,2],[2,4]]
		// Explanation: The palindromes are ["abcddcba","dcbaabcd","slls","llssssll"]
		
		String[] words = {"abcd","dcba","lls","s","sssll"};
		
		List<String> listWords = Arrays.asList(words);
		
		TestExample.printPalindromes(words);
		
		
		
//		String combined = words[0] + words[1];
		
//		listWords.forEach((n) -> System.out.print(n));
//		for(int i = 0; i < words.length; i++) {
//			for(int j = 0; j < palindromes)
//		}

//		int count = 10;
//		
//		IntStream stream = IntStream.range(1, count +1);
//		IntStream stream2 = IntStream.range(1, count +1);
//		Stream<Integer> stream1 = stream.boxed();
//
//		stream1.filter(x -> x % 2 != 0).forEach(x -> System.out.print(x + " "));
//		
//		System.out.println();
//		Stream<Integer> stream3 = stream2.boxed();
//		stream3.filter(x -> x % 2 == 0).forEach(x -> System.out.print(x + " "));
//		
	}

}
