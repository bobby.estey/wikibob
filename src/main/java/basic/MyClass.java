package basic;
public class MyClass {
	
	// Default Constructor
	// not defined in the class and has no implementation
	// created by the Compiler
	// this example there is no Default Constructor because we have a no argument Constructor defined
	// if the no argument Constructor is not defined then there would be a Default Constructor
	
	// No Argument Constructor
	// defined in the class and can have implementation - this case there is no implementation
	// created by the Developer
	
	public MyClass() {}  // if this line is commented out then a Default Constructor is implied
}
