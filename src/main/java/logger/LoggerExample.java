package logger;

import java.util.logging.Logger;

public class LoggerExample {

	private static Logger logger = Logger.getLogger(LoggerExample.class.getName());

	public static void main(String[] args) {
		logger.fine("fine");
		logger.finer("finer");
		logger.finest("finest");
		logger.info("info");
		logger.warning("warning");
	}
}
