package exam;

import java.util.Scanner;

public class Exam {

	// static - means class
	// final - means constant
	// Java naming convention for CONSTANTS are always UPPERCASE
	// private static final int SPEED_LIMIT = 70;

	public Exam() {

	}

	public static void basic() {
		System.out.println("\tHow '\"confounding' \"\n\\it is !");

		// % modulus means remainder
		// 5 % 2 = 1
		// 49 % 7 = 0 50 % 7 = 1 51 % 7 = 2 52 % 7 = 3

		// 5 % 2 = 2 remainder of 1
		System.out.println("5 % 2: " + 5 % 2);

		// 120 % 9 = 3 this means 9 * 13 = 117 => 120 - 117 = 3
		System.out.println("120 % 9: " + 120 % 9);

		// precedence - order of execution
		// 19 / 5 = 3.8 => 11 * 5 % 2 = 55 % 2 = 1 || 11
		// 5.8 - 1 + 4 = 8.8
		double d = 2 + 19 / 5 - 11 * 5 % 2 + 4;
		System.out.println(d);

		double e = 2 + 19 / 5 - (11 * 5) % 2 + 4;
		System.out.println(e);

		double f = 2 + 19 / 5 - 11 * (5 % 2) + 4;
		System.out.println(f);
	}

	public static void ifElse() {
		int x = 6;
		int y = 2;
		if (x < 10) {
			if (y < 5) {
				y = y + 1;
			} else {
				y = 7;
			}
		} else {
			y = y + 10;
		}
		System.out.println(y);
	}

	public static void compute() {
		int x = 6;
		int y = x - 3;
		int z = -x * y + 8;
		x = z * z - x % 2;
		y += x; // (y = y + x;)
		System.out.println(" " + x + " , " + y + " , " + z);
	}

	/*
	 * public class ReportScores { public static void main(String[] args) { double
	 * myscores = 98.5; int numOfQuestions = 10; average = myscores / numOfQuestions
	 * System.out.print("My average score is " + average); } }
	 */

	public static void mathInput() {

		// get input from the keyboard with System.in
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the radius");
		double radius = scanner.nextDouble();
		System.out.println("Enter the height");
		double height = scanner.nextDouble();

		double volume = (0.3333) * Math.PI * Math.pow(radius, 2) * height;
		System.out.println("Volume is " + volume);
		scanner.close();
	}

	public static void printout() {

		// get input from the keyboard with System.in
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter an integer between 5 and 20: ");
		int value = scanner.nextInt();

		if ((value < 5) || (value > 20)) {
			System.err.println("Invalid input: " + value);
		} else {
			// for loop i starts with 0; while i < value; i++ increment i by 1
			for (int i = 0; i < value; i++) {
				System.out.print(value + "\t");
			}
		}

		scanner.close();
	}

	public static void main(String[] args) {

		// Exam.basic();
		// Exam.ifElse();
		// Exam.compute();
		// Exam.mathInput();
		Exam.printout();
	}
}
