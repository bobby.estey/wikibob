package examples;
/* 
 * NOTE:  static modifier - is a Class attribute or method, use static if you don't 
 * need to create an instance (object)
 * 
 * Driving is expensive.  Write a program with a car's miles/gallon and gas 
 * dollars/gallon (both doubles) as input, 
 * and output the gas cost for 20 miles, 75 miles and 500 miles.  
 * Output each floating point value with two digits after the decimal point.
 */
import java.util.Scanner;

public class DrivingCosts {

	// keyboard - object that is accepting the keyboard entries
	// System.in - is the keyboard itself
	private static Scanner keyboard = new Scanner(System.in);

	private static double carMilesToGallon = 0;
	private static double gasDollarsToGallon = 0;
	private static double expense = 0;

	// do work, methods are functions f(x) - should only do one thing
	// method getInput - gets the users input, carMilesToGallon and gasDollarsToGallon
	public static void getInput() {

		System.out.println("Enter the Car Miles to Gallon / Gas Dollars To Gallon, e.g. 20.0 3.1599");
		carMilesToGallon = keyboard.nextDouble();
		gasDollarsToGallon = keyboard.nextDouble();
	}

	// method getOutput - computes the expense
	public static void getOutput(int miles) {

		expense = (miles * gasDollarsToGallon) / carMilesToGallon;
	}

	// main method - instructs the Java Virtual Machine (JVM) where to start
	// main method - should have the least amount of implementation
	public static void main(String[] args) {

		DrivingCosts.getInput();

		DrivingCosts.getOutput(20);
		System.out.printf("%.2f", expense);

		DrivingCosts.getOutput(75);
		System.out.print(" ");
		System.out.printf("%.2f", expense);

		DrivingCosts.getOutput(500);
		System.out.print(" ");
		System.out.printf("%.2f", expense);

		System.out.println();
	}
}
