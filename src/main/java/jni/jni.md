# Java Native Interface (JNI)

JNI is an interface between Java ByteCode and Native Code

## Media

- [Java Native Interface Overview](https://www.youtube.com/watch?v=N7ViTwn682k)
- [JNI Linux](https://www.youtube.com/watch?v=q1ciTploypg)
- [JNI Ubuntu C to Java](https://www.youtube.com/watch?v=YiX0SgRgjmo)
- [JNI Linux Users Guide](https://developers.redhat.com/blog/2016/11/03/eclipse-for-jni-development-and-debugging-on-linux-java-and-c#general_overview_of_jni_compilation_and_the_eclipse_project)

|Definitions|Description|
|--|--|
|function table|- Contains pointers in memory to implementation of the native methods|
|jobject|- Instance of JniMainApp class|
|JNICALL|- Ensures methods are available to be used by the JNI Framework|
|JNIEnv|- Environment Pointer pass an an argument for each native function mapped to a Java method|
|JNIEXPORT|- Method is exportable and located in the function table for JNI to access|
|Shared Library|- Code is referenced<br>- File Size is Smaller<br>- Languages are separated<br>- Resources need to be accessible|
|Static Library|- All code included in executable file, both ByteCode and Native Code<br>- File Size is Larger<br>- A combination of programming languages|

## Coding Examples

|Command|Description|
|--|--|
|- javac -h . FILENAME.java, e.g. JNI.java|-h flag - creates the definition of the method<br>- . - current directory<br>- creates .h file|
|- gcc -o SHARED_OBJECT -shared -fPIC -I INCLUDE_DIRECTORY -I LINUX_INCLUDE_DIRECTORY FILENAME.c -lc|- creates the shared object<br>- gcc -o libJNI.so -shared -fPIC -I/opt/java/jdk-20.0.1/include/ -I/opt/java/jdk-20.0.1/include/linux JNI.c -lc|
|- sudo cp libJNI.so /usr/lib|- copy shared object to /usr/lib<br>- reference the shared object in the FILENAME.java file, see [JNI.java](JNI.java)|

#### Example 1

- [JNI.h](JNI.h)
- [JNI.c](JNI.c)
- [JNI.java](JNI.java)

#### Example 2

- [JniMainApp.h](jni_JniMainApp.h)
- [JniMainApp.c](jni_JniMainApp.c)
- [JniMainApp.java](JniMainApp.java)

## JNI Negatives

- function calls to JNI methods are expensive
- applications lose platform portability
- extra layer of communication between the code executing the Java Virtual Machine (JVM) and native code
- garbage collection is now performed manually

## JNI Exceptions

|Exception|Solution|
|--|--|
|wrong ELF Class: ELFCLASS32|compile with -m flag, e.g. -m64|

