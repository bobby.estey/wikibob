#include "JNI.h"

JNIEXPORT void JNICALL Java_jni_JNI_sayHi
  (JNIEnv * env, jobject obj) {
	printf("Hi JNI");
}

JNIEXPORT jint JNICALL Java_jni_JNI_sum
  (JNIEnv * env, jobject obj, jint a, jint b) {
	return a + b;
}
