package jni;

public class JniMainApp {

	// load the static library to be ready when needed
	static {
		System.loadLibrary("native-lib");
	}

	// native keyword means implementation will be provided by native code
	// native code will be implemented in a separate native shared library
	private native void hello(String name);

	public static void main(String[] args) {
		new JniMainApp().hello("JNI");
	}
}
