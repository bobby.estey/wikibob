package jni;

public class JNI {

	// load the static library to be ready when needed
	static {
		System.load("/usr/lib/libJNI.so");
	}
	
	// native keyword means implementation will be provided by native code
	// native code will be implemented in a separate native shared library
	public native void sayHi();
	public native int sum(int a, int b);
	
	public static void main(String[] args) {
		
		JNI jni = new JNI();
		jni.sayHi();
		int result = jni.sum(1, 3);
		System.out.println("jni.sum: " + result);
	}
}
