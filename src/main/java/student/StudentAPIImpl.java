package student;
// implementation the actual and work
// implements means, I AM REQUIRED to have all the methods defined in the Interface
public class StudentAPIImpl implements StudentAPI {

	private Student[] students = null;
	private int count = 0;

	public StudentAPIImpl(Student[] students, int count) {
		this.students = students;
		this.count = count;
	}

	@Override
	public void printStudentStatistics() {

		for (int i = 0; i < count; i++) {
			System.out.print(students[i].toString());
		}
	}

	@Override
	public void printScore(Student student) {

		System.out.print("Student: " + student.toString());
	}
}
