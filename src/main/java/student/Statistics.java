package student;

import java.io.Serializable;
import java.text.DecimalFormat;

class Statistics implements Serializable {

	private static final long serialVersionUID = 1L;
	DecimalFormat decimalFormat = new DecimalFormat("#.##");
	int[] lowscores = new int[5];
	int[] highscores = new int[5];
	float[] avgscores = new float[5];
	String output = "";

	/*
	 * This method will find the lowest score and store it in an array names
	 * lowscores.
	 */
	void findlow(Student[] a, int count) {

		System.out.print("\nLowest: ");
		output += "\nLowest: ";

		lowscores = a[0].getScores();

		for (int quiz = 0; quiz < 5; quiz++) {

			int lowscore = 100;
			for (int student = 0; student < a.length - 1; student++) {

				if (a[student + 1] != null) {
					lowscore = a[student + 1].getScores()[quiz];
					if (lowscores[quiz] > lowscore) {
						lowscores[quiz] = lowscore;
					}
				}
			}

			System.out.print(lowscores[quiz] + "\t");
			output += lowscores[quiz] + "\t";
		}
	}

	/*
	 * This method will find the highest score and store it in an array names
	 * highscores.
	 */
	void findhigh(Student[] a, int count) {

		System.out.print("\nHighest: ");
		output += "\nHighest: ";

		highscores = a[0].getScores();

		for (int quiz = 0; quiz < 5; quiz++) {

			int highscore = 0;
			for (int student = 0; student < a.length - 1; student++) {

				if (a[student + 1] != null) {
					highscore = a[student + 1].getScores()[quiz];
					if (highscores[quiz] < highscore) {
						highscores[quiz] = highscore;
					}
				}
			}

			System.out.print(highscores[quiz] + "\t");
			output += highscores[quiz] + "\t";
		}
	}

	/*
	 * This method will find avg score for each quiz and store it in an array names
	 * avgscores.
	 */
	void findavg(Student[] avg, int count) {

		System.out.print("\nAverage: ");
		output += "\nAverage: ";

		for (int quiz = 0; quiz < 5; quiz++) {
			for (int i = 0; i < count; i++) {
				avgscores[quiz] += (float) avg[i].getScores()[quiz];
			}
		}

		for (int i = 0; i < 5; i++) {

			String string = decimalFormat.format(avgscores[i] / (count + 1)) + "\t";
			System.out.print(string);
			output += string;
		}
	}

	// add methods to print values of instance variables.
	void printValues(Student[] a, int count) {

		System.out.print("Stud\tQu1\tQu2\tQu3\tQu4\tQu5\n");
		for (int i = 0; i <= count; i++) {
			System.out.print("\n" + a[i].getSID() + "\t");
			for (int j = 0; j < 5; j++) {
				System.out.print(a[i].getScores()[j] + "\t");
			}
		}
		System.out.println("");
	}

	public String getOutput() {
		return output;
	}

	// Serializable
	@Override
	public String toString() {

		return output;
	}
}