package student;

import java.io.BufferedWriter;
import java.io.IOException;

public class Driver1 {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		BufferedWriter bufferedWriter = null;
		Student lab2[] = new Student[40];
		StudentGradingException studentGradingException = new StudentGradingException();

		System.out.print("Enter the maximum number of students in the array: ");
		// int numberStudents =
		// studentGradingException.checkNumberOfStudents(keyboard.nextLine());

		// Populate the student array

		try {
			lab2 = Util.readFile("students.txt", lab2);

			int count = Util.counter;

			Statistics statlab2 = new Statistics();

			statlab2.printValues(lab2, count);
			statlab2.findlow(lab2, count);
			statlab2.findhigh(lab2, count);

			// read a second time, due to the readFile method being static
			lab2 = Util.readFile("students.txt", lab2);

			statlab2.findavg(lab2, count);

		} catch (StudentGradingException e) {
			studentGradingException.write2File("\nDriver StudentGradingException: " + e.getMessage());
		} catch (IOException e) {
			studentGradingException.write2File("\nDriver IOException: " + e.getMessage());
		} catch (Exception e) {
			studentGradingException.write2File("\nDriver Exception: " + e.getMessage());
		} finally {
			studentGradingException.closeFile();
		}
	}
}
