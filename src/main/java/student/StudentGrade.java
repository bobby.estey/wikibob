package student;

public class StudentGrade {

	private static final String HEADER = "Stud\tQu1\tQu2\tQu3\tQu4\tQu5\n\n";

	private Student[] students = null;
	private Statistics statistics = new Statistics();
	private int count = 0;
	private String statisticsString = "";

	public StudentGrade(Student[] students, int count, String statisticsString) {

		this.students = students;
		this.count = count;
		this.statisticsString = statisticsString;
	}

	public StudentGrade() {

	}

	@Override
	public String toString() {

		String studentsString = "";

		for (int i = 0; i < count + 1; i++) {
			studentsString += students[i].toString();
		}

		return HEADER + studentsString + statisticsString;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

}
