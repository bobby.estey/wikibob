// https://mkyong.com/java/java-generate-random-integers-in-a-range/
// https://mkyong.com/java/how-to-sort-an-array-in-java/
package mathematics;

import java.util.Arrays;
import java.util.Random;

public class RandomExample {

	private static final int ARRAY_SIZE = 10;

	/**
	 * method getRandomNumberInRange receives an inclusive range of integers and
	 * returns a random value within the range
	 * 
	 * @param min the minimum value of the random number range inclusive
	 * @param max the maximum value of the random number range inclusive
	 * @return a random number between the minimum and maximum values
	 */
	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random random = new Random();
		return random.nextInt((max - min) + 1) + min;
	}

	private static int getRandomNumber(int seed) {

		Random random = new Random();
		return random.nextInt(seed);
	}

	public static void main(String[] args) {

		int[] seeds = new int[ARRAY_SIZE];
		int[] randomNumbers = new int[ARRAY_SIZE];

		// loading the array seeds
		System.out.print("\nseeds:          ");
		for (int i = 0; i < seeds.length; i++) {
			seeds[i] = getRandomNumber(2);
			System.out.print(seeds[i] + " ");
		}

		// loading the array randomNumbers
		System.out.print("\nrandomNumbers:  ");
		for (int i = 0; i < randomNumbers.length; i++) {
			randomNumbers[i] = getRandomNumberInRange(0, 99);
			System.out.print(randomNumbers[i] + " ");
		}

		// sort the array
		Arrays.sort(randomNumbers);

		// print the new array randomNumbers that was sorted
		System.out.print("\nsorted Numbers: ");
		for (int i = 0; i < randomNumbers.length; i++) {
			System.out.print(randomNumbers[i] + " ");
		}

		System.out.println("\nEnd Program");
	}
}
