package mathematics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class RPN2 {

	private static List<String> tokens = new ArrayList<>();

	public static void infix2postFix() {

		List<String> temp = new ArrayList<>();
		String operator = "";
		String number = "";

		temp.add(tokens.get(0));

		for (int i = 1; i < tokens.size(); i++) {

			if (i % 2 == 1) {
				operator = tokens.get(i);
			} else {
				number = tokens.get(i);
				temp.add(number);
				temp.add(operator);
			}
		}

		tokens = temp;

		System.out.println();
		for (int i = 0; i < tokens.size(); i++) {
			System.out.print(tokens.get(i) + " ");
		}
	}

	public static int infixEval() {

		String operators = "+-*/";
		boolean isOperator = true;

		Stack<String> stack = new Stack<String>();

		stack.push(tokens.get(0));

		for (int i = 1; i < tokens.size(); i++) {

			String token = tokens.get(i);

			if (isOperator) {
				stack.push(token);
				isOperator = false;
			} else {
				isOperator = true;
				int b = Integer.valueOf(token);
				int index = operators.indexOf(stack.pop());
				int a = Integer.valueOf(stack.pop());

				switch (index) {
				case 0:
					stack.push(String.valueOf(a + b));
					break;
				case 1:
					stack.push(String.valueOf(b - a));
					break;
				case 2:
					stack.push(String.valueOf(a * b));
					break;
				case 3:
					stack.push(String.valueOf(b / a));
					break;
				}
			}
		}

		// return the final number on the stack
		return Integer.valueOf(stack.pop());
	}

	public static int postfixEval() {

		String operators = "+-*/";

		Stack<String> stack = new Stack<String>();

		for (String token : tokens) {
			if (!operators.contains(token)) {

				// if a number push on stack
				stack.push(token);

				// if operand pop 2 numbers, compute value and push on stack
			} else {
				int a = Integer.valueOf(stack.pop());
				int b = Integer.valueOf(stack.pop());
				int index = operators.indexOf(token);
				switch (index) {
				case 0:
					stack.push(String.valueOf(a + b));
					break;
				case 1:
					stack.push(String.valueOf(b - a));
					break;
				case 2:
					stack.push(String.valueOf(a * b));
					break;
				case 3:
					stack.push(String.valueOf(b / a));
					break;
				}
			}
		}

		// return the final number on the stack
		return Integer.valueOf(stack.pop());
	}

	public static void checkParenthesesMatching() {

		int tokenCount = 0;

		for (int i = 0; i < tokens.size(); i++) {

			if (tokens.get(i).equals("(")) {
				tokenCount++;
			} else if (tokens.get(i).equals(")")) {
				tokenCount--;
			}
		}

		if (tokenCount != 0) {
			System.err.println("Matching parentheses missing");
		}
	}

	public static void main(String[] args) {

//		String[] tokens = new String[] { "2", "1", "+", "3", "*" };
//		String[] tokens = new String[] { "2", "1", "+", "3", "*", "7", "-", "32", "*" };
//		String string = "2 1 + 3 * 7 - 32 *";
//		String string = "5 + 4 * 3"; // = 5 4 + 3 *;

		Scanner keyboard = new Scanner(System.in);

		// gets user input
		System.out.print("Enter the computation in postfix, e.g. 5 + 4 * 3: ");
		String string = keyboard.nextLine();

		// put the string into a String array and then uses the Arrays Class to import
		// into an ArrayList
		tokens = Arrays.asList(string.split(" "));

		for (int i = 0; i < tokens.size(); i++) {
			System.out.print(tokens.get(i) + " ");
		}

		// check parentheses
		RPN2.checkParenthesesMatching();
		// System.out.println("postfix notation is" +
		// RPN2.infixToPostfix(string));

		// clear tokens arraylist
		tokens = new ArrayList<String>();

		// only add numbers and operators to the tokens
		String[] elements = string.split(" ");

		for (int i = 0; i < elements.length; i++) {
			if ((elements[i].contains("(")) || (elements[i].contains(")"))) {
				continue;
			} else {
				tokens.add(elements[i]);
			}
		}

		// change infix 2 postfix
		RPN2.infix2postFix();

		// compute infix 2 eval
		// System.out.println("result = " + RPN2.infixEval());

		// compute postfix 2 eval
		System.out.println("result = " + RPN2.postfixEval());

		keyboard.close();
	}
}