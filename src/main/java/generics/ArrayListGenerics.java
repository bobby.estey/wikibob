package generics;

import java.util.ArrayList;

public class ArrayListGenerics<E> implements ListGenerics<E> {
	Object[] data;
	int size;
	private static final int defaultlength = 10;

	public ArrayListGenerics() {
		data = new Object[defaultlength];
		size = 0;
	}

	public ArrayListGenerics(int initialCapacity) throws IllegalArgumentException {

		// so why don't we just check for the initialCapacity here
		// if initialCapacity is less than 0 then throw an IllegalArgumentException
		if (initialCapacity < 0) {
			throw new IllegalArgumentException("intialCapacity must be greater than 0");
		}

		data = new Object[initialCapacity];
		size = 0;
	}

	public ArrayListGenerics(E[] arr) {
		if (arr == null) {
			data = new Object[defaultlength];
			size = 0;
		} else {
			data = new Object[arr.length];
			for (int i = 0; i < arr.length; i++) {
				data[i] = arr[i];
			}
			size = arr.length;
		}
	}

	@Override
	public void checkCapacity(int requiredCapacity) {
		int newCapacity = data.length;
		if (newCapacity >= requiredCapacity) {
			return;
		}
		if (newCapacity == 0) {
			newCapacity = defaultlength;
		} else {
			newCapacity = 2 * newCapacity;
		}
		if (newCapacity < requiredCapacity) {
			newCapacity = requiredCapacity;
		}
		Object[] newdata = new Object[newCapacity];
		for (int i = 0; i < data.length; i++) {
			newdata[i] = data[i];
		}
		data = newdata;
	}

	@Override
	public int getCapacity() {
		return data.length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void insert(int index, E element) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}
		if (size == data.length) {
			checkCapacity(size + 1);
		}
		E temp = null;
		E previous = element;
		for (int i = index; i < data.length; i++) {
			temp = (E) data[i];
			data[i] = previous;
			previous = temp;
		}
		size++;
	}

	@Override
	public void append(E element) {
		if (size == data.length) {
			checkCapacity(size + 1);
		}
		data[size] = element;
		size++;
	}

	@Override
	public void prepend(E element) {
		insert(0, element);
	}

	@SuppressWarnings("unchecked")
	@Override
	public E get(int index) {
		E temp = null;

		if (index < 0 || index >= data.length) {
			throw new IndexOutOfBoundsException();
		}
		for (int i = 0; i < data.length; i++) {
			if (i == index) {
				temp = (E) data[i];
			}
		}

		return temp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E set(int index, E element) {
		E prevElement = null;

		if (index < 0 || index >= data.length) {
			throw new IndexOutOfBoundsException();
		}
		for (int i = 0; i < data.length; i++) {
			if (i == index) {
				prevElement = (E) data[i];
				data[i] = element;
			}
		}
		return prevElement;
	}

	/*
	 * Throw IndexOutOfBoundsException when index is strictly less than 0 or greater
	 * than or equal to size of the ArrayList.
	 */
	@Override
	public E remove(int index) throws IndexOutOfBoundsException {
		E remAtI = null;
		ArrayList<Integer> arrayList = new ArrayList<>();
		if (index < 0 || index >= data.length) {
			throw new IndexOutOfBoundsException();
		}
		Object[] remData = new Object[data.length - 1];
		for (int i = 0; i < remData.length; i++) {

			// adds Integers to an ArrayList, except the index
			if (i != index) {
				// remAtI = (E) data[i];
				arrayList.add((Integer) data[i]);
			}
		}
		/*
		 * you don't need of this for (int i = 0; i < remData.length - 1; i++) { if (i
		 * == index) { i++; } remData[i] = data[i]; }
		 */
		// data = remData;
		// size--;
		return remAtI;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void trimToSize() {
		int trimsize = 0;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null) {
				trimsize++;
			}
		}
		Object[] trimdata = new Object[trimsize];
		for (int i = 0; i < trimsize; i++) {
			trimdata[i] = data[i];
		}
		data = trimdata;
	}
}