package generics;

public class Driver2 {

	@SuppressWarnings({ "unchecked" })
	public static void main(String[] args) {

		LinkedListNode<Integer> linkedListNode = new LinkedListNode<>(Integer.valueOf(0));
		linkedListNode.add(Integer.valueOf(1));
		linkedListNode.add(Integer.valueOf(12));

		Target<Integer> target = new Target<>();
		boolean exist = target.contains(12, linkedListNode);

		System.out.println(exist);
	}
}
