package lambda.basic;

public class LambdaExamples {

	public void printValue() {
		System.out.println("value");
	}

	public void greet(Greeting greeting) {
		greeting.perform();
	}
	
	public static void printLambda(StringLength l) {
		System.out.println(l.getLength("print   Lambda"));
	}

	// Lambdas
	private static MyLambda myLambda = () -> System.out.println("myLambda");
	Mathematics adder = (int a, int b) -> a + b;
	Mathematics safeDivideFunction = (int a, int b) -> {
		if (b == 0)
			return 0;
		return a / b;
	};

	public static void main(String[] args) {

		Greeter greeter = new Greeter();

		LambdaExamples.myLambda.systemOutPrintln();

		// These two lines are equivalent
		// StringLength stringLengthCountFunction = (String s) -> s.length();
		StringLength stringLengthCountFunction = s -> s.length();
		System.out.println(stringLengthCountFunction.getLength("Bobby Estey"));

		Greeting innerClass = new Greeting() {
			@Override
			public void perform() {
				System.out.println("innerClass.perform()");
			}

		};

		innerClass.perform();
		greeter.greet(innerClass);
		printLambda(s -> s.length());
	}

}

interface MyLambda {
	void systemOutPrintln();
}

interface Mathematics {
	int add(int x, int y);
}

interface StringLength {
	int getLength(String s);
}
