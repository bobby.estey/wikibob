package lambda;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class LambdaExpression {

	public static void main(String[] args) {

		Map<String, Integer> items = new HashMap<>();
		items.put("A", 10);
		items.put("B", 20);
		items.put("C", 30);
		items.put("D", 40);
		items.put("E", 50);
		items.put("F", 60);
		
		items.remove("C");

		items.forEach((k, v) -> System.out.println("Item : " + k + " Count : " + v));

		items.forEach((k, v) -> {
			System.out.println("Item : " + k + " Count : " + v);
			if ("E".equals(k)) {
				System.out.println("Hello E");
			}
		});
		
		System.out.println(Stream.of("green", "yellow", "blue").max((s1,s2 ) -> s1.compareTo(s2)).filter(s -> s.endsWith("n")).orElse("yellow"));
		
		Integer x = 3;
		Integer y = null;
		try {
			System.out.print(Integer.compareUnsigned(x, 3) == 0|| Integer.compareUnsigned(y, 0) == 0);
			System.out.print(y.compareTo(null) == 0 || true);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
Runnable r = () -> System.out.println("HI");
new Thread(r).start();
	}
	

	
	
}
