package lambda.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {
	  public static void main(String[] args) {
	  
	    List<Integer> integerList = new ArrayList<>();
	    integerList.add(2);
	    integerList.add(15);
	    integerList.add(25);
	    
	    List<Integer> streamList = new ArrayList<>();
	    
	    // creating a stream - .stream()
	    // filter - keeps objects that match the criteria, e.g. anything that is >= 15
	    // collect - creates a new list which is being returned to the streamList object
	    streamList = integerList.stream().filter(x -> x >= 15).collect(Collectors.toList());
	    streamList.stream().forEach(x -> System.out.println(x));
	    
	    // Create a stream object - Stream stream
	    // Configuration stream - .filter();
	    // Process stream - forEach, Collect, Count, Sorted, Min, Max, toArray, of()    

//// Stream<Integer> stream = integerList.stream().filter(x -> i % 2 == 0);
//// stream.forEach(x -> System.out.println(x));

	    // or on one line, an example of chaining
	    //// integerList.stream().filter(x -> i % 2 == 0).forEach(x -> System.out.println(x));
	    
	    // map - process the collection and create a new collection
	    // Create a stream object - Stream stream
	    // Configuration stream - .map();
	    // Process stream - forEach, Collect, Count, Sorted, Min, Max, toArray, of()
//// Stream stream = integerList.stream().map(x -> i * i);
//// stream.forEach(x -> System.out.println(x));
	    
	    // or on one line, an example of chaining
//// integerList.stream().map(x -> i * i).forEach(x -> System.out.println(x));
	  }
	}