package lambda.functional.interfaces;

import java.util.Optional;

public class OptionalExample {

	public static Integer sum(Optional<Integer> one, Optional<Integer> two) {

		// Optional.isPresent - checks the value is present or not
		System.out.println("First parameter is present: " + one.isPresent());
		System.out.println("Second parameter is present: " + two.isPresent());

		// Optional.orElse - returns the value if present otherwise returns
		// the default value passed.
		@SuppressWarnings("removal")
		Integer int1 = one.orElse(new Integer(0));

		// Optional.get - gets the value, value should be present
		Integer int2 = two.get();
		return int1 + int2;
	}

	public static void main(String args[]) {

		try {

			// Optional.of - throws NullPointerException if passed parameter is null
			// Optional<Integer> throwsNullPointerException = Optional.of(null);

			// Optional.ofNullable - allows passed parameter to be null.
			Optional<Integer> one = Optional.ofNullable(null);

			// Optional.of - throws NullPointerException if passed parameter is null
			@SuppressWarnings("removal")
			Optional<Integer> two = Optional.of(new Integer(13));

			// Optional.of - throws NullPointerException if passed parameter is null
			@SuppressWarnings("unused")
			Optional<Integer> throwsNullPointerException = Optional.of(null);

			System.out.println(OptionalExample.sum(one, two));
		} catch (NullPointerException e) {
			System.out.println("NullPointerException: " + e.getMessage());
		}
	}
}