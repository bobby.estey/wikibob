// Static Method - call interface and method
// Memory and Performance are not impeded, Classes require memory, Constructors, Static blocks
// Static methods are not default methods and are called explicitly unlike default methods
package lambda.functional.interfaces;

public interface StaticMethod {

	// to call:  StaticMethod.staticMethod();
	static void staticMethod() {
		System.out.println("Static Method");
	}
}
