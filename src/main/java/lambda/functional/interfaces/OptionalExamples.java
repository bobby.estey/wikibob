package lambda.functional.interfaces;

import java.util.Optional;

public class OptionalExamples {

	public static void isPresent(Optional<Integer> value) {

		int result;
		
		if (value.isPresent()) {
			result = value.get();
		} else {
			result = 0;
		}
		
		System.out.println(result);
	}
	
	public static void ofNullable() {
		int value = 2;
		Optional<Integer> i = Optional.ofNullable(value);
		if (i.isPresent()) {
			System.out.println("optional: " + i + " value: " + value);
		} else {
			System.out.println("not present");
		}
	}

	public static void main(String args[]) {
		
		Integer value;

//		isPresent(Optional.of(10));
//		isPresent(Optional.empty());
//		isPresent(Optional.of(value)); // variable has not been initialized (compile error)
//		isPresent(Optional.of(null));
//		isPresent(null);
		ofNullable();
		
		
	}
}