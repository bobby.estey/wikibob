// Default Method - add methods to interface without affecting implementing classes
// Implementation classes utilize default method and can override the default method
// default is not an access modifier or the default keyword in switch case control statement
// default is public access
// default method cannot override a method from java.lang.Object
// diamond problem of default methods - 2 implemented interfaces contain the same default method
// Interface.super.method <- solves the diamond problem
package lambda.functional.interfaces;

public interface DefaultMethod {

	default void defaultMethod() { System.out.println("example default method"); }
}
