// https://www.youtube.com/watch?v=5K4eVKCpuHQ
package lambda.functional.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamMapCollectExample {

	public static void main(String[] args) {

		List<User> users = new ArrayList<User>();
		users.add(new User(1, "Bobby", "Estey", "T"));
		users.add(new User(2, "Ester", "Estey", "S"));
		users.add(new User(3, "Rebby", "Estey", "C"));

		List<UserDTO> usersDTO = new ArrayList<UserDTO>();

		for (User user : users) {
			usersDTO.add(new UserDTO(user.getId(), user.getFirstName(), user.getLastName()));
		}

		// using stream().map()
		// <R> Stream<R> map(Function<? super T, ? extends R> mapper);
		// Users get added to a map and then the map get put into a Collector and the Collector List is returned
		List<UserDTO> usersDTOs = users.stream()
				.map((User user) -> new UserDTO(user.getId(), user.getFirstName(), user.getLastName()))
				.collect(Collectors.toList());
		
		// print out users
		usersDTOs.forEach(System.out::println);
	}
}

class UserDTO {
	private int id;
	private String firstName;
	private String lastName;

	public UserDTO(int id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "User [ id: " + id + " firstName: " + firstName + " lastName: " + lastName + " ]";
	}
}

class User {
	private int id;
	private String firstName;
	private String lastName;
	private String password;

	public User(int id, String firstName, String lastName, String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [ id: " + id + " firstName: " + firstName + " lastName: " + lastName + " password: " + password
				+ " ]";
	}
}
