package poi.config;

public class SpreadsheetProperties {

	// Keeps class from being instantiated
	private SpreadsheetProperties() {
	}

	/** default path SSAPP_HOME */
	public static final String SSAPP_HOME = "/home/rwe001/git/cv64/git/java/jdk13/output/data/";

	/** default input directory */
	public static final String INPUT_DIRECTORY = "templates/";

	/** default directory for reports */
	public static final String OUTPUT_DIRECTORY = "reports/";

	/** Default Worksheet Font Name */
	public final static String WORKSHEET_FONT_NAME = "Arial";

	/** Default Worksheet Font Size */
	public final static short WORKSHEET_FONT_SIZE = 16;

	/**
	 * 604800000 = 1 week in milliseconds, this parameter is used for cleaning up
	 * old reports
	 */
	public static final long PURGE_FILE_AGE_MILLISECONDS = 604800000L;
}
