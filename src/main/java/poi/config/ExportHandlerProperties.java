package poi.config;

public class ExportHandlerProperties {
	
	// Keeps class from being instantiated
	private ExportHandlerProperties() {
	}

	/** Maximum Column Width */
	public final static int MAXIMUM_COLUMN_WIDTH = 23000;

	/** Maximum Rows per page */
	public static final int MAXIMUM_ROWS_PER_PAGE = 42;

	/** Classified Row Height */
	public final static int CLASSIFIED_ROW_HEIGHT = 5;
}
