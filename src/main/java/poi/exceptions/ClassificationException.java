package poi.exceptions;

public class ClassificationException extends Exception {
	private static final long serialVersionUID = 0;

	private String msg;

	public ClassificationException() {
	}

	/**
	 * @param aMsg
	 * @param t
	 */
	public ClassificationException(String aMsg, Throwable t) {
		super(aMsg, t);
		msg = aMsg;
	}

	/**
	 * @param string
	 */
	public ClassificationException(String aMsg) {
		super(aMsg);
		msg = aMsg;
	}

	/**
	 * @param aMsg
	 */
	public void setMessage(String aMsg) {
		msg = aMsg;
	}

	public String getMessage() {
		return msg;
	}
}
