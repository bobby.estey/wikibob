package fundamentals.arrays;

import java.util.Arrays; //newly added
import java.util.Comparator;//newly added
import java.util.Scanner;

public class ArrayExample {

	private static int[][] cumulativeProduct(int[][] c) {

		int[][] d = new int[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (j == 0) {
					d[i][j] = c[i][j];
				} else {
					d[i][j] = c[i][j] * d[i][j - 1];
				}
			}
		}

		return d;
	}

	private static void loadArray() {
		int[][] a = { { 1, 2, 3 }, { 3, 2, 5 }, { 6, 6, 6 } };
		int[][] b = cumulativeProduct(a);

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(b[i][j] + " ");
			}
		}
	}

	private static void simpleArray() {

		int sum = 0;
		String[] vals = new String[5];

		for (int i = 0; i < vals.length; i++) {
			vals[i] = Integer.toString(i);
			sum += i;
		}

		System.out.println("\nAverage: " + sum / vals.length + "\n");
	}

	@SuppressWarnings("unchecked")
	private static void comparatorExample() {
		// Scanner is the Scanner Class
		// scanner is the object
		// new means a Constructor is following
		// Scanner() is the Constructor() which takes a parameter in this case System.in
		// System.in is the keyboard
		Scanner scanner = new Scanner(System.in);

		System.out.print("Please enter the element in the array to search, e.g. Rogers.Steve : ");
		String name = scanner.nextLine();

		/*
		 * Create a two dimensional array with 10 rows and 3 columns with user data. The
		 * first column will correspond to the ID #. The second column will correspond
		 * with the User ID name. The third column will correspond with the user
		 * password.
		 */

		String[][] usersData = new String[10][3]; // Declare array with 10 rows and 3 columns

		usersData[0][0] = "1"; // Column 1 represents the User ID #
		usersData[1][0] = "2";
		usersData[2][0] = "2";
		usersData[3][0] = "3";
		usersData[4][0] = "4";
		usersData[5][0] = "5";
		usersData[6][0] = "6";
		usersData[7][0] = "7";
		usersData[8][0] = "8";
		usersData[9][0] = "9";

		usersData[0][1] = "Vale.Vicky"; // Column 2 represents the User ID name
		usersData[1][1] = "Lane.Lois";
		usersData[2][1] = "Kent.Clark";
		usersData[3][1] = "Wayne.Bruce";
		usersData[4][1] = "Peter.Parker";
		usersData[5][1] = "Rogers.Steve";
		usersData[6][1] = "Luther.Lex";
		usersData[7][1] = "Osborn.Harry";
		usersData[8][1] = "Prince.Diana";
		usersData[9][1] = "LindaZoel";

		usersData[0][2] = "ZZZZZZZZZ"; // Column 3 represents User Password with a minimum of 8 characters
		usersData[1][2] = "VVVVVVVVVV";
		usersData[2][2] = "AAAAAAAAAAAA";
		usersData[3][2] = "FFFFFFFFFFF";
		usersData[4][2] = "RRRRRRRR";
		usersData[5][2] = "QQQQQQQQQ";
		usersData[6][2] = "GGGGGGGG";
		usersData[7][2] = "YYYYYYYYY";
		usersData[8][2] = "LLLLLLLLLLLLLL";
		usersData[9][2] = "PPPPPPPPPPPPPPP";

		// Sort the array by User ID name or column 2
		Arrays.sort(usersData, new ColumnComparator(1));

		for (int i = 0; i <= 9; i++) { // loop through the User ID #s

			System.out.println("User ID# " + usersData[i][0] + " has the User ID name  " + usersData[i][1]
					+ " with Password " + usersData[i][2]);
			System.out.println("\tThe Password for User ID# " + usersData[i][0] + " has " + usersData[i][2].length()
					+ " chracters\n");
		}

		// Print the sorted array
		for (int i = 0; i < usersData.length; i++) {
			String[] row = usersData[i];
			for (int j = 0; j < row.length; j++) {
				System.out.print(row[j] + "   ");
			}
			System.out.print("\n");
		}

		// traverse the array for Rogers.Steve
		boolean found = false;
		for (int i = 0; i < usersData.length; i++) {
			String[] row = usersData[i];
			for (int j = 0; j < row.length; j++) {

				// print out the record for the matched name
				// .equals - row[j] == name that the user entered in the scanner
				String lowercaseElement = row[j].toLowerCase();
				String lowercaseName = name.toLowerCase();
				if (lowercaseElement.contains(lowercaseName)) {
					found = true;
					System.out.print(row[0] + " " + row[1] + " " + row[2]);
				}
			}

			System.out.print("\n");
		}
		if (!found)
			System.out.println("Record not found");

		scanner.close();
	}

	public static void main(String[] args) {

		loadArray();
		simpleArray();
		comparatorExample();
	}
}

// Class that extends Comparator
@SuppressWarnings("rawtypes")
class ColumnComparator implements Comparator {
	int columnToSort;

	ColumnComparator(int columnToSort) {
		this.columnToSort = columnToSort;
	}

	// overriding compare method
	public int compare(Object o1, Object o2) {
		String[] row1 = (String[]) o1;
		String[] row2 = (String[]) o2;
		// compare the columns to sort
		return row1[columnToSort].compareTo(row2[columnToSort]);
	}
}