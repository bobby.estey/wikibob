package fundamentals.abstracts.overloadOverride;

/**
 * This is a Class about Goats
 * 
 * @author bobby
 * @version 0.1
 */
public class Goat extends Animal {

	/** name of Goat */
	private String name = "";
	
	/**
	 * @return name of Goat
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name set name of Goat
	 */
	public void setName(String name, String breed) {
		this.name = name + ": " + breed;
	}
}
