package fundamentals.abstracts.overloadOverride;

/**
 * This is a Class about Dogs
 * 
 * @author bobby
 * @version 0.1
 */
public class Dog extends Animal {

	/** name of Dog */
	private String name = "";
	
	/**
	 * @return name of Dog
	 */
	public String getName() {
		return "Bobby: " + name;
	}
	
	/**
	 * @param name set name of Dog
	 */
	public void setName(String name) {
		this.name = name;
	}
}
