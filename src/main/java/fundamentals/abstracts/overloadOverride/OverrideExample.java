package fundamentals.abstracts.overloadOverride;

/**
 * @author bobby
 * @version 0.1
 */
public class OverrideExample {

	public OverrideExample() {
		Animal animal = new Animal();
		Cat cat = new Cat();
		Dog dog = new Dog();

		animal.setName("Max");
		cat.setName("Cat");
		dog.setName("Max");

		System.out.println("Animal Name: " + animal.getName());
		System.out.println("Cat Name:    " + cat.getName());
		System.out.println("Dog Name:    " + dog.getName());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new OverrideExample();

	}
}
