package fundamentals.abstracts.overloadOverride;

/**
 * @author bobby
 * @version 0.1
 */
public class OverloadExample {

	public OverloadExample() {
		Animal animal = new Animal();
		Cat cat = new Cat();
		Dog dog = new Dog();
		Goat goat = new Goat();

		animal.setName("Max");
		cat.setName("Cat");
		dog.setName("Max");
		goat.setName("Gus", "Pygmy");

		System.out.println("Animal Name: " + animal.getName());
		System.out.println("Cat Name:    " + cat.getName());
		System.out.println("Dog Name:    " + dog.getName());
		System.out.println("Goat Name:   " + goat.getName());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new OverloadExample();

	}
}
