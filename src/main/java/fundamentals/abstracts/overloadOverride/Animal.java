package fundamentals.abstracts.overloadOverride;


/**
 * This is a Class about Animals
 * 
 * @author bobby
 * @version 0.1
 */
public class Animal {
	
	/** name of Animal */
	private String name = "";

	/**
	 * @return name of Animal
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name set name of Animal
	 */
	public void setName(String name) {
		this.name = name;
	}
}

