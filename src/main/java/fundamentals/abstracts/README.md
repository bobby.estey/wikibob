# Abstracts

The typical example that everyone utilizes is the Shape Type.  Think about a Shape.  A Shape can contain several attributes, in this example the Shape Class has the following attributes sides and colors, so do the other Classes.  However, here’s the difference, how do you compute the area and perimeter of a Shape.  This is not possible because a Shape is something that is ABSTRACT, not clear and can be any form.  For instance, a Shape can have 1, 2, 3, 10, 100, etc. sides.  How would you compute each of these Shapes???  Looking at the other Classes, we can conceptualize what are a Circle, Rectangle and Triangle, and most of all, we can COMPUTE the area and perimeter due to these Shapes being Concrete Class.  Please look at both the code and table below for clarification on ABSTRACTION.

![abstraction](abstraction.png)

![Abstract Shape UML](abstractShapeUML.drawio.png)

## Overriding / Overloading

- Located below is an example of both Overriding and Overloading
- The Classes Cat, Dog, Human and Hound are Overriding the method eat() from Animal
- The Class Hound is Overloading the method bark() from both Animal and Dog with the method bark(String pitch)

![Overriding / Overloading](overridingOverloading.drawio.png)
