package fundamentals.abstracts;

public class StaticClassDriver {

	public static class ClassExample {
		public ClassExample() {
			System.out.println("ClassExample Constructor");
		}
	}

	public static void main(String[] args) {
		new StaticClassDriver.ClassExample();
	}
}