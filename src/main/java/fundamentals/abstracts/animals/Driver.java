// https://www.youtube.com/watch?v=yY6XMBUCNYE
// https://www.youtube.com/watch?v=E2tYw6pEW6I
// https://www.quora.com/What-is-the-difference-between-overload-and-override
// overriding a subclass's Constructor / method is (overriding) and superclass method.
// overloading a Constructor / method with different signatures
package fundamentals.abstracts.animals;

public class Driver {

	public static void main(String[] args) {
		Dog dog = new Dog();
		Cat cat = new Cat();
		Person person = new Person();
		dog.eat();
		cat.eat();
		dog.walk();
		cat.walk();
		dog.bark();
		cat.meow();
		System.out.println("canIDrink(): " + person.canIDrink());
		System.out.println("canIDrink(8): " + person.canIDrink(8));
		System.out.println("canIDrink(28): " + person.canIDrink(28));
	}
}
