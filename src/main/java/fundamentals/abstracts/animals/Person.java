package fundamentals.abstracts.animals;

public class Person extends Animal {

	public void eat() {
		System.out.println("Person eatting");
	}
	
	public void speak() {
		System.out.println("Hello");
	}
}
