//class Dog has the implementation of the method
package fundamentals.abstracts.animals;

public class Dog extends Animal {

	public void eat() {
		System.out.println("Dog eatting");
	}

	public void bark() {
		System.out.println("Dog barking");
	}
}
