// Circle (Concrete) must implement abstract methods coming from Shape
package fundamentals.abstracts.shapes;

public class Circle extends Shape {

	private static final double PI = Math.PI;
	private double area = 0;

	@Override
	public double getArea() {
		return area;
	}

	@Override
	public void setArea(double pi, double radius) {
		area = PI * (radius * radius);
	}
}
