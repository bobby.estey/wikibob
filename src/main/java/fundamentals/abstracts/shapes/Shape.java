// Source -> Generate Getters and Setters -> Select -> Generate
package fundamentals.abstracts.shapes;

import java.awt.Color;

public abstract class Shape {

	private int numberSides = 0;
	private Color color = null;
	// private double area = 0;

	// just a definition / no implementation
	// abstract modifier - returns double - no curly braces
	public abstract double getArea();

	public abstract void setArea(double length, double width);

	public int getNumberSides() {
		return numberSides;
	}

	public void setNumberSides(int numberSides) {
		this.numberSides = numberSides;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}