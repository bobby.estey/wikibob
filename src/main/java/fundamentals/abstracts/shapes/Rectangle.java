// Rectangle (Concrete) must implement abstract methods coming from Shape
package fundamentals.abstracts.shapes;

public class Rectangle extends Shape {

	private double area = 0;

	@Override
	public double getArea() {
		return area;
	}

	@Override
	public void setArea(double length, double width) {
		area = length * width;
	}
}
