package fundamentals.abstracts.shapes;

public class Square extends Shape {

	private String name = "";
	private double edgeLength = 0;
	@SuppressWarnings("unused")
	private double area = 0;

	// a. A constructor that takes in a String name and a double edgeLength.
	public Square(String name, double edgeLength) {
		this.name = name;
		this.edgeLength = edgeLength;
	}

	// b. A constructor that takes in a double edgeLength and sets name to ��.
	public Square(double edgeLength) {
		// this.edgeLength = edgeLength;
		// this.name = name;
		this("", edgeLength);
	}

	// c. A constructor that takes in nothing and sets name to �� and edgeLength to
	// 1.0.
	public Square() {
		// this.edgeLength = 1.0;
		// this.name = "";
		this("", 1.0);
	}

	// d. A static method called isSquare that takes in 4 doubles to verify if the 4
	// doubles are equal. This method should return a boolean.
	public static boolean isSquare(double a, double b, double c, double d) {
		if ((a == b) && (b == c) && (c == d))
			return true;

		return false;
	}

	// e. A method that returns the area of the Square. Call this method area.
	public double area(double edgeLength) {
		return Math.pow(edgeLength, 2);
	}

	// f. A method that takes in another Square as a parameter and returns boolean
	// whether they are equal to each other. Call this method equals.
	// don't call at this time
	public boolean equals(Square s) {
		boolean isTrue = this.equals(s);

		return isTrue;
	}

	// g. A method to change the name of the Square. Call this method setName
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	// h. A method to change the edgeLength of the Square. Call this method setEdge.
	public double getEdgeLength() {
		return edgeLength;
	}

	public void setEdgeLength(double edgeLength) {
		this.edgeLength = edgeLength;
	}

	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setArea(double length, double width) {
		area = length * width;

	}
}
