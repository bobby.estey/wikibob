package fundamentals.modifiers2;

import fundamentals.modifiers.A;

public class C extends A {

	public static void main(String[] args) {
		A a = new A();
		C c = new C();

		System.out.println(a.myPublic);
		System.out.println(c.getMyPrivate());
		System.out.println(c.myProtected);
		System.out.println(c.myPublic);
	}
}
