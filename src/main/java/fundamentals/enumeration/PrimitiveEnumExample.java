package fundamentals.enumeration;

/*
 * enum - is a primitive compared to the counterpart Interface Enumeration
 * Think of enums as arrays that are accessed syntactically different
 * Size is an enumeration and to reference the contents in the enum, 
 * you simply put the enum Collection and element name, e.g.  Size.OHIO
 * Instead of an Array reference of Suit[5]
 */
public class PrimitiveEnumExample {
	enum Size {
		SMALL, MEDIUM, LARGE, XL, XXL, OHIO, HAWAII
	}

	public static void main(String[] args) {
		Size myEnum = Size.OHIO;
		System.out.println(myEnum);
	}
}
