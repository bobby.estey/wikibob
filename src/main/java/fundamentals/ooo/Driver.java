package fundamentals.ooo;

/**
 * Driver Class - is simply a Class that has a main method
 * 
 * This is an example of a general familiarization of Object Oriented Computing
 * Program
 * 
 * cd ~/git/cv64/website/bobbyU/computing/java/jdk8/src/fundamentals/ooo javadoc
 * -sourcepath * -d javadoc -verbose -author -private
 * 
 * @author rwe001
 *
 */
public class Driver {

	/**
	 * main method - instructs the Java Virtual Machine (JVM) where to start
	 * 
	 * @param args - command line arguments
	 */
	public static void main(String[] args) {

		// Vehicle - Class
		// car - object
		// new means a Constructor is following
		// Vehicle() is a Constructor
		Vehicle viper = new Vehicle();
		Vehicle geo = new Vehicle(40);
		Truck ram = new Truck();
		Truck dakota = new Truck();

		viper.setSpeed(50);
		ram.setSpeed(100);
		ram.setTorque(5000);
		dakota.setSpeed(70);

		System.out.println("Viper speed:  " + viper.getSpeed() + ":" + viper.getWeight());
		System.out.println("Geo speed:    " + geo.getSpeed() + ":" + geo.getWeight());
		System.out.println("Ram speed:    " + ram.getSpeed() + ":" + ram.getWeight());
		System.out.println("Ram Torque:   " + ram.getTorque() + ":" + ram.isFourWheel());
		System.out.println("Dakota speed: " + dakota.getSpeed() + ":" + dakota.getWeight());

		int numberVehicles = Vehicle.numberVehicles;
		System.out.println("Number of Vehicles: " + numberVehicles);
	}
}