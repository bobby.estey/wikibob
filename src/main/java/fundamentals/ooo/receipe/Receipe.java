package fundamentals.ooo.receipe;

// Class is a collection of attributes and methods and is a type of thing
public class Receipe {

	// private - no other Class cannot access
	// information hiding
	// [] - array - collection of the same type and in this case we have 4 elements
	// all Strings
	private String[] ingredients = new String[4];
	private double cost = 0;
	private int time = 0;
	private int temperature = 0;

	// 3 argument Constructor
	public Receipe(double cost, int time, int temperature) {
		this.cost = cost;
		this.time = time;
		this.temperature = temperature;
	}

	// Constructors create objects, Constructors always match the Class name
	// no argument Constructor <- correct term is <---- default Constructor
	public Receipe() {
		System.out.println("Receipe() Constructor");
	}

	public String[] getIngredients() {
		return ingredients;
	}

	// public - Class can access, void returns nothing
	public void setIngredients(String[] ingredients) {
		this.ingredients = ingredients;
	}

	// public - Class can access, double returns double
	// getCost method receives no attributes
	public double getCost() {
		return cost;
	}

	// public - Class can access, void returns nothing
	// setCost method receives a cost attribute of type double
	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}
}
