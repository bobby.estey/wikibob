/**
 * this equals Farmer (this always equals the Class name strength (initial value
 * = 75) health (initial value = 100) stamina (initial value = 75) speed
 * (initial value = 10) attackPower (initial value = 1)
 */
package fundamentals.ooo.game;

public class Constable extends Human {

	private int jurisdiction = 0;

	public Constable(String name, int jurisdiction) {
		this.setName(name);
		this.setStrength(60);
		this.setHealth(100);
		this.setStamina(60);
		this.setSpeed(20);
		this.setAttackPower(5);
		this.setJurisdiction(jurisdiction);
	}

	public int getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(int jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
}
