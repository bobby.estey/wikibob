package fundamentals.format;

import java.text.DecimalFormat;
import java.util.Scanner;

public class DecimalFormatLoop {

	private static DecimalFormat decimalFormat = new DecimalFormat("$#0.00"); // # digits

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		double amountSpent = 0;
		double couponAmount = 0;
		double couponPercent = 0;
		String couponPercentString = "";
		double discount = 0;
		double netAmount = 0;
		double subTotal = 0;
		double total = 0;
		int transactions = -1;

		// execute the loop until the user enters the amount of 0
		do {

			// Please enter the cost of your groceries.
			System.out.print("Please enter the cost of your groceries: ");
			amountSpent = scanner.nextDouble();

			// all conditions are greater than equals on the left decision and less than on
			// the right
			// being consistent
			if (amountSpent < 10) {
				couponPercent = 0.00;
				couponPercentString = "0";
			} else if (amountSpent >= 10 && amountSpent < 50) {
				couponPercent = 0.05;
				couponPercentString = "5";
			} else if (amountSpent >= 50 && amountSpent < 75) {
				couponPercent = 0.1;
				couponPercentString = "10";
			} else if (amountSpent >= 75 && amountSpent < 125) {
				couponPercent = 0.15;
				couponPercentString = "15";
			} else if (amountSpent >= 125 && amountSpent < 200) {
				couponPercent = 0.18;
				couponPercentString = "18";
			} else if (amountSpent >= 200) {
				couponPercent = 0.2;
				couponPercentString = "20";
			}

			discount = amountSpent * couponPercent;
			subTotal = amountSpent - discount;
			couponAmount += discount;

			// Print the total amount spent on groceries, the coupon percent, and the end
			// price.
			if (amountSpent > 0) {
				System.out.println("You earned a discount coupon of " + decimalFormat.format(discount) + ". ("
						+ couponPercentString + "% of your purchase)");

				System.out
						.println("Please pay " + decimalFormat.format(subTotal) + ".  Thank you for shopping with us!");
			}

			transactions++; // increment the number of transaction
			netAmount += subTotal;
			total += amountSpent;

			System.out.println();

		} while (amountSpent != 0);

		netAmount = total - couponAmount;

		System.out.println("Transactions: " + transactions);
		System.out.println("Total Amount: " + decimalFormat.format(total));
		System.out.println("Coupon Amount: " + decimalFormat.format(couponAmount));
		System.out.println("Net Amount: " + decimalFormat.format(netAmount));

		scanner.close();
	}
}