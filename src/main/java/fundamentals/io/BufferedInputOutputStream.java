// https://mkyong.com/java/how-to-read-file-from-java-bufferedinputstream-example/
// https://mkyong.com/java/how-to-write-to-file-in-java-fileoutputstream-example/
// https://mkyong.com/java/java-read-a-file-from-resources-folder/
package fundamentals.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class BufferedInputOutputStream {

	private static List<byte[]> memory = new ArrayList<>();
	private static final int BYTE_SIZE = 1000;
	private static final String INPUT_FILE = "files/bufferedInputFile.txt";
	private static final String OUTPUT_FILE = "files/bufferedOutputFile.txt";

	private static void readFile() {

		InputStream inputStream = null;
		BufferedInputStream bufferedInputStream = null;
		byte[] buffer = new byte[BYTE_SIZE]; // buffer size is 1000 bytes
		int bufferSize = 0;

		try {
			// inputStream = getClass().getClassLoader().getResourceAsStream(INPUT_FILE);
			inputStream = new ByteArrayInputStream(INPUT_FILE.getBytes());
			bufferedInputStream = new BufferedInputStream(inputStream);

			do {
				bufferSize = bufferedInputStream.read(buffer);
				memory.add(buffer);

			} while (bufferSize != -1); // if -1 no more data

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
				bufferedInputStream.close();
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
				ex.printStackTrace();
			}
		}
	}

	private static void writeFile() {

		FileOutputStream fileOutputStream = null;
		File outputFile = null;
		byte[] buffer = new byte[BYTE_SIZE];

		try {

			// outputFile = new File(getResourcePath() + "/abc.txt");
			outputFile = new File(OUTPUT_FILE);

			// write something to file here
			System.out.println(outputFile.lastModified());

			// outputFile = new File(OUTPUT_FILE);
			fileOutputStream = new FileOutputStream(outputFile);

			// if file doesn't exists, then create
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}

			for (int i = 0; i < memory.size(); i++) {
				buffer = memory.get(i);
				fileOutputStream.write(buffer);
			}

			// make sure that all io has been completed
			fileOutputStream.flush();
			fileOutputStream.close();

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unused")
	private static void readWriteFile() {

		File inputFile = new File(INPUT_FILE);
		File outputFile = new File(OUTPUT_FILE);
		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		BufferedInputStream bufferedInputStream = null;
		BufferedOutputStream bufferedOutputStream = null;
		byte[] buffer = new byte[BYTE_SIZE]; // buffer size is 1000 bytes
		int bufferSize = 0;

		try {
			fileInputStream = new FileInputStream(inputFile);
			bufferedInputStream = new BufferedInputStream(fileInputStream);

			fileOutputStream = new FileOutputStream(outputFile);
			bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}

			do {
				bufferSize = bufferedInputStream.read(buffer);
				fileOutputStream.write(buffer);

			} while (bufferSize != -1);

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				fileInputStream.close();
				bufferedInputStream.close();
				fileOutputStream.close();
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Reads the relative path to the resource directory from the
	 * <code>RESOURCE_PATH</code> file located in <code>src/main/resources</code>
	 * 
	 * @return the relative path to the <code>resources</code> in the file system,
	 *         or <code>null</code> if there was an error
	 */
	@SuppressWarnings("unused")
	private static String getResourcePath() {
		try {
			URI resourcePathFile = System.class.getResource("RESOURCE_PATH").toURI();
			String resourcePath = Files.readAllLines(Paths.get(resourcePathFile)).get(0);
			URI rootURI = new File("").toURI();
			URI resourceURI = new File(resourcePath).toURI();
			URI relativeResourceURI = rootURI.relativize(resourceURI);
			return relativeResourceURI.getPath();
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {

		BufferedInputOutputStream.readFile();
		BufferedInputOutputStream.writeFile();
		BufferedInputOutputStream.readWriteFile();

		System.out.println("End Program");
	}
}
