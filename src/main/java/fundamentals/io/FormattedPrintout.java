package fundamentals.io;

public class FormattedPrintout {

	public static void print09() {
		int q = 0;
		int i = 0;
		int j = 0;

		// Row loop
		for (i = 0; i < 10; i++) {
			System.out.print(q + " ");

			// Column loop
			for (j = 1; j < 10; j++) {

				if (i == 9 || i == 0) {
					System.out.print(j + " ");
				}
				if (i == 1) {
					System.out.print("              " + "8 9"); // 14
					break;
				}
				if (i == 2) {
					System.out.print("            " + "7   9"); // 12
					break;
				}
				if (i == 3) {
					System.out.print("          " + "6     9");
					break;
				}
				if (i == 4) {
					System.out.print("        " + "5       9");
					break;
				}
				if (i == 5) {
					System.out.print("      " + "4         9");
					break;
				}
				if (i == 6) {
					System.out.print("    " + "3           9");
					break;
				}
				if (i == 7) {
					System.out.print("  " + "2             9");
					break;
				}
				if (i == 8) {
					System.out.print("1               9");
					break;
				}
			}

			System.out.println();
		}
	}

	public static void printSpaces() {
		System.out.println("0 1 2 3 4 5 6 7 8 9");

		System.out.print("0 ");
		System.out.printf("%15s", "8");
		System.out.printf("%3s", "9\n");
		System.out.print("0 ");
		System.out.printf("%13s", "7");
		System.out.printf("%5s", "9\n");
		System.out.print("0 ");
		System.out.printf("%11s", "6");
		System.out.printf("%7s", "9\n");
		System.out.print("0 ");
		System.out.printf("%9s", "5");
		System.out.printf("%9s", "9\n");
		System.out.print("0 ");
		System.out.printf("%7s", "4");
		System.out.printf("%11s", "9\n");
		System.out.print("0 ");
		System.out.printf("%5s", "3");
		System.out.printf("%13s", "9\n");
		System.out.print("0 ");
		System.out.printf("%3s", "2");
		System.out.printf("%15s", "9\n");
		System.out.print("0 ");
		System.out.printf("%1s", "1");
		System.out.printf("%17s", "9\n");

		System.out.println("0 1 2 3 4 5 6 7 8 9");
	}

	public static void main(String[] args) {
		print09();
		printSpaces();
	}
}
