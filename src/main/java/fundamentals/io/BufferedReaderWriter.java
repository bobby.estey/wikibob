package fundamentals.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BufferedReaderWriter {

	private static final String INPUT_FILE = "files/bufferedInputFile.txt";
	private static final String OUTPUT_FILE = "files/bufferedOutputFile.txt";
	private static StringBuilder stringBuilder = new StringBuilder();

	public static void readFile() {

		try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(INPUT_FILE))) {

			// read line by line
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line).append("\n");
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		System.out.println("BufferedReaderWriter.readFile\n");
		System.out.println(stringBuilder);
	}

	public static void writeFile() {

		// If the file doesn't exists, create and write to it
		// If the file exists, truncate (remove all content) and write to it
		try (FileWriter fileWriter = new FileWriter(OUTPUT_FILE);
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

			System.out.println("BufferedReaderWriter.writeFile\n");
			System.out.println(stringBuilder);
			bufferedWriter.write(stringBuilder.toString());

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		BufferedReaderWriter.readFile();
		BufferedReaderWriter.writeFile();
	}
}
