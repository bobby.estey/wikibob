package fundamentals;

public class OverloadingOverriding extends OverridingParent {

	@Override
	public String toString() {
		return "";
	}

	// Overloading - no argument Constructor
	public void overloading() {
	}
	
	// Overloading - one argument Constructor, returning an integer
	public int overloading(int value) {
		return 0;
	}

	// Overloading - two arguments Constructor, returning an integer
	public int overloading(int value1, int value2) {
		return 0;
	}

	// Overriding - the overridding method in Class OverridingParent
	public String overridding() {
		return "OverloadingOverriding.overridding";
	}
	
	// this is overriding not Overloading and even through the return type is
	// different not allowed
    //	public int Overloading() {
    //		return 0;
    //	}
	
	public static void main(String[] args) {
	}
}
