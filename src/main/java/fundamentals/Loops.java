package fundamentals;

public class Loops {

	public Loops() {
		whileExample();
		doWhileExample();
		forExample();
	}

	// while loops execute 0 to many times as long as the condition is true
	private void whileExample() {
		int i = 0;

		while (i++ < 3) {
			System.out.println("whileExample: " + i);
		}
	}

	// do while loops execute 1 to many times as long as the condition is true
	private void doWhileExample() {
		int i = 1;

		do {
			System.out.println("doWhileExample: " + i);
		} while (i++ < 3);
	}

	private void forExample() {

		// for loops execute a number of times as long as the condition is true
		for (int i = 0; i < 3; i++) {
			System.out.println("forExample: " + i);
		}
	}

	public static void main(String[] args) {

		// calls the Loops Constructor - Constructors create objects
		new Loops();
	}
}
