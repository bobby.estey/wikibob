package fundamentals.modifiers;

/*
 * (-) private - only the Class has access
 * default - same package (formerly called package)
 * (#) protected - same package or subclass
 * (+) public - all access
 */
public class A {

	private String myPrivate = "private"; // private
	String myDefault = "default"; // default
	protected String myProtected = "protected"; // protected
	public String myPublic = "public"; // public

	protected String getMyPrivate() {
		return myPrivate;
	}
}
