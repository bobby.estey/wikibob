# Recursion - methods calling themselves

 - Why Recursion?  Graphics and Scientific applications, processing the same code over and over

## Terminology
 - direct recursion - method calling itself
 - indirect recursion - method calling another method which in turn calls the original method, for example:
     - method1 -> method2 -> method 1
 - base case - keep calling the method until a return occurs, for example:
     - if (n == 1) return 1;
			
## Explained

![Factorial](recursionFactorial.png)
