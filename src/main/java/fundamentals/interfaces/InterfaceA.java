// interfaces are like public abstract Classes with no implementation
package fundamentals.interfaces;

public interface InterfaceA {

	public String getA1();

	public String getA2();
}
