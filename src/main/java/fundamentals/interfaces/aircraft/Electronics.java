package fundamentals.interfaces.aircraft;

public class Electronics implements Engines2Electronics, Wings2Electronics {

	private int speed = 0;
	private int flaps = 0;

	@Override
	public int getFlaps() {
		return flaps;
	}

	@Override
	public void setFlaps(int flaps) {
		this.flaps = flaps;
	}

	@Override
	public int getSpeed() {
		return speed;
	}

	@Override
	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
