package fundamentals.interfaces;

public class Child extends Parent implements InterfaceA, InterfaceB {

	// Interface A methods

	public String getA1() {
		return "child.A1";
	}

	public String getA2() {
		return "child.A2";
	}

	// Interface B methods

	public String getB1() {
		return "child.B1";
	}

	public String getB2() {
		return "child.B2";
	}
}