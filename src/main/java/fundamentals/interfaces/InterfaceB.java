// interfaces are like public abstract Classes with no implementation
package fundamentals.interfaces;

public interface InterfaceB {

	public String getB1();

	public String getB2();
}
