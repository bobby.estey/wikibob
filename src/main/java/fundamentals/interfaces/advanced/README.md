# Question asked wrong

I was given this question on a Job Interview [Interface Question](interfaceQuestion.png)

This question was wrongly asked, should of been:

```
interface ICar {}
abstract class Car implements ICar {}
class Nissan extends Car{}
class Versa extends Nissan {}
```
