package fundamentals.interfaces.advanced;

public class Driver {

	public static void main(String[] args) {
//		Car c1 = new Car();  // false
//		ICar c2 = new Car(); // false
		ICar c3 = new Dodge(); // true
		Car c4 = new Dodge(); // true
//		Dodge c5 = new Car();  // false
		Dodge c6 = new Viper(); // true
//		Viper c7 = new Dodge();  // false	
	}
}
