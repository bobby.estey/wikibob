package fundamentals.interfaces;

// MyClass must have the implementation of Interfaces InterfaceA and InterfaceB
// NOTE:  MyClass is also known as an Adapter Pattern, containg all methods required for any Interfaces.

// ADAPTERS are an awesome approach to development, benefits are:  Universal  acceptance between Classes and Interfaces, an agreed handshake on protocol.  Original code needs not modification
// see https://www.baeldung.com/java-adapter-pattern

public class MyClass implements InterfaceA, InterfaceB {

	// Interface A methods

	@Override
	public String getA1() {
		return "A1";
	}

	@Override
	public String getA2() {
		return "A2";
	}

	// Interface B methods

	@Override
	public String getB1() {
		return "B1";
	}

	@Override
	public String getB2() {
		return "B2";
	}
}