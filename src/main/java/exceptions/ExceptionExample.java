package exceptions;

import java.io.IOException;

public class ExceptionExample {
	/**
	 * Checks range between 0-9
	 *
	 * @param value the value to check
	 * @exception Exception if value out of range, 0-9
	 */
	public static void rangeCheck(int value) throws Exception {
		System.out.println("In rangeCheck, value: " + value);
		if (value < 0 || value > 9) {
			String message = "Out of range, 0-9. Value: " + value;

			// new Exception(message) turns an object of Exception.
			// throw - throws an Exception object
			throw new Exception(message);
		}
	}

	public static int somethingInteresting() {
		try {
			return 1;
		} catch (Exception e) {
			System.err.println("In catch: somethingInteresting: " + e.getMessage());
		} finally {
			System.out.println("In finally: somethingInteresting");
		}

		System.out.println("After finally: somethingInteresting");
		return 2;
	}

	public static void main(String[] args) {

		// try - to process the statements within the try block
		try {
			System.out.println("ExceptionExample.somethingInteresting(): ");
			ExceptionExample.somethingInteresting();

			ExceptionExample.rangeCheck(3);
			ExceptionExample.rangeCheck(10); // exception thrown
			ExceptionExample.rangeCheck(5); // not reached

			// exception - if there is an Exception process the statements within the catch
			// block and do not forgot to put general exceptions at the bottom or the
			// specific exceptions are never reached
		} catch (NullPointerException e) {
			System.err.println("In NullPointerException catch: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("In IOException catch: " + e.getMessage());
		} catch (Exception e) {

			System.err.println("In Exception catch: " + e.getMessage());
			e.printStackTrace();

			// finally - process the statements within the finally block whether an
			// exception or no exception
			// the purpose is to clean up after processing
		} finally {
			System.out.println("In finally: Optional");
		}
	}
}
