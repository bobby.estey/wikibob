# Java Exceptions

 - throws - means send the exception back to the caller, the method is not going to do any exception handling (the method is not going to deal with the exception)
 - try block - try to do the processing
 - catch block - if there is an exception, if something is wrong, then stop processing and do what is inside the catch block
 - finally block - is always executed, usually cleanup process statements

# Links
 - [Checked / Unchecked Exceptions](https://www.geeksforgeeks.org/checked-vs-unchecked-exceptions-in-java/)
 - checked exceptions - exceptions at compile time
 - unchecked exceptions - exceptions at run time